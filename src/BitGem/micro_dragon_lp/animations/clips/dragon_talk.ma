//Maya ASCII 2013 scene
//Name: dragon_talk.ma
//Last modified: Mon, Jul 14, 2014 01:54:06 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "talkSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 651;
	setAttr ".se" 728;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU2341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 3.2171449661254883 728 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -26.658763885498047 728 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -1.5793838500976563 728 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 0 655 7.5457711219787598 662 -26.742698669433594
		 666 -12.835383415222168 672 -27.089422225952148 678 -13.556082725524902 684 -19.190338134765625
		 692 -8.2442531585693359 702 -39.939411163330078 712 -5.1039090156555176 721 -21.909660339355469
		 728 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  0.78460776805877686 1 1 1 1 1 1 1 1 1 1 
		1;
	setAttr -s 12 ".koy[0:11]"  0.61999249458312988 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "animCurveTA2345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 0 655 -0.54949295520782471 662 -11.344520568847656
		 666 -10.115787506103516 672 -11.868609428405762 678 -11.460921287536621 684 -12.978439331054687
		 692 -7.0761504173278809 702 -11.989323616027832 712 -7.8898563385009757 721 -1.8484658002853396
		 728 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 0.98542475700378418 1 1 1 1 1 1 1 0.93984347581863403 
		0.98134762048721313 1;
	setAttr -s 12 ".kiy[0:11]"  0 -0.17011219263076782 0 0 0 0 0 0 0 0.34160542488098145 
		0.19224159419536591 0;
	setAttr -s 12 ".kox[0:11]"  0.99834847450256348 0.98542475700378418 
		1 1 1 1 1 1 1 0.93984347581863403 0.98134762048721313 1;
	setAttr -s 12 ".koy[0:11]"  -0.057447943836450577 -0.17011219263076782 
		0 0 0 0 0 0 0 0.34160542488098145 0.19224159419536591 0;
createNode animCurveTA -n "animCurveTA2346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 0 655 1.9172419309616091 662 9.7968034744262695
		 666 10.359628677368164 672 10.622981071472168 678 12.731694221496582 684 12.502530097961426
		 692 11.020196914672852 702 24.664989471435547 712 20.459325790405273 721 21.815677642822266
		 728 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 0.89817506074905396 0.98472517728805542 
		1 0.99848228693008423 1 0.99885016679763794 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0.43963789939880371 0.17411567270755768 
		0 0.055072769522666931 0 -0.047940861433744431 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  0.98043453693389893 0.89817506074905396 
		0.98472517728805542 1 0.99848228693008423 1 0.99885016679763794 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0.19684562087059021 0.43963789939880371 
		0.17411567270755768 0 0.055072769522666931 0 -0.047940861433744431 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -4.7867727279663086 728 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 32.754745483398438 728 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -56.147525787353516 728 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 3.2171449661254883 728 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -26.658763885498047 728 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1.5793838500976563 728 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2352";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  651 0 660 22.03807258605957 666 6.2247376441955566
		 672 19.310043334960938 679 5.7757954597473145 686 11.263674736022949 693 3.3780274391174316
		 697 19.796014785766602 702 6.8976359367370605 708 34.791999816894531 714 14.75493812561035
		 720 24.885288238525391 728 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes no;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.69808006286621094 1 1 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 13 ".koy[0:12]"  0.71601974964141846 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTA -n "animCurveTA2351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  651 0 660 1.0281970500946045 666 -3.7696709632873535
		 672 -5.6054816246032715 679 -4.4477849006652832 686 -4.9512972831726074 693 -4.2145271301269531
		 697 -5.6415534019470215 702 -4.5542302131652832 708 -6.5434198379516602 714 -5.2480182647705078
		 720 -5.9943742752075195 728 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes no;
	setAttr -s 13 ".kix[0:12]"  1 1 0.93582946062088013 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 -0.35245329141616821 0 0 0 0 0 0 0 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.99885690212249756 1 0.93582946062088013 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0.047799766063690186 0 -0.35245329141616821 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2352";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  651 0 660 -6.2697596549987793 666 4.7601947784423828
		 672 4.2893433570861816 679 5.4807610511779785 686 5.030789852142334 693 5.6618690490722656
		 697 4.2417221069335938 702 5.3927197456359863 708 2.6410491466522217 714 4.7203607559204102
		 720 3.7254931926727295 728 0;
	setAttr -s 13 ".ktl[12]" no;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 1 1 1 1 1 0.97897428274154663 
		1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 -0.2039836049079895 
		0;
	setAttr -s 13 ".kox[0:12]"  0.95996350049972534 1 1 1 1 1 1 1 1 1 1 
		0.97897428274154663 1;
	setAttr -s 13 ".koy[0:12]"  -0.28012490272521973 0 0 0 0 0 0 0 0 0 
		0 -0.2039836049079895 0;
createNode animCurveTL -n "animCurveTL2350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -4.7867727279663086 728 -4.7867727279663086;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 32.754745483398438 728 32.754745483398438;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2352";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 56.147525787353516 728 56.147525787353516;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2353";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2354";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2355";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2353";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2354";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2355";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2353";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 40.544437408447266 728 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2354";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 43.055271148681641 728 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2355";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2356";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2357";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2358";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2356";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2357";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2358";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2356";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 8.0282459259033203 728 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2357";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 9.9087905883789063 728 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2358";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2359";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 1 681 1 686 1 691 1 696 1 701 1 706 1
		 711 1 716 1 721 1 728 1;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2360";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 1 681 1 686 1 691 1 696 1 701 1 706 1
		 711 1 716 1 721 1 728 1;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2361";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 1 681 1 686 1 691 1 696 1 701 1 706 1
		 711 1 716 1 721 1 728 1;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2359";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  651 0 655 -76.069984436035156 660 -66.031654357910156
		 665 13.833324432373047 671 -91.210166931152344 676 -0.55291026830673218 681 -91.210166931152344
		 686 -0.55291026830673218 691 -91.210166931152344 696 -0.55291026830673218 701 -91.210166931152344
		 706 -0.55291026830673218 711 -91.210166931152344 716 -0.55291026830673218 721 -91.210166931152344
		 728 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.36847841739654541 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0.92963635921478271 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  0.12455575168132782 1 0.36847841739654541 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  -0.99221259355545044 0 0.92963635921478271 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2360";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 660 -26.511959075927734 665 24.148157119750977
		 671 -25.249216079711914 676 12.489650726318359 681 -25.249216079711914 686 12.489650726318359
		 691 -25.249216079711914 696 12.489650726318359 701 -25.249216079711914 706 12.489650726318359
		 711 -25.249216079711914 716 12.489650726318359 721 -25.249216079711914 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes no;
	setAttr -s 15 ".kix[0:14]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2361";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  651 0 655 14.044892311096191 660 49.062751770019531
		 665 15.712542533874512 671 43.414539337158203 676 35.671676635742187 681 43.414539337158203
		 686 35.671676635742187 691 43.414539337158203 696 35.671676635742187 701 43.414539337158203
		 706 35.671676635742187 711 43.414539337158203 716 35.671676635742187 721 43.414539337158203
		 728 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 0.24243257939815521 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0.97016823291778564 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 0.24243257939815521 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0.97016823291778564 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2359";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 -19.597047805786133 681 -19.597047805786133
		 686 -19.597047805786133 691 -19.597047805786133 696 -19.597047805786133 701 -19.597047805786133
		 706 -19.597047805786133 711 -19.597047805786133 716 -19.597047805786133 721 -19.597047805786133
		 728 -19.597047805786133;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2360";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 36.545459747314453 681 36.545459747314453
		 686 36.545459747314453 691 36.545459747314453 696 36.545459747314453 701 36.545459747314453
		 706 36.545459747314453 711 36.545459747314453 716 36.545459747314453 721 36.545459747314453
		 728 36.545459747314453;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2361";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  651 39.212558746337891 681 39.212558746337891
		 686 39.212558746337891 691 39.212558746337891 696 39.212558746337891 701 39.212558746337891
		 706 39.212558746337891 711 39.212558746337891 716 39.212558746337891 721 39.212558746337891
		 728 39.212558746337891;
	setAttr -s 11 ".ktl[0:10]" no yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2362";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2363";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2364";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2362";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 654 0.43200200796127319 662 -0.64224958419799805
		 668 -0.11093731224536896 675 -0.26846992969512939 682 0.26947188377380371 688 0.079296804964542389
		 692 1.9821410179138184 696 1.8354119062423704 699 4.143956184387207 706 1.6854665279388428
		 710 3.4816713333129883 718 -0.65751403570175171 723 1.482518196105957 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes no;
	setAttr -s 15 ".kix[0:14]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.99818575382232666 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.060209453105926514 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2363";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 654 0.17818202078342438 662 0.36760127544403076
		 668 0.42597168684005737 675 -7.5967392921447754 682 -6.6842098236083984 688 9.6792621612548828
		 692 9.4924030303955078 696 6.4873504638671875 699 7.803534984588623 706 -1.1155052185058594
		 710 0.67586737871170044 718 -3.9160516262054448 723 0.7352219820022583 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes no;
	setAttr -s 15 ".kix[0:14]"  1 0.99968564510345459 0.99997550249099731 
		1 1 0.98684614896774292 1 0.99828141927719116 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0.025071021169424057 0.0069963224232196808 
		0 0 0.1616620272397995 0 -0.058602593839168549 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.99969065189361572 0.99968564510345459 
		0.99997550249099731 1 1 0.98684614896774292 1 0.99828141927719116 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.024871209636330605 0.025071021169424057 
		0.0069963224232196808 0 0 0.1616620272397995 0 -0.058602593839168549 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "animCurveTA2364";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 654 6.0112133026123047 662 -19.491640090942383
		 668 -8.8163270950317383 675 -15.980943679809569 682 -1.1853036880493164 688 -9.3513097763061523
		 692 2.6464903354644775 696 -6.6675543785095215 699 3.887814044952393 706 -12.85645866394043
		 710 3.5845520496368408 718 -7.2500739097595224 723 6.5434894561767578 728 0;
	setAttr -s 15 ".ktl[14]" no;
	setAttr -s 15 ".kix[0:14]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.76595991849899292 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.64288830757141113 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTL -n "animCurveTL2362";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 51.6451416015625 728 51.6451416015625;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2363";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -11.264523506164551 728 -11.264523506164551;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2364";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2365";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 1 676 1 681 1 686 1 691 1 696 1 701 1
		 706 1 711 1 716 1 721 1 728 1;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2366";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 1 676 1 681 1 686 1 691 1 696 1 701 1
		 706 1 711 1 716 1 721 1 728 1;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2367";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 1 676 1 681 1 686 1 691 1 696 1 701 1
		 706 1 711 1 716 1 721 1 728 1;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2365";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  651 0 654 77.585823059082031 661 47.687965393066406
		 666 -26.429576873779297 671 90.097190856933594 676 -26.429576873779297 681 90.097190856933594
		 686 -26.429576873779297 691 90.097190856933594 696 -26.429576873779297 701 90.097190856933594
		 706 -26.429576873779297 711 90.097190856933594 716 -26.429576873779297 721 90.097190856933594
		 728 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.18316306173801422 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 -0.98308253288269043 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  0.091919533908367157 1 0.18316306173801422 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0.99576646089553833 0 -0.98308253288269043 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2366";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  651 0 654 21.678476333618164 661 17.426675796508789
		 666 2.4861574172973633 671 24.85004997253418 676 2.4861574172973633 681 24.85004997253418
		 686 2.4861574172973633 691 24.85004997253418 696 2.4861574172973633 701 24.85004997253418
		 706 2.4861574172973633 711 24.85004997253418 716 2.4861574172973633 721 24.85004997253418
		 728 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.79490393400192261 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 -0.60673528909683228 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  0.3136964738368988 1 0.79490393400192261 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0.94952327013015747 0 -0.60673528909683228 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2367";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  651 0 654 56.694412231445313 661 50.7972412109375
		 666 18.746026992797852 671 55.607784271240234 676 18.746026992797852 681 55.607784271240234
		 686 18.746026992797852 691 55.607784271240234 696 18.746026992797852 701 55.607784271240234
		 706 18.746026992797852 711 55.607784271240234 716 18.746026992797852 721 55.607784271240234
		 728 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.68668061494827271 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 -0.726959228515625 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 0.68668061494827271 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 -0.726959228515625 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2365";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 -19.609930038452148 676 -19.609930038452148
		 681 -19.609930038452148 686 -19.609930038452148 691 -19.609930038452148 696 -19.609930038452148
		 701 -19.609930038452148 706 -19.609930038452148 711 -19.609930038452148 716 -19.609930038452148
		 721 -19.609930038452148 728 -19.609930038452148;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2366";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 36.542636871337891 676 36.542636871337891
		 681 36.542636871337891 686 36.542636871337891 691 36.542636871337891 696 36.542636871337891
		 701 36.542636871337891 706 36.542636871337891 711 36.542636871337891 716 36.542636871337891
		 721 36.542636871337891 728 36.542636871337891;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2367";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 -39.208473205566406 676 -39.208473205566406
		 681 -39.208473205566406 686 -39.208473205566406 691 -39.208473205566406 696 -39.208473205566406
		 701 -39.208473205566406 706 -39.208473205566406 711 -39.208473205566406 716 -39.208473205566406
		 721 -39.208473205566406 728 -39.208473205566406;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2368";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2369";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2370";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2368";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2369";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2370";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2368";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -4.502251148223877 728 -4.502251148223877;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2369";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 37.76336669921875 728 37.76336669921875;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2370";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2371";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2372";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2373";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2371";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 0 659 -0.039331339299678802 671 0.4878915548324585
		 676 1.6356812715530396 686 4.0993056297302246 691 -1.1114023923873901 695 -1.7717217206954956
		 703 -3.5577051639556885 708 -3.8858535289764404 715 -0.97518575191497814 720 -1.2418261766433716
		 728 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 1 0.99847930669784546 0.96921265125274658 
		1 0.97915363311767578 0.99670892953872681 0.99817478656768799 1 1 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0.055126693099737167 0.24622528254985809 
		0 -0.20312100648880005 -0.081063516438007355 -0.060391947627067566 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  0.99999785423278809 1 0.99847930669784546 
		0.96921265125274658 1 0.97915363311767578 0.99670892953872681 0.99817478656768799 
		1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  -0.0020593758672475815 0 0.055126693099737167 
		0.24622528254985809 0 -0.20312100648880005 -0.081063516438007355 -0.060391947627067566 
		0 0 0 0;
createNode animCurveTA -n "animCurveTA2372";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  651 0 659 0.086692407727241516 671 0.14346982538700104
		 676 -4.8536281585693359 686 -4.4803371429443359 691 2.8765885829925537 695 3.677232027053833
		 703 6.8493094444274902 708 5.1813416481018066 715 3.5360572338104248 720 1.9326951503753662
		 728 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 0.99998164176940918 1 1 0.99890154600143433 
		0.96979248523712158 0.99228203296661377 1 0.99298280477523804 0.99329495429992676 
		0.98864066600799561 1;
	setAttr -s 12 ".kiy[0:11]"  0 0.0060606724582612514 0 0 0.046857621520757675 
		0.24393144249916077 0.12400186806917191 0 -0.1182587593793869 -0.11560786515474319 
		-0.15029866993427277 0;
	setAttr -s 12 ".kox[0:11]"  0.99998968839645386 0.99998164176940918 
		1 1 0.99890154600143433 0.96979248523712158 0.99228203296661377 1 0.99298280477523804 
		0.99329495429992676 0.98864066600799561 1;
	setAttr -s 12 ".koy[0:11]"  0.0045391484163701534 0.0060606724582612514 
		0 0 0.046857621520757675 0.24393144249916077 0.12400186806917191 0 -0.1182587593793869 
		-0.11560786515474319 -0.15029866993427277 0;
createNode animCurveTA -n "animCurveTA2373";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  651 0 659 3.5152692794799805 666 -10.798345565795898
		 671 -7.2711715698242187 676 -14.504085540771484 686 -0.87152391672134399 691 -15.163037300109863
		 695 -16.037822723388672 703 -3.0638642311096191 708 -11.626650810241699 715 -2.9615225791931152
		 720 -12.238223075866699 728 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 0.96424919366836548 1 1 1 1 
		1 0.81209641695022583;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 -0.2649969756603241 0 0 0 0 
		0 0.58352333307266235;
	setAttr -s 13 ".kox[0:12]"  0.98347979784011841 1 1 1 1 1 0.96424919366836548 
		1 1 1 1 1 0.81209629774093628;
	setAttr -s 13 ".koy[0:12]"  0.18101799488067627 0 0 0 0 0 -0.2649969756603241 
		0 0 0 0 0 0.58352333307266235;
createNode animCurveTL -n "animCurveTL2371";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -1.4725730419158936 728 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2372";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 22.299345016479492 728 22.299345016479492;
	setAttr -s 2 ".kit[0:1]"  2 1;
	setAttr -s 2 ".kot[1]"  2;
	setAttr -s 2 ".kix[1]"  1;
	setAttr -s 2 ".kiy[1]"  0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2373";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2374";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2375";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2376";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2374";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2375";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2376";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2374";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2375";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2376";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 3.2083339691162109;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.041667938232421875;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2377";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2378";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2379";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 1 728 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2377";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 656 0.22695232927799225 663 1.8496519327163699
		 670 0.69125741720199585 675 3.8174993991851807 679 1.0392718315124512 687 0.80854201316833496
		 695 1.269489049911499 701 -0.61520981788635254 707 3.3862361907958984 710 0.57870513200759888
		 713 -1.3875302076339722 717 3.2676911354064941 722 -0.20093913376331329 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.99837720394134521 1 1 1 0.9993438720703125 
		1 1 1 1 0.822456955909729 1 1 1 0.99993175268173218;
	setAttr -s 15 ".kiy[0:14]"  0 0.056946754455566406 0 0 0 -0.036219183355569839 
		0 0 0 0 -0.56882721185684204 0 0 0 0.011684736236929893;
	setAttr -s 15 ".kox[0:14]"  0.99981927871704102 0.99837720394134521 
		1 1 1 0.9993438720703125 1 1 1 1 0.822456955909729 1 1 1 0.99993163347244263;
	setAttr -s 15 ".koy[0:14]"  0.019009631127119064 0.056946754455566406 
		0 0 0 -0.036219183355569839 0 0 0 0 -0.56882721185684204 0 0 0 0.01168473344296217;
createNode animCurveTA -n "animCurveTA2378";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 656 -2.7913868427276611 663 -3.5768132209777832
		 670 -0.41154065728187561 675 -0.095144607126712799 679 -1.5889118909835815 687 -3.0350055694580078
		 695 33.165348052978516 701 27.260335922241211 707 29.745559692382816 710 22.089641571044922
		 713 24.542299270629883 717 25.547569274902344 722 16.057411193847656 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.99020540714263916 1 0.9968532919883728 
		1 0.97515833377838135 1 1 1 1 1 0.9535757303237915 1 0.56420332193374634 0.89653968811035156;
	setAttr -s 15 ".kiy[0:14]"  0 -0.13961838185787201 0 0.079268760979175568 
		0 -0.22150902450084686 0 0 0 0 0 0.30115333199501038 0 -0.82563585042953491 -0.44296342134475708;
	setAttr -s 15 ".kox[0:14]"  0.97372978925704956 0.99020540714263916 
		1 0.9968532919883728 1 0.97515833377838135 1 1 1 1 1 0.9535757303237915 1 0.56420332193374634 
		0.89653968811035156;
	setAttr -s 15 ".koy[0:14]"  -0.2277066707611084 -0.13961838185787201 
		0 0.079268760979175568 0 -0.22150902450084686 0 0 0 0 0 0.30115333199501038 0 -0.82563585042953491 
		-0.44296327233314514;
createNode animCurveTA -n "animCurveTA2379";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  651 0 656 5.1702704429626465 663 11.915661811828613
		 670 -10.12564754486084 675 0.38955724239349365 679 -10.343217849731445 687 -0.42676994204521179
		 695 -10.53911304473877 701 -16.718997955322266 707 2.6984641551971436 710 -7.4171485900878915
		 713 -15.210105895996096 717 2.0710642337799072 722 -11.034672737121582 728 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.92705786228179932 1 1 1 1 1 0.64353519678115845 
		1 1 0.35111963748931885 1 1 1 0.60650408267974854;
	setAttr -s 15 ".kiy[0:14]"  0 0.37491840124130249 0 0 0 0 0 -0.76541656255722046 
		0 0 -0.93633061647415161 0 0 0 0.79508036375045776;
	setAttr -s 15 ".kox[0:14]"  0.91761946678161621 0.92705786228179932 
		1 1 1 1 1 0.64353519678115845 1 1 0.35111963748931885 1 1 1 0.60650390386581421;
	setAttr -s 15 ".koy[0:14]"  0.39745989441871643 0.37491816282272339 
		0 0 0 0 0 -0.76541656255722046 0 0 -0.93633061647415161 0 0 0 0.79508048295974731;
createNode animCurveTL -n "animCurveTL2377";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 -54.946929931640625 728 -54.946929931640625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2378";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 2.3482637405395508 728 2.3482637405395508;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2379";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  651 0 728 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 728;
	setAttr ".unw" 728;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "talkSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU2341.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU2342.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU2343.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA2341.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA2342.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA2343.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL2341.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL2342.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL2343.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU2344.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU2345.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU2346.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA2344.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA2345.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA2346.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL2344.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL2345.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL2346.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU2347.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU2348.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU2349.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA2347.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA2348.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA2349.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL2347.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL2348.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL2349.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU2350.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU2351.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU2352.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA2350.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA2351.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA2352.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL2350.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL2351.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL2352.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU2353.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU2354.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU2355.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA2353.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA2354.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA2355.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL2353.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL2354.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL2355.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU2356.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU2357.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU2358.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA2356.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA2357.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA2358.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL2356.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL2357.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL2358.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU2359.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU2360.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU2361.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA2359.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA2360.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA2361.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL2359.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL2360.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL2361.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU2362.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU2363.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU2364.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA2362.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA2363.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA2364.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL2362.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL2363.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL2364.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU2365.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU2366.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU2367.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA2365.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA2366.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA2367.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL2365.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL2366.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL2367.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU2368.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU2369.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU2370.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA2368.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA2369.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA2370.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL2368.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL2369.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL2370.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU2371.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU2372.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU2373.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA2371.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA2372.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA2373.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL2371.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL2372.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL2373.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU2374.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU2375.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU2376.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA2374.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA2375.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA2376.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL2374.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL2375.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL2376.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU2377.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU2378.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU2379.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA2377.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA2378.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA2379.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL2377.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL2378.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL2379.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_talk.ma
