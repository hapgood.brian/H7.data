//Maya ASCII 2013 scene
//Name: dragon_getup_take_off.ma
//Last modified: Mon, Jul 14, 2014 01:36:08 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "getup_take_offSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 447;
	setAttr ".se" 511;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1873";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1874";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1875";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1873";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1874";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1875";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1873";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 3.2171449661254883 451 3.2171449661254883
		 456 3.2171449661254883 457 3.2171449661254883 511 3.2171449661254883;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1874";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -26.658763885498047 451 -26.658763885498047
		 456 -26.658763885498047 457 -26.658763885498047 511 -26.658763885498047;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1875";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -1.5793838500976563 451 -1.5793838500976563
		 456 -1.5793838500976563 457 -1.5793838500976563 511 -1.5793838500976563;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1876";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 485 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1877";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 485 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1878";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 485 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1876";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 0 451 -23.514070510864258 456 -14.25190544128418
		 461 5.0281400680541992 471 -7.5339584350585946 485 -3.8269257545471191 489 0.36858692765235901
		 499 -11.385903358459473 506 -9.1527347564697266 511 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.39470469951629639 1 1 0.94885134696960449 
		1 1 0.92818921804428101 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0.9188079833984375 0 0 0.31572306156158447 
		0 0 0.37210854887962341 0;
	setAttr -s 10 ".kox[0:9]"  1 1 0.39470469951629639 1 1 0.94885134696960449 
		1 1 0.92818921804428101 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0.9188079833984375 0 0 0.31572306156158447 
		0 0 0.37210854887962341 0;
createNode animCurveTA -n "animCurveTA1877";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 0 451 -2.2746801376342773 456 5.3209147453308105
		 461 3.1475851535797119 471 0.87319332361221313 485 0.79158616065979004 489 3.3938367366790771
		 499 -1.2170324325561523 506 -0.45490458607673651 511 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 1 0.96211832761764526 0.99997317790985107 
		1 1 1 0.99388802051544189 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 -0.27263244986534119 -0.0073248445987701416 
		0 0 0 0.11039276421070099 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 0.96211832761764526 0.99997317790985107 
		1 1 1 0.99388802051544189 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 -0.27263244986534119 -0.0073248445987701416 
		0 0 0 0.11039276421070099 0;
createNode animCurveTA -n "animCurveTA1878";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 0 451 -2.484386682510376 456 -7.7033166885375977
		 461 1.9395600557327271 471 -1.9359426498413086 485 -1.1208552122116089 489 -2.2303905487060547
		 499 0.038782242685556412 506 0.053398575633764267 511 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 0.80961108207702637 1 1 1 1 1 0.9999966025352478 
		1 1;
	setAttr -s 10 ".kiy[0:9]"  0 -0.58696669340133667 0 0 0 0 0 0.0026239089202135801 
		0 0;
	setAttr -s 10 ".kox[0:9]"  1 0.80961108207702637 1 1 1 1 1 0.9999966025352478 
		1 1;
	setAttr -s 10 ".koy[0:9]"  0 -0.58696669340133667 0 0 0 0 0 0.0026239089202135801 
		0 0;
createNode animCurveTL -n "animCurveTL1876";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -4.7867727279663086 451 -4.7867727279663086
		 456 -4.7867727279663086 485 -4.7867727279663086 511 -4.7867727279663086;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1877";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 32.754745483398438 451 32.754745483398438
		 456 32.754745483398438 485 32.754745483398438 511 32.754745483398438;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1878";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -56.147525787353516 451 -56.147525787353516
		 456 -56.147525787353516 485 -56.147525787353516 511 -56.147525787353516;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1879";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1880";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1881";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1879";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1880";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1881";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1879";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 3.2171449661254883 451 3.2171449661254883
		 456 3.2171449661254883 457 3.2171449661254883 511 3.2171449661254883;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1880";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -26.658763885498047 451 -26.658763885498047
		 456 -26.658763885498047 457 -26.658763885498047 511 -26.658763885498047;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1881";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1.5793838500976563 451 1.5793838500976563
		 456 1.5793838500976563 457 1.5793838500976563 511 1.5793838500976563;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1882";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 1 451 1 511 1;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1883";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 1 451 1 511 1;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1884";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 1 451 1 511 1;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1882";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 0 451 15.535760879516602 461 -3.4327239990234375
		 479 6.0283083915710449 485 -1.1292744874954224 490 -2.0188193321228027 500 6.3055477142333984
		 507 4.6735668182373047 511 0;
	setAttr -s 9 ".ktl[0:8]" no no yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 0.52365309000015259 1 1 0.97590833902359009 
		1 1 0.95966237783432007 0.89819800853729248;
	setAttr -s 9 ".kiy[0:8]"  0 0.85193157196044922 0 0 -0.21818085014820099 
		0 0 -0.28115475177764893 -0.43959113955497742;
	setAttr -s 9 ".kox[0:8]"  0.5236515998840332 0.78294682502746582 
		1 1 0.97590833902359009 1 1 0.95966237783432007 1;
	setAttr -s 9 ".koy[0:8]"  0.85193246603012085 -0.62208861112594604 
		0 0 -0.21818085014820099 0 0 -0.28115475177764893 0;
createNode animCurveTA -n "animCurveTA1883";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 0 451 -0.1953963041305542 461 -2.4277570247650146
		 479 4.456871509552002 485 -0.81270742416381836 490 -1.4382439851760864 500 3.8294122219085693
		 507 3.23337721824646 511 0;
	setAttr -s 9 ".ktl[0:8]" no no yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 0.9997907280921936 1 1 0.98786634206771851 
		1 1 0.99432414770126343 0.94717663526535034;
	setAttr -s 9 ".kiy[0:8]"  0 -0.020457563921809196 0 0 -0.15530687570571899 
		0 0 -0.10639266669750214 -0.32071247696876526;
	setAttr -s 9 ".kox[0:8]"  0.9997907280921936 0.99565654993057251 
		1 1 0.98786634206771851 1 1 0.99432414770126343 1;
	setAttr -s 9 ".koy[0:8]"  -0.020457645878195763 -0.093102462589740753 
		0 0 -0.15530687570571899 0 0 -0.10639266669750214 0;
createNode animCurveTA -n "animCurveTA1884";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 0 451 -3.9311475753784184 461 -0.69426524639129639
		 479 -3.8488700389862061 485 -1.0392343997955322 490 -1.259474515914917 500 -2.2857723236083984
		 507 -2.606651782989502 511 0;
	setAttr -s 9 ".ktl[0:8]" no no yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 0.92470932006835938 0.98765188455581665 
		1 1 0.99952840805053711 0.99834495782852173 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 -0.38067394495010376 -0.15666443109512329 
		0 0 -0.030705641955137253 -0.057508781552314758 0 0;
	setAttr -s 9 ".kox[0:8]"  0.9247087836265564 0.99093306064605713 
		0.98765188455581665 1 1 0.99952840805053711 0.99834495782852173 1 1;
	setAttr -s 9 ".koy[0:8]"  -0.38067522644996643 0.13435640931129456 
		-0.15666443109512329 0 0 -0.030705641955137253 -0.057508781552314758 0 0;
createNode animCurveTL -n "animCurveTL1882";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 -4.7867727279663086 451 -4.7867727279663086
		 511 -4.7867727279663086;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1883";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 32.754745483398438 451 32.754745483398438
		 511 32.754745483398438;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1884";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  447 56.147525787353516 451 56.147525787353516
		 511 56.147525787353516;
	setAttr -s 3 ".ktl[0:2]" no no yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1885";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1886";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1887";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1885";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1886";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1887";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1885";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 40.544437408447266 451 40.544437408447266
		 456 40.544437408447266 457 40.544437408447266 511 40.544437408447266;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1886";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 43.055271148681641 451 43.055271148681641
		 456 43.055271148681641 457 43.055271148681641 511 43.055271148681641;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1887";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1888";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1889";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1890";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1888";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1889";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1890";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1888";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 8.0282459259033203 451 8.0282459259033203
		 456 8.0282459259033203 457 8.0282459259033203 511 8.0282459259033203;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1889";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 9.9087905883789063 451 9.9087905883789063
		 456 9.9087905883789063 457 9.9087905883789063 511 9.9087905883789063;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1890";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1891";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 455 1 457 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1892";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 455 1 457 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1893";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 455 1 457 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1891";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 -93.274017333984375 449 25.210294723510742
		 451 -90.143089294433594 453 24.725862503051758 455 -80.890213012695312 457 25.299190521240234
		 460 -77.568130493164063 461 -80.175880432128906 463 48.127605438232422 464 50.777908325195313
		 466 -80.591926574707031 467 -87.989875793457031 469 6.2896389961242676 479 29.859518051147464
		 481 -81.106491088867187 482 -83.598381042480469 484 24.800180435180664 485 27.467361450195313
		 487 -81.106491088867187 490 -83.598381042480469 497 -15.535813331604004 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.58275556564331055 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 -0.95645803213119507 0 0.95775818824768066 
		0 -0.99426442384719849 0 0.94745677709579468 0 -0.95260506868362427 0 0.95825827121734619 
		0 -0.72209542989730835 0 0.81264752149581909 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 1 1 1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.58275556564331055 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 0 0 0 -0.95645803213119507 0 0.95775818824768066 
		0 -0.99426442384719849 0 0.94745677709579468 0 -0.95260506868362427 0 0.95825827121734619 
		0 -0.72209542989730835 0 0.81264752149581909 0;
createNode animCurveTA -n "animCurveTA1892";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 -4.0435328483581543 449 27.98271369934082
		 451 -16.590557098388672 453 31.308498382568359 455 -20.208251953125 457 15.148503303527832
		 460 24.755352020263672 461 25.994792938232422 463 -19.358005523681641 464 -20.762310028076172
		 466 36.406421661376953 467 33.905223846435547 469 2.2449593544006348 479 -5.6701068878173828
		 481 31.235822677612305 482 30.325757980346683 484 -7.809575080871582 485 -9.9204740524291992
		 487 31.235822677612305 490 30.325757980346683 497 7.8860201835632324 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 0.27436357736587524 0.73217231035232544 
		1 0.4930138885974884 1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 
		0.35275033116340637 1 1 0.93440860509872437 0.8162115216255188 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0.96162599325180054 0.68111944198608398 
		0 -0.87002146244049072 0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 
		-0.93571746349334717 0 0 -0.35620281100273132 -0.57775324583053589 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 1 1 0.27436357736587524 0.73217231035232544 
		1 0.4930138885974884 1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 
		0.35275033116340637 1 1 0.93440860509872437 0.8162115216255188 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 0 0 0.96162599325180054 0.68111944198608398 
		0 -0.87002146244049072 0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 
		-0.93571746349334717 0 0 -0.35620281100273132 -0.57775324583053589 0;
createNode animCurveTA -n "animCurveTA1893";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 28.003265380859375 449 26.792665481567383
		 451 36.362819671630859 453 27.950145721435547 455 27.338918685913086 457 14.136126518249512
		 460 32.980312347412109 461 34.735298156738281 463 31.418289184570312 464 33.537071228027344
		 466 28.683307647705078 467 35.122848510742188 469 20.297872543334961 479 16.59162712097168
		 481 50.610061645507813 482 51.810520172119141 484 26.127241134643555 485 26.807085037231445
		 487 50.610061645507813 490 51.810520172119141 497 7.9200544357299805 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 0.93352395296096802 0.93352395296096802 
		1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 1 0.55252093076705933 1 1 0.76031714677810669 
		0.89340776205062866 1 0.81503564119338989 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 -0.35851505398750305 -0.35851505398750305 
		0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 0 0.83349913358688354 0 0 0.64955204725265503 
		0.44924667477607727 0 -0.57941079139709473 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 0.93352395296096802 0.93352395296096802 
		1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 1 0.55252093076705933 1 1 0.76031714677810669 
		0.89340776205062866 1 0.81503564119338989 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 -0.35851505398750305 -0.35851505398750305 
		0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 0 0.83349913358688354 0 0 0.64955204725265503 
		0.44924667477607727 0 -0.57941079139709473 0;
createNode animCurveTL -n "animCurveTL1891";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -19.597047805786133 451 -19.597047805786133
		 455 -19.597047805786133 457 -19.597047805786133 511 -19.597047805786133;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1892";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 36.545459747314453 451 36.545459747314453
		 455 36.545459747314453 457 36.545459747314453 511 36.545459747314453;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1893";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 39.212558746337891 451 39.212558746337891
		 455 39.212558746337891 457 39.212558746337891 511 39.212558746337891;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1894";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 449 1 451 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1895";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 449 1 451 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1896";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 449 1 451 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1894";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  447 0 449 0 451 -0.6784740686416626 457 -1.3068418502807617
		 462 -1.4152520895004272 467 -0.24558219313621518 477 -0.088783964514732361 483 -1.91673743724823
		 496 0.50768536329269409 505 0.30051490664482117 511 0;
	setAttr -s 11 ".ktl[0:10]" no no no no yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 0.99005424976348877 0.99903923273086548 
		1 0.99980592727661133 1 1 1 0.99982380867004395 0.99976927042007446;
	setAttr -s 11 ".kiy[0:10]"  0 0 -0.14068596065044403 -0.043826200067996979 
		0 0.019700018689036369 0 0 0 -0.018770208582282066 -0.021483592689037323;
	setAttr -s 11 ".kox[0:10]"  1 0.99005389213562012 0.99903923273086548 
		0.99995881319046021 1 0.99980592727661133 1 1 1 0.99982380867004395 0.99976927042007446;
	setAttr -s 11 ".koy[0:10]"  0 -0.14068812131881714 -0.043826200067996979 
		-0.0090817529708147049 0 0.019700018689036369 0 0 0 -0.018770208582282066 -0.021483587101101875;
createNode animCurveTA -n "animCurveTA1895";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  447 0 449 0 451 -3.8185770511627197 457 -2.9898421764373779
		 462 0.29329201579093933 467 0.50766146183013916 477 -0.15926961600780487 483 -1.1446186304092407
		 496 0.11611964553594589 505 0.22753418982028961 511 0;
	setAttr -s 11 ".ktl[0:10]" no no no yes yes yes yes yes yes yes no;
	setAttr -s 11 ".kix[0:10]"  1 1 0.7809598445892334 1 0.99924492835998535 
		1 0.9965064525604248 1 0.99988621473312378 1 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 -0.62458121776580811 0 0.038852039724588394 
		0 -0.083516240119934082 0 0.015083270147442818 0 0;
	setAttr -s 11 ".kox[0:10]"  1 0.78095525503158569 0.99833047389984131 
		1 0.99924492835998535 1 0.9965064525604248 1 0.99988621473312378 1 1;
	setAttr -s 11 ".koy[0:10]"  0 -0.62458693981170654 0.057760018855333328 
		0 0.038852039724588394 0 -0.083516240119934082 0 0.015083270147442818 0 0;
createNode animCurveTA -n "animCurveTA1896";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  447 -12.90634822845459 449 -12.90634822845459
		 451 -24.76951789855957 457 -10.509469985961914 462 -3.7771325111389165 467 0.9922214150428772
		 477 2.785752534866333 483 -3.8511304855346684 496 3.0445451736450195 505 2.1559481620788574
		 511 0;
	setAttr -s 11 ".ktl[0:10]" no no no no yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  0.18188461661338806 1 0.37337049841880798 
		0.70868575572967529 1 0.97552996873855591 1 1 1 0.99239087104797363 0.98553097248077393;
	setAttr -s 11 ".kiy[0:10]"  -0.98331987857818604 0 -0.92768239974975586 
		0.70552432537078857 0 0.2198665589094162 0 0 0 -0.12312744557857513 -0.16949521005153656;
	setAttr -s 11 ".kox[0:10]"  1 0.37336555123329163 0.70868575572967529 
		0.87101477384567261 1 0.97552996873855591 1 1 1 0.99239087104797363 0.98553097248077393;
	setAttr -s 11 ".koy[0:10]"  0 -0.9276842474937439 0.70552432537078857 
		0.49125692248344421 0 0.2198665589094162 0 0 0 -0.12312744557857513 -0.16949522495269775;
createNode animCurveTL -n "animCurveTL1894";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 51.6451416015625 449 51.6451416015625
		 451 51.6451416015625 457 51.6451416015625 511 51.6451416015625;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1895";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -11.264523506164551 449 -11.264523506164551
		 451 -11.264523506164551 457 -11.264523506164551 511 -11.264523506164551;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1896";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 449 0 451 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no yes no yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1897";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 1 449 1 451 1 455 1 465 1 468 1 478 1
		 496 1 511 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1898";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 1 449 1 451 1 455 1 465 1 468 1 478 1
		 496 1 511 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1899";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 1 449 1 451 1 455 1 465 1 468 1 478 1
		 496 1 511 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1897";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 93.281486511230469 449 -14.038240432739258
		 451 93.344207763671875 453 -0.5852971076965332 455 68.499000549316406 457 -9.0224161148071289
		 459 77.568130493164063 460 80.175880432128906 462 -48.127605438232422 463 -50.777908325195313
		 465 80.591926574707031 466 87.989875793457031 468 -6.2896385192871094 478 -29.859518051147464
		 480 81.106491088867187 481 83.598381042480469 483 -24.800180435180664 484 -27.467361450195313
		 486 81.106491088867187 489 83.598381042480469 496 15.535812377929689 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.72124814987182617 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0.95645803213119507 0 -0.95775818824768066 
		0 0.99426442384719849 0 -0.94745677709579468 0 0.95260506868362427 0 -0.95825827121734619 
		0 0.72209542989730835 0 -0.69267678260803223 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 1 1 1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.72124814987182617 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 0 0 0 0.95645803213119507 0 -0.95775818824768066 
		0 0.99426442384719849 0 -0.94745677709579468 0 0.95260506868362427 0 -0.95825827121734619 
		0 0.72209542989730835 0 -0.69267678260803223 0;
createNode animCurveTA -n "animCurveTA1898";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 7.1292028427124023 449 -23.37019157409668
		 451 11.086817741394043 453 -32.379146575927734 455 24.556015014648438 457 -19.984058380126953
		 459 24.755352020263672 460 25.994792938232422 462 -19.358005523681641 463 -20.762310028076172
		 465 36.406421661376953 466 33.905223846435547 468 2.2449593544006348 478 -5.6701068878173828
		 480 31.235822677612305 481 30.325757980346683 483 -7.809575080871582 484 -9.9204740524291992
		 486 31.235822677612305 489 30.325757980346683 496 7.8860201835632324 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 1 1 1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 0 0 0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
createNode animCurveTA -n "animCurveTA1899";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  447 18.65416145324707 449 16.48675537109375
		 451 23.160837173461914 453 8.3637781143188477 455 21.211563110351563 457 1.6625189781188965
		 459 32.980312347412109 460 34.735298156738281 462 31.418289184570312 463 33.537071228027344
		 465 28.683307647705078 466 35.122848510742188 468 20.297872543334961 478 16.59162712097168
		 480 50.610061645507813 481 51.810520172119141 483 26.127241134643555 484 26.807085037231445
		 486 50.610061645507813 489 51.810520172119141 496 7.9200544357299805 511 0;
	setAttr -s 22 ".ktl[21]" no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 0.41296577453613281 1 1 1 1 
		1 0.90650463104248047 1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 
		1 0.86371892690658569 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0.91074657440185547 0 0 0 0 
		0 -0.42219585180282593 0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 
		0 -0.50397384166717529 0;
	setAttr -s 22 ".kox[0:21]"  1 1 1 1 1 1 0.41296577453613281 1 1 1 1 
		1 0.90650463104248047 1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 
		1 0.86371892690658569 1;
	setAttr -s 22 ".koy[0:21]"  0 0 0 0 0 0 0.91074657440185547 0 0 0 0 
		0 -0.42219585180282593 0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 
		0 -0.50397384166717529 0;
createNode animCurveTL -n "animCurveTL1897";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 -19.609930038452148 449 -19.609930038452148
		 451 -19.609930038452148 455 -19.609930038452148 465 -19.609930038452148 468 -19.609930038452148
		 478 -19.609930038452148 496 -19.609930038452148 511 -19.609930038452148;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1898";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 36.542636871337891 449 36.542636871337891
		 451 36.542636871337891 455 36.542636871337891 465 36.542636871337891 468 36.542636871337891
		 478 36.542636871337891 496 36.542636871337891 511 36.542636871337891;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1899";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 -39.208473205566406 449 -39.208473205566406
		 451 -39.208473205566406 455 -39.208473205566406 465 -39.208473205566406 468 -39.208473205566406
		 478 -39.208473205566406 496 -39.208473205566406 511 -39.208473205566406;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1900";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1901";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1902";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1900";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1901";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1902";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1900";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -4.502251148223877 451 -4.502251148223877
		 456 -4.502251148223877 457 -4.502251148223877 511 -4.502251148223877;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1901";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 37.76336669921875 451 37.76336669921875
		 456 37.76336669921875 457 37.76336669921875 511 37.76336669921875;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1902";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1903";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1904";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1905";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 456 1 457 1 511 1;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1903";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  447 0 451 0 456 0 457 0 462 -0.13593044877052307
		 468 0.1689608246088028 475 -0.25842064619064331 484 -0.25360524654388428 496 -0.81208699941635132
		 502 -0.74729651212692261 511 0;
	setAttr -s 11 ".ktl[0:10]" no no yes no yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 1 1 1 1 0.99990791082382202 0.99904173612594604;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0 0 0 0 0.01356844324618578 0.043768759816884995;
	setAttr -s 11 ".kox[0:10]"  1 1 1 0.99993520975112915 1 1 1 1 1 0.99990791082382202 
		0.99904173612594604;
	setAttr -s 11 ".koy[0:10]"  0 0 0 -0.011386910453438759 0 0 0 0 0 0.01356844324618578 
		0.043768759816884995;
createNode animCurveTA -n "animCurveTA1904";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  447 0 451 0 456 0 457 0 462 -1.1088451147079468
		 468 -0.24560017883777618 475 0.73505294322967529 484 -0.3662811815738678 496 1.437603235244751
		 502 1.1907507181167603 511 0;
	setAttr -s 11 ".ktl[0:10]" no no yes no yes yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 0.99480611085891724 1 1 1 0.99866622686386108 
		0.99849241971969604;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 0.1017879918217659 0 0 0 -0.05163172259926796 
		-0.05488971620798111;
	setAttr -s 11 ".kox[0:10]"  1 1 1 0.9957129955291748 1 0.99480611085891724 
		1 1 1 0.99866622686386108 0.99849241971969604;
	setAttr -s 11 ".koy[0:10]"  0 0 0 -0.092495858669281006 0 0.1017879918217659 
		0 0 0 -0.05163172259926796 -0.054889723658561707;
createNode animCurveTA -n "animCurveTA1905";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  447 0 451 0 456 0 457 0 462 -2.2276911735534668
		 468 -1.4962397813796997 475 -0.20659753680229187 480 -1.9135464429855344 484 -4.38568115234375
		 496 -1.7283238172531128 502 1.1924782991409302 511 0;
	setAttr -s 12 ".ktl[0:11]" no no yes no yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 0.9884682297706604 1 0.93669140338897705 
		1 0.96339350938796997 1 0.99141174554824829;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0.15142825245857239 0 -0.35015606880187988 
		0 0.26809108257293701 0 -0.13077715039253235;
	setAttr -s 12 ".kox[0:11]"  1 1 1 0.98302739858627319 1 0.9884682297706604 
		1 0.93669140338897705 1 0.96339350938796997 1 0.99141174554824829;
	setAttr -s 12 ".koy[0:11]"  0 0 0 -0.18345852196216583 0 0.15142825245857239 
		0 -0.35015606880187988 0 0.26809108257293701 0 -0.13077718019485474;
createNode animCurveTL -n "animCurveTL1903";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 -1.4725730419158936 451 -1.4725730419158936
		 456 -1.4725730419158936 457 -1.4725730419158936 511 -1.4725730419158936;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1904";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 22.299345016479492 451 22.299345016479492
		 456 22.299345016479492 457 22.299345016479492 511 22.299345016479492;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1905";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 0 451 0 456 0 457 0 511 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes yes;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1906";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 482 1 490 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1907";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 482 1 490 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1908";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  447 1 451 1 482 1 490 1 511 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1906";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 8.3964118957519531 451 6.8745036125183105
		 456 2.3929557800292969 463 0.29277992248535156 474 -0.063073001801967621 482 0.16941116750240326
		 490 0.24168832600116733 495 -0.077064946293830872 501 -0.36784809827804565 511 0;
	setAttr -s 10 ".ktl[9]" no;
	setAttr -s 10 ".kix[0:9]"  1 0.90218305587768555 0.97001457214355469 
		0.99917471408843994 1 0.99993562698364258 1 0.99919891357421875 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 -0.43135333061218262 -0.24304662644863129 
		-0.040618989616632462 0 0.011352538131177425 0 -0.040019344538450241 0 0;
	setAttr -s 10 ".kox[0:9]"  1 0.90218305587768555 0.97001457214355469 
		0.99917471408843994 1 0.99993562698364258 1 0.99919891357421875 1 1;
	setAttr -s 10 ".koy[0:9]"  0 -0.43135333061218262 -0.24304662644863129 
		-0.040618989616632462 0 0.011352538131177425 0 -0.040019344538450241 0 0;
createNode animCurveTA -n "animCurveTA1907";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 -5.007056713104248 451 -4.4419918060302734
		 456 -2.2537462711334229 463 0.021117612719535828 474 0.089085608720779419 482 0.32429292798042297
		 490 -0.32775837182998657 495 0.028010895475745205 501 0.28513193130493164 511 0;
	setAttr -s 10 ".ktl[9]" no;
	setAttr -s 10 ".kix[0:9]"  1 0.9927295446395874 0.9757876992225647 
		0.99996989965438843 0.99997222423553467 1 1 0.99888145923614502 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0.12036620825529099 0.21871963143348694 
		0.0077644120901823044 0.007451613899320364 0 0 0.047284740954637527 0 0;
	setAttr -s 10 ".kox[0:9]"  1 0.9927295446395874 0.9757876992225647 
		0.99996989965438843 0.99997222423553467 1 1 0.99888145923614502 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0.12036620825529099 0.21871963143348694 
		0.0077644120901823044 0.007451613899320364 0 0 0.047284740954637527 0 0;
createNode animCurveTA -n "animCurveTA1908";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 11.037167549133301 451 9.4760541915893555
		 456 1.2319904565811157 463 -2.3035626411437988 474 0.80299609899520874 482 1.8759468793869021
		 490 -2.232053279876709 495 -0.48707431554794306 501 1.7187179327011108 511 0;
	setAttr -s 10 ".ktl[9]" no;
	setAttr -s 10 ".kix[0:9]"  1 0.91964554786682129 0.84429669380187988 
		1 0.98609292507171631 1 1 0.95863842964172363 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 -0.39274942874908447 -0.53587597608566284 
		0 0.1661948561668396 0 0 0.28462675213813782 0 0;
	setAttr -s 10 ".kox[0:9]"  1 0.91964554786682129 0.84429669380187988 
		1 0.98609292507171631 1 1 0.95863842964172363 1 1;
	setAttr -s 10 ".koy[0:9]"  0 -0.39274942874908447 -0.53587597608566284 
		0 0.1661948561668396 0 0 0.28462675213813782 0 0;
createNode animCurveTL -n "animCurveTL1906";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  447 0 451 0 456 -0.22539572417736053 482 0
		 490 0 511 0;
	setAttr -s 6 ".ktl[1:5]" no yes no yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1907";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  447 -2.2946529388427734 451 -2.2946529388427734
		 456 -0.91977530717849731 463 13.693543434143066 467 17.586925506591797 482 10.791976928710938
		 490 24.317514419555664 495 26.571771621704102 511 0;
	setAttr -s 9 ".ktl[0:8]" no no yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 0.050445232540369034 0.017801720649003983 
		1 1 0.030791314318776131 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0.99872684478759766 0.99984157085418701 
		0 0 0.99952590465545654 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.050445232540369034 0.017801720649003983 
		1 1 0.030791314318776131 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0.99872684478759766 0.99984157085418701 
		0 0 0.99952590465545654 0 0;
createNode animCurveTL -n "animCurveTL1908";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  447 0 451 0 456 0.23959445953369141 482 0
		 490 0 511 0;
	setAttr -s 6 ".ktl[1:5]" no yes no yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1909";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  447 1 451 1 494 1 511 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1910";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  447 1 451 1 494 1 511 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1911";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  447 1 451 1 494 1 511 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1909";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 -4.817873477935791 451 -2.7788476943969727
		 456 -9.3305940628051758 459 1.5995619297027588 462 6.0639548301696777 466 -0.57336306571960449
		 471 1.7815185785293579 477 -1.0614374876022339 494 -0.70235580205917358 511 0;
	setAttr -s 10 ".kix[0:9]"  1 1 1 0.47155919671058655 1 1 1 1 0.99985378980636597 
		0.99978101253509521;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0.88183444738388062 0 0 0 0 0.017097815871238708 
		0.020927129313349724;
	setAttr -s 10 ".kox[0:9]"  1 1 1 0.47155919671058655 1 1 1 1 0.99985378980636597 
		0.99978101253509521;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0.88183444738388062 0 0 0 0 0.017097821459174156 
		0.020927134901285172;
createNode animCurveTA -n "animCurveTA1910";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 2.2509026527404785 451 -1.4357391595840454
		 456 23.164876937866211 459 33.545169830322266 462 26.170217514038086 466 2.4428699016571045
		 471 28.041593551635742 477 7.0878419876098633 494 2.1343567371368408 511 0;
	setAttr -s 10 ".ktl[9]" no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.31749677658081055 1 0.30797329545021057 
		1 1 0.9541887640953064 0.99861997365951538 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0.94825929403305054 0 -0.95139503479003906 
		0 0 -0.29920533299446106 -0.052517849951982498 0;
	setAttr -s 10 ".kox[0:9]"  1 1 0.31749677658081055 1 0.30797329545021057 
		1 1 0.9541887640953064 0.99861997365951538 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0.94825929403305054 0 -0.95139503479003906 
		0 0 -0.29920533299446106 -0.052517849951982498 0;
createNode animCurveTA -n "animCurveTA1911";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  447 -65.016624450683594 451 -59.747497558593743
		 456 -54.840415954589844 459 -19.13679313659668 462 -8.2171163558959961 466 -18.651880264282227
		 471 -11.47330379486084 477 -6.8645796775817871 482 -10.902566909790039 487 -18.439697265625
		 495.908 -4.6132702827453613 501 7.0690317153930664 511 0;
	setAttr -s 13 ".kix[0:12]"  1 0.9010169506072998 0.62981724739074707 
		0.21358121931552887 1 1 0.71949797868728638 1 0.70187371969223022 1 0.49949237704277039 
		1 0.90229082107543945;
	setAttr -s 13 ".kiy[0:12]"  0 0.4337838888168335 0.77674329280853271 
		0.9769253134727478 0 0 0.69449454545974731 0 -0.71230137348175049 0 0.86631834506988525 
		0 -0.43112790584564209;
	setAttr -s 13 ".kox[0:12]"  1 0.9010169506072998 0.62981724739074707 
		0.21358121931552887 1 1 0.71949797868728638 1 0.70187371969223022 1 0.49949237704277039 
		1 0.90229082107543945;
	setAttr -s 13 ".koy[0:12]"  0 0.4337838888168335 0.77674329280853271 
		0.9769253134727478 0 0 0.69449454545974731 0 -0.71230137348175049 0 0.86631834506988525 
		0 -0.43112790584564209;
createNode animCurveTL -n "animCurveTL1909";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 -47.862842559814453 451 -47.862842559814453
		 462 -46.847499847412109 466 -47.386089324951172 477 -52.141513824462891 482 -52.637279510498047
		 487 -52.271835327148438 494 -54.989700317382813 498 -56.458881378173828 511 -54.946929931640625;
	setAttr -s 10 ".ktl[0:9]" no no yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 0.1026056706905365 0.15490682423114777 
		1 1 0.067703187465667725 1 0.22282746434211731;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 -0.99472206830978394 -0.98792910575866699 
		0 0 -0.99770551919937134 0 0.9748578667640686;
	setAttr -s 10 ".kox[0:9]"  1 1 1 0.1026056706905365 0.15490682423114777 
		1 1 0.067703187465667725 1 0.22282752394676208;
	setAttr -s 10 ".koy[0:9]"  0 0 0 -0.99472206830978394 -0.98792910575866699 
		0 0 -0.99770551919937134 0 0.9748578667640686;
createNode animCurveTL -n "animCurveTL1910";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 -10.917769432067871 451 -10.917769432067871
		 462 1.2566131353378296 466 4.680389404296875 477 9.185154914855957 482 5.681556224822998
		 487 9.811004638671875 494 7.6069731712341309 498 3.1013684272766113 511 2.3482637405395508;
	setAttr -s 10 ".ktl[0:9]" no no yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 0.030407363548874855 0.065709441900253296 
		1 1 1 0.044068239629268646 0.46345451474189758 0.70931452512741089;
	setAttr -s 10 ".kiy[0:9]"  0 0 0.99953764677047729 0.99783885478973389 
		0 0 0 -0.99902850389480591 -0.88612073659896851 -0.70489215850830078;
	setAttr -s 10 ".kox[0:9]"  1 1 0.030407363548874855 0.065709441900253296 
		1 1 1 0.044068239629268646 0.46345451474189758 0.70931452512741089;
	setAttr -s 10 ".koy[0:9]"  0 0 0.99953764677047729 0.99783885478973389 
		0 0 0 -0.99902850389480591 -0.88612073659896851 -0.70489215850830078;
createNode animCurveTL -n "animCurveTL1911";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  447 1.7520284652709961 451 1.7520284652709961
		 462 -0.59498012065887451 466 -1.0637115240097046 477 -1.3494876623153687 482 -0.73749023675918579
		 487 1.1823307275772095 494 -0.067849248647689819 498 -1.4310612678527832 511 0;
	setAttr -s 10 ".ktl[0:9]" no no yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.17007854580879211 0.50306844711303711 
		1 0.11274827271699905 1 0.080945320427417755 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 -0.98543053865432739 -0.86424654722213745 
		0 0.99362361431121826 0 -0.99671852588653564 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 0.17007854580879211 0.50306844711303711 
		1 0.11274827271699905 1 0.080945320427417755 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 -0.98543053865432739 -0.86424654722213745 
		0 0.99362361431121826 0 -0.99671852588653564 0 0;
select -ne :time1;
	setAttr ".o" 511;
	setAttr ".unw" 511;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "getup_take_offSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1873.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1874.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1875.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1873.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1874.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1875.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1873.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1874.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1875.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1876.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1877.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1878.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1876.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1877.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1878.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1876.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1877.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1878.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1879.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1880.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1881.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1879.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1880.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1881.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1879.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1880.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1881.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1882.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1883.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1884.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1882.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1883.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1884.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1882.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1883.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1884.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1885.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1886.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1887.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1885.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1886.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1887.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1885.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1886.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1887.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1888.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1889.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1890.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1888.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1889.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1890.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1888.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1889.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1890.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1891.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1892.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1893.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1891.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1892.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1893.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1891.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1892.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1893.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1894.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1895.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1896.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1894.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1895.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1896.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1894.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1895.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1896.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1897.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1898.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1899.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1897.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1898.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1899.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1897.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1898.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1899.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1900.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1901.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1902.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1900.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1901.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1902.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1900.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1901.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1902.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1903.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1904.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1905.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1903.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1904.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1905.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1903.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1904.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1905.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1906.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1907.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1908.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1906.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1907.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1908.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1906.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1907.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1908.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1909.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1910.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1911.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1909.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1910.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1911.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1909.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1910.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1911.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_getup_take_off.ma
