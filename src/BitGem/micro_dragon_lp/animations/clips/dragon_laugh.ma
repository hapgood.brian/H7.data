//Maya ASCII 2013 scene
//Name: dragon_laugh.ma
//Last modified: Mon, Jul 14, 2014 01:55:00 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "laughSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 729;
	setAttr ".se" 791;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU2458";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2459";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2460";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2458";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2459";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2460";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2458";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 3.2171449661254883 791 3.2171449661254883;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2459";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -26.658763885498047 791 -26.658763885498047;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2460";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -1.5793838500976563 791 -1.5793838500976563;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2461";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2462";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2463";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2461";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  729 0 733 11.505229949951172 736 12.776043891906738
		 740 -133.915771484375 742 -138.15037536621094 745 -25.053995132446289 747 -56.534645080566406
		 750 -74.41473388671875 756 -18.266441345214844 762 -64.790985107421875 768 -23.506168365478516
		 772 -55.771224975585938 779 4.0576343536376953 785 -6.1831254959106445 791 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.88272517919540405 1 0.35181549191474915 
		1 1 0.13234405219554901 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0.46988964080810547 0 -0.93606936931610107 
		0 0 -0.99120378494262695 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.63866615295410156 0.88272517919540405 
		1 0.35181549191474915 1 1 0.13234405219554901 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.76948386430740356 0.46988964080810547 
		0 -0.93606936931610107 0 0 -0.99120378494262695 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2462";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  729 0 733 -2.1008434295654297 736 3.8420858383178711
		 740 -42.333057403564453 742 -37.594837188720703 745 -26.476591110229492 747 -38.890201568603516
		 750 -37.550052642822266 756 -36.025535583496094 762 -41.735816955566406 768 -37.746803283691406
		 772 -43.609970092773438 779 -10.060004234313965 785 -3.52239990234375 791 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 1 1 1 0.31841316819190979 1 1 0.95261895656585693 
		1 1 1 1 0.66491734981536865 0.94351649284362793 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0 0 0.9479520320892334 0 0 0.3041660487651825 
		0 0 0 0 0.74691689014434814 0.33132559061050415 0;
	setAttr -s 15 ".kox[0:14]"  0.97664433717727661 1 1 1 0.31841316819190979 
		1 1 0.95261895656585693 1 1 1 1 0.66491734981536865 0.94351649284362793 1;
	setAttr -s 15 ".koy[0:14]"  -0.2148624062538147 0 0 0 0.9479520320892334 
		0 0 0.3041660487651825 0 0 0 0 0.74691689014434814 0.33132559061050415 0;
createNode animCurveTA -n "animCurveTA2463";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  729 0 733 -16.062694549560547 736 -18.541086196899414
		 740 51.513160705566406 742 52.607498168945313 745 -2.9089541435241699 747 11.324995994567871
		 750 23.414386749267578 756 -7.5781259536743164 762 18.494758605957031 768 -5.6392545700073242
		 772 10.350617408752441 779 0.64035648107528687 785 -0.021965650841593742 791 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.69375097751617432 1 0.82400649785995483 
		1 1 0.19373144209384918 1 1 1 1 1 0.99051558971405029 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 -0.72021496295928955 0 0.56658029556274414 
		0 0 0.98105454444885254 0 0 0 0 0 -0.13740080595016479 0 0;
	setAttr -s 15 ".kox[0:14]"  0.51101481914520264 0.69375097751617432 
		1 0.82400649785995483 1 1 0.19373144209384918 1 1 1 1 1 0.99051558971405029 1 1;
	setAttr -s 15 ".koy[0:14]"  -0.85957187414169312 -0.72021496295928955 
		0 0.56658029556274414 0 0 0.98105454444885254 0 0 0 0 0 -0.13740080595016479 0 0;
createNode animCurveTL -n "animCurveTL2461";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -4.7867727279663086 791 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2462";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 32.754745483398438 791 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2463";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -56.147525787353516 791 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2464";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2465";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2466";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2464";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2465";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2466";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2464";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 3.2171449661254883 791 3.2171449661254883;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2465";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -26.658763885498047 791 -26.658763885498047;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2466";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1.5793838500976563 791 1.5793838500976563;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2467";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2468";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2469";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2467";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 3.8336954116821285 738 111.24797058105469
		 743 124.74274444580078 747 54.168937683105469 752 77.20074462890625 757 20.039302825927734
		 763 47.188579559326172 767 14.976258277893065 774 33.924293518066406 782 15.117812156677248
		 786 12.510866165161133 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 0.77975422143936157 0.2828088104724884 
		1 1 1 1 1 1 1 0.804066002368927 0.82590115070343018 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.62608575820922852 0.95917630195617676 
		0 0 0 0 0 0 0 -0.59454011917114258 -0.56381499767303467 0;
	setAttr -s 13 ".kox[0:12]"  0.96599990129470825 0.77975422143936157 
		0.2828088104724884 1 1 1 1 1 1 1 0.804066002368927 0.82590115070343018 1;
	setAttr -s 13 ".koy[0:12]"  0.25854253768920898 0.62608575820922852 
		0.95917630195617676 0 0 0 0 0 0 0 -0.59454011917114258 -0.56381499767303467 0;
createNode animCurveTA -n "animCurveTA2468";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 -10.056730270385742 738 32.564376831054687
		 743 31.167060852050781 747 27.616209030151367 752 40.038951873779297 757 10.048148155212402
		 763 33.458908081054687 767 25.087207794189453 774 35.954196929931641 782 18.861572265625
		 786 13.57282829284668 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 0.94350969791412354 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 -0.33134493231773376 0 0 0 0 0 0 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.81842660903930664 1 1 0.94350969791412354 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  -0.57461094856262207 0 0 -0.33134493231773376 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2469";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 -27.075536727905273 738 28.079788208007813
		 743 36.413883209228516 747 -12.82164192199707 752 2.6447165012359619 757 -22.814939498901367
		 763 -15.449691772460936 767 -17.003095626831055 774 -17.537422180175781 782 -7.1113271713256836
		 786 -9.9248685836791992 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 0.43083873391151428 1 1 1 1 1 0.99543094635009766 
		1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0.90242898464202881 0 0 0 0 0 -0.095483839511871338 
		0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.4676285982131958 1 0.43083873391151428 
		1 1 1 1 1 0.99543094635009766 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  -0.88392508029937744 0 0.90242898464202881 
		0 0 0 0 0 -0.095483839511871338 0 0 0 0;
createNode animCurveTL -n "animCurveTL2467";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -4.7867727279663086 791 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2468";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 32.754745483398438 791 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2469";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 56.147525787353516 791 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2470";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2471";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2472";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2470";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2471";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2472";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2470";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 40.544437408447266 791 40.544437408447266;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2471";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 43.055271148681641 791 43.055271148681641;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2472";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2473";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2474";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2475";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2473";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2474";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2475";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2473";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 8.0282459259033203 791 8.0282459259033203;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2474";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 9.9087905883789063 791 9.9087905883789063;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2475";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2476";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2477";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2478";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2476";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 734 -72.301765441894531 736 -65.367477416992188
		 742 7.0099306106567383 747 -50.369041442871094 753 -10.303532600402832 757 -40.529632568359375
		 762 -11.938051223754883 768 -44.722908020019531 774 -26.141168594360352 779 -44.643096923828125
		 785 -29.211513519287106 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 0.22370219230651855 1 1 1 1 1 1 1 1 
		0.34450653195381165 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0.97465753555297852 0 0 0 0 0 0 0 0 
		0.93878394365310669 0;
	setAttr -s 13 ".kox[0:12]"  0.16289003193378448 1 0.22370219230651855 
		1 1 1 1 1 1 1 1 0.34450653195381165 1;
	setAttr -s 13 ".koy[0:12]"  -0.98664426803588867 0 0.97465753555297852 
		0 0 0 0 0 0 0 0 0.93878394365310669 0;
createNode animCurveTA -n "animCurveTA2477";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 734 -39.246334075927734 736 -48.840476989746094
		 742 6.6315169334411621 747 -23.103620529174805 753 -12.647639274597168 757 -35.512340545654297
		 762 -15.639493942260742 768 -31.244256973266602 774 -16.55126953125 779 -33.547939300537109
		 785 -20.014537811279297 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 0.19836120307445526 1 1 1 1 1 1 1 1 1 
		0.42784920334815979 1;
	setAttr -s 13 ".kiy[0:12]"  0 -0.9801289439201355 0 0 0 0 0 0 0 0 0 
		0.90385019779205322 0;
	setAttr -s 13 ".kox[0:12]"  0.29098570346832275 0.19836120307445526 
		1 1 1 1 1 1 1 1 1 0.42784920334815979 1;
	setAttr -s 13 ".koy[0:12]"  -0.95672738552093506 -0.9801289439201355 
		0 0 0 0 0 0 0 0 0 0.90385019779205322 0;
createNode animCurveTA -n "animCurveTA2478";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 734 40.303272247314453 736 39.145984649658203
		 742 28.521141052246094 747 38.16571044921875 753 20.979475021362305 757 31.43675422668457
		 762 23.809896469116211 768 31.407825469970707 774 17.8038330078125 779 26.300849914550781
		 785 16.774782180786133 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 0.80878478288650513 1 1 1 1 1 1 1 1 
		0.54087686538696289 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 -0.58810478448867798 0 0 0 0 0 0 0 
		0 -0.84110176563262939 0;
	setAttr -s 13 ".kox[0:12]"  0.28397783637046814 1 0.80878478288650513 
		1 1 1 1 1 1 1 1 0.54087686538696289 1;
	setAttr -s 13 ".koy[0:12]"  0.95883089303970337 0 -0.58810478448867798 
		0 0 0 0 0 0 0 0 -0.84110176563262939 0;
createNode animCurveTL -n "animCurveTL2476";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -19.597047805786133 791 -19.597047805786133;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2477";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 36.545459747314453 791 36.545459747314453;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2478";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 39.212558746337891 791 39.212558746337891;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2479";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2480";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2481";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2479";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2480";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2481";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2479";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 51.6451416015625 791 51.6451416015625;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2480";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -11.264523506164551 791 -11.264523506164551;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2481";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2482";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 787 1 791 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2483";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 787 1 791 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2484";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 787 1 791 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA2482";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 86.684326171875 739 -20.309850692749023
		 743 9.7436227798461914 747 75.260856628417969 752 21.411909103393555 757 62.203369140625007
		 762 3.7250237464904785 769 38.062286376953125 773 10.472290992736816 780 46.573104858398438
		 787 -6.813136100769043 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 0.1053253710269928 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0.99443787336349487 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 13 ".kox[0:12]"  0.16303172707557678 1 1 0.1053253710269928 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0.98662084341049194 0 0 0.99443787336349487 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2483";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 26.937461853027344 739 -25.020366668701172
		 743 -28.211685180664063 747 33.332660675048828 752 9.2133598327636719 757 13.312129974365234
		 762 12.905970573425293 769 24.213794708251953 773 2.3904197216033936 780 24.17711067199707
		 787 -7.8765420913696298 791 0;
	setAttr -s 13 ".kix[0:12]"  1 1 0.70619446039199829 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 -0.70801794528961182 0 0 0 0 0 0 0 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.46949818730354309 1 0.70619446039199829 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0.88293343782424927 0 -0.70801794528961182 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2484";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 41.330356597900391 739 19.357355117797852
		 743 7.1375069618225098 747 56.530448913574219 752 35.489719390869141 757 38.200618743896484
		 762 39.702793121337891 769 26.274587631225586 773 16.381563186645508 780 29.901088714599609
		 787 -1.3860301971435547 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 0.25207445025444031 1 1 1 0.93554621934890747 
		1 0.52683055400848389 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 -0.96770787239074707 0 0 0 0.35320436954498291 
		0 -0.84997028112411499 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.32746338844299316 1 0.25207445025444031 
		1 1 1 0.93554621934890747 1 0.52683055400848389 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0.94486385583877563 0 -0.96770787239074707 
		0 0 0 0.35320436954498291 0 -0.84997028112411499 0 0 0 0;
createNode animCurveTL -n "animCurveTL2482";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 -19.609930038452148 787 -19.609930038452148
		 791 -19.609930038452148;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2483";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 36.542636871337891 787 36.542636871337891
		 791 36.542636871337891;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2484";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 -39.208473205566406 787 -39.208473205566406
		 791 -39.208473205566406;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2485";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 784 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2486";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 784 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2487";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 784 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA2485";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  729 0 731 0.47999262809753418 735 0.41988155245780945
		 737 4.2351384162902832 742 5.1072373390197754 746 3.6686384677886958 750 0.50176191329956055
		 756 3.4299142360687256 761 2.4862740039825439 768 3.3623142242431641 772 3.1945910453796387
		 780 3.4175021648406982 784 2.998126745223999 791 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 1 0.97681176662445068 1 0.91830748319625854 
		1 1 1 1 1 1 0.9914323091506958 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0.21409995853900909 0 -0.39586791396141052 
		0 0 0 0 0 0 -0.13062185049057007 0;
	setAttr -s 14 ".kox[0:13]"  0.99498498439788818 1 1 0.97681176662445068 
		1 0.91830748319625854 1 1 1 1 1 1 0.9914323091506958 1;
	setAttr -s 14 ".koy[0:13]"  0.1000244989991188 0 0 0.21409995853900909 
		0 -0.39586791396141052 0 0 0 0 0 0 -0.13062185049057007 0;
createNode animCurveTA -n "animCurveTA2486";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  729 0 731 0.53125381469726563 735 -0.6608731746673584
		 737 -3.2668545246124268 742 -1.3898971080780029 746 -0.13021531701087952 750 6.4252395629882812
		 756 6.3082609176635742 761 1.6109552383422852 768 0.81981253623962402 772 -0.57072234153747559
		 780 1.7632761001586914 784 0.21712709963321686 791 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 0.93647760152816772 1 0.98970961570739746 
		0.92983603477478027 1 0.99970000982284546 0.99006438255310059 0.99537807703018188 
		1 1 0.9992411732673645 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 -0.35072746872901917 0 0.14309082925319672 
		0.36797401309013367 0 -0.024492617696523666 -0.14061449468135834 -0.096033260226249695 
		0 0 -0.038948997855186462 0;
	setAttr -s 14 ".kox[0:13]"  0.99386698007583618 1 0.93647760152816772 
		1 0.98970961570739746 0.92983603477478027 1 0.99970000982284546 0.99006438255310059 
		0.99537807703018188 1 1 0.9992411732673645 1;
	setAttr -s 14 ".koy[0:13]"  0.11058231443166733 0 -0.35072746872901917 
		0 0.14309082925319672 0.36797401309013367 0 -0.024492617696523666 -0.14061449468135834 
		-0.096033260226249695 0 0 -0.038948997855186462 0;
createNode animCurveTA -n "animCurveTA2487";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  729 0 731 5.8255167007446289 735 3.387376070022583
		 737 -15.78236198425293 742 -21.530733108520508 746 -6.9485235214233398 750 -26.232892990112305
		 756 -9.0099954605102539 761 -23.148906707763672 768 -5.5207910537719727 772 -11.996899604797363
		 780 -0.31945624947547913 784 -5.4473733901977539 791 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 0.79387551546096802 0.56913560628890991 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 -0.60808038711547852 -0.82224363088607788 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  0.63390189409255981 1 0.79387551546096802 
		0.56913560628890991 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0.77341347932815552 0 -0.60808038711547852 
		-0.82224363088607788 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2485";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 -4.502251148223877 784 -4.502251148223877
		 791 -4.502251148223877;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2486";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 37.76336669921875 784 37.76336669921875
		 791 37.76336669921875;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2487";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 0 784 0 791 0;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2488";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2489";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2490";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2488";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2489";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2490";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2488";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -1.4725730419158936 791 -1.4725730419158936;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2489";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 22.299345016479492 791 22.299345016479492;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2490";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kot[1]"  10;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2491";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 778 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2492";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 778 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2493";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 1 778 1 791 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA2491";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  729 0 732 0.6702197790145874 735 0.80191576480865479
		 738 2.1315138339996338 748 -4.8991212844848633 750 -5.9874129295349121 753 -7.1924557685852051
		 755 -6.0854077339172363 758 -1.5392483472824097 760 -0.96405917406082153 766 -1.6842318773269653
		 769 -1.3022853136062622 778 0 785 -0.231512650847435 791 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.99848192930221558 0.99848192930221558 
		1 0.92427706718444824 0.98674750328063965 1 0.84032434225082397 0.94046646356582642 
		1 1 0.99522995948791504 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0.055080924183130264 0.055080924183130264 
		0 -0.38172245025634766 -0.16226312518119812 0 0.54208391904830933 0.33988645672798157 
		0 0 0.097557447850704193 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.99564987421035767 0.99848192930221558 
		0.99848192930221558 1 0.92427706718444824 0.98674750328063965 1 0.84032434225082397 
		0.94046646356582642 1 1 0.99522995948791504 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.093173228204250336 0.055080924183130264 
		0.055080924183130264 0 -0.38172245025634766 -0.16226312518119812 0 0.54208391904830933 
		0.33988645672798157 0 0 0.097557447850704193 0 0 0;
createNode animCurveTA -n "animCurveTA2492";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  729 0 732 0.92241853475570679 735 1.1313341856002808
		 738 -6.5197710990905762 748 -4.5203404426574707 750 -3.8526365756988525 753 -1.6584004163742065
		 755 -1.0270817279815674 758 -3.2114508152008057 760 -3.1923115253448486 766 -1.7447853088378906
		 769 -1.7640950679779053 778 0 785 -0.7798086404800415 791 0;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  1 0.9961928129196167 1 1 0.99380862712860107 
		0.98264777660369873 0.98774337768554688 1 1 0.99992769956588745 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0.087177254259586334 0 0 0.11110550910234451 
		0.18548145890235901 0.15608663856983185 0 0 0.012024581432342529 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.99180781841278076 0.9961928129196167 
		1 1 0.99380862712860107 0.98264777660369873 0.98774337768554688 1 1 0.99992769956588745 
		1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.12773887813091278 0.087177254259586334 
		0 0 0.11110550910234451 0.18548145890235901 0.15608663856983185 0 0 0.012024581432342529 
		0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2493";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  729 0 732 8.9652671813964844 735 11.093047142028809
		 738 -16.524221420288086 740 -18.190389633178711 745 -5.0206232070922852 748 -14.056247711181641
		 750 -15.813784599304199 753 -8.6726522445678711 755 -5.8565363883972168 758 -14.469531059265137
		 760 -16.482048034667969 766 -8.0191020965576172 769 -12.365959167480469 778 0 785 -3.7232587337493901
		 791 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes;
	setAttr -s 17 ".kix[0:16]"  1 0.74652153253555298 1 0.69072896242141724 
		1 1 0.67123717069625854 1 0.49201783537864685 1 0.62029707431793213 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.66536128520965576 0 -0.72311371564865112 
		0 0 -0.74124258756637573 0 0.87058514356613159 0 -0.78436696529388428 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  0.62415057420730591 0.74652153253555298 
		1 0.69072896242141724 1 1 0.67123717069625854 1 0.49201783537864685 1 0.62029707431793213 
		1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0.7813040018081665 0.66536128520965576 
		0 -0.72311371564865112 0 0 -0.74124258756637573 0 0.87058514356613159 0 -0.78436696529388428 
		0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2491";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 0 778 0 791 0;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2492";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 0 778 0 791 0;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL2493";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  729 0 778 0 791 0;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU2494";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2495";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2496";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 1 791 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2494";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  729 0 737 -1.1122269630432129 740 11.804862976074219
		 743 5.1266708374023437 747 14.028186798095703 752 -2.0514640808105469 757 -4.1107883453369141
		 762 -6.2935266494750977 768 -2.6866343021392822 773 -8.2884502410888672 783 -9.0873346328735352
		 791 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 0.88810044527053833 0.98457717895507813 
		1 1 0.99499863386154175 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 -0.45964926481246948 -0.17495059967041016 
		0 0 -0.099888652563095093 0 0;
	setAttr -s 12 ".kox[0:11]"  0.99830865859985352 1 1 1 1 0.88810044527053833 
		0.98457717895507813 1 1 0.99499863386154175 1 1;
	setAttr -s 12 ".koy[0:11]"  -0.058137461543083191 0 0 0 0 -0.45964926481246948 
		-0.17495059967041016 0 0 -0.099888652563095093 0 0;
createNode animCurveTA -n "animCurveTA2495";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  729 0 737 -0.85824936628341675 740 25.862026214599609
		 743 31.455888748168949 747 7.1984519958496094 752 8.5345296859741211 757 -9.7538986206054687
		 762 7.3832321166992188 768 -2.3228094577789307 773 3.9964394569396977 783 -7.0208487510681152
		 791 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 0.39252358675003052 1 1 1 1 1 1 1 1 
		1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0.9197419285774231 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 12 ".kox[0:11]"  0.99899184703826904 1 0.39252358675003052 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  -0.044892441481351852 0 0.9197419285774231 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2496";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  729 0 735 22.072404861450195 737 31.957317352294922
		 740 -13.630984306335449 743 -25.754602432250977 747 7.4537987709045419 752 -14.526173591613768
		 757 13.69045352935791 762 -2.0385510921478271 768 14.578550338745117 773 -4.9520907402038574
		 783 7.1692943572998047 791 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 0.22652553021907806 1 0.1932048499584198 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.97400522232055664 0 -0.98115837574005127 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.54437065124511719 0.22652553021907806 
		1 0.1932048499584198 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0.83884483575820923 0.97400522232055664 
		0 -0.98115837574005127 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2494";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 -54.946929931640625 791 -54.946929931640625;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2495";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 2.3482637405395508 791 2.3482637405395508;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2496";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  729 0 791 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 765;
	setAttr ".unw" 765;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "laughSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU2458.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU2459.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU2460.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA2458.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA2459.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA2460.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL2458.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL2459.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL2460.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU2461.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU2462.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU2463.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA2461.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA2462.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA2463.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL2461.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL2462.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL2463.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU2464.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU2465.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU2466.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA2464.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA2465.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA2466.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL2464.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL2465.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL2466.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU2467.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU2468.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU2469.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA2467.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA2468.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA2469.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL2467.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL2468.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL2469.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU2470.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU2471.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU2472.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA2470.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA2471.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA2472.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL2470.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL2471.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL2472.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU2473.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU2474.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU2475.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA2473.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA2474.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA2475.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL2473.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL2474.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL2475.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU2476.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU2477.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU2478.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA2476.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA2477.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA2478.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL2476.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL2477.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL2478.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU2479.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU2480.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU2481.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA2479.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA2480.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA2481.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL2479.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL2480.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL2481.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU2482.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU2483.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU2484.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA2482.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA2483.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA2484.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL2482.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL2483.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL2484.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU2485.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU2486.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU2487.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA2485.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA2486.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA2487.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL2485.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL2486.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL2487.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU2488.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU2489.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU2490.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA2488.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA2489.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA2490.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL2488.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL2489.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL2490.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU2491.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU2492.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU2493.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA2491.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA2492.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA2493.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL2491.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL2492.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL2493.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU2494.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU2495.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU2496.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA2494.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA2495.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA2496.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL2494.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL2495.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL2496.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_laugh.ma
