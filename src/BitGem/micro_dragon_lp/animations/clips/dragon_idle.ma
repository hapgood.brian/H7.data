//Maya ASCII 2013 scene
//Name: dragon_idle.ma
//Last modified: Mon, Jul 14, 2014 12:15:49 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "idleSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 6;
	setAttr ".se" 60;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU157";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU158";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU159";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA157";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA158";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA159";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL157";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 3.2171449661254883 60 3.2171449661254883;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL158";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -26.658763885498047 60 -26.658763885498047;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL159";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -1.5793838500976563 60 -1.5793838500976563;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU160";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 34 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU161";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 34 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU162";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 34 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA160";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 5.0281400680541992 20 -7.5339584350585946
		 34 -3.8269257545471191 38 0.36858692765235901 48 -11.385903358459473 55 -9.1527347564697266
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.94885134696960449 1 1 0.92818921804428101 
		0.79356366395950317;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0.31572306156158447 0 0 0.37210854887962341 
		0.60848712921142578;
	setAttr -s 8 ".kox[0:7]"  0.88483446836471558 1 1 0.94885134696960449 
		1 1 0.92818921804428101 1;
	setAttr -s 8 ".koy[0:7]"  0.46590566635131836 0 0 0.31572306156158447 
		0 0 0.37210854887962341 0;
createNode animCurveTA -n "animCurveTA161";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 3.1475851535797119 20 0.87319332361221313
		 34 0.79158616065979004 38 3.3938367366790771 48 -1.2170324325561523 55 -0.45490458607673651
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.99997317790985107 1 1 1 0.99388802051544189 
		1;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.0073248445987701416 0 0 0 0.11039276421070099 
		0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.99997317790985107 1 1 1 0.99388802051544189 
		1;
	setAttr -s 8 ".koy[0:7]"  0 0 -0.0073248445987701416 0 0 0 0.11039276421070099 
		0;
createNode animCurveTA -n "animCurveTA162";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 1.9395600557327271 20 -1.9359426498413086
		 34 -1.1208552122116089 38 -2.2303905487060547 48 0.038782242685556412 55 0.053398575633764267
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 0.9999966025352478 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0.0026239089202135801 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 0.9999966025352478 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0.0026239089202135801 0 0;
createNode animCurveTL -n "animCurveTL160";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 -4.7867727279663086 34 -4.7867727279663086
		 60 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL161";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 32.754745483398438 34 32.754745483398438
		 60 32.754745483398438;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL162";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 -56.147525787353516 34 -56.147525787353516
		 60 -56.147525787353516;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU163";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU164";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU165";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA163";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA164";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA165";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL163";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 3.2171449661254883 60 3.2171449661254883;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL164";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -26.658763885498047 60 -26.658763885498047;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL165";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1.5793838500976563 60 1.5793838500976563;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU166";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU167";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU168";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA166";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 -3.4327239990234375 28 6.0283083915710449
		 34 -1.1292744874954224 39 -2.0188193321228027 49 6.3055477142333984 56 4.6735668182373047
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.97590833902359009 1 1 0.95966237783432007 
		0.89819800853729248;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 -0.21818085014820099 0 0 -0.28115475177764893 
		-0.43959113955497742;
	setAttr -s 8 ".kox[0:7]"  0.94104504585266113 1 1 0.97590833902359009 
		1 1 0.95966237783432007 1;
	setAttr -s 8 ".koy[0:7]"  -0.33828127384185791 0 0 -0.21818085014820099 
		0 0 -0.28115475177764893 0;
createNode animCurveTA -n "animCurveTA167";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 -2.4277570247650146 28 4.456871509552002
		 34 -0.81270742416381836 39 -1.4382439851760864 49 3.8294122219085693 56 3.23337721824646
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.98786634206771851 1 1 0.99432414770126343 
		0.94717663526535034;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 -0.15530687570571899 0 0 -0.10639266669750214 
		-0.32071247696876526;
	setAttr -s 8 ".kox[0:7]"  0.9691692590713501 1 1 0.98786634206771851 
		1 1 0.99432414770126343 1;
	setAttr -s 8 ".koy[0:7]"  -0.24639590084552765 0 0 -0.15530687570571899 
		0 0 -0.10639266669750214 0;
createNode animCurveTA -n "animCurveTA168";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 10 -0.69426524639129639 28 -3.8488700389862061
		 34 -1.0392343997955322 39 -1.259474515914917 49 -2.2857723236083984 56 -2.606651782989502
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.98765188455581665 1 1 0.99952840805053711 
		0.99834495782852173 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 -0.15666443109512329 0 0 -0.030705641955137253 
		-0.057508781552314758 0 0;
	setAttr -s 8 ".kox[0:7]"  1 0.98765188455581665 1 1 0.99952840805053711 
		0.99834495782852173 1 1;
	setAttr -s 8 ".koy[0:7]"  0 -0.15666443109512329 0 0 -0.030705641955137253 
		-0.057508781552314758 0 0;
createNode animCurveTL -n "animCurveTL166";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -4.7867727279663086 60 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL167";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 32.754745483398438 60 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL168";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 56.147525787353516 60 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU169";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU170";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU171";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA169";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA170";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA171";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL169";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 40.544437408447266 60 40.544437408447266;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL170";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 43.055271148681641 60 43.055271148681641;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL171";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU172";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU173";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU174";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA172";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA173";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA174";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL172";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 8.0282459259033203 60 8.0282459259033203;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL173";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 9.9087905883789063 60 9.9087905883789063;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL174";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU175";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU176";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU177";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA175";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 9 -77.568130493164063 10 -80.175880432128906
		 12 48.127605438232422 13 50.777908325195313 15 -80.591926574707031 16 -87.989875793457031
		 18 6.2896389961242676 28 29.859518051147464 30 -81.106491088867187 31 -83.598381042480469
		 33 24.800180435180664 34 27.467361450195313 36 -81.106491088867187 39 -83.598381042480469
		 46 -15.535813331604004 60 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.60926169157028198 1;
	setAttr -s 17 ".kiy[0:16]"  0 -0.95645803213119507 0 0.95775818824768066 
		0 -0.99426442384719849 0 0.94745677709579468 0 -0.95260506868362427 0 0.95825827121734619 
		0 -0.72209542989730835 0 0.7929692268371582 0;
	setAttr -s 17 ".kox[0:16]"  1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.60926169157028198 1;
	setAttr -s 17 ".koy[0:16]"  0 -0.95645803213119507 0 0.95775818824768066 
		0 -0.99426442384719849 0 0.94745677709579468 0 -0.95260506868362427 0 0.95825827121734619 
		0 -0.72209542989730835 0 0.7929692268371582 0;
createNode animCurveTA -n "animCurveTA176";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 9 24.755352020263672 10 25.994792938232422
		 12 -19.358005523681641 13 -20.762310028076172 15 36.406421661376953 16 33.905223846435547
		 18 2.2449593544006348 28 -5.6701068878173828 30 31.235822677612305 31 30.325757980346683
		 33 -7.809575080871582 34 -9.9204740524291992 36 31.235822677612305 39 30.325757980346683
		 46 7.8860201835632324 60 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
	setAttr -s 17 ".kox[0:16]"  1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 17 ".koy[0:16]"  0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
createNode animCurveTA -n "animCurveTA177";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 9 32.980312347412109 10 34.735298156738281
		 12 31.418289184570312 13 33.537071228027344 15 28.683307647705078 16 35.122848510742188
		 18 20.297872543334961 28 16.59162712097168 30 50.610061645507813 31 51.810520172119141
		 33 26.127241134643555 34 26.807085037231445 36 50.610061645507813 39 51.810520172119141
		 46 7.9200544357299805 60 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 
		1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 1 0.83326274156570435 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 
		0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 0 -0.55287718772888184 
		0;
	setAttr -s 17 ".kox[0:16]"  1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 
		1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 1 0.83326274156570435 
		1;
	setAttr -s 17 ".koy[0:16]"  0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 
		0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 0 -0.55287718772888184 
		0;
createNode animCurveTL -n "animCurveTL175";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -19.597047805786133 60 -19.597047805786133;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL176";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 36.545459747314453 60 36.545459747314453;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL177";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 39.212558746337891 60 39.212558746337891;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU178";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU179";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU180";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA178";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 11 -1.4152520895004272 16 -0.24558219313621518
		 26 -0.088783964514732361 32 -1.91673743724823 45 0.50768536329269409 54 0.30051490664482117
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes yes;
	setAttr -s 8 ".kix[0:7]"  0.76478725671768188 1 0.99980592727661133 
		1 1 1 0.99982380867004395 0.99976927042007446;
	setAttr -s 8 ".kiy[0:7]"  0.64428287744522095 0 0.019700018689036369 
		0 0 0 -0.018770208582282066 -0.021483592689037323;
	setAttr -s 8 ".kox[0:7]"  0.99304461479187012 1 0.99980592727661133 
		1 1 1 0.99982380867004395 0.99976927042007446;
	setAttr -s 8 ".koy[0:7]"  -0.11773920059204102 0 0.019700018689036369 
		0 0 0 -0.018770208582282066 -0.021483587101101875;
createNode animCurveTA -n "animCurveTA179";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 11 0.29329201579093933 16 0.50766146183013916
		 26 -0.15926961600780487 32 -1.1446186304092407 45 0.11611964553594589 54 0.22753418982028961
		 60 0;
	setAttr -s 8 ".ktl[7]" no;
	setAttr -s 8 ".kix[0:7]"  1 0.99924492835998535 1 0.9965064525604248 
		1 0.99988621473312378 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.038852039724588394 0 -0.083516240119934082 
		0 0.015083270147442818 0 0;
	setAttr -s 8 ".kox[0:7]"  1 0.99924492835998535 1 0.9965064525604248 
		1 0.99988621473312378 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0.038852039724588394 0 -0.083516240119934082 
		0 0.015083270147442818 0 0;
createNode animCurveTA -n "animCurveTA180";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 11 -3.7771325111389165 16 0.9922214150428772
		 26 2.785752534866333 32 -3.8511304855346684 45 3.0445451736450195 54 2.1559481620788574
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes yes;
	setAttr -s 8 ".kix[0:7]"  0.31706118583679199 1 0.97552996873855591 
		1 1 1 0.99239087104797363 0.98553097248077393;
	setAttr -s 8 ".kiy[0:7]"  0.94840502738952637 0 0.2198665589094162 
		0 0 0 -0.12312744557857513 -0.16949521005153656;
	setAttr -s 8 ".kox[0:7]"  0.95340651273727417 1 0.97552996873855591 
		1 1 1 0.99239087104797363 0.98553097248077393;
	setAttr -s 8 ".koy[0:7]"  -0.30168861150741577 0 0.2198665589094162 
		0 0 0 -0.12312744557857513 -0.16949522495269775;
createNode animCurveTL -n "animCurveTL178";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 51.6451416015625 60 51.6451416015625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL179";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -11.264523506164551 60 -11.264523506164551;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL180";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU181";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 1 14 1 17 1 27 1 45 1 60 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU182";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 1 14 1 17 1 27 1 45 1 60 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU183";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 1 14 1 17 1 27 1 45 1 60 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA181";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 8 77.568130493164063 9 80.175880432128906
		 11 -48.127605438232422 12 -50.777908325195313 14 80.591926574707031 15 87.989875793457031
		 17 -6.2896385192871094 27 -29.859518051147464 29 81.106491088867187 30 83.598381042480469
		 32 -24.800180435180664 33 -27.467361450195313 35 81.106491088867187 38 83.598381042480469
		 45 15.535812377929689 60 0;
	setAttr -s 17 ".ktl[16]" no;
	setAttr -s 17 ".kix[0:16]"  1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.72124814987182617 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.95645803213119507 0 -0.95775818824768066 
		0 0.99426442384719849 0 -0.94745677709579468 0 0.95260506868362427 0 -0.95825827121734619 
		0 0.72209542989730835 0 -0.69267678260803223 0;
	setAttr -s 17 ".kox[0:16]"  1 0.29186975955963135 1 0.28757494688034058 
		1 0.10695000737905502 1 0.31988361477851868 1 0.30421003699302673 1 0.28590410947799683 
		1 0.69179350137710571 1 0.72124814987182617 1;
	setAttr -s 17 ".koy[0:16]"  0 0.95645803213119507 0 -0.95775818824768066 
		0 0.99426442384719849 0 -0.94745677709579468 0 0.95260506868362427 0 -0.95825827121734619 
		0 0.72209542989730835 0 -0.69267678260803223 0;
createNode animCurveTA -n "animCurveTA182";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 8 24.755352020263672 9 25.994792938232422
		 11 -19.358005523681641 12 -20.762310028076172 14 36.406421661376953 15 33.905223846435547
		 17 2.2449593544006348 27 -5.6701068878173828 29 31.235822677612305 30 30.325757980346683
		 32 -7.809575080871582 33 -9.9204740524291992 35 31.235822677612305 38 30.325757980346683
		 45 7.8860201835632324 60 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 17 ".kiy[0:16]"  0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
	setAttr -s 17 ".kox[0:16]"  1 0.54027318954467773 1 0.4930138885974884 
		1 1 0.30318260192871094 0.70900559425354004 1 1 0.65825557708740234 0.35275033116340637 
		1 1 0.93440860509872437 0.83435708284378052 1;
	setAttr -s 17 ".koy[0:16]"  0 0.84148973226547241 0 -0.87002146244049072 
		0 0 -0.95293247699737549 -0.70520275831222534 0 0 -0.75279450416564941 -0.93571746349334717 
		0 0 -0.35620281100273132 -0.55122429132461548 0;
createNode animCurveTA -n "animCurveTA183";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  6 0 8 32.980312347412109 9 34.735298156738281
		 11 31.418289184570312 12 33.537071228027344 14 28.683307647705078 15 35.122848510742188
		 17 20.297872543334961 27 16.59162712097168 29 50.610061645507813 30 51.810520172119141
		 32 26.127241134643555 33 26.807085037231445 35 50.610061645507813 38 51.810520172119141
		 45 7.9200544357299805 60 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 
		1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 1 0.86371892690658569 
		1;
	setAttr -s 17 ".kiy[0:16]"  0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 
		0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 0 -0.50397384166717529 
		0;
	setAttr -s 17 ".kox[0:16]"  1 0.41296577453613281 1 1 1 1 1 0.90650463104248047 
		1 0.55252093076705933 1 1 0.76031714677810669 0.89340776205062866 1 0.86371892690658569 
		1;
	setAttr -s 17 ".koy[0:16]"  0 0.91074657440185547 0 0 0 0 0 -0.42219585180282593 
		0 0.83349913358688354 0 0 0.64955204725265503 0.44924667477607727 0 -0.50397384166717529 
		0;
createNode animCurveTL -n "animCurveTL181";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 -19.609930038452148 14 -19.609930038452148
		 17 -19.609930038452148 27 -19.609930038452148 45 -19.609930038452148 60 -19.609930038452148;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL182";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 36.542636871337891 14 36.542636871337891
		 17 36.542636871337891 27 36.542636871337891 45 36.542636871337891 60 36.542636871337891;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL183";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  6 -39.208473205566406 14 -39.208473205566406
		 17 -39.208473205566406 27 -39.208473205566406 45 -39.208473205566406 60 -39.208473205566406;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU184";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA184";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL184";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -4.502251148223877 60 -4.502251148223877;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 37.76336669921875 60 37.76336669921875;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 1 60 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 11 -0.13593044877052307 17 0.1689608246088028
		 24 -0.25842064619064331 33 -0.25360524654388428 45 -0.81208699941635132 51 -0.74729651212692261
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 0.99990791082382202 0.99904173612594604;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0.01356844324618578 0.043768759816884995;
	setAttr -s 8 ".kox[0:7]"  0.99993515014648438 1 1 1 1 1 0.99990791082382202 
		0.99904173612594604;
	setAttr -s 8 ".koy[0:7]"  -0.01138694304972887 0 0 0 0 0 0.01356844324618578 
		0.043768759816884995;
createNode animCurveTA -n "animCurveTA188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 11 -1.1088451147079468 17 -0.24560017883777618
		 24 0.73505294322967529 33 -0.3662811815738678 45 1.437603235244751 51 1.1907507181167603
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 0.99480611085891724 1 1 1 0.99866622686386108 
		0.99849241971969604;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.1017879918217659 0 0 0 -0.05163172259926796 
		-0.05488971620798111;
	setAttr -s 8 ".kox[0:7]"  0.9957129955291748 1 0.99480611085891724 
		1 1 1 0.99866622686386108 0.99849241971969604;
	setAttr -s 8 ".koy[0:7]"  -0.092496141791343689 0 0.1017879918217659 
		0 0 0 -0.05163172259926796 -0.054889723658561707;
createNode animCurveTA -n "animCurveTA189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  6 0 11 -2.2276911735534668 17 -1.4962397813796997
		 24 -0.20659753680229187 29 -1.9135464429855344 33 -4.38568115234375 45 -1.7283238172531128
		 51 1.1924782991409302 60 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 0.9884682297706604 1 0.93669140338897705 
		1 0.96339350938796997 1 0.99141174554824829;
	setAttr -s 9 ".kiy[0:8]"  0 0 0.15142825245857239 0 -0.35015606880187988 
		0 0.26809108257293701 0 -0.13077715039253235;
	setAttr -s 9 ".kox[0:8]"  0.98302739858627319 1 0.9884682297706604 
		1 0.93669140338897705 1 0.96339350938796997 1 0.99141174554824829;
	setAttr -s 9 ".koy[0:8]"  -0.18345907330513 0 0.15142825245857239 
		0 -0.35015606880187988 0 0.26809108257293701 0 -0.13077718019485474;
createNode animCurveTL -n "animCurveTL187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 -1.4725730419158936 60 -1.4725730419158936;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 22.299345016479492 60 22.299345016479492;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  6 0 60 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  6 1 31 1 39 1 60 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  6 1 31 1 39 1 60 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  6 1 31 1 39 1 60 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 12 0.29277992248535156 23 -0.063073001801967621
		 31 0.16941116750240326 39 0.24168832600116733 44 -0.077064946293830872 50 -0.36784809827804565
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.99988549947738647 1 0.99918627738952637 
		1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0.015135959722101688 0 -0.04033394530415535 
		0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 0.99988549947738647 1 0.99918627738952637 
		1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0.015135959722101688 0 -0.04033394530415535 
		0 0;
createNode animCurveTA -n "animCurveTA191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 12 0.021117612719535828 23 0.089085608720779419
		 31 0.32429292798042297 39 -0.32775837182998657 44 0.028010895475745205 50 0.28513193130493164
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.99999797344207764 0.99998021125793457 
		1 1 0.99874323606491089 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.0020312471315264702 0.0062902341596782207 
		0 0 0.05011972039937973 0 0;
	setAttr -s 8 ".kox[0:7]"  1 0.99999797344207764 0.99998021125793457 
		1 1 0.99874323606491089 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0.0020312471315264702 0.0062902341596782207 
		0 0 0.05011972039937973 0 0;
createNode animCurveTA -n "animCurveTA192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  6 0 12 -2.3035626411437988 23 0.80299609899520874
		 31 1.8759468793869021 39 -2.232053279876709 44 -0.48707431554794306 50 1.7187179327011108
		 60 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.99924224615097046 1 1 0.95742601156234741 
		1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.038922861218452454 0 0 0.28867864608764648 
		0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.99924224615097046 1 1 0.95742601156234741 
		1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0.038922861218452454 0 0 0.28867864608764648 
		0 0;
createNode animCurveTL -n "animCurveTL190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  6 0 31 0 39 0 60 0;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  6 0 12 13.693543434143066 16 17.586925506591797
		 31 10.791976928710938 39 24.317514419555664 44 26.571771621704102 60 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 0.016391491517424583 1 1 0.030791314318776131 
		1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.99986565113067627 0 0 0.99952590465545654 
		0 0;
	setAttr -s 7 ".kox[0:6]"  1 0.016391491517424583 1 1 0.030791314318776131 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0 0.99986565113067627 0 0 0.99952590465545654 
		0 0;
createNode animCurveTL -n "animCurveTL192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  6 0 31 0 39 0 60 0;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 43 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 43 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  6 1 43 1 60 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  6 0 11 3.8731269836425781 15 -0.57336306571960449
		 20 1.7815185785293579 26 -1.0614374876022339 43 -0.70235580205917358 60 0;
	setAttr -s 7 ".kix[0:6]"  0.99658316373825073 1 1 1 1 0.99985378980636597 
		0.99978101253509521;
	setAttr -s 7 ".kiy[0:6]"  0.082595124840736389 0 0 0 0 0.017097815871238708 
		0.020927129313349724;
	setAttr -s 7 ".kox[0:6]"  0.99658322334289551 1 1 1 1 0.99985378980636597 
		0.99978101253509521;
	setAttr -s 7 ".koy[0:6]"  0.082595065236091614 0 0 0 0 0.017097821459174156 
		0.020927134901285172;
createNode animCurveTA -n "animCurveTA194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  6 0 11 32.419986724853516 15 2.4428699016571045
		 20 28.041593551635742 26 7.0878419876098633 43 2.1343567371368408 60 0;
	setAttr -s 7 ".ktl[6]" no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.98778176307678223 0.99861997365951538 
		1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.15584354102611542 -0.052517849951982498 
		0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 0.98778176307678223 0.99861997365951538 
		1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 -0.15584354102611542 -0.052517849951982498 
		0;
createNode animCurveTA -n "animCurveTA195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  6 0 11 -14.58132266998291 15 -18.651880264282227
		 20 -11.47330379486084 26 -6.8645796775817871 31 -10.902566909790039 36 -18.439697265625
		 44.908 -4.6132702827453613 50 7.0690317153930664 60 0;
	setAttr -s 10 ".kix[0:9]"  0.15576204657554626 0.61600160598754883 
		1 0.74941354990005493 1 0.70187371969223022 1 0.49949237704277039 1 0.90229082107543945;
	setAttr -s 10 ".kiy[0:9]"  0.98779463768005371 -0.78774487972259521 
		0 0.66210216283798218 0 -0.71230137348175049 0 0.86631834506988525 0 -0.43112790584564209;
	setAttr -s 10 ".kox[0:9]"  0.99924111366271973 0.61600160598754883 
		1 0.74941354990005493 1 0.70187371969223022 1 0.49949237704277039 1 0.90229082107543945;
	setAttr -s 10 ".koy[0:9]"  0.03895099088549614 -0.78774487972259521 
		0 0.66210216283798218 0 -0.71230137348175049 0 0.86631834506988525 0 -0.43112790584564209;
createNode animCurveTL -n "animCurveTL193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  6 -54.946929931640625 11 -46.847499847412109
		 15 -47.386089324951172 26 -52.141513824462891 31 -52.637279510498047 36 -52.271835327148438
		 43 -54.989700317382813 47 -56.458881378173828 60 -54.946929931640625;
	setAttr -s 9 ".kix[0:8]"  0.15536434948444366 1 0.1026056706905365 
		0.15490682423114777 1 1 0.067703187465667725 1 0.22282746434211731;
	setAttr -s 9 ".kiy[0:8]"  -0.98785722255706787 0 -0.99472206830978394 
		-0.98792910575866699 0 0 -0.99770551919937134 0 0.9748578667640686;
	setAttr -s 9 ".kox[0:8]"  0.09438672661781311 1 0.1026056706905365 
		0.15490682423114777 1 1 0.067703187465667725 1 0.22282752394676208;
	setAttr -s 9 ".koy[0:8]"  0.99553561210632324 0 -0.99472206830978394 
		-0.98792910575866699 0 0 -0.99770551919937134 0 0.9748578667640686;
createNode animCurveTL -n "animCurveTL194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  6 2.3482637405395508 11 1.2566131353378296
		 15 4.680389404296875 26 9.185154914855957 31 5.681556224822998 36 9.811004638671875
		 43 7.6069731712341309 47 3.1013684272766113 60 2.3482637405395508;
	setAttr -s 9 ".kix[0:8]"  0.25632256269454956 1 0.035322710871696472 
		1 1 1 0.044068239629268646 0.46345451474189758 0.70931452512741089;
	setAttr -s 9 ".kiy[0:8]"  -0.9665912389755249 0 0.99937587976455688 
		0 0 0 -0.99902850389480591 -0.88612073659896851 -0.70489215850830078;
	setAttr -s 9 ".kox[0:8]"  0.25632259249687195 1 0.035322710871696472 
		1 1 1 0.044068239629268646 0.46345451474189758 0.70931452512741089;
	setAttr -s 9 ".koy[0:8]"  -0.9665912389755249 0 0.99937587976455688 
		0 0 0 -0.99902850389480591 -0.88612073659896851 -0.70489215850830078;
createNode animCurveTL -n "animCurveTL195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  6 0 11 -0.59498012065887451 15 -1.0637115240097046
		 26 -1.3494876623153687 31 -0.73749023675918579 36 1.1823307275772095 43 -0.067849248647689819
		 47 -1.4310612678527832 60 0;
	setAttr -s 9 ".ktl[8]" no;
	setAttr -s 9 ".kix[0:8]"  1 0.23721055686473846 0.50306844711303711 
		1 0.11274827271699905 1 0.080945320427417755 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 -0.97145825624465942 -0.86424654722213745 
		0 0.99362361431121826 0 -0.99671852588653564 0 0;
	setAttr -s 9 ".kox[0:8]"  1 0.23721055686473846 0.50306844711303711 
		1 0.11274827271699905 1 0.080945320427417755 1 1;
	setAttr -s 9 ".koy[0:8]"  0 -0.97145825624465942 -0.86424654722213745 
		0 0.99362361431121826 0 -0.99671852588653564 0 0;
select -ne :time1;
	setAttr ".o" 60;
	setAttr ".unw" 60;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "idleSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU157.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU158.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU159.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA157.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA158.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA159.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL157.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL158.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL159.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU160.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU161.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU162.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA160.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA161.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA162.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL160.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL161.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL162.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU163.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU164.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU165.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA163.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA164.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA165.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL163.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL164.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL165.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU166.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU167.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU168.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA166.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA167.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA168.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL166.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL167.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL168.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU169.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU170.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU171.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA169.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA170.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA171.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL169.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL170.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL171.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU172.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU173.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU174.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA172.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA173.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA174.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL172.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL173.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL174.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU175.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU176.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU177.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA175.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA176.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA177.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL175.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL176.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL177.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU178.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU179.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU180.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA178.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA179.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA180.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL178.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL179.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL180.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU181.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU182.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU183.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA181.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA182.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA183.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL181.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL182.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL183.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU184.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU185.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU186.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA184.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA185.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA186.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL184.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL185.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL186.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU187.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU188.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU189.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA187.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA188.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA189.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL187.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL188.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL189.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU190.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU191.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU192.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA190.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA191.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA192.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL190.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL191.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL192.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU193.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU194.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU195.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA193.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA194.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA195.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL193.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL194.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL195.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_idle.ma
