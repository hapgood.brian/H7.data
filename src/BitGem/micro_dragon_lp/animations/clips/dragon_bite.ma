//Maya ASCII 2013 scene
//Name: dragon_bite.ma
//Last modified: Mon, Jul 14, 2014 12:38:47 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "biteSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 61;
	setAttr ".se" 80;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU313";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU314";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU315";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA313";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA314";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA315";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL313";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 3.2171449661254883 80 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL314";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -26.658763885498047 80 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL315";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -1.5793838500976563 80 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU316";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU317";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU318";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA316";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 -14.552690505981444 67 -9.3637275695800781
		 75 6.2991905212402344 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 0.52289247512817383 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0.85239869356155396 0 0;
	setAttr -s 5 ".kox[0:4]"  0.31174331903457642 1 0.52289247512817383 
		1 1;
	setAttr -s 5 ".koy[0:4]"  -0.95016634464263916 0 0.85239869356155396 
		0 0;
createNode animCurveTA -n "animCurveTA317";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 -15.798990249633791 67 -8.1717729568481445
		 75 2.8877105712890625 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 0.49888303875923157 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0.8666694164276123 0 0;
	setAttr -s 5 ".kox[0:4]"  0.28928989171981812 1 0.49888303875923157 
		1 1;
	setAttr -s 5 ".koy[0:4]"  -0.95724153518676758 0 0.8666694164276123 
		0 0;
createNode animCurveTA -n "animCurveTA318";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 44.818260192871094 67 -25.893665313720703
		 75 -9.8472490310668945 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.37463301420211792 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.9271731972694397 0;
	setAttr -s 5 ".kox[0:4]"  0.10593409091234207 1 1 0.37463301420211792 
		1;
	setAttr -s 5 ".koy[0:4]"  0.9943731427192688 0 0 0.9271731972694397 
		0;
createNode animCurveTL -n "animCurveTL316";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -4.7867727279663086 80 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL317";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 32.754745483398438 80 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL318";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -56.147525787353516 80 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU319";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU320";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU321";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA319";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA320";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA321";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL319";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 3.2171449661254883 80 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL320";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -26.658763885498047 80 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL321";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1.5793838500976563 80 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU322";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU323";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU324";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA322";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 1.1756051778793335 67 10.647705078125
		 75 -4.0284647941589355 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.80435913801193237 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0.5941433310508728 0 0 0;
	setAttr -s 5 ".kox[0:4]"  0.97100037336349487 0.80435913801193237 
		1 1 1;
	setAttr -s 5 ".koy[0:4]"  0.23907816410064697 0.5941433310508728 
		0 0 0;
createNode animCurveTA -n "animCurveTA323";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 8.5887165069580078 67 -5.2819881439208984
		 75 -3.0007865428924561 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.94138729572296143 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.33732759952545166 0;
	setAttr -s 5 ".kox[0:4]"  0.48588675260543823 1 1 0.94138729572296143 
		1;
	setAttr -s 5 ".koy[0:4]"  0.87402182817459106 0 0 0.33732759952545166 
		0;
createNode animCurveTA -n "animCurveTA324";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 25.011196136474609 67 -29.146938323974613
		 75 -9.3057479858398437 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.39314261078834534 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.91947752237319946 0;
	setAttr -s 5 ".kox[0:4]"  0.18751412630081177 1 1 0.39314261078834534 
		1;
	setAttr -s 5 ".koy[0:4]"  0.98226183652877808 0 0 0.91947752237319946 
		0;
createNode animCurveTL -n "animCurveTL322";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -4.7867727279663086 80 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL323";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 32.754745483398438 80 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL324";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 56.147525787353516 80 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU325";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU326";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU327";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA325";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA326";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA327";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL325";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 40.544437408447266 80 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL326";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 43.055271148681641 80 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL327";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU328";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU329";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU330";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA328";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA329";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA330";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL328";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 8.0282459259033203 80 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL329";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 9.9087905883789063 80 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL330";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU331";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 1 68 1 70 1 71 1 73 1 74 1 76 1 77 1
		 80 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU332";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 1 68 1 70 1 71 1 73 1 74 1 76 1 77 1
		 80 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU333";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 1 68 1 70 1 71 1 73 1 74 1 76 1 77 1
		 80 1;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA331";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 -80.043121337890625 68 8.0542812347412109
		 70 -88.699951171875 71 -94.526527404785156 73 8.0542812347412109 74 14.682226181030275
		 76 -88.699951171875 77 -94.526527404785156 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 1 0.13532054424285889 1 0.11920743435621262 
		1 0.13532054424285889 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 -0.99080193042755127 0 0.99286937713623047 
		0 -0.99080193042755127 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 0.13532054424285889 1 0.11920743435621262 
		1 0.13532054424285889 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 -0.99080193042755127 0 0.99286937713623047 
		0 -0.99080193042755127 0 0;
createNode animCurveTA -n "animCurveTA332";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 26.761899948120117 68 23.045242309570313
		 70 -18.751214981079102 71 -17.428119659423828 73 23.045242309570313 74 17.496448516845703
		 76 -18.751214981079102 77 -17.428119659423828 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.73077648878097534 1 0.51540881395339966 
		1 0.14196144044399261 1 0.51540881395339966 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 -0.68261688947677612 0 0.8569445013999939 
		0 -0.98987221717834473 0 0.8569445013999939 0;
	setAttr -s 10 ".kox[0:9]"  1 1 0.73077648878097534 1 0.51540881395339966 
		1 0.14196144044399261 1 0.51540881395339966 1;
	setAttr -s 10 ".koy[0:9]"  0 0 -0.68261688947677612 0 0.8569445013999939 
		0 -0.98987221717834473 0 0.8569445013999939 0;
createNode animCurveTA -n "animCurveTA333";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 2.6492002010345459 68 79.238670349121094
		 70 49.735687255859375 71 49.643165588378906 73 79.238670349121094 74 78.104606628417969
		 76 49.735687255859375 77 49.643165588378906 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 0.51497858762741089 1 0.99330872297286987 
		1 1 0.57439696788787842 0.99330872297286987 0.99330872297286987 1;
	setAttr -s 10 ".kiy[0:9]"  0 0.85720312595367432 0 -0.11548865586519241 
		0 0 -0.8185768723487854 -0.11548865586519241 -0.11548865586519241 0;
	setAttr -s 10 ".kox[0:9]"  1 0.51497858762741089 1 0.99330872297286987 
		1 1 0.57439696788787842 0.99330872297286987 0.99330872297286987 1;
	setAttr -s 10 ".koy[0:9]"  0 0.85720312595367432 0 -0.11548865586519241 
		0 0 -0.8185768723487854 -0.11548865586519241 -0.11548865586519241 0;
createNode animCurveTL -n "animCurveTL331";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 -19.597047805786133 68 -19.597047805786133
		 70 -19.597047805786133 71 -19.597047805786133 73 -19.597047805786133 74 -19.597047805786133
		 76 -19.597047805786133 77 -19.597047805786133 80 -19.597047805786133;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL332";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 36.545459747314453 68 36.545459747314453
		 70 36.545459747314453 71 36.545459747314453 73 36.545459747314453 74 36.545459747314453
		 76 36.545459747314453 77 36.545459747314453 80 36.545459747314453;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL333";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  61 39.212558746337891 68 39.212558746337891
		 70 39.212558746337891 71 39.212558746337891 73 39.212558746337891 74 39.212558746337891
		 76 39.212558746337891 77 39.212558746337891 80 39.212558746337891;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU334";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU335";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU336";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA334";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 62 -0.92624503374099743 64 -0.77915763854980469
		 68 -2.2666542530059814 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA335";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 62 -0.97197812795639038 64 -2.1859660148620605
		 68 5.6063799858093262 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.81921190023422241 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.57349085807800293 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 0.81921190023422241 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 -0.57349085807800293 0 0 0;
createNode animCurveTA -n "animCurveTA336";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 62 7.7344050407409668 64 -44.502704620361328
		 68 -5.0687093734741211 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 0.88328260183334351 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0.46884104609489441 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 0.88328260183334351 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0.46884104609489441 0;
createNode animCurveTL -n "animCurveTL334";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 51.6451416015625 80 51.6451416015625;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL335";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -11.264523506164551 80 -11.264523506164551;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL336";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU337";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 1 63 1 68 1 70 1 71 1 73 1 74 1 76 1
		 77 1 80 1;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU338";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 1 63 1 68 1 70 1 71 1 73 1 74 1 76 1
		 77 1 80 1;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU339";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 1 63 1 68 1 70 1 71 1 73 1 74 1 76 1
		 77 1 80 1;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA337";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 85.5325927734375 68 -3.3403100967407227
		 70 76.521369934082031 71 76.521369934082031 73 -3.3403100967407227 74 -10.072053909301758
		 76 76.521369934082031 77 76.521369934082031 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes no no yes yes no no no;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 0.11739485710859299 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 -0.99308532476425171 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 0.11739485710859299 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 -0.99308532476425171 0 0 0 0;
createNode animCurveTA -n "animCurveTA338";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 -26.715377807617187 68 10.852347373962402
		 70 17.654531478881836 71 17.654531478881836 73 10.852347373962402 74 12.505141258239746
		 76 17.654531478881836 77 17.654531478881836 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes no no yes yes no no no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.22782336175441742 1 1 1 0.51052629947662354 
		1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0.97370243072509766 0 0 0 0.85986214876174927 
		0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 0.22782336175441742 1 1 1 0.51052629947662354 
		1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0.97370243072509766 0 0 0 0.85986214876174927 
		0 0 0;
createNode animCurveTA -n "animCurveTA339";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 0 63 0.39031988382339478 68 65.929977416992188
		 70 67.973304748535156 71 67.973304748535156 73 65.929977416992188 74 62.850906372070313
		 76 67.973304748535156 77 67.973304748535156 80 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes no no yes yes no no no;
	setAttr -s 10 ".kix[0:9]"  1 0.97121930122375488 0.61449247598648071 
		1 1 0.61449247598648071 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0.23818688094615936 0.78892272710800171 
		0 0 -0.78892272710800171 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 0.97121930122375488 0.61449247598648071 
		1 1 0.61449247598648071 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0.23818688094615936 0.78892272710800171 
		0 0 -0.78892272710800171 0 0 0 0;
createNode animCurveTL -n "animCurveTL337";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 -19.609930038452148 63 -19.609930038452148
		 68 -19.609930038452148 70 -19.609930038452148 71 -19.609930038452148 73 -19.609930038452148
		 74 -19.609930038452148 76 -19.609930038452148 77 -19.609930038452148 80 -19.609930038452148;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL338";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 36.542636871337891 63 36.542636871337891
		 68 36.542636871337891 70 36.542636871337891 71 36.542636871337891 73 36.542636871337891
		 74 36.542636871337891 76 36.542636871337891 77 36.542636871337891 80 36.542636871337891;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL339";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  61 -39.208473205566406 63 -39.208473205566406
		 68 -39.208473205566406 70 -39.208473205566406 71 -39.208473205566406 73 -39.208473205566406
		 74 -39.208473205566406 76 -39.208473205566406 77 -39.208473205566406 80 -39.208473205566406;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU340";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  61 1 66 1 69 1 80 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  61 1 66 1 69 1 80 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  61 1 66 1 69 1 80 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA340";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  61 0 63 -6.0033698081970215 64 -7.4241185188293466
		 66 -5.6832795143127441 69 -3.4084749221801758 80 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.55972796678543091 1 0.91115331649780273 
		0.98375064134597778 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.82867640256881714 0 0.41206759214401245 
		0.17954008281230927 0;
	setAttr -s 6 ".kox[0:5]"  0.62246263027191162 0.55972796678543091 
		1 0.91115331649780273 0.98375064134597778 1;
	setAttr -s 6 ".koy[0:5]"  -0.78264951705932617 -0.82867640256881714 
		0 0.41206759214401245 0.17954008281230927 0;
createNode animCurveTA -n "animCurveTA341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  61 0 63 0.72100627422332764 64 0.064429111778736115
		 66 -2.1434977054595947 69 -2.9215860366821289 80 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 0.871135413646698 0.96156960725784302 
		1 0.98614346981048584;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.49104288220405579 -0.27456116676330566 
		0 0.16589458286762238;
	setAttr -s 6 ".kox[0:5]"  0.98878979682922363 1 0.871135413646698 
		0.96156960725784302 1 0.98614346981048584;
	setAttr -s 6 ".koy[0:5]"  0.1493145227432251 0 -0.49104288220405579 
		-0.27456116676330566 0 0.16589465737342834;
createNode animCurveTA -n "animCurveTA342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  61 0 63 -28.952939987182614 64 -26.441411972045898
		 66 4.3605389595031738 69 5.801365852355957 80 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 0.30204936861991882 0.85615581274032593 
		1 0.86207407712936401;
	setAttr -s 6 ".kiy[0:5]"  0 0 0.95329231023788452 0.51671767234802246 
		0 -0.5067821741104126;
	setAttr -s 6 ".kox[0:5]"  0.16271287202835083 1 0.30204936861991882 
		0.85615581274032593 1 0.86207425594329834;
	setAttr -s 6 ".koy[0:5]"  -0.98667347431182861 0 0.95329231023788452 
		0.51671767234802246 0 -0.50678199529647827;
createNode animCurveTL -n "animCurveTL340";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  61 -4.502251148223877 63 5.6482486724853516
		 64 2.5126373767852783 65 9.2903833389282227 66 6.4029860496520996 69 -6.6977443695068359
		 80 -4.502251148223877;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.0057989703491330147 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.99998319149017334 0 0;
	setAttr -s 7 ".kox[0:6]"  0.0082094939425587654 1 1 1 0.0057989703491330147 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0.99996626377105713 0 0 0 -0.99998319149017334 
		0 0;
createNode animCurveTL -n "animCurveTL341";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  61 37.76336669921875 63 41.89251708984375
		 64 41.02386474609375 65 44.723323822021484 66 42.937530517578125 69 36.770278930664063
		 80 37.76336669921875;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.010882384143769741 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.99994081258773804 0 0;
	setAttr -s 7 ".kox[0:6]"  0.020177584141492844 1 1 1 0.010882384143769741 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0.99979645013809204 0 0 0 -0.99994081258773804 
		0 0;
createNode animCurveTL -n "animCurveTL342";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  61 0 63 1.8275718688964844 64 1.616051197052002
		 65 3.2285187244415283 66 2.6150286197662354 69 -0.5034019947052002 80 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.025179162621498108 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 -0.9996829628944397 0 0;
	setAttr -s 7 ".kox[0:6]"  0.04555046558380127 1 1 1 0.025179162621498108 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0.99896204471588135 0 0 0 -0.9996829628944397 
		0 0;
createNode animCurveTU -n "animCurveTU343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL343";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -1.4725730419158936 80 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL344";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 22.299345016479492 80 22.299345016479492;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL345";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 1 74 1 80 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 1 74 1 80 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 1 74 1 80 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 0 74 0 80 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 0 74 0 80 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  61 0 62 -6.7621951103210449 63 -0.10713867843151093
		 64 24.928863525390625 67 29.138395309448242 74 2.4281995296478271 80 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 0.11872877180576324 0.49331352114677429 
		1 0.89135408401489258 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0.99292677640914917 0.86985158920288086 
		0 -0.45330777764320374 0;
	setAttr -s 7 ".kox[0:6]"  0.33290168642997742 1 0.11872877180576324 
		0.49331352114677429 1 0.89135408401489258 1;
	setAttr -s 7 ".koy[0:6]"  -0.94296163320541382 0 0.99292677640914917 
		0.86985158920288086 0 -0.45330777764320374 0;
createNode animCurveTL -n "animCurveTL346";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 64 31.808847427368164 67 37.231517791748047
		 74 2.70853590965271 80 0;
	setAttr -s 5 ".ktl[4]" no;
	setAttr -s 5 ".kix[0:4]"  1 0.0076835630461573601 1 0.030752375721931458 
		1;
	setAttr -s 5 ".kiy[0:4]"  0 0.99997049570083618 0 -0.9995269775390625 
		0;
	setAttr -s 5 ".kox[0:4]"  0.0039296937175095081 0.0076835630461573601 
		1 0.030752375721931458 1;
	setAttr -s 5 ".koy[0:4]"  0.9999922513961792 0.99997049570083618 
		0 -0.9995269775390625 0;
createNode animCurveTL -n "animCurveTL347";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  61 0 62 15.668669700622559 63 26.189048767089844
		 64 27.974813461303711 71 -4.2606906890869141 74 -2.3389010429382324 80 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 0.0021706586703658104 0.0077773239463567734 
		1 1 0.03560667484998703 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.99999761581420898 0.9999697208404541 
		0 0 0.99936586618423462 0;
	setAttr -s 7 ".kox[0:6]"  0.0026592158246785402 0.0021706586703658104 
		0.0077773239463567734 1 1 0.03560667484998703 1;
	setAttr -s 7 ".koy[0:6]"  0.99999648332595825 0.99999761581420898 
		0.9999697208404541 0 0 0.99936586618423462 0;
createNode animCurveTL -n "animCurveTL348";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  61 0 74 0 80 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 1 80 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  61 0 64 -20.642921447753906 71 0.05793369933962822
		 80 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  61 0 64 22.776775360107422 71 20.744972229003906
		 80 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.9394574761390686 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 -0.34266564249992371 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.9394574761390686 1;
	setAttr -s 4 ".koy[0:3]"  0 0 -0.34266564249992371 0;
createNode animCurveTA -n "animCurveTA351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  61 0 63 -43.262630462646484 64 -69.623542785644531
		 71 12.302217483520508 80 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.061224024742841721 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.99812400341033936 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 0.061224024742841721 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 -0.99812400341033936 0 0 0;
createNode animCurveTL -n "animCurveTL349";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 -54.946929931640625 80 -54.946929931640625;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL350";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 2.3482637405395508 80 2.3482637405395508;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL351";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  61 0 80 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 80;
	setAttr ".unw" 80;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "biteSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU313.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU314.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU315.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA313.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA314.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA315.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL313.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL314.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL315.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU316.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU317.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU318.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA316.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA317.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA318.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL316.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL317.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL318.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU319.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU320.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU321.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA319.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA320.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA321.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL319.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL320.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL321.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU322.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU323.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU324.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA322.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA323.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA324.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL322.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL323.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL324.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU325.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU326.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU327.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA325.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA326.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA327.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL325.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL326.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL327.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU328.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU329.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU330.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA328.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA329.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA330.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL328.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL329.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL330.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU331.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU332.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU333.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA331.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA332.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA333.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL331.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL332.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL333.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU334.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU335.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU336.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA334.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA335.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA336.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL334.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL335.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL336.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU337.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU338.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU339.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA337.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA338.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA339.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL337.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL338.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL339.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU340.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU341.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU342.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA340.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA341.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA342.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL340.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL341.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL342.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU343.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU344.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU345.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA343.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA344.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA345.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL343.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL344.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL345.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU346.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU347.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU348.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA346.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA347.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA348.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL346.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL347.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL348.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU349.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU350.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU351.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA349.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA350.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA351.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL349.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL350.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL351.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_bite.ma
