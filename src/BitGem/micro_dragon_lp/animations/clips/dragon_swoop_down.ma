//Maya ASCII 2013 scene
//Name: dragon_swoop_down.ma
//Last modified: Mon, Jul 14, 2014 01:05:18 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "swoop_downSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 211;
	setAttr ".se" 250;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1093";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1094";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1095";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1093";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1094";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1095";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1093";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 3.2171449661254883 250 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1094";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -26.658763885498047 250 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1095";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -1.5793838500976563 250 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1096";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1097";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1098";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1096";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  211 0 215 -54.374477386474609 226 10.414916038513184
		 235 -13.936113357543945 240 -10.282938957214355 245 17.919708251953125 250 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.73661226034164429 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.6763153076171875 0 0;
	setAttr -s 7 ".kox[0:6]"  0.17297296226024628 1 1 1 0.73661226034164429 
		1 1;
	setAttr -s 7 ".koy[0:6]"  -0.98492664098739624 0 0 0 0.6763153076171875 
		0 0;
createNode animCurveTA -n "animCurveTA1097";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  211 0 215 -22.373991012573242 235 -26.977142333984375
		 240 -12.038155555725098 245 1.0727007389068604 250 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.96062815189361572 1 0.48379489779472351 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.27783727645874023 0 0.87518143653869629 
		0 0;
	setAttr -s 6 ".kox[0:5]"  0.39254391193389893 0.96062815189361572 
		1 0.48379489779472351 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.91973328590393066 -0.27783727645874023 
		0 0.87518143653869629 0 0;
createNode animCurveTA -n "animCurveTA1098";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  211 0 215 -0.12738437950611115 235 -2.5969016551971436
		 240 5.6778178215026855 245 8.6214561462402344 250 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.99920022487640381 1 0.80391222238540649 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.03998696431517601 0 0.59474796056747437 
		0 0;
	setAttr -s 6 ".kox[0:5]"  0.99991101026535034 0.99920022487640381 
		1 0.80391222238540649 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.013338525779545307 -0.03998696431517601 
		0 0.59474796056747437 0 0;
createNode animCurveTL -n "animCurveTL1096";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -4.7867727279663086 250 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1097";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 32.754745483398438 250 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1098";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -56.147525787353516 250 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1099";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1100";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1101";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1099";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1100";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1101";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1099";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 3.2171449661254883 250 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1100";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -26.658763885498047 250 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1101";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1.5793838500976563 250 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1102";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1103";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1104";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1102";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  211 0 215 54.312305450439453 226 -11.838081359863281
		 235 10.430399894714355 240 17.70063591003418 245 -10.437652587890625 250 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.48008757829666138 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0.87722057104110718 0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.17316514253616333 1 1 0.48008757829666138 
		1 1 1;
	setAttr -s 7 ".koy[0:6]"  0.98489278554916382 0 0 0.87722057104110718 
		0 0 0;
createNode animCurveTA -n "animCurveTA1103";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  211 0 215 -1.3304039239883423 226 -12.618378639221191
		 235 -6.0234756469726562 240 -0.43405249714851379 245 -2.0989704132080078 250 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 0.92265313863754272 1 0.81009888648986816 
		1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.38563090562820435 0 0.58629328012466431 
		0 0 0;
	setAttr -s 7 ".kox[0:6]"  0.99043399095535278 0.92265313863754272 
		1 0.81009888648986816 1 1 1;
	setAttr -s 7 ".koy[0:6]"  -0.13798739016056061 -0.38563090562820435 
		0 0.58629328012466431 0 0 0;
createNode animCurveTA -n "animCurveTA1104";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  211 0 215 -37.461910247802734 226 3.581923246383667
		 235 -11.169399261474609 240 -11.224898338317871 245 -6.2552895545959473 250 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 0.99990272521972656 1 0.81675183773040771 
		1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 -0.013947089202702045 0 0.57698899507522583 
		0;
	setAttr -s 7 ".kox[0:6]"  0.24700731039047241 1 1 0.99990272521972656 
		1 0.81675183773040771 1;
	setAttr -s 7 ".koy[0:6]"  -0.96901369094848633 0 0 -0.013947089202702045 
		0 0.57698899507522583 0;
createNode animCurveTL -n "animCurveTL1102";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -4.7867727279663086 250 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1103";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 32.754745483398438 250 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1104";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 56.147525787353516 250 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1105";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1106";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1107";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1105";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1106";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1107";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1105";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 40.544437408447266 250 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1106";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 43.055271148681641 250 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1107";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1108";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1109";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1110";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1108";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1109";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1110";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1108";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 8.0282459259033203 250 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1109";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 9.9087905883789063 250 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1110";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1111";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1112";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1113";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1111";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  211 40.397052764892578 216 46.25201416015625
		 218 32.535018920898438 220 -82.934394836425781 222 32.535018920898438 224 -82.934394836425781
		 226 32.535018920898438 228 -82.934394836425781 234 32.535018920898438 236 -82.934394836425781
		 238 32.535018920898438 240 -82.934394836425781 242 32.535018920898438 246 -82.934394836425781
		 250 7.4917421340942392;
	setAttr -s 15 ".ktl[14]" no;
	setAttr -s 15 ".kix[0:14]"  1 1 0.11525440216064453 1 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 -0.993336021900177 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  1 1 0.11525440216064453 1 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0 0 -0.993336021900177 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTA -n "animCurveTA1112";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1113";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 32.840129852294922 250 32.840129852294922;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  0.072504520416259766 1;
	setAttr -s 2 ".kiy[0:1]"  0.99736809730529785 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1111";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -19.597047805786133 250 -19.597047805786133;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1112";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 36.545459747314453 250 36.545459747314453;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1113";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 39.212558746337891 250 39.212558746337891;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1114";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1115";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1116";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1114";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1115";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1116";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1114";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 51.6451416015625 250 51.6451416015625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1115";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -11.264523506164551 250 -11.264523506164551;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1116";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1117";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  211 1 218 1 222 1 224 1 226 1 228 1 234 1
		 236 1 238 1 240 1 242 1 246 1 250 1;
	setAttr -s 13 ".ktl[0:12]" no no yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1118";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  211 1 218 1 222 1 224 1 226 1 228 1 234 1
		 236 1 238 1 240 1 242 1 246 1 250 1;
	setAttr -s 13 ".ktl[0:12]" no no yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1119";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  211 1 218 1 222 1 224 1 226 1 228 1 234 1
		 236 1 238 1 240 1 242 1 246 1 250 1;
	setAttr -s 13 ".ktl[0:12]" no no yes yes yes yes yes yes yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 13 ".kox[0:12]"  1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 13 ".koy[0:12]"  0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1117";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  211 -32.905311584472656 216 -38.760269165039063
		 218 -25.04327392578125 220 90.426139831542969 222 -25.04327392578125 224 90.426139831542969
		 226 -25.04327392578125 228 90.426139831542969 234 -25.04327392578125 236 90.426139831542969
		 238 -25.04327392578125 240 90.426139831542969 242 -25.04327392578125 246 90.426139831542969
		 250 0;
	setAttr -s 15 ".ktl[14]" no;
	setAttr -s 15 ".kix[0:14]"  0.07236180454492569 1 0.32873556017875671 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  -0.99737852811813354 0 0.94442200660705566 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.89781111478805542 1 0.041314993053674698 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  -0.44038069248199463 0 0.9991462230682373 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1118";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  211 -3.5807332992553711 216 -5.8973445892333984
		 218 -7.7918944358825684 220 -1.813296318054199 222 -7.7918944358825684 224 -1.813296318054199
		 226 -7.7918944358825684 228 -1.813296318054199 234 -7.7918944358825684 236 -1.813296318054199
		 238 -7.7918944358825684 240 -1.813296318054199 242 -7.7918944358825684 246 -1.813296318054199
		 250 0;
	setAttr -s 15 ".ktl[14]" no;
	setAttr -s 15 ".kix[0:14]"  0.55473023653030396 1 0.92950034141540527 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  -0.83203023672103882 0 -0.36882120370864868 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.98168301582336426 1 0.62404173612594604 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  -0.19052137434482574 0 0.7813909649848938 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1119";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  211 49.030250549316406 216 46.066768646240234
		 218 47.532573699951172 220 47.822013854980469 222 47.532573699951172 224 47.822013854980469
		 226 47.532573699951172 228 47.822013854980469 234 47.532573699951172 236 47.822013854980469
		 238 47.532573699951172 240 47.822013854980469 242 47.532573699951172 246 47.822013854980469
		 250 0;
	setAttr -s 15 ".ktl[14]" no;
	setAttr -s 15 ".kix[0:14]"  0.048633478581905365 1 0.95596522092819214 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0.99881666898727417 0 0.29348006844520569 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.97053658962249756 1 0.99816763401031494 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  -0.24095387756824493 0 0.060509003698825836 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1117";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  211 -19.609930038452148 218 -19.975275039672852
		 220 -19.975275039672852 222 -19.975275039672852 224 -19.975275039672852 226 -19.975275039672852
		 228 -19.975275039672852 234 -19.975275039672852 236 -19.975275039672852 238 -19.975275039672852
		 240 -19.975275039672852 242 -19.975275039672852 246 -19.975275039672852 250 -19.609930038452148;
	setAttr -s 14 ".ktl[0:13]" no no yes yes yes yes yes yes yes yes yes 
		yes no no;
	setAttr -s 14 ".kix[0:13]"  1 0.62390005588531494 1 1 1 1 1 1 1 1 1 
		1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 -0.78150421380996704 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  0.62389922142028809 1 1 1 1 1 1 1 1 1 1 
		1 1 1;
	setAttr -s 14 ".koy[0:13]"  -0.78150475025177002 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTL -n "animCurveTL1118";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  211 43.318531036376953 216 47.955127716064453
		 218 40.274101257324219 220 40.274101257324219 222 40.274101257324219 224 40.274101257324219
		 226 40.274101257324219 228 40.274101257324219 234 40.274101257324219 236 40.274101257324219
		 238 40.274101257324219 240 40.274101257324219 242 40.274101257324219 246 40.274101257324219
		 250 36.542636871337891;
	setAttr -s 15 ".ktl[2:14]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 15 ".kix[0:14]"  0.006149176973849535 1 0.010848566889762878 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0.9999811053276062 0 -0.99994117021560669 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.044887050986289978 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 15 ".koy[0:14]"  0.99899208545684814 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTL -n "animCurveTL1119";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  211 -39.208473205566406 218 -36.804862976074219
		 220 -36.804862976074219 222 -36.804862976074219 224 -36.804862976074219 226 -36.804862976074219
		 228 -36.804862976074219 234 -36.804862976074219 236 -36.804862976074219 238 -36.804862976074219
		 240 -36.804862976074219 242 -36.804862976074219 246 -36.804862976074219 250 -39.208473205566406;
	setAttr -s 14 ".ktl[0:13]" no no yes yes yes yes yes yes yes yes yes 
		yes no no;
	setAttr -s 14 ".kix[0:13]"  1 0.12046158313751221 1 1 1 1 1 1 1 1 1 
		1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0.99271804094314575 0 0 0 0 0 0 0 0 0 
		0 0 0;
	setAttr -s 14 ".kox[0:13]"  0.1204613521695137 1 1 1 1 1 1 1 1 1 1 
		1 1 1;
	setAttr -s 14 ".koy[0:13]"  0.99271804094314575 0 0 0 0 0 0 0 0 0 0 
		0 0 0;
createNode animCurveTU -n "animCurveTU1120";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1121";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1122";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1120";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1121";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1122";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1120";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -4.502251148223877 250 -4.502251148223877;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1121";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 45.318515777587891 220 43.304428100585938
		 227 33.75531005859375 236 45.712623596191406 250 37.76336669921875;
	setAttr -s 5 ".ktl[4]" no;
	setAttr -s 5 ".kix[0:4]"  1 0.048215009272098541 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.9988369345664978 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 0.048215009272098541 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 -0.9988369345664978 0 0 0;
createNode animCurveTL -n "animCurveTL1122";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1123";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1124";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1125";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1123";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1124";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1125";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1123";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 -1.4725730419158936 250 -1.4725730419158936;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1124";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 29.854494094848633 218 27.840408325195313
		 225 22.461189270019531 234 25.156711578369141 250 22.299345016479492;
	setAttr -s 5 ".ktl[4]" no;
	setAttr -s 5 ".kix[0:4]"  0.0055149625986814499 0.048215009272098541 
		1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0.99998480081558228 -0.9988369345664978 
		0 0 0;
	setAttr -s 5 ".kox[0:4]"  0.14331820607185364 0.048215009272098541 
		1 1 1;
	setAttr -s 5 ".koy[0:4]"  -0.98967665433883667 -0.9988369345664978 
		0 0 0;
createNode animCurveTL -n "animCurveTL1125";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 0 250 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1126";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 1 223 1 228 1 243 1 250 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1127";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 1 223 1 228 1 243 1 250 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1128";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 1 223 1 228 1 243 1 250 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1126";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 0 223 0 228 0 243 0 250 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1127";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 0 223 0 228 0 243 0 250 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1128";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 0 223 0 228 0 243 0 250 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1126";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 0 223 0 228 0 243 0 250 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1127";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  211 309.53347778320313 223 38.061374664306641
		 228 69.15643310546875 232 81.192558288574219 243 4.6111888885498047 250 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  0.00013461077469401062 1 0.0046156854368746281 
		1 0.015058279037475586 1;
	setAttr -s 6 ".kiy[0:5]"  1 0 0.99998939037322998 0 -0.99988663196563721 
		0;
	setAttr -s 6 ".kox[0:5]"  0.00072956387884914875 1 0.0046156854368746281 
		1 0.015058279037475586 1;
	setAttr -s 6 ".koy[0:5]"  -0.9999997615814209 0 0.99998939037322998 
		0 -0.99988663196563721 0;
createNode animCurveTL -n "animCurveTL1128";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  211 0 223 0 228 0 243 0 250 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1129";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1130";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1131";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  211 1 250 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1129";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  211 0 218 7.8968949317932129 221 7.5609083175659189
		 225 11.506748199462891 233 23.821689605712891 237 26.068304061889648 240 15.315674781799316
		 244 4.534703254699707 250 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 1 0.80137336254119873 0.915302574634552 
		1 0.45586884021759033 0.81046408414840698 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0.59816431999206543 0.40276685357093811 
		0 -0.89004695415496826 -0.5857883095741272 0;
	setAttr -s 9 ".kox[0:8]"  0.90413415431976318 1 1 0.80137336254119873 
		0.915302574634552 1 0.45586884021759033 0.81046408414840698 1;
	setAttr -s 9 ".koy[0:8]"  0.42724868655204773 0 0 0.59816431999206543 
		0.40276685357093811 0 -0.89004695415496826 -0.5857883095741272 0;
createNode animCurveTA -n "animCurveTA1130";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  211 0 218 8.0135498046875 221 15.153881072998047
		 225 23.513343811035156 233 42.628467559814453 237 33.925334930419922 240 22.413673400878906
		 244 12.889694213867188 250 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 0.80451309680938721 0.73001444339752197 
		0.72930675745010376 1 0.62185144424438477 0.55452227592468262 0.72550290822982788 
		1;
	setAttr -s 9 ".kiy[0:8]"  0 0.59393495321273804 0.68343180418014526 
		0.68418693542480469 0 -0.78313523530960083 -0.83216893672943115 -0.68821913003921509 
		0;
	setAttr -s 9 ".kox[0:8]"  0.9016880989074707 0.80451309680938721 
		0.73001444339752197 0.72930675745010376 1 0.62185144424438477 0.55452227592468262 
		0.72550290822982788 1;
	setAttr -s 9 ".koy[0:8]"  0.43238699436187744 0.59393495321273804 
		0.68343180418014526 0.68418693542480469 0 -0.78313523530960083 -0.83216893672943115 
		-0.68821913003921509 0;
createNode animCurveTA -n "animCurveTA1131";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  211 39.613922119140625 218 -8.4930458068847656
		 221 -45.094402313232422 225 -46.233264923095703 229 -38.031513214111328 233 2.7422287464141846
		 237 29.24774169921875 240 12.49737548828125 244 -12.203648567199707 250 0;
	setAttr -s 10 ".ktl[9]" no;
	setAttr -s 10 ".kix[0:9]"  0.060156069695949554 0.11502400785684586 
		0.94155055284500122 1 0.36331483721733093 0.17416049540042877 1 0.21238502860069275 
		1 1;
	setAttr -s 10 ".kiy[0:9]"  0.99818903207778931 -0.99336272478103638 
		-0.3368716835975647 0 0.93166643381118774 0.98471730947494507 0 -0.97718608379364014 
		0 0;
	setAttr -s 10 ".kox[0:9]"  0.32814168930053711 0.11502400785684586 
		0.94155055284500122 1 0.36331483721733093 0.17416049540042877 1 0.21238502860069275 
		1 1;
	setAttr -s 10 ".koy[0:9]"  -0.94462853670120239 -0.99336272478103638 
		-0.3368716835975647 0 0.93166643381118774 0.98471730947494507 0 -0.97718608379364014 
		0 0;
createNode animCurveTL -n "animCurveTL1129";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  211 -63.554134368896484 225 -57.915843963623047
		 237 -63.14825439453125 250 -54.946929931640625;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  0.004840887151658535 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  -0.99998831748962402 0 0 0;
	setAttr -s 4 ".kox[0:3]"  0.10290993005037308 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0.99469071626663208 0 0 0;
createNode animCurveTL -n "animCurveTL1130";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  211 24.314489364624023 225 2.363215446472168
		 237 12.067303657531738 250 2.3482637405395508;
	setAttr -s 4 ".ktl[3]" no;
	setAttr -s 4 ".kix[0:3]"  0.001896860427223146 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0.99999821186065674 0 0 0;
	setAttr -s 4 ".kox[0:3]"  0.026564616709947586 1 1 1;
	setAttr -s 4 ".koy[0:3]"  -0.99964714050292969 0 0 0;
createNode animCurveTL -n "animCurveTL1131";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  211 3.1278786659240723 225 7.2058539390563965
		 237 6.8117160797119141 250 0;
	setAttr -s 4 ".ktl[3]" no;
	setAttr -s 4 ".kix[0:3]"  0.013319980353116989 1 0.38947317004203796 
		1;
	setAttr -s 4 ".kiy[0:3]"  0.99991124868392944 0 -0.92103785276412964 
		0;
	setAttr -s 4 ".kox[0:3]"  0.14160336554050446 1 0.38947317004203796 
		1;
	setAttr -s 4 ".koy[0:3]"  0.98992341756820679 0 -0.92103785276412964 
		0;
select -ne :time1;
	setAttr ".o" 310;
	setAttr ".unw" 310;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "swoop_downSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1093.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1094.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1095.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1093.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1094.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1095.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1093.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1094.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1095.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1096.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1097.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1098.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1096.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1097.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1098.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1096.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1097.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1098.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1099.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1100.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1101.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1099.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1100.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1101.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1099.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1100.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1101.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1102.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1103.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1104.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1102.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1103.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1104.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1102.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1103.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1104.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1105.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1106.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1107.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1105.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1106.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1107.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1105.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1106.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1107.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1108.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1109.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1110.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1108.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1109.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1110.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1108.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1109.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1110.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1111.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1112.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1113.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1111.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1112.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1113.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1111.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1112.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1113.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1114.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1115.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1116.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1114.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1115.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1116.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1114.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1115.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1116.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1117.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1118.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1119.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1117.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1118.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1119.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1117.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1118.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1119.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1120.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1121.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1122.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1120.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1121.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1122.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1120.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1121.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1122.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1123.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1124.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1125.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1123.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1124.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1125.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1123.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1124.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1125.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1126.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1127.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1128.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1126.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1127.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1128.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1126.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1127.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1128.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1129.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1130.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1131.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1129.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1130.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1131.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1129.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1130.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1131.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_swoop_down.ma
