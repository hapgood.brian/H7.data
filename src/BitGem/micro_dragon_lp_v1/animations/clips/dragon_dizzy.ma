//Maya ASCII 2013 scene
//Name: dragon_dizzy.ma
//Last modified: Mon, Jul 14, 2014 01:53:23 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "dizzySource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 560;
	setAttr ".se" 650;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU2185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2185";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 3.2171449661254883 650 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2186";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -26.658763885498047 650 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2187";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -1.5793838500976563 650 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  560 0 571 10.588632583618164 584 3.7028155326843262
		 597 -17.864067077636719 613 10.035284042358398 623 13.344033241271973 640 -23.552728652954102
		 650 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.83245480060577393 1 0.92336416244506836 
		1 1 0.71187037229537964;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.55409300327301025 0 0.38392525911331177 
		0 0 0.70231091976165771;
	setAttr -s 8 ".kox[0:7]"  0.92744487524032593 1 0.83245480060577393 
		1 0.92336416244506836 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0.37395995855331421 0 -0.55409300327301025 
		0 0.38392525911331177 0 0 0;
createNode animCurveTA -n "animCurveTA2189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  560 0 571 12.610037803649902 584 -12.097770690917969
		 597 -2.1589128971099854 613 12.547904968261719 623 -3.7554206848144536 640 -12.695335388183594
		 650 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.74377119541168213 1 0.83428823947906494 
		1 0.88292193412780762;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0.6684342622756958 0 -0.55132853984832764 
		0 0.46951967477798462;
	setAttr -s 8 ".kox[0:7]"  0.90145611763000488 1 1 0.74377119541168213 
		1 0.83428823947906494 1 1;
	setAttr -s 8 ".koy[0:7]"  0.43287041783332825 0 0 0.6684342622756958 
		0 -0.55132853984832764 0 0;
createNode animCurveTA -n "animCurveTA2190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  560 0 571 -2.2250597476959229 584 15.281905174255373
		 597 17.933174133300781 613 -1.2657308578491211 623 5.5246639251708984 640 6.2601776123046875
		 650 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.96869343519210815 1 1 0.99852532148361206 
		1 0.967296302318573;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.2482600212097168 0 0 0.054288856685161591 
		0 -0.25364917516708374;
	setAttr -s 8 ".kox[0:7]"  0.99642956256866455 1 0.96869343519210815 
		1 1 0.99852532148361206 1 1;
	setAttr -s 8 ".koy[0:7]"  -0.084427788853645325 0 0.2482600212097168 
		0 0 0.054288856685161591 0 0;
createNode animCurveTL -n "animCurveTL2188";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -4.7867727279663086 650 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2189";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 32.754745483398438 650 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2190";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -56.147525787353516 650 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2191";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 3.2171449661254883 650 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2192";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -26.658763885498047 650 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2193";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1.5793838500976563 650 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2196";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  560 0 568 -8.6231193542480469 580 10.798525810241699
		 600 -17.100978851318359 612 -13.156990051269531 630 7.5285172462463379 642 -16.343843460083008
		 650 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 0.92427122592926025 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0.38173645734786987 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 0.92427122592926025 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0.38173645734786987 0 0 0;
createNode animCurveTA -n "animCurveTA2195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  560 0 580 11.388463020324707 600 -9.3189907073974609
		 612 10.359735488891602 630 -1.0970742702484131 642 -3.4442923069000244 650 0;
	setAttr -s 7 ".kix[0:6]"  0.95405179262161255 1 1 1 0.97109454870223999 
		1 0.95757073163986206;
	setAttr -s 7 ".kiy[0:6]"  0.29964187741279602 0 0 0 -0.23869512975215912 
		0 0.28819844126701355;
	setAttr -s 7 ".kox[0:6]"  0.95405185222625732 1 1 1 0.97109454870223999 
		1 0.95757073163986206;
	setAttr -s 7 ".koy[0:6]"  0.29964151978492737 0 0 0 -0.23869512975215912 
		0 0.28819841146469116;
createNode animCurveTA -n "animCurveTA2196";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  560 0 580 8.6561403274536133 600 -1.2082138061523437
		 612 8.2723751068115234 630 -2.8083827495574951 642 6.2062482833862305 650 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2194";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -4.7867727279663086 650 -4.7867727279663086;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2195";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 32.754745483398438 650 32.754745483398438;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2196";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 56.147525787353516 650 56.147525787353516;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2197";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2198";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2199";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2197";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2198";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2199";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2197";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 40.544437408447266 650 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2198";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 43.055271148681641 650 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2199";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2200";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2201";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2202";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2200";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2201";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2202";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2200";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 8.0282459259033203 650 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2201";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 9.9087905883789063 650 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2202";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2203";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2204";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2205";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2203";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 -16.725616455078125 603 16.918115615844727
		 622 -12.056648254394531 642 10.690389633178711 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.87260043621063232;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.48843482136726379;
	setAttr -s 6 ".kox[0:5]"  0.95285046100616455 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.30344021320343018 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2204";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 2.0089128017425537 603 -2.3698117733001709
		 622 1.250916600227356 642 -1.7078711986541748 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99602550268173218;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0.089068345725536346;
	setAttr -s 6 ".kox[0:5]"  0.99926924705505371 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0.038221679627895355 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2205";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 3.1137356758117676 603 6.8910961151123047
		 622 9.9353399276733398 642 7.89243507385254 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.99774199724197388 0.99476569890975952 
		1 0.99186235666275024 1;
	setAttr -s 6 ".kiy[0:5]"  0 0.067163482308387756 0.10218244045972824 
		0 -0.12731494009494781 0;
	setAttr -s 6 ".kox[0:5]"  0.99824720621109009 0.99774199724197388 
		0.99476569890975952 1 0.99186235666275024 1;
	setAttr -s 6 ".koy[0:5]"  0.059181511402130127 0.067163482308387756 
		0.10218244045972824 0 -0.12731494009494781 0;
createNode animCurveTL -n "animCurveTL2203";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -19.597047805786133 650 -19.597047805786133;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2204";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 36.545459747314453 650 36.545459747314453;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2205";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 39.212558746337891 650 39.212558746337891;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2206";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2207";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2208";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2206";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 580 -0.48836374282836914 597 0.71348941326141357
		 617 -0.57323712110519409 638 0.3134729266166687 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99994015693664551;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.010941614396870136;
	setAttr -s 6 ".kox[0:5]"  0.99994766712188721 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.010227746330201626 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2207";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 580 -7.52699851989746 597 5.8346676826477051
		 617 -5.5914888381958008 638 5.8793792724609375 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.9795832633972168;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.2010389119386673;
	setAttr -s 6 ".kox[0:5]"  0.98780089616775513 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.15572218596935272 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2208";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 580 1.4838768243789673 597 1.1983580589294434
		 617 0.79147469997406006 638 0.12200920283794404 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.99996978044509888 0.9999077320098877 
		0.999961256980896 0.99999088048934937;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.0077782203443348408 -0.013581716455519199 
		-0.0088059082627296448 -0.0042588864453136921;
	setAttr -s 6 ".kox[0:5]"  0.99951744079589844 1 0.99996978044509888 
		0.9999077320098877 0.999961256980896 1;
	setAttr -s 6 ".koy[0:5]"  0.031063295900821686 0 -0.0077782203443348408 
		-0.013581716455519199 -0.0088059082627296448 0;
createNode animCurveTL -n "animCurveTL2206";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 51.6451416015625 650 51.6451416015625;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2207";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -11.264523506164551 650 -11.264523506164551;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2208";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2209";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2210";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2211";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2209";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 -19.412815093994141 603 20.202251434326172
		 622 -16.899154663085938 642 17.940883636474609 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.72885286808013916;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.68467038869857788;
	setAttr -s 6 ".kox[0:5]"  0.93797796964645386 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.34669479727745056 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2210";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 2.4792459011077881 603 -2.8749754428863525
		 622 2.1529078483581543 642 -2.0627007484436035 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99421828985214233;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0.10737808793783188;
	setAttr -s 6 ".kox[0:5]"  0.99888771772384644 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0.047152258455753326 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2211";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 582 2.7505362033843994 603 5.4206938743591309
		 622 5.9556541442871094 642 -0.69799977540969849 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.99710375070571899 0.99947136640548706 
		1 1 0.99933278560638428;
	setAttr -s 6 ".kiy[0:5]"  0 0.07605365663766861 0.032510045915842056 
		0 0 0.036522727459669113;
	setAttr -s 6 ".kox[0:5]"  0.99863153696060181 0.99710375070571899 
		0.99947136640548706 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0.052298460155725479 0.07605365663766861 
		0.032510045915842056 0 0 0;
createNode animCurveTL -n "animCurveTL2209";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -19.609930038452148 650 -19.609930038452148;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2210";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 36.542636871337891 650 36.542636871337891;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2211";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -39.208473205566406 650 -39.208473205566406;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2212";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 578 1 596 1 616 1 638 1 650 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2213";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 578 1 596 1 616 1 638 1 650 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2214";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 578 1 596 1 616 1 638 1 650 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2212";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 578 -4.6166825294494629 596 3.7503139972686763
		 616 -4.5340652465820313 638 3.3418405055999756 650 0;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99326473474502563;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.11586655676364899;
	setAttr -s 6 ".kox[0:5]"  0.99427837133407593 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.10682037472724915 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2213";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  560 0 569 -3.1551299095153809 578 0 587 8.526341438293457
		 596 0 606 -7.0631322860717773 616 0 628 8.7597084045410156 642 4.0816946029663086
		 650 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 0.91513299942016602 1 0.82204365730285645 
		1 0.85107463598251343 1 0.98308402299880981 0.97791635990142822;
	setAttr -s 10 ".kiy[0:9]"  0 0 0.40315195918083191 0 -0.56942445039749146 
		0 0.52504479885101318 0 -0.18315516412258148 -0.20899698138237;
	setAttr -s 10 ".kox[0:9]"  0.98938930034637451 1 0.91513299942016602 
		1 0.82204365730285645 1 0.85107463598251343 1 0.98308402299880981 1;
	setAttr -s 10 ".koy[0:9]"  -0.14528831839561462 0 0.40315195918083191 
		0 -0.56942445039749146 0 0.52504479885101318 0 -0.18315504491329193 0;
createNode animCurveTA -n "animCurveTA2214";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 578 0 596 0 616 0 638 0 650 0;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2212";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 -4.502251148223877 578 -4.502251148223877
		 596 -4.502251148223877 616 -4.502251148223877 638 -4.502251148223877 650 -4.502251148223877;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2213";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 37.76336669921875 578 37.76336669921875
		 596 37.76336669921875 616 37.76336669921875 638 37.76336669921875 650 37.76336669921875;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2214";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 578 0 596 0 616 0 638 0 650 0;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2215";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 576 1 594 1 614 1 636 1 650 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2216";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 576 1 594 1 614 1 636 1 650 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2217";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 576 1 594 1 614 1 636 1 650 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2215";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 576 -4.6166825294494629 594 3.7503139972686763
		 614 -4.5340652465820313 636 3.3418405055999756 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.99503844976425171;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.099491439759731293;
	setAttr -s 6 ".kox[0:5]"  0.99277496337890625 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.11999133229255676 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2216";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 576 0 594 0 614 0 636 0 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2217";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 576 0 594 0 614 0 636 0 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2215";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 -1.4725730419158936 576 -1.4725730419158936
		 594 -1.4725730419158936 614 -1.4725730419158936 636 -1.4725730419158936 650 -1.4725730419158936;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2216";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 22.299345016479492 576 22.299345016479492
		 594 22.299345016479492 614 22.299345016479492 636 22.299345016479492 650 22.299345016479492;
	setAttr -s 6 ".kit[3:5]"  2 2 1;
	setAttr -s 6 ".kot[3:5]"  2 2 2;
	setAttr -s 6 ".ktl[0:5]" no no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2217";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 576 0 594 0 614 0 636 0 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2218";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 574 1 592 1 612 1 634 1 650 1;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2219";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 574 1 592 1 612 1 634 1 650 1;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2220";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 1 574 1 592 1 612 1 634 1 650 1;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2218";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 -5.8009843826293945 592 8.7093629837036133
		 612 -5.8009843826293945 634 8.7093629837036133 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.97497719526290894;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 -0.22230468690395355;
	setAttr -s 6 ".kox[0:5]"  0.98526948690414429 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  -0.17100873589515686 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2219";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 0 592 0 612 0 634 0 650 0;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2220";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 0 592 0 612 0 634 0 650 0;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2218";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 0 592 0 612 0 634 0 650 0;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2219";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 0 592 0 612 0 634 0 650 0;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2220";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 574 0 592 0 612 0 634 0 650 0;
	setAttr -s 6 ".ktl[1:5]" no no yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2221";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2222";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU2223";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 1 650 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA2221";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 570 3.1142275333404541 584 -8.1452264785766602
		 604 -9.1347255706787109 632 6.8063182830810547 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.99807298183441162 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.062052313238382339 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.99807298183441162 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.062052313238382339 0 0 0;
createNode animCurveTA -n "animCurveTA2222";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 570 10.055176734924316 584 -17.005149841308594
		 604 55.68341064453125 632 -24.177276611328125 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 0.87152677774429321;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0.49034783244132996;
	setAttr -s 6 ".kox[0:5]"  0.92158979177474976 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0.38816523551940918 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2223";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  560 0 570 -9.1428489685058594 584 -13.249481201171875
		 604 -24.719345092773438 632 -22.611970901489258 650 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.96947216987609863 0.98391681909561157 
		1 0.99555724859237671 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.24520130455493927 -0.17862731218338013 
		0 0.0941585972905159 0;
	setAttr -s 6 ".kox[0:5]"  1 0.96947216987609863 0.98391681909561157 
		1 0.99555724859237671 1;
	setAttr -s 6 ".koy[0:5]"  0 -0.24520130455493927 -0.17862731218338013 
		0 0.0941585972905159 0;
createNode animCurveTL -n "animCurveTL2221";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 -54.946929931640625 650 -54.946929931640625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2222";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 2.3482637405395508 650 2.3482637405395508;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL2223";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  560 0 650 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 650;
	setAttr ".unw" 650;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "dizzySource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU2185.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU2186.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU2187.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA2185.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA2186.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA2187.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL2185.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL2186.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL2187.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU2188.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU2189.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU2190.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA2188.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA2189.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA2190.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL2188.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL2189.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL2190.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU2191.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU2192.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU2193.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA2191.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA2192.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA2193.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL2191.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL2192.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL2193.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU2194.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU2195.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU2196.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA2194.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA2195.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA2196.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL2194.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL2195.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL2196.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU2197.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU2198.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU2199.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA2197.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA2198.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA2199.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL2197.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL2198.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL2199.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU2200.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU2201.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU2202.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA2200.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA2201.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA2202.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL2200.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL2201.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL2202.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU2203.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU2204.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU2205.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA2203.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA2204.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA2205.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL2203.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL2204.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL2205.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU2206.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU2207.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU2208.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA2206.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA2207.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA2208.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL2206.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL2207.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL2208.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU2209.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU2210.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU2211.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA2209.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA2210.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA2211.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL2209.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL2210.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL2211.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU2212.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU2213.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU2214.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA2212.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA2213.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA2214.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL2212.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL2213.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL2214.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU2215.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU2216.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU2217.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA2215.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA2216.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA2217.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL2215.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL2216.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL2217.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU2218.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU2219.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU2220.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA2218.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA2219.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA2220.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL2218.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL2219.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL2220.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU2221.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU2222.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU2223.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA2221.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA2222.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA2223.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL2221.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL2222.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL2223.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_dizzy.ma
