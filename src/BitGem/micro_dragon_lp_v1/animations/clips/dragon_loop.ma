//Maya ASCII 2013 scene
//Name: dragon_loop.ma
//Last modified: Mon, Jul 14, 2014 01:05:28 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "loopSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 251;
	setAttr ".se" 270;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1249";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1250";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1251";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1249";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1250";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1251";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1249";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 3.2171449661254883 270 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1250";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -26.658763885498047 270 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1251";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -1.5793838500976563 270 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1252";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 1 268 1 270 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1253";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 1 268 1 270 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1254";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 1 268 1 270 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1252";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 -4.2241663932800293 263 -0.31973996758460999
		 268 0 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes no yes;
	setAttr -s 5 ".kix[0:4]"  1 0.49201861023902893 0.99678677320480347 
		1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.87058466672897339 0.08010120689868927 
		0 0;
	setAttr -s 5 ".kox[0:4]"  0.49202138185501099 0.98912698030471802 
		0.99678677320480347 1 1;
	setAttr -s 5 ".koy[0:4]"  -0.870583176612854 0.1470637172460556 0.08010120689868927 
		0 0;
createNode animCurveTA -n "animCurveTA1253";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 -19.02900505065918 263 -27.676540374755859
		 268 0 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes no yes;
	setAttr -s 5 ".kix[0:4]"  1 0.12448134273290634 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.99222201108932495 0 0 0;
	setAttr -s 5 ".kox[0:4]"  0.124482162296772 0.94982695579528809 1 
		1 1;
	setAttr -s 5 ".koy[0:4]"  -0.99222183227539063 -0.31277582049369812 
		0 0 0;
createNode animCurveTA -n "animCurveTA1254";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 58.120052337646491 263 55.687728881835937
		 268 3.5093708038330078 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.041041258722543716 0.96349543333053589 
		0.41302436590194702 1;
	setAttr -s 5 ".kiy[0:4]"  0 0.99915742874145508 -0.2677246630191803 
		-0.91071993112564087 0;
	setAttr -s 5 ".kox[0:4]"  0.0410415418446064 0.99573785066604614 
		0.96349543333053589 0.41302436590194702 1;
	setAttr -s 5 ".koy[0:4]"  0.99915742874145508 -0.092227980494499207 
		-0.2677246630191803 -0.91071993112564087 0;
createNode animCurveTL -n "animCurveTL1252";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 -4.7867727279663086 268 -4.7867727279663086
		 270 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1253";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 32.754745483398438 268 32.754745483398438
		 270 32.754745483398438;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1254";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  251 -56.147525787353516 268 -56.147525787353516
		 270 -56.147525787353516;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1255";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1256";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1257";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1255";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1256";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1257";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1255";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 3.2171449661254883 270 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1256";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -26.658763885498047 270 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1257";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1.5793838500976563 270 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1258";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1259";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1260";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1258";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 -2.5060455799102783 259 -14.857380867004395
		 264 2.2857427597045898 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.68974828720092773 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.72404927015304565 0 0 0;
	setAttr -s 5 ".kox[0:4]"  0.68975090980529785 0.80418729782104492 
		1 1 1;
	setAttr -s 5 ".koy[0:4]"  -0.72404664754867554 -0.59437602758407593 
		0 0 0;
createNode animCurveTA -n "animCurveTA1259";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 2.7178845405578613 259 6.6480317115783691
		 264 14.017509460449219 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.65993982553482056 0.91990596055984497 
		1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0.75131845474243164 0.39213904738426208 
		0 0;
	setAttr -s 5 ".kox[0:4]"  0.65994268655776978 0.97344207763671875 
		0.91990596055984497 1 1;
	setAttr -s 5 ".koy[0:4]"  0.7513158917427063 0.22893333435058594 
		0.39213904738426208 0 0;
createNode animCurveTA -n "animCurveTA1260";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  251 0 252 50.949203491210937 259 53.526363372802734
		 264 20.771316528320312 270 0;
	setAttr -s 5 ".ktl[0:4]" no no yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.046805672347545624 1 0.29962271451950073 
		1;
	setAttr -s 5 ".kiy[0:4]"  0 0.99890398979187012 0 -0.95405775308609009 
		0;
	setAttr -s 5 ".kox[0:4]"  0.046805955469608307 0.98831659555435181 
		1 0.29962271451950073 1;
	setAttr -s 5 ".koy[0:4]"  0.99890398979187012 0.15241499245166779 
		0 -0.95405775308609009 0;
createNode animCurveTL -n "animCurveTL1258";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -4.7867727279663086 270 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1259";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 32.754745483398438 270 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1260";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 56.147525787353516 270 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1261";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1262";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1263";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1261";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1262";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1263";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1261";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 40.544437408447266 270 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1262";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 43.055271148681641 270 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1263";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1264";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1265";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1266";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1264";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1265";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1266";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1264";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 8.0282459259033203 270 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1265";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 9.9087905883789063 270 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1266";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1267";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1268";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1269";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1267";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 7.4917421340942392 252 -82.625900268554688
		 253 36.861705780029297 254 -87.131538391113281 255 33.924564361572266 256 -77.164909362792969
		 257 53.979213714599609 258 -80.073341369628906 261 -81.136116027832031 264 -12.502083778381348
		 267 -44.414981842041016 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 0.91356426477432251 1 1 1 
		0.053672842681407928;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 -0.40669447183609009 0 0 
		0 0.99855852127075195;
	setAttr -s 12 ".kox[0:11]"  0.026482010260224342 1 1 1 1 1 1 0.91356426477432251 
		1 1 1 0.053672842681407928;
	setAttr -s 12 ".koy[0:11]"  -0.9996492862701416 0 0 0 0 0 0 -0.40669447183609009 
		0 0 0 0.99855852127075195;
createNode animCurveTA -n "animCurveTA1268";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 0 252 10.169535636901855 253 4.1584773063659668
		 254 -25.390541076660156 255 20.723457336425781 256 -0.45287910103797913 257 -3.1983160972595215
		 258 4.9339251518249512 261 21.725337982177734 264 -6.070979118347168 267 -6.5184597969055176
		 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		no;
	setAttr -s 12 ".kix[0:11]"  1 1 0.131240114569664 1 1 0.27839484810829163 
		1 0.17667765915393829 1 0.98288285732269287 1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0 -0.99135065078735352 0 0 -0.96046674251556396 
		0 0.98426884412765503 0 -0.18423168361186981 0 0;
	setAttr -s 12 ".kox[0:11]"  0.22854149341583252 1 0.131240114569664 
		1 1 0.27839484810829163 1 0.17667765915393829 1 0.98288285732269287 1 1;
	setAttr -s 12 ".koy[0:11]"  0.97353416681289673 0 -0.99135065078735352 
		0 0 -0.96046674251556396 0 0.98426884412765503 0 -0.18423168361186981 0 0;
createNode animCurveTA -n "animCurveTA1269";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 32.840129852294922 252 51.433483123779297
		 253 63.069068908691406 254 56.018745422363281 255 56.186405181884766 256 46.122295379638672
		 257 75.985984802246094 258 50.537345886230469 261 58.798824310302741 264 49.025318145751953
		 267 51.982742309570313 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 0.097240865230560303 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 12 ".kiy[0:11]"  0 0.99526083469390869 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 12 ".kox[0:11]"  0.12735210359096527 0.097240865230560303 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 12 ".koy[0:11]"  0.99185758829116821 0.99526083469390869 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1267";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -19.597047805786133 270 -19.597047805786133;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1268";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 36.545459747314453 270 36.545459747314453;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1269";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 39.212558746337891 270 39.212558746337891;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1270";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1271";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1272";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1270";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1271";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1272";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 252 -17.758138656616211 264 -2.960031270980835
		 270 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.13323689997196198 0.84992259740829468 
		1;
	setAttr -s 4 ".kiy[0:3]"  0 -0.99108421802520752 0.52690768241882324 
		0;
	setAttr -s 4 ".kox[0:3]"  0.13323800265789032 0.88846766948699951 
		0.84992259740829468 1;
	setAttr -s 4 ".koy[0:3]"  -0.99108403921127319 0.45893919467926025 
		0.52690768241882324 0;
createNode animCurveTL -n "animCurveTL1270";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 51.6451416015625 270 51.6451416015625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1271";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -11.264523506164551 270 -11.264523506164551;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1272";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1273";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 1 252 1 253 1 254 1 265 1 270 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1274";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 1 252 1 253 1 254 1 265 1 270 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1275";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 1 252 1 253 1 254 1 265 1 270 1;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1273";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 0 252 84.542701721191406 253 -30.801658630371094
		 254 91.286521911621094 255 -43.916145324707031 256 85.588211059570313 257 -34.038818359375
		 258 83.743560791015625 259 71.14788818359375 265 6.4500069618225098 267 41.82562255859375
		 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 0.073598966002464294 1 
		1 0.056985385715961456;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 -0.99728792905807495 0 
		0 -0.99837499856948853;
	setAttr -s 12 ".kox[0:11]"  0.028227102011442184 1 1 1 1 1 1 1 0.073598966002464294 
		1 1 0.056985385715961456;
	setAttr -s 12 ".koy[0:11]"  0.99960154294967651 0 0 0 0 0 0 0 -0.99728792905807495 
		0 0 -0.99837499856948853;
createNode animCurveTA -n "animCurveTA1274";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 0 252 -9.4770412445068359 253 9.168635368347168
		 254 -26.300125122070313 255 -2.2906320095062256 256 -21.836807250976562 257 9.4207077026367188
		 258 -12.155120849609375 259 -6.3308062553405762 265 -4.0620889663696289 267 -10.614306449890137
		 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 1 1 1 1 1 1 1 0.90322190523147583 1 1 
		1;
	setAttr -s 12 ".kiy[0:11]"  0 0 0 0 0 0 0 0 0.42917394638061523 0 0 
		0;
	setAttr -s 12 ".kox[0:11]"  0.24427652359008789 1 1 1 1 1 1 1 0.90322190523147583 
		1 1 1;
	setAttr -s 12 ".koy[0:11]"  -0.96970558166503906 0 0 0 0 0 0 0 0.42917394638061523 
		0 0 0;
createNode animCurveTA -n "animCurveTA1275";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  251 0 252 57.049190521240234 253 72.155288696289062
		 254 52.896404266357422 255 77.868637084960938 256 65.425125122070312 257 74.00115966796875
		 258 63.311214447021484 259 59.975532531738274 265 40.174674987792969 267 25.570638656616211
		 270 0;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 12 ".kix[0:11]"  1 0.052606213837862015 1 1 1 1 1 0.33533978462219238 
		0.98046892881393433 0.41062745451927185 0.21128231287002563 1;
	setAttr -s 12 ".kiy[0:11]"  0 0.9986153244972229 0 0 0 0 0 -0.9420972466468811 
		-0.1966744065284729 -0.91180324554443359 -0.97742509841918945 0;
	setAttr -s 12 ".kox[0:11]"  0.041810467839241028 0.052606213837862015 
		1 1 1 1 1 0.33533978462219238 0.98046892881393433 0.41062745451927185 0.21128231287002563 
		1;
	setAttr -s 12 ".koy[0:11]"  0.99912559986114502 0.9986153244972229 
		0 0 0 0 0 -0.9420972466468811 -0.1966744065284729 -0.91180324554443359 -0.97742509841918945 
		0;
createNode animCurveTL -n "animCurveTL1273";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 -19.609930038452148 252 -19.609930038452148
		 253 -19.609930038452148 254 -19.609930038452148 265 -19.609930038452148 270 -19.609930038452148;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1274";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 36.542636871337891 252 36.542636871337891
		 253 36.542636871337891 254 36.542636871337891 265 36.542636871337891 270 36.542636871337891;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1275";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 -39.208473205566406 252 -39.208473205566406
		 253 -39.208473205566406 254 -39.208473205566406 265 -39.208473205566406 270 -39.208473205566406;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1276";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1277";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1278";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1276";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 252 11.438923835754395 261 -9.7815618515014648
		 270 0;
	setAttr -s 4 ".ktl[0:3]" no no yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.20430004596710205 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0.97890830039978027 0 0;
	setAttr -s 4 ".kox[0:3]"  0.2043013870716095 0.71148788928985596 
		1 1;
	setAttr -s 4 ".koy[0:3]"  0.97890806198120117 -0.70269834995269775 
		0 0;
createNode animCurveTA -n "animCurveTA1277";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 252 5.2625775337219238 261 2.1168758869171143
		 270 0;
	setAttr -s 4 ".ktl[0:3]" no no yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.41312059760093689 0.95898717641830444 
		1;
	setAttr -s 4 ".kiy[0:3]"  0 0.91067636013031006 -0.28344941139221191 
		0;
	setAttr -s 4 ".kox[0:3]"  0.41312325000762939 0.98945164680480957 
		0.95898717641830444 1;
	setAttr -s 4 ".koy[0:3]"  0.91067516803741455 -0.14486326277256012 
		-0.28344941139221191 0;
createNode animCurveTA -n "animCurveTA1278";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 0 252 -31.291475296020508 261 -16.846889495849609
		 265 -4.986720085144043 267 -1.3570601940155029 270 0;
	setAttr -s 6 ".ktl[0:5]" no no yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 0.076072089374065399 0.72241950035095215 
		0.61379605531692505 0.90333437919616699 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.99710232019424438 0.69145500659942627 
		0.78946465253829956 0.42893719673156738 0;
	setAttr -s 6 ".kox[0:5]"  0.076072655618190765 0.82989346981048584 
		0.72241950035095215 0.61379605531692505 0.90333437919616699 1;
	setAttr -s 6 ".koy[0:5]"  -0.99710220098495483 0.55792200565338135 
		0.69145500659942627 0.78946465253829956 0.42893719673156738 0;
createNode animCurveTL -n "animCurveTL1276";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -4.502251148223877 270 -4.502251148223877;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1277";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 37.76336669921875 270 37.76336669921875;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1278";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1279";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1280";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1281";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1279";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1280";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1281";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1279";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -1.4725730419158936 270 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1280";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 22.299345016479492 270 22.299345016479492;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.0055149621330201626;
	setAttr -s 2 ".koy[0:1]"  0 0.99998480081558228;
createNode animCurveTL -n "animCurveTL1281";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1282";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 1 256 1 260 1 270 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1283";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 1 256 1 260 1 270 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1284";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 1 256 1 260 1 270 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1282";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 256 0 260 0 270 0;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1283";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 256 0 260 0 270 0;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1284";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  251 0 253 -89.814376831054688 256 -181.28311157226562
		 260 -276.59878540039062 263 -331.19198608398437 264 -344.6683349609375 265 -354.9453125
		 266 -358.23956298828125 270 -360;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes no no no no yes;
	setAttr -s 9 ".kix[0:8]"  1 0.0471368208527565 0.08756323903799057 
		0.13240520656108856 0.13007344305515289 0.17443452775478363 0.226273313164711 0.58680516481399536 
		1;
	setAttr -s 9 ".kiy[0:8]"  0 -0.99888843297958374 -0.99615901708602905 
		-0.99119561910629272 -0.99150437116622925 -0.98466873168945313 -0.97406387329101563 
		-0.80972820520401001 0;
	setAttr -s 9 ".kox[0:8]"  1 0.0471368208527565 0.08756323903799057 
		0.13240520656108856 0.17443342506885529 0.226273313164711 0.58679938316345215 0.98342865705490112 
		1;
	setAttr -s 9 ".koy[0:8]"  0 -0.99888843297958374 -0.99615901708602905 
		-0.99119561910629272 -0.98466891050338745 -0.97406387329101563 -0.80973237752914429 
		-0.1812957376241684 0;
createNode animCurveTL -n "animCurveTL1282";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  251 0 252 131.17205810546875 253 172.34063720703125
		 254 130.08648681640625 256 -1.756906270980835 258 -111.72095489501953 260 -159.44410705566406
		 261 -147.23429870605469 262 -123.39049530029297 263 -93.683624267578125 264 -63.796066284179688
		 265 -36.123695373535156 270 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes no no yes yes 
		yes no;
	setAttr -s 13 ".kix[0:12]"  1 0.00033736781915649772 1 0.00077035161666572094 
		0.00057559704873710871 0.00097029824974015355 1 0.0034125682432204485 0.001747479778714478 
		0.0013095794711261988 0.0014477648073807359 0.0023879637010395527 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.99999994039535522 0 -0.99999970197677612 
		-0.99999982118606567 -0.9999995231628418 0 0.99999421834945679 0.99999845027923584 
		0.99999916553497314 0.99999898672103882 0.99999713897705078 0;
	setAttr -s 13 ".kox[0:12]"  1 0.00033736781915649772 1 0.00077035161666572094 
		0.00057559704873710871 0.00097029824974015355 1 0.001747479778714478 0.0014025714481249452 
		0.0013095794711261988 0.0014477648073807359 0.0023879637010395527 1;
	setAttr -s 13 ".koy[0:12]"  0 0.99999994039535522 0 -0.99999970197677612 
		-0.99999982118606567 -0.9999995231628418 0 0.99999845027923584 0.99999904632568359 
		0.99999916553497314 0.99999898672103882 0.99999713897705078 0;
createNode animCurveTL -n "animCurveTL1283";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  251 0 252 55.394744873046875 254 266.51962280273437
		 256 319.23751831054687 258 270.2069091796875 260 149.4661865234375 261 93.075553894042969
		 262 52.669353485107422 263 23.64531135559082 264 6.2961807250976562 265 0.12226559221744537
		 266 -0.32377123832702637 270 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes no no yes yes yes 
		yes yes;
	setAttr -s 13 ".kix[0:12]"  1 0.00036508595803752542 0.0006316868239082396 
		1 0.00098171387799084187 0.00057511014165356755 0.0007388998637907207 0.0010311945807188749 
		0.0017970111221075058 0.003542603924870491 0.031123345717787743 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.99999994039535522 0.99999982118606567 
		0 -0.9999995231628418 -0.99999982118606567 -0.9999997615814209 -0.99999946355819702 
		-0.99999839067459106 -0.99999374151229858 -0.99951553344726563 0 0;
	setAttr -s 13 ".kox[0:12]"  1 0.00036508595803752542 0.0006316868239082396 
		1 0.00098171387799084187 0.00057511014165356755 0.0010311945807188749 0.001435565878637135 
		0.0017970111221075058 0.003542603924870491 0.031123345717787743 1 1;
	setAttr -s 13 ".koy[0:12]"  0 0.99999994039535522 0.99999982118606567 
		0 -0.9999995231628418 -0.99999982118606567 -0.99999946355819702 -0.99999892711639404 
		-0.99999839067459106 -0.99999374151229858 -0.99951553344726563 0 0;
createNode animCurveTL -n "animCurveTL1284";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  251 0 256 0 260 0 270 0;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1285";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1286";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1287";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 1 270 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1285";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 0 252 2.9725813865661621 255 2.0605828762054443
		 263 3.9397294521331792 268 0.0068011274561285973 270 0;
	setAttr -s 6 ".kit[0:5]"  2 1 1 1 1 1;
	setAttr -s 6 ".ktl[2:5]" no no yes yes;
	setAttr -s 6 ".kix[1:5]"  0.62617480754852295 0.99198967218399048 
		0.99519437551498413 0.94977432489395142 1;
	setAttr -s 6 ".kiy[1:5]"  0.7796826958656311 -0.12631897628307343 
		0.097919069230556488 -0.31293565034866333 0;
	setAttr -s 6 ".kox[0:5]"  0.62617766857147217 0.99198967218399048 
		0.99519443511962891 0.94977468252182007 0.99999898672103882 1;
	setAttr -s 6 ".koy[0:5]"  0.77968043088912964 -0.12631899118423462 
		0.097919143736362457 -0.31293472647666931 -0.0014244288904592395 0;
createNode animCurveTA -n "animCurveTA1286";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 0 252 20.200931549072266 255 19.187831878662109
		 263 27.464101791381836 268 10.912449836730957 270 0;
	setAttr -s 6 ".kit[0:5]"  2 1 1 1 1 1;
	setAttr -s 6 ".ktl[2:5]" no no no yes;
	setAttr -s 6 ".kix[1:5]"  0.11736215651035309 0.990142822265625 0.91755193471908569 
		0.58493179082870483 1;
	setAttr -s 6 ".kiy[1:5]"  0.99308919906616211 -0.14006105065345764 
		0.39761605858802795 -0.81108254194259644 0;
	setAttr -s 6 ".kox[0:5]"  0.11736308038234711 0.990142822265625 0.91755175590515137 
		0.58493292331695557 0.40084934234619141 1;
	setAttr -s 6 ".koy[0:5]"  0.99308907985687256 -0.14006105065345764 
		0.39761644601821899 -0.8110816478729248 -0.91614401340484619 0;
createNode animCurveTA -n "animCurveTA1287";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  251 0 252 -20.02882194519043 255 1.8564205169677734
		 263 11.407915115356445 268 0.035926099866628647 270 0;
	setAttr -s 6 ".ktl[0:5]" no no yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  1 0.11835673451423645 0.43323701620101929 
		1 0.99975442886352539 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.99297124147415161 0.90128004550933838 
		0 -0.022159410640597343 0;
	setAttr -s 6 ".kox[0:5]"  0.11835756897926331 0.31102052330970764 
		0.43323701620101929 1 0.99975442886352539 1;
	setAttr -s 6 ".koy[0:5]"  -0.99297100305557251 0.95040315389633179 
		0.90128004550933838 0 -0.022159410640597343 0;
createNode animCurveTL -n "animCurveTL1285";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 -54.946929931640625 270 -54.946929931640625;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1286";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 2.3482637405395508 270 2.3482637405395508;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1287";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  251 0 270 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 310;
	setAttr ".unw" 310;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "loopSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1249.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1250.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1251.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1249.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1250.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1251.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1249.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1250.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1251.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1252.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1253.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1254.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1252.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1253.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1254.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1252.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1253.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1254.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1255.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1256.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1257.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1255.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1256.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1257.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1255.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1256.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1257.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1258.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1259.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1260.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1258.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1259.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1260.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1258.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1259.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1260.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1261.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1262.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1263.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1261.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1262.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1263.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1261.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1262.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1263.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1264.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1265.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1266.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1264.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1265.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1266.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1264.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1265.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1266.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1267.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1268.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1269.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1267.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1268.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1269.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1267.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1268.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1269.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1270.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1271.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1272.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1270.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1271.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1272.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1270.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1271.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1272.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1273.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1274.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1275.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1273.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1274.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1275.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1273.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1274.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1275.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1276.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1277.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1278.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1276.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1277.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1278.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1276.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1277.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1278.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1279.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1280.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1281.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1279.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1280.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1281.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1279.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1280.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1281.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1282.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1283.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1284.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1282.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1283.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1284.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1282.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1283.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1284.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1285.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1286.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1287.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1285.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1286.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1287.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1285.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1286.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1287.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_loop.ma
