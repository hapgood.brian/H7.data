//Maya ASCII 2013 scene
//Name: dragon_get_hit_from_left.ma
//Last modified: Mon, Jul 14, 2014 12:44:50 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "get_hit_from_leftSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 101;
	setAttr ".se" 120;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU625";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU626";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU627";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA625";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA626";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA627";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL625";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 3.2171449661254883 120 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL626";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 -26.658763885498047 120 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL627";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 -1.5793838500976563 120 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU628";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 115 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU629";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 115 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU630";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 115 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA628";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 0 103 14.534157752990723 107 -76.902748107910156
		 110 -70.068840026855469 115 -14.013767242431641 120 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.33367940783500671 0.27313035726547241 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0.94268661737442017 0.96197700500488281 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 0.33367940783500671 0.27313035726547241 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0.94268661737442017 0.96197700500488281 
		0;
createNode animCurveTA -n "animCurveTA629";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 0 115 0 120 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA630";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 0 115 0 120 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL628";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 -4.7867727279663086 115 -4.7867727279663086
		 120 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL629";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 32.754745483398438 103 31.795219421386719
		 115 32.641860961914063 120 32.754745483398438;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.52396821975708008 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.8517378568649292 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.52396821975708008 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.8517378568649292 0;
createNode animCurveTL -n "animCurveTL630";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 -56.147525787353516 103 -52.036880493164062
		 115 -55.663921356201172 120 -56.147525787353516;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.14213930070400238 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 -0.98984658718109131 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.14213930070400238 1;
	setAttr -s 4 ".koy[0:3]"  0 0 -0.98984658718109131 0;
createNode animCurveTU -n "animCurveTU631";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU632";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU633";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA631";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA632";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA633";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL631";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 3.2171449661254883 120 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL632";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 -26.658763885498047 120 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL633";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1.5793838500976563 120 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU634";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU635";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU636";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA634";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  101 0 104 28.213159561157227 107 27.064813613891602
		 112 0.71175450086593628 120 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 0.901164710521698 0.99380815029144287 
		1;
	setAttr -s 5 ".kiy[0:4]"  0 0 -0.43347668647766113 -0.11110987514257431 
		0;
	setAttr -s 5 ".kox[0:4]"  1 1 0.901164710521698 0.99380815029144287 
		1;
	setAttr -s 5 ".koy[0:4]"  0 0 -0.43347668647766113 -0.11110987514257431 
		0;
createNode animCurveTA -n "animCurveTA635";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 0 107 2.0655460357666016 112 0 120 0;
	setAttr -s 4 ".ktl[0:3]" no yes no yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA636";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 0 107 -2.3886418342590332 112 0 120 0;
	setAttr -s 4 ".ktl[0:3]" no yes no yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL634";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 -4.7867727279663086 112 -4.7867727279663086
		 120 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL635";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 32.754745483398438 107 35.388702392578125
		 112 34.598518371582031 120 32.754745483398438;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.10331742465496063 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 -0.99464845657348633 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.10331742465496063 1;
	setAttr -s 4 ".koy[0:3]"  0 0 -0.99464845657348633 0;
createNode animCurveTL -n "animCurveTL636";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 56.147525787353516 107 50.219627380371094
		 112 51.997997283935547 120 56.147525787353516;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.046105183660984039 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.99893659353256226 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.046105183660984039 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.99893659353256226 0;
createNode animCurveTU -n "animCurveTU637";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU638";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU639";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA637";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA638";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA639";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL637";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 40.544437408447266 120 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL638";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 43.055271148681641 120 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL639";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU640";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU641";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU642";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA640";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA641";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA642";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL640";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 8.0282459259033203 120 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL641";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 9.9087905883789063 120 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL642";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU643";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 1 109 1 117 1 120 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU644";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 1 109 1 117 1 120 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU645";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 1 109 1 117 1 120 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA643";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  101 0 102 10.552135467529297 104 -86.635856628417969
		 106 -82.377975463867188 109 -11.935365676879883 111 5.675285816192627 114 -19.441884994506836
		 117 11.381820678710937 120 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 1 0.35012853145599365 0.092225134372711182 
		1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0.93670159578323364 0.99573814868927002 
		0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 0.35012853145599365 0.092225134372711182 
		1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0.93670159578323364 0.99573814868927002 
		0 0 0 0;
createNode animCurveTA -n "animCurveTA644";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  101 0 104 -0.54121017456054688 106 0.54942506551742554
		 109 -5.9443988800048828 111 -7.5678548812866211 114 -20.643405914306641 117 3.3258247375488281
		 120 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.84862232208251953 0.70005476474761963 
		1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 -0.5289992094039917 -0.71408909559249878 
		0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 0.84862232208251953 0.70005476474761963 
		1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 -0.5289992094039917 -0.71408909559249878 
		0 0 0;
createNode animCurveTA -n "animCurveTA645";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  101 0 104 28.782493591308594 106 29.422065734863278
		 109 15.218696594238283 111 11.667854309082031 114 24.11558723449707 117 2.6690034866333008
		 120 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.92788183689117432 1 0.40901145339012146 
		1 1 0.66668140888214111 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.37287434935569763 0 -0.91252923011779785 
		0 0 -0.74534285068511963 0;
	setAttr -s 8 ".kox[0:7]"  1 0.92788183689117432 1 0.40901145339012146 
		1 1 0.66668140888214111 1;
	setAttr -s 8 ".koy[0:7]"  0 0.37287434935569763 0 -0.91252923011779785 
		0 0 -0.74534285068511963 0;
createNode animCurveTL -n "animCurveTL643";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 -19.597047805786133 109 -19.597047805786133
		 117 -19.597047805786133 120 -19.597047805786133;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL644";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 36.545459747314453 109 36.545459747314453
		 117 36.545459747314453 120 36.545459747314453;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL645";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 39.212558746337891 109 39.212558746337891
		 117 39.212558746337891 120 39.212558746337891;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU646";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU647";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU648";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA646";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 0 103 3.5015206336975098 108 -3.939217329025269
		 111 -4.8842663764953613 116 -2.8686754703521729 120 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.92979776859283447 1 0.93324494361877441 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.36807090044021606 0 0.35924056172370911 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.92979776859283447 1 0.93324494361877441 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.36807090044021606 0 0.35924056172370911 
		0;
createNode animCurveTA -n "animCurveTA647";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 0 103 20.511039733886719 108 -8.9767169952392578
		 111 -13.630450248718262 116 -7.03204345703125 120 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.45643699169158936 1 0.66293686628341675 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.8897557258605957 0 0.74867534637451172 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.45643699169158936 1 0.66293686628341675 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.8897557258605957 0 0.74867534637451172 
		0;
createNode animCurveTA -n "animCurveTA648";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 0 103 -4.3226995468139648 108 -9.0165214538574219
		 111 -1.5600637197494507 116 3.8277661800384521 120 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.64662182331085205 1 0.6044732928276062 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.76281070709228516 0 0.79662543535232544 
		0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.64662182331085205 1 0.6044732928276062 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 -0.76281070709228516 0 0.79662543535232544 
		0 0;
createNode animCurveTL -n "animCurveTL646";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 51.6451416015625 103 51.586952209472656
		 108 51.456439971923828 111 51.364288330078125 116 51.43060302734375 120 51.6451416015625;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.83369898796081543 0.82622116804122925 
		1 0.7232135534286499 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.5522192120552063 -0.56334596872329712 
		0 0.69062447547912598 0;
	setAttr -s 6 ".kox[0:5]"  1 0.83369898796081543 0.82622116804122925 
		1 0.7232135534286499 1;
	setAttr -s 6 ".koy[0:5]"  0 -0.5522192120552063 -0.56334596872329712 
		0 0.69062447547912598 0;
createNode animCurveTL -n "animCurveTL647";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 -11.264523506164551 103 -10.301763534545898
		 108 -10.231081962585449 111 -11.26385498046875 116 -12.151564598083496 120 -11.264523506164551;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.7008395791053772 1 0.092132516205310822 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0.71331894397735596 0 -0.99574673175811768 
		0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.7008395791053772 1 0.092132516205310822 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 0.71331894397735596 0 -0.99574673175811768 
		0 0;
createNode animCurveTL -n "animCurveTL648";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  101 0 103 3.209270715713501 108 -3.3175864219665527
		 111 -4.4870624542236328 116 -2.8120462894439697 120 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.035605892539024353 1 0.047512732446193695 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.99936592578887939 0 0.9988706111907959 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.035605892539024353 1 0.047512732446193695 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.99936592578887939 0 0.9988706111907959 
		0;
createNode animCurveTU -n "animCurveTU649";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU650";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU651";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA649";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  101 0 103 -68.98681640625 106 36.251277923583984
		 109 50.560276031494141 111 48.635669708251953 114 39.24249267578125 117 -2.4917316436767578
		 120 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.16456612944602966 1 0.75919342041015625 
		0.27245217561721802 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.98636609315872192 0 -0.65086507797241211 
		-0.962169349193573 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.16456612944602966 1 0.75919342041015625 
		0.27245217561721802 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0.98636609315872192 0 -0.65086507797241211 
		-0.962169349193573 0 0;
createNode animCurveTA -n "animCurveTA650";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  101 0 103 -30.969709396362305 106 -0.64480406045913696
		 109 5.263798713684082 111 7.1049447059631348 114 9.8094511032104492 117 -2.0475137233734131
		 120 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.43171221017837524 0.85548514127731323 
		0.93428176641464233 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.90201139450073242 0.51782739162445068 
		0.35653549432754517 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.43171221017837524 0.85548514127731323 
		0.93428176641464233 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0.90201139450073242 0.51782739162445068 
		0.35653549432754517 0 0 0;
createNode animCurveTA -n "animCurveTA651";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  101 0 103 37.098281860351562 106 59.735298156738274
		 109 51.804573059082031 111 44.738895416259766 114 40.022663116455078 117 16.683032989501953
		 120 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.11718445271253586 1 0.36312055587768555 
		0.68308722972869873 0.4547153115272522 0.24103030562400818 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.99311017990112305 0 -0.93174213171005249 
		-0.73033684492111206 -0.89063680171966553 -0.97051763534545898 0;
	setAttr -s 8 ".kox[0:7]"  1 0.11718445271253586 1 0.36312055587768555 
		0.68308722972869873 0.4547153115272522 0.24103030562400818 1;
	setAttr -s 8 ".koy[0:7]"  0 0.99311017990112305 0 -0.93174213171005249 
		-0.73033684492111206 -0.89063680171966553 -0.97051763534545898 0;
createNode animCurveTL -n "animCurveTL649";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 -19.609930038452148 106 -20.540256500244141
		 120 -19.609930038452148;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL650";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 36.542636871337891 106 37.093368530273437
		 120 36.542636871337891;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL651";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 -39.208473205566406 106 -43.915534973144531
		 120 -39.208473205566406;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU652";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU653";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU654";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 1 112 1 120 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA652";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  101 0 102 -9.5518598556518555 103 -16.935991287231445
		 106 6.2262754440307617 109 8.9774618148803711 112 7.3384523391723633 120 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 0.16102659702301025 1 0.65539491176605225 
		1 0.87404745817184448 1;
	setAttr -s 7 ".kiy[0:6]"  0 -0.9869500994682312 0 0.75528639554977417 
		0 -0.48584064841270447 0;
	setAttr -s 7 ".kox[0:6]"  1 0.16102659702301025 1 0.65539491176605225 
		1 0.87404745817184448 1;
	setAttr -s 7 ".koy[0:6]"  0 -0.9869500994682312 0 0.75528639554977417 
		0 -0.48584064841270447 0;
createNode animCurveTA -n "animCurveTA653";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 0 112 0 120 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA654";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 0 112 0 120 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL652";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  101 -4.502251148223877 112 -4.502251148223877
		 120 -4.502251148223877;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL653";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  101 37.76336669921875 102 37.474746704101563
		 106 43.543212890625 107 43.580028533935547 108 41.652359008789063 109 39.124855041503906
		 112 36.216270446777344 117 39.532108306884766 120 37.76336669921875;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 0.35300132632255554 1 0.01340419240295887 
		0.023822950199246407 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0.93562281131744385 0 -0.99991017580032349 
		-0.99971622228622437 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.35300132632255554 1 0.01340419240295887 
		0.023822950199246407 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0.93562281131744385 0 -0.99991017580032349 
		-0.99971622228622437 0 0 0;
createNode animCurveTL -n "animCurveTL654";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  101 0 102 3.9458136558532715 106 -8.4793462753295898
		 107 -10.837972640991211 108 -11.414670944213867 109 -9.3194952011108398 112 -7.4307632446289063
		 117 -1.441595196723938 120 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 0.0084326006472110748 0.028378380462527275 
		1 0.030569516122341156 0.045554507523775101 0.049590405076742172 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 -0.99996447563171387 -0.99959725141525269 
		0 0.99953263998031616 0.99896180629730225 0.99876970052719116 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.0084326006472110748 0.028378380462527275 
		1 0.030569516122341156 0.045554507523775101 0.049590405076742172 1;
	setAttr -s 9 ".koy[0:8]"  0 0 -0.99996447563171387 -0.99959725141525269 
		0 0.99953263998031616 0.99896180629730225 0.99876970052719116 0;
createNode animCurveTU -n "animCurveTU655";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU656";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU657";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA655";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA656";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA657";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL655";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 -1.4725730419158936 120 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL656";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 22.299345016479492 120 22.299345016479492;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL657";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU658";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU659";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU660";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA658";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 0 104 21.474340438842773 107 27.701995849609375
		 120 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.3579433262348175 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0.93374329805374146 0 0;
	setAttr -s 4 ".kox[0:3]"  1 0.3579433262348175 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0.93374329805374146 0 0;
createNode animCurveTA -n "animCurveTA659";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA660";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL658";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL659";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 0 111 -4.7826738357543945 116 1.406557559967041
		 120 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL660";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  101 0 104 -27.000385284423828 107 -31.466835021972656
		 120 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.0093284090980887413 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 -0.99995648860931396 0 0;
	setAttr -s 4 ".kox[0:3]"  1 0.0093284090980887413 1 1;
	setAttr -s 4 ".koy[0:3]"  0 -0.99995648860931396 0 0;
createNode animCurveTU -n "animCurveTU661";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU662";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU663";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 1 120 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA661";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA662";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA663";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL661";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 -54.946929931640625 120 -54.946929931640625;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL662";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 2.3482637405395508 120 2.3482637405395508;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL663";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  101 0 120 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 120;
	setAttr ".unw" 120;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "get_hit_from_leftSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU625.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU626.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU627.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA625.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA626.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA627.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL625.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL626.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL627.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU628.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU629.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU630.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA628.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA629.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA630.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL628.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL629.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL630.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU631.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU632.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU633.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA631.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA632.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA633.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL631.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL632.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL633.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU634.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU635.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU636.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA634.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA635.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA636.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL634.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL635.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL636.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU637.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU638.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU639.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA637.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA638.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA639.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL637.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL638.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL639.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU640.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU641.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU642.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA640.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA641.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA642.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL640.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL641.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL642.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU643.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU644.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU645.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA643.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA644.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA645.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL643.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL644.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL645.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU646.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU647.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU648.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA646.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA647.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA648.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL646.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL647.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL648.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU649.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU650.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU651.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA649.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA650.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA651.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL649.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL650.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL651.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU652.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU653.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU654.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA652.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA653.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA654.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL652.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL653.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL654.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU655.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU656.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU657.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA655.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA656.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA657.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL655.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL656.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL657.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU658.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU659.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU660.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA658.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA659.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA660.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL658.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL659.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL660.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU661.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU662.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU663.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA661.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA662.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA663.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL661.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL662.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL663.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_get_hit_from_left.ma
