//Maya ASCII 2013 scene
//Name: dragon_get_hit_from_right.ma
//Last modified: Mon, Jul 14, 2014 12:45:38 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "get_hit_from_rightSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 121;
	setAttr ".se" 140;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU781";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU782";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU783";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA781";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA782";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA783";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL781";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 3.2171449661254883 140 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL782";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 -26.658763885498047 140 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL783";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 -1.5793838500976563 140 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU784";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 135 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU785";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 135 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU786";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 135 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA784";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 123 -37.736118316650391 127 8.8350372314453125
		 130 12.442283630371094 135 7.5144972801208496 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.5518951416015625 1 0.74045675992965698 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0.83391344547271729 0 -0.67210394144058228 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.5518951416015625 1 0.74045675992965698 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 0.83391344547271729 0 -0.67210394144058228 
		0;
createNode animCurveTA -n "animCurveTA785";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 0 135 0 140 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA786";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 0 135 0 140 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL784";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 -4.7867727279663086 135 -4.7867727279663086
		 140 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL785";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 32.754745483398438 123 31.795219421386719
		 135 32.641860961914063 140 32.754745483398438;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.52396821975708008 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.8517378568649292 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.52396821975708008 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.8517378568649292 0;
createNode animCurveTL -n "animCurveTL786";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 -56.147525787353516 123 -52.036880493164062
		 135 -55.663921356201172 140 -56.147525787353516;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.14213930070400238 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 -0.98984658718109131 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.14213930070400238 1;
	setAttr -s 4 ".koy[0:3]"  0 0 -0.98984658718109131 0;
createNode animCurveTU -n "animCurveTU787";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU788";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU789";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA787";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA788";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA789";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL787";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 3.2171449661254883 140 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL788";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 -26.658763885498047 140 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL789";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1.5793838500976563 140 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU790";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU791";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU792";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA790";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 124 -10.789311408996582 127 85.30572509765625
		 129 86.302238464355469 132 50.559104919433594 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.84756731986999512 1 0.1704593300819397 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0.53068780899047852 0 -0.98536473512649536 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.84756731986999512 1 0.1704593300819397 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 0.53068780899047852 0 -0.98536473512649536 
		0;
createNode animCurveTA -n "animCurveTA791";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 124 1.3429807424545288 127 1.4248238801956177
		 129 0.72253698110580444 132 0 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes no yes;
	setAttr -s 6 ".kix[0:5]"  1 0.99941295385360718 1 0.98263365030288696 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0.034262273460626602 0 -0.18555633723735809 
		0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.99941295385360718 1 0.98263365030288696 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 0.034262273460626602 0 -0.18555633723735809 
		0 0;
createNode animCurveTA -n "animCurveTA792";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 124 -0.83029943704605103 127 2.8180015087127686
		 129 1.9143686294555664 132 0 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes no yes;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.9051058292388916 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 -0.42518633604049683 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 0.9051058292388916 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 -0.42518633604049683 0 0;
createNode animCurveTL -n "animCurveTL790";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 -4.7867727279663086 132 -4.7867727279663086
		 140 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL791";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 32.754745483398438 127 35.388702392578125
		 132 34.598518371582031 140 32.754745483398438;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.10331742465496063 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 -0.99464845657348633 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.10331742465496063 1;
	setAttr -s 4 ".koy[0:3]"  0 0 -0.99464845657348633 0;
createNode animCurveTL -n "animCurveTL792";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 56.147525787353516 127 50.219627380371094
		 132 51.997997283935547 140 56.147525787353516;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 0.046105183660984039 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0.99893659353256226 0;
	setAttr -s 4 ".kox[0:3]"  1 1 0.046105183660984039 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0.99893659353256226 0;
createNode animCurveTU -n "animCurveTU793";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU794";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU795";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA793";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA794";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA795";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL793";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 40.544437408447266 140 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL794";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 43.055271148681641 140 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL795";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU796";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU797";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU798";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA796";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA797";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA798";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL796";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 8.0282459259033203 140 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL797";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 9.9087905883789063 140 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL798";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU799";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 1 129 1 137 1 140 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU800";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 1 129 1 137 1 140 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU801";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 1 129 1 137 1 140 1;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA799";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  121 0 122 -10.552136421203613 124 86.635856628417969
		 126 82.377975463867188 129 11.935365676879883 131 -5.675285816192627 134 19.441884994506836
		 137 -11.381819725036621 140 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 1 0.35012853145599365 0.092225134372711182 
		1 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 -0.93670165538787842 -0.99573814868927002 
		0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 0.35012853145599365 0.092225134372711182 
		1 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0 -0.93670165538787842 -0.99573814868927002 
		0 0 0 0;
createNode animCurveTA -n "animCurveTA800";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  121 0 124 -0.54121017456054688 126 0.54942506551742554
		 129 -5.9443988800048828 131 -7.5678548812866211 134 -20.643405914306641 137 3.3258247375488281
		 140 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 0.84862232208251953 0.70005476474761963 
		1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 -0.5289992094039917 -0.71408909559249878 
		0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 0.84862232208251953 0.70005476474761963 
		1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 -0.5289992094039917 -0.71408909559249878 
		0 0 0;
createNode animCurveTA -n "animCurveTA801";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  121 0 124 28.782493591308594 126 29.422065734863278
		 129 15.218696594238283 131 11.667854309082031 134 24.11558723449707 137 2.6690034866333008
		 140 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.92788183689117432 1 0.40901145339012146 
		1 1 0.66668140888214111 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.37287434935569763 0 -0.91252923011779785 
		0 0 -0.74534285068511963 0;
	setAttr -s 8 ".kox[0:7]"  1 0.92788183689117432 1 0.40901145339012146 
		1 1 0.66668140888214111 1;
	setAttr -s 8 ".koy[0:7]"  0 0.37287434935569763 0 -0.91252923011779785 
		0 0 -0.74534285068511963 0;
createNode animCurveTL -n "animCurveTL799";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 -19.597047805786133 129 -19.597047805786133
		 137 -19.597047805786133 140 -19.597047805786133;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL800";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 36.545459747314453 129 36.545459747314453
		 137 36.545459747314453 140 36.545459747314453;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL801";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 39.212558746337891 129 39.212558746337891
		 137 39.212558746337891 140 39.212558746337891;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU802";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU803";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU804";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA802";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 123 3.5015206336975098 128 -3.939217329025269
		 131 -4.8842663764953613 136 -2.8686754703521729 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.92979776859283447 1 0.93324494361877441 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.36807090044021606 0 0.35924056172370911 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.92979776859283447 1 0.93324494361877441 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.36807090044021606 0 0.35924056172370911 
		0;
createNode animCurveTA -n "animCurveTA803";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 123 20.511039733886719 128 -8.9767169952392578
		 131 -13.630450248718262 136 -7.03204345703125 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.45643699169158936 1 0.66293686628341675 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.8897557258605957 0 0.74867534637451172 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.45643699169158936 1 0.66293686628341675 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.8897557258605957 0 0.74867534637451172 
		0;
createNode animCurveTA -n "animCurveTA804";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 123 -4.3226995468139648 128 -9.0165214538574219
		 131 -1.5600637197494507 136 3.8277661800384521 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.64662182331085205 1 0.6044732928276062 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.76281070709228516 0 0.79662543535232544 
		0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.64662182331085205 1 0.6044732928276062 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 -0.76281070709228516 0 0.79662543535232544 
		0 0;
createNode animCurveTL -n "animCurveTL802";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 51.6451416015625 123 51.586952209472656
		 128 51.456439971923828 131 51.364288330078125 136 51.43060302734375 140 51.6451416015625;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.83369898796081543 0.82622116804122925 
		1 0.7232135534286499 1;
	setAttr -s 6 ".kiy[0:5]"  0 -0.5522192120552063 -0.56334596872329712 
		0 0.69062447547912598 0;
	setAttr -s 6 ".kox[0:5]"  1 0.83369898796081543 0.82622116804122925 
		1 0.7232135534286499 1;
	setAttr -s 6 ".koy[0:5]"  0 -0.5522192120552063 -0.56334596872329712 
		0 0.69062447547912598 0;
createNode animCurveTL -n "animCurveTL803";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 -11.264523506164551 123 -10.301763534545898
		 128 -10.231081962585449 131 -11.26385498046875 136 -12.151564598083496 140 -11.264523506164551;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 0.7008395791053772 1 0.092132516205310822 
		1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0.71331894397735596 0 -0.99574673175811768 
		0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.7008395791053772 1 0.092132516205310822 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 0.71331894397735596 0 -0.99574673175811768 
		0 0;
createNode animCurveTL -n "animCurveTL804";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  121 0 123 3.209270715713501 128 -3.3175864219665527
		 131 -4.4870624542236328 136 -2.8120462894439697 140 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.035605892539024353 1 0.047512732446193695 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.99936592578887939 0 0.9988706111907959 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.035605892539024353 1 0.047512732446193695 
		1;
	setAttr -s 6 ".koy[0:5]"  0 0 -0.99936592578887939 0 0.9988706111907959 
		0;
createNode animCurveTU -n "animCurveTU805";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU806";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU807";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA805";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  121 0 123 68.98681640625 126 -36.251277923583984
		 129 -50.560276031494141 131 -48.635669708251953 134 -39.24249267578125 137 2.4917318820953369
		 140 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.16456612944602966 1 0.75919336080551147 
		0.27245217561721802 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.98636609315872192 0 0.65086507797241211 
		0.962169349193573 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.16456612944602966 1 0.75919336080551147 
		0.27245217561721802 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 -0.98636609315872192 0 0.65086507797241211 
		0.962169349193573 0 0;
createNode animCurveTA -n "animCurveTA806";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  121 0 123 -30.969709396362305 126 -0.64480406045913696
		 129 5.263798713684082 131 7.1049447059631348 134 9.8094511032104492 137 -2.0475137233734131
		 140 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.43171221017837524 0.85548514127731323 
		0.93428176641464233 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.90201139450073242 0.51782739162445068 
		0.35653549432754517 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.43171221017837524 0.85548514127731323 
		0.93428176641464233 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0.90201139450073242 0.51782739162445068 
		0.35653549432754517 0 0 0;
createNode animCurveTA -n "animCurveTA807";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  121 0 123 37.098281860351562 126 59.735298156738274
		 129 51.804573059082031 131 44.738895416259766 134 40.022663116455078 137 16.683032989501953
		 140 0;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 0.11718445271253586 1 0.36312055587768555 
		0.68308722972869873 0.4547153115272522 0.24103030562400818 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.99311017990112305 0 -0.93174213171005249 
		-0.73033684492111206 -0.89063680171966553 -0.97051763534545898 0;
	setAttr -s 8 ".kox[0:7]"  1 0.11718445271253586 1 0.36312055587768555 
		0.68308722972869873 0.4547153115272522 0.24103030562400818 1;
	setAttr -s 8 ".koy[0:7]"  0 0.99311017990112305 0 -0.93174213171005249 
		-0.73033684492111206 -0.89063680171966553 -0.97051763534545898 0;
createNode animCurveTL -n "animCurveTL805";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 -19.609930038452148 126 -20.540256500244141
		 140 -19.609930038452148;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL806";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 36.542636871337891 126 37.093368530273437
		 140 36.542636871337891;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL807";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 -39.208473205566406 126 -43.915534973144531
		 140 -39.208473205566406;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU808";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU809";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU810";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 1 132 1 140 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA808";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  121 0 122 9.5518589019775391 123 16.935991287231445
		 126 -6.2262749671936035 129 -8.9774618148803711 132 -7.338451862335206 140 0;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 0.16102659702301025 1 0.6553947925567627 
		1 0.87404739856719971 1;
	setAttr -s 7 ".kiy[0:6]"  0 0.9869500994682312 0 -0.75528651475906372 
		0 0.48584064841270447 0;
	setAttr -s 7 ".kox[0:6]"  1 0.16102659702301025 1 0.6553947925567627 
		1 0.87404739856719971 1;
	setAttr -s 7 ".koy[0:6]"  0 0.9869500994682312 0 -0.75528651475906372 
		0 0.48584064841270447 0;
createNode animCurveTA -n "animCurveTA809";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 0 132 0 140 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA810";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 0 132 0 140 0;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL808";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  121 -4.502251148223877 132 -4.502251148223877
		 140 -4.502251148223877;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL809";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  121 37.76336669921875 122 37.474746704101563
		 126 43.543212890625 127 43.580028533935547 128 41.652359008789063 129 39.124855041503906
		 132 36.216270446777344 137 39.532108306884766 140 37.76336669921875;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 0.35300132632255554 1 0.01340419240295887 
		0.023822950199246407 1 1 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0.93562281131744385 0 -0.99991017580032349 
		-0.99971622228622437 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.35300132632255554 1 0.01340419240295887 
		0.023822950199246407 1 1 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0.93562281131744385 0 -0.99991017580032349 
		-0.99971622228622437 0 0 0;
createNode animCurveTL -n "animCurveTL810";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  121 0 122 -3.9458138942718506 126 8.4793462753295898
		 127 10.837972640991211 128 11.414670944213867 129 9.3194961547851562 132 7.430762767791748
		 137 1.4415949583053589 140 0;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes no;
	setAttr -s 9 ".kix[0:8]"  1 1 0.0084325959905982018 0.028378376737236977 
		1 0.030569525435566902 0.045554507523775101 0.049590405076742172 1;
	setAttr -s 9 ".kiy[0:8]"  0 0 0.99996447563171387 0.99959725141525269 
		0 -0.99953263998031616 -0.99896180629730225 -0.99876970052719116 0;
	setAttr -s 9 ".kox[0:8]"  1 1 0.0084325959905982018 0.028378376737236977 
		1 0.030569525435566902 0.045554507523775101 0.049590405076742172 1;
	setAttr -s 9 ".koy[0:8]"  0 0 0.99996447563171387 0.99959725141525269 
		0 -0.99953263998031616 -0.99896180629730225 -0.99876970052719116 0;
createNode animCurveTU -n "animCurveTU811";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU812";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU813";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA811";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA812";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA813";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL811";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 -1.4725730419158936 140 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL812";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 22.299345016479492 140 22.299345016479492;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL813";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU814";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU815";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU816";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA814";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 0 124 -21.474340438842773 127 -27.701995849609375
		 140 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.3579433262348175 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 -0.93374329805374146 0 0;
	setAttr -s 4 ".kox[0:3]"  1 0.3579433262348175 1 1;
	setAttr -s 4 ".koy[0:3]"  0 -0.93374329805374146 0 0;
createNode animCurveTA -n "animCurveTA815";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA816";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL814";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL815";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 0 131 -4.7826738357543945 136 1.406557559967041
		 140 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL816";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  121 0 124 27.000385284423828 127 31.466833114624023
		 140 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  1 0.0093284081667661667 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0.99995648860931396 0 0;
	setAttr -s 4 ".kox[0:3]"  1 0.0093284081667661667 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0.99995648860931396 0 0;
createNode animCurveTU -n "animCurveTU817";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU818";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU819";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 1 140 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA817";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA818";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA819";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL817";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 -54.946929931640625 140 -54.946929931640625;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL818";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 2.3482637405395508 140 2.3482637405395508;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL819";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  121 0 140 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
select -ne :time1;
	setAttr ".o" 140;
	setAttr ".unw" 140;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "get_hit_from_rightSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU781.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU782.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU783.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA781.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA782.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA783.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL781.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL782.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL783.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU784.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU785.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU786.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA784.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA785.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA786.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL784.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL785.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL786.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU787.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU788.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU789.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA787.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA788.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA789.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL787.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL788.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL789.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU790.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU791.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU792.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA790.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA791.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA792.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL790.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL791.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL792.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU793.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU794.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU795.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA793.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA794.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA795.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL793.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL794.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL795.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU796.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU797.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU798.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA796.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA797.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA798.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL796.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL797.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL798.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU799.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU800.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU801.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA799.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA800.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA801.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL799.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL800.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL801.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU802.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU803.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU804.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA802.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA803.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA804.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL802.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL803.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL804.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU805.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU806.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU807.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA805.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA806.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA807.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL805.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL806.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL807.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU808.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU809.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU810.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA808.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA809.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA810.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL808.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL809.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL810.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU811.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU812.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU813.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA811.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA812.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA813.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL811.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL812.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL813.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU814.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU815.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU816.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA814.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA815.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA816.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL814.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL815.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL816.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU817.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU818.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU819.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA817.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA818.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA819.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL817.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL818.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL819.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_get_hit_from_right.ma
