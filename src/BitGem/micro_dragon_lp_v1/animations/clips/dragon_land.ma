//Maya ASCII 2013 scene
//Name: dragon_land.ma
//Last modified: Mon, Jul 14, 2014 01:05:37 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "landSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 271;
	setAttr ".se" 310;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1405";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1406";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1407";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1405";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1406";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1407";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1405";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 3.2171449661254883 310 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1406";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -26.658763885498047 310 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1407";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -1.5793838500976563 310 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1408";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1409";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1410";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1408";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  271 0 275 -24.114261627197266 286 10.414916038513184
		 295 -13.936113357543945 300 -10.282938957214355 310 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 0.76724404096603394 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0.64135527610778809 0;
	setAttr -s 6 ".kox[0:5]"  0.17297296226024628 1 1 1 0.76724404096603394 
		1;
	setAttr -s 6 ".koy[0:5]"  -0.98492664098739624 0 0 0 0.64135527610778809 
		0;
createNode animCurveTA -n "animCurveTA1409";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 275 -24.667076110839844 295 -26.977142333984375
		 300 -12.038155555725098 310 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 0.98962992429733276 1 0.60127025842666626 
		1;
	setAttr -s 5 ".kiy[0:4]"  0 -0.14364054799079895 0 0.7990456223487854 
		0;
	setAttr -s 5 ".kox[0:4]"  0.39254391193389893 0.98962992429733276 
		1 0.60127025842666626 1;
	setAttr -s 5 ".koy[0:4]"  -0.91973328590393066 -0.14364054799079895 
		0 0.7990456223487854 0;
createNode animCurveTA -n "animCurveTA1410";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 275 2.0662462711334229 295 -2.5969016551971436
		 300 5.6778178215026855 310 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes no;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  0.99991101026535034 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  -0.013338525779545307 0 0 0 0;
createNode animCurveTL -n "animCurveTL1408";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -4.7867727279663086 310 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1409";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 32.754745483398438 310 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1410";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -56.147525787353516 310 -56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1411";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1412";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1413";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1411";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1412";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1413";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1411";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 3.2171449661254883 310 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1412";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -26.658763885498047 310 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1413";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1.5793838500976563 310 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1414";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1415";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1416";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1414";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  271 0 275 20.413934707641602 286 -11.838081359863281
		 295 10.430399894714355 300 17.70063591003418 310 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.50120019912719727 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0.86533141136169434 0 0;
	setAttr -s 6 ".kox[0:5]"  0.17316514253616333 1 1 0.50120019912719727 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0.98489278554916382 0 0 0.86533141136169434 
		0 0;
createNode animCurveTA -n "animCurveTA1415";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  271 0 275 1.3721617460250854 286 -12.618378639221191
		 295 -6.0234756469726562 300 -0.43405249714851379 310 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.81536626815795898 0.998515784740448 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0.57894545793533325 0.054463688284158707 
		0;
	setAttr -s 6 ".kox[0:5]"  0.99043399095535278 1 1 0.81536626815795898 
		0.998515784740448 1;
	setAttr -s 6 ".koy[0:5]"  -0.13798739016056061 0 0 0.57894545793533325 
		0.054463688284158707 0;
createNode animCurveTA -n "animCurveTA1416";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  271 0 275 -35.545444488525391 286 3.581923246383667
		 295 -11.169399261474609 300 -11.224898338317871 310 0;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.99990272521972656 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 -0.013947089202702045 0 0;
	setAttr -s 6 ".kox[0:5]"  0.24700731039047241 1 1 0.99990272521972656 
		1 1;
	setAttr -s 6 ".koy[0:5]"  -0.96901369094848633 0 0 -0.013947089202702045 
		0 0;
createNode animCurveTL -n "animCurveTL1414";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -4.7867727279663086 310 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1415";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 32.754745483398438 310 32.754745483398438;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1416";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 56.147525787353516 310 56.147525787353516;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1417";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1418";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1419";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1417";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1418";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1419";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1417";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 40.544437408447266 310 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1418";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 43.055271148681641 310 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1419";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1420";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1421";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1422";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1420";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1421";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1422";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1420";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 8.0282459259033203 310 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1421";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 9.9087905883789063 310 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1422";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1423";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 1 273 1 310 1;
	setAttr -s 3 ".ktl[2]" no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1424";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 1 273 1 310 1;
	setAttr -s 3 ".ktl[2]" no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1425";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 1 273 1 310 1;
	setAttr -s 3 ".ktl[2]" no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1423";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 40.397052764892578 273 -79.549003601074219
		 276 46.25201416015625 278 32.535018920898438 280 -82.934394836425781 282 32.535018920898438
		 284 -82.934394836425781 286 32.535018920898438 288 -82.934394836425781 294 32.535018920898438
		 296 -82.934394836425781 298 32.535018920898438 300 -82.934394836425781 302 32.535018920898438
		 306 -82.934394836425781 310 7.4917421340942392;
	setAttr -s 16 ".kix[0:15]"  1 1 1 0.11525440216064453 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 -0.993336021900177 0 0 0 0 0 0 0 
		0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 0.11525440216064453 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 -0.993336021900177 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1424";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 0 273 2.6004264354705811 310 0;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1425";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 32.840129852294922 273 32.856006622314453
		 310 32.840129852294922;
	setAttr -s 3 ".ktl[2]" no;
	setAttr -s 3 ".kix[0:2]"  0.99995023012161255 1 1;
	setAttr -s 3 ".kiy[0:2]"  0.0099747339263558388 0 0;
	setAttr -s 3 ".kox[0:2]"  0.99995023012161255 1 0.072503961622714996;
	setAttr -s 3 ".koy[0:2]"  0.0099747339263558388 0 -0.99736815690994263;
createNode animCurveTL -n "animCurveTL1423";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 -19.597047805786133 273 -18.70335578918457
		 310 -19.597047805786133;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1424";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 36.545459747314453 273 40.988937377929687
		 310 36.545459747314453;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1425";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 39.212558746337891 273 36.634956359863281
		 310 39.212558746337891;
	setAttr -s 3 ".ktl[0:2]" no yes no;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1426";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1427";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1428";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1426";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1427";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1428";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1426";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 51.6451416015625 310 51.6451416015625;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1427";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -11.264523506164551 310 -11.264523506164551;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1428";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1429";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  271 1 273 1 278 1 282 1 284 1 286 1 288 1
		 294 1 296 1 298 1 300 1 302 1 306 1 310 1;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1430";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  271 1 273 1 278 1 282 1 284 1 286 1 288 1
		 294 1 296 1 298 1 300 1 302 1 306 1 310 1;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1431";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  271 1 273 1 278 1 282 1 284 1 286 1 288 1
		 294 1 296 1 298 1 300 1 302 1 306 1 310 1;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1429";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 -32.905311584472656 273 83.925910949707031
		 276 -31.701253890991211 278 -25.04327392578125 280 90.426139831542969 282 -25.04327392578125
		 284 90.426139831542969 286 -25.04327392578125 288 90.426139831542969 294 -25.04327392578125
		 296 90.426139831542969 298 -25.04327392578125 300 90.426139831542969 302 -25.04327392578125
		 306 90.426139831542969 310 0;
	setAttr -s 16 ".ktl[15]" no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 0.23249351978302002 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0.97259789705276489 0 0 0 0 0 0 0 
		0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 0.23249351978302002 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0.97259789705276489 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1430";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 -3.5807332992553711 273 17.360256195068359
		 276 -6.5291533470153809 278 -7.7918944358825684 280 -1.813296318054199 282 -7.7918944358825684
		 284 -1.813296318054199 286 -7.7918944358825684 288 -1.813296318054199 294 -7.7918944358825684
		 296 -1.813296318054199 298 -7.7918944358825684 300 -1.813296318054199 302 -7.7918944358825684
		 306 -1.813296318054199 310 0;
	setAttr -s 16 ".ktl[15]" no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.78338319063186646 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 -0.62153899669647217 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 0.78338319063186646 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 -0.62153899669647217 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1431";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 49.030250549316406 273 40.117877960205078
		 276 45.582130432128906 278 47.532573699951172 280 47.822013854980469 282 47.532573699951172
		 284 47.822013854980469 286 47.532573699951172 288 47.822013854980469 294 47.532573699951172
		 296 47.822013854980469 298 47.532573699951172 300 47.822013854980469 302 47.532573699951172
		 306 47.822013854980469 310 0;
	setAttr -s 16 ".ktl[15]" no;
	setAttr -s 16 ".kix[0:15]"  1 1 0.65136659145355225 0.98386240005493164 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0.75876319408416748 0.17892682552337646 
		0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 0.65136659145355225 0.98386240005493164 
		1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0.75876319408416748 0.17892682552337646 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1429";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 -19.609930038452148 273 -19.714313507080078
		 276 -20.086034774780273 278 -19.975275039672852 280 -19.975275039672852 282 -19.975275039672852
		 284 -19.975275039672852 286 -19.975275039672852 288 -19.975275039672852 294 -19.975275039672852
		 296 -19.975275039672852 298 -19.975275039672852 300 -19.975275039672852 302 -19.975275039672852
		 306 -19.975275039672852 310 -19.609930038452148;
	setAttr -s 16 ".ktl[0:15]" no yes yes no yes yes yes yes yes yes yes 
		yes yes yes no no;
	setAttr -s 16 ".kix[0:15]"  1 0.27457225322723389 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 -0.96156644821166992 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 0.27457225322723389 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 -0.96156644821166992 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1430";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 43.318531036376953 273 45.618278503417969
		 276 43.381893157958984 278 40.274101257324219 280 40.274101257324219 282 40.274101257324219
		 284 40.274101257324219 286 40.274101257324219 288 40.274101257324219 294 40.274101257324219
		 296 40.274101257324219 298 40.274101257324219 300 40.274101257324219 302 40.274101257324219
		 306 40.274101257324219 310 36.542636871337891;
	setAttr -s 16 ".ktl[3:15]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 16 ".kix[0:15]"  0.012077750638127327 1 0.020704777911305428 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0.9999271035194397 0 -0.9997856616973877 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  0.012077750638127327 1 0.020704777911305428 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0.9999271035194397 0 -0.9997856616973877 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1431";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  271 -39.208473205566406 273 -38.521728515625
		 276 -38.042190551757813 278 -36.804862976074219 280 -36.804862976074219 282 -36.804862976074219
		 284 -36.804862976074219 286 -36.804862976074219 288 -36.804862976074219 294 -36.804862976074219
		 296 -36.804862976074219 298 -36.804862976074219 300 -36.804862976074219 302 -36.804862976074219
		 306 -36.804862976074219 310 -39.208473205566406;
	setAttr -s 16 ".ktl[0:15]" no yes yes no yes yes yes yes yes yes yes 
		yes yes yes no no;
	setAttr -s 16 ".kix[0:15]"  1 0.16337572038173676 0.10643404722213745 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0.98656392097473145 0.99431973695755005 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 0.16337572038173676 0.10643404722213745 
		1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0.98656392097473145 0.99431973695755005 
		0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1432";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1433";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1434";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1432";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1433";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1434";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  271 -7.0125217437744141 293 -7.8022685050964355
		 310 0;
	setAttr -s 3 ".ktl[2]" no;
	setAttr -s 3 ".kix[0:2]"  0.32227599620819092 1 1;
	setAttr -s 3 ".kiy[0:2]"  -0.94664573669433594 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1432";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -4.502251148223877 310 -4.502251148223877;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1433";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 45.318515777587891 310 37.76336669921875;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1434";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1435";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1436";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1437";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1435";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1436";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1437";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1435";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -1.4725730419158936 310 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1436";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  271 29.854494094848633 278 26.330440521240234
		 285 23.380947113037109 310 22.299345016479492;
	setAttr -s 4 ".ktl[0:3]" no yes yes no;
	setAttr -s 4 ".kix[0:3]"  0.0055149164982140064 0.082127846777439117 
		0.29380986094474792 1;
	setAttr -s 4 ".kiy[0:3]"  0.99998480081558228 -0.99662172794342041 
		-0.9558638334274292 0;
	setAttr -s 4 ".kox[0:3]"  0.14331820607185364 0.082127846777439117 
		0.29380971193313599 1;
	setAttr -s 4 ".koy[0:3]"  -0.98967665433883667 -0.99662172794342041 
		-0.95586389303207397 0;
createNode animCurveTL -n "animCurveTL1437";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0 310 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1438";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 1 283 1 288 1 303 1 310 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1439";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 1 283 1 288 1 303 1 310 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1440";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 1 283 1 288 1 303 1 310 1;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1438";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 283 0 288 0 303 0 310 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1439";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 283 0 288 0 303 0 310 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1440";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 283 0 288 0 303 0 310 0;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes yes;
	setAttr -s 5 ".kix[0:4]"  0.0066313110291957855 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0.99997800588607788 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1438";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 283 0 288 0 303 0 310 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1439";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  271 309.53347778320313 283 53.636260986328125
		 303 2.6545455455780029 310 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes yes;
	setAttr -s 4 ".kix[0:3]"  0.00013461077469401062 0.0087946290150284767 
		0.035734180361032486 1;
	setAttr -s 4 ".kiy[0:3]"  1 -0.99996131658554077 -0.99936127662658691 
		0;
	setAttr -s 4 ".kox[0:3]"  0.00072956387884914875 0.0087946290150284767 
		0.035734180361032486 1;
	setAttr -s 4 ".koy[0:3]"  -0.9999997615814209 -0.99996131658554077 
		-0.99936127662658691 0;
createNode animCurveTL -n "animCurveTL1440";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  271 0 283 0 288 0 303 0 310 0;
	setAttr -s 5 ".kix[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".kiy[0:4]"  0 0 0 0 0;
	setAttr -s 5 ".kox[0:4]"  1 1 1 1 1;
	setAttr -s 5 ".koy[0:4]"  0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1441";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1442";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1443";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 1 310 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1441";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  271 -4.8159346580505371 280 24.402734756469727
		 301 1.5596892833709717 310 0;
	setAttr -s 4 ".ktl[3]" no;
	setAttr -s 4 ".kix[0:3]"  0.44414135813713074 1 0.97709876298904419 
		1;
	setAttr -s 4 ".kiy[0:3]"  -0.89595669507980347 0 -0.21278640627861023 
		0;
	setAttr -s 4 ".kox[0:3]"  0.90413415431976318 1 0.97709876298904419 
		1;
	setAttr -s 4 ".koy[0:3]"  0.42724868655204773 0 -0.21278640627861023 
		0;
createNode animCurveTA -n "animCurveTA1442";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  271 -10.889255523681641 280 34.315910339355469
		 301 -7.8305664062499991 310 0;
	setAttr -s 4 ".ktl[3]" no;
	setAttr -s 4 ".kix[0:3]"  0.21415211260318756 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  -0.97680032253265381 0 0 0;
	setAttr -s 4 ".kox[0:3]"  0.9016880989074707 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0.43238699436187744 0 0 0;
createNode animCurveTA -n "animCurveTA1443";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  271 33.232242584228516 280 32.171669006347656
		 301 -6.993506908416748 310 0;
	setAttr -s 4 ".ktl[3]" no;
	setAttr -s 4 ".kix[0:3]"  0.071653507649898529 0.69340842962265015 
		1 1;
	setAttr -s 4 ".kiy[0:3]"  0.99742954969406128 -0.72054469585418701 
		0 0;
	setAttr -s 4 ".kox[0:3]"  0.32814168930053711 0.69340842962265015 
		1 1;
	setAttr -s 4 ".koy[0:3]"  -0.94462853670120239 -0.72054469585418701 
		0 0;
createNode animCurveTL -n "animCurveTL1441";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 -64.720123291015625 310 -54.946929931640625;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  0.0042633544653654099 1;
	setAttr -s 2 ".kiy[0:1]"  -0.99999088048934937 0;
	setAttr -s 2 ".kox[0:1]"  0.10290993005037308 1;
	setAttr -s 2 ".koy[0:1]"  0.99469071626663208 0;
createNode animCurveTL -n "animCurveTL1442";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 2.2900974750518799 310 2.3482637405395508;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  0.5823441743850708 1;
	setAttr -s 2 ".kiy[0:1]"  -0.81294238567352295 0;
	setAttr -s 2 ".kox[0:1]"  0.026564616709947586 1;
	setAttr -s 2 ".koy[0:1]"  -0.99964714050292969 0;
createNode animCurveTL -n "animCurveTL1443";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  271 0.90679603815078735 310 0;
	setAttr -s 2 ".ktl[1]" no;
	setAttr -s 2 ".kix[0:1]"  0.04590124636888504 1;
	setAttr -s 2 ".kiy[0:1]"  0.99894601106643677 0;
	setAttr -s 2 ".kox[0:1]"  0.14160336554050446 1;
	setAttr -s 2 ".koy[0:1]"  0.98992341756820679 0;
select -ne :time1;
	setAttr ".o" 310;
	setAttr ".unw" 310;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "landSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1405.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1406.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1407.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1405.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1406.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1407.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1405.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1406.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1407.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1408.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1409.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1410.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1408.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1409.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1410.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1408.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1409.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1410.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1411.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1412.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1413.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1411.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1412.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1413.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1411.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1412.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1413.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1414.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1415.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1416.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1414.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1415.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1416.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1414.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1415.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1416.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1417.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1418.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1419.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1417.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1418.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1419.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1417.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1418.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1419.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1420.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1421.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1422.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1420.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1421.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1422.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1420.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1421.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1422.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1423.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1424.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1425.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1423.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1424.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1425.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1423.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1424.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1425.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1426.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1427.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1428.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1426.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1427.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1428.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1426.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1427.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1428.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1429.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1430.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1431.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1429.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1430.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1431.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1429.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1430.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1431.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1432.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1433.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1434.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1432.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1433.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1434.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1432.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1433.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1434.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1435.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1436.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1437.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1435.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1436.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1437.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1435.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1436.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1437.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1438.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1439.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1440.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1438.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1439.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1440.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1438.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1439.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1440.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1441.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1442.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1443.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1441.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1442.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1443.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1441.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1442.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1443.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_land.ma
