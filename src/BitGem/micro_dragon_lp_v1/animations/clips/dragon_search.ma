//Maya ASCII 2013 scene
//Name: dragon_search.ma
//Last modified: Mon, Jul 14, 2014 01:08:03 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "searchSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 347;
	setAttr ".se" 411;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1717";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1718";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1719";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1717";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1718";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1719";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1717";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 3.2171449661254883 411 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1718";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -26.658763885498047 411 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1719";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -1.5793838500976563 411 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1720";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1721";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1722";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1720";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 -18.352380752563477 357 -4.1801977157592773
		 360 -13.603762626647949 367 2.4833061695098877 375 4.8186869621276855 382 -2.1509265899658203
		 390 -16.193643569946289 404 -15.630568504333496 411 -10.662186791888033;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  0.015182428993284702 1 1 1 0.93882346153259277 
		1 0.74365144968032837 1 0.99872517585754395 0.29166412353515625;
	setAttr -s 10 ".kiy[0:9]"  0.99988466501235962 0 0 0 0.34439870715141296 
		0 -0.66856753826141357 0 0.050476949661970139 0.1232849508523941;
	setAttr -s 10 ".kox[0:9]"  0.36354541778564453 1 1 1 0.93882346153259277 
		1 0.74365144968032837 1 0.99872517585754395 0.29166412353515625;
	setAttr -s 10 ".koy[0:9]"  -0.93157649040222168 0 0 0 0.34439870715141296 
		0 -0.66856753826141357 0 0.050477392971515656 0.12328493595123291;
createNode animCurveTA -n "animCurveTA1721";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 -24.421012878417969 357 -14.576563835144043
		 360 -21.582595825195313 367 20.525264739990234 375 11.819466590881348 382 -0.48389452695846552
		 390 -10.345715522766113 404 -14.338242530822754 411 -6.634759496625585;
	setAttr -s 10 ".kix[0:9]"  0.49759289622306824 1 1 1 1 0.85899263620376587 
		0.78253650665283203 0.95300066471099854 1 0.29166412353515625;
	setAttr -s 10 ".kiy[0:9]"  -0.86741071939468384 0 0 0 0 -0.51198792457580566 
		-0.62260466814041138 -0.30296820402145386 0 0.20262756943702698;
	setAttr -s 10 ".kox[0:9]"  0.28141844272613525 1 1 1 1 0.85899263620376587 
		0.78253650665283203 0.95300066471099854 1 0.29166412353515625;
	setAttr -s 10 ".koy[0:9]"  -0.95958513021469116 0 0 0 0 -0.51198792457580566 
		-0.62260466814041138 -0.30296820402145386 0 0.20262758433818817;
createNode animCurveTA -n "animCurveTA1722";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 28.261690139770508 357 14.699428558349611
		 360 40.974765777587891 367 4.6623530387878418 375 10.174643516540527 382 3.0273966789245605
		 390 23.019340515136719 404 25.780160903930664 411 16.645605222091138;
	setAttr -s 10 ".kix[0:9]"  0.24364009499549866 1 1 1 1 1 1 0.97064042091369629 
		1 0.29166412353515625;
	setAttr -s 10 ".kiy[0:9]"  -0.96986567974090576 0 0 0 0 0 0 0.24053514003753662 
		0 -0.24063229560852051;
	setAttr -s 10 ".kox[0:9]"  0.24565108120441437 1 1 1 1 1 1 0.97064042091369629 
		1 0.29166412353515625;
	setAttr -s 10 ".koy[0:9]"  0.96935826539993286 0 0 0 0 0 0 0.24053514003753662 
		0 -0.24063226580619812;
createNode animCurveTL -n "animCurveTL1720";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -4.7867727279663086 411 -4.7867727279663086;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1721";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 32.754745483398438 411 32.754745483398438;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1722";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -56.147525787353516 411 -56.147525787353516;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1723";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1724";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1725";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1723";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1724";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1725";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1723";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 3.2171449661254883 411 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1724";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -26.658763885498047 411 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1725";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1.5793838500976563 411 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1726";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1727";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1728";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1726";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 4.8213415145874023 358 4.1335635185241699
		 361 14.06214427947998 370 15.531326293945314 375 12.028477668762207 384 7.767996311187745
		 394 5.0410537719726563 406 5.2159795761108398 411 5.0947328569659849;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  0.015924690291285515 1 1 0.97960102558135986 
		1 0.941680908203125 0.98801064491271973 1 1 0.20833396911621094;
	setAttr -s 10 ".kiy[0:9]"  -0.99987316131591797 0 0 0.20095200836658478 
		0 -0.33650729060173035 -0.15438540279865265 0 0 -0.004002460278570652;
	setAttr -s 10 ".kox[0:9]"  0.82954579591751099 1 1 0.97960102558135986 
		1 0.941680908203125 0.98801064491271973 1 1 0.54166603088378906;
	setAttr -s 10 ".koy[0:9]"  0.55843877792358398 0 0 0.20095200836658478 
		0 -0.33650729060173035 -0.15438540279865265 0 0 -0.010406344197690487;
createNode animCurveTA -n "animCurveTA1727";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 10.918863296508789 358 2.3981034755706787
		 361 -0.59749311208724976 370 10.853014945983887 375 24.850448608398438 384 16.212600708007813
		 394 5.0824236869812012 406 1.1481328010559082 411 2.6684529972752391;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  0.15010032057762146 1 0.65207481384277344 
		1 0.55875414609909058 1 0.73960137367248535 0.95735704898834229 1 0.20833396911621094;
	setAttr -s 10 ".kiy[0:9]"  -0.98867076635360718 0 -0.75815469026565552 
		0 0.82933342456817627 0 -0.67304521799087524 -0.28890761733055115 0 0.047038577497005463;
	setAttr -s 10 ".kox[0:9]"  0.54846739768981934 1 0.65207481384277344 
		1 0.55875414609909058 1 0.73960137367248535 0.95735704898834229 1 0.54166603088378906;
	setAttr -s 10 ".koy[0:9]"  0.83617192506790161 0 -0.75815469026565552 
		0 0.82933342456817627 0 -0.67304521799087524 -0.28890761733055115 0 0.12229978293180466;
createNode animCurveTA -n "animCurveTA1728";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 0 350 10.378559112548828 358 0.22596044838428497
		 361 -16.867368698120117 370 17.496425628662109 375 17.357364654541016 384 7.5343036651611328
		 394 -5.9098196029663086 406 -10.897102355957031 411 -8.2083563780080553;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  0.29674521088600159 1 0.53124797344207764 
		1 1 0.99938982725143433 0.81199067831039429 0.93828332424163818 1 0.20833396911621094;
	setAttr -s 10 ".kiy[0:9]"  0.95495671033859253 0 -0.84721642732620239 
		0 0 -0.034928224980831146 -0.58367043733596802 -0.34586772322654724 0 0.084185831248760223;
	setAttr -s 10 ".kox[0:9]"  0.56796646118164063 1 0.53124797344207764 
		1 1 0.99938982725143433 0.81199067831039429 0.93828332424163818 1 0.54166603088378906;
	setAttr -s 10 ".koy[0:9]"  0.82305175065994263 0 -0.84721642732620239 
		0 0 -0.034928224980831146 -0.58367043733596802 -0.34586772322654724 0 0.21888218820095062;
createNode animCurveTL -n "animCurveTL1726";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -4.7867727279663086 411 -4.7867727279663086;
	setAttr -s 2 ".kix[0:1]"  0.10084491223096848 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0.99490219354629517 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1727";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 32.754745483398438 411 32.754745483398438;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  0.0023208046332001686 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  -0.99999731779098511 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1728";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 56.147525787353516 411 56.147525787353516;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  0.015743521973490715 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0.99987608194351196 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1729";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1730";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1731";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1729";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1730";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1731";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1729";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 40.544437408447266 411 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1730";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 43.055271148681641 411 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1731";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1732";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1733";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1734";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1732";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1733";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1734";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1732";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 8.0282459259033203 411 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1733";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 9.9087905883789063 411 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1734";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4583320617675781;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1735";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 1 377 1 382 1 387 1 392 1 397 1 402 1
		 407 1 411 1;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1736";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 1 377 1 382 1 387 1 392 1 397 1 402 1
		 407 1 411 1;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1737";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 1 377 1 382 1 387 1 392 1 397 1 402 1
		 407 1 411 1;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1735";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  347 0 351 -76.069984436035156 357 -66.031654357910156
		 361 13.833324432373047 367 -91.210166931152344 372 -0.55291026830673218 377 -91.210166931152344
		 382 -0.55291026830673218 387 -91.210166931152344 392 -0.55291026830673218 397 -91.210166931152344
		 402 -0.55291026830673218 407 -91.210166931152344 411 -9.9811056023581468;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes;
	setAttr -s 14 ".kix[0:13]"  0.1147770956158638 1 0.4295298159122467 
		1 1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 14 ".kiy[0:13]"  0.99339127540588379 0 0.90305262804031372 
		0 0 0 0 0 0 0 0 0 0 1.2151761054992676;
	setAttr -s 14 ".kox[0:13]"  0.12455575168132782 1 0.4295298159122467 
		1 1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 14 ".koy[0:13]"  -0.99221259355545044 0 0.90305262804031372 
		0 0 0 0 0 0 0 0 0 0 0.30379050970077515;
createNode animCurveTA -n "animCurveTA1736";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  347 0 357 -26.511959075927734 361 24.148157119750977
		 367 -25.249216079711914 372 12.489650726318359 377 -25.249216079711914 382 12.489650726318359
		 387 -25.249216079711914 392 12.489650726318359 397 -25.249216079711914 402 12.489650726318359
		 407 -25.249216079711914 411 8.564874916520159;
	setAttr -s 13 ".kix[0:12]"  0.10981984436511993 1 1 1 1 1 1 1 1 1 1 
		1 0.16666603088378906;
	setAttr -s 13 ".kiy[0:12]"  -0.99395149946212769 0 0 0 0 0 0 0 0 0 
		0 0 0.50585430860519409;
	setAttr -s 13 ".kox[0:12]"  0.39899340271949768 1 1 1 1 1 1 1 1 1 1 
		1 0.041667938232421875;
	setAttr -s 13 ".koy[0:12]"  -0.91695386171340942 0 0 0 0 0 0 0 0 0 
		0 0 0.12646211683750153;
createNode animCurveTA -n "animCurveTA1737";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  347 0 351 14.044892311096191 357 49.062751770019531
		 361 15.712542533874512 367 43.414539337158203 372 35.671676635742187 377 43.414539337158203
		 382 35.671676635742187 387 43.414539337158203 392 35.671676635742187 397 43.414539337158203
		 402 35.671676635742187 407 43.414539337158203 411 36.476920746152267;
	setAttr -s 14 ".kix[0:13]"  1 0.22103187441825867 1 1 1 1 1 1 1 1 1 
		1 1 0.16666603088378906;
	setAttr -s 14 ".kiy[0:13]"  0 0.97526657581329346 0 0 0 0 0 0 0 0 0 
		0 0 -0.10378583520650864;
	setAttr -s 14 ".kox[0:13]"  1 0.22103187441825867 1 1 1 1 1 1 1 1 1 
		1 1 0.041667938232421875;
	setAttr -s 14 ".koy[0:13]"  0 0.97526657581329346 0 0 0 0 0 0 0 0 0 
		0 0 -0.025946173816919327;
createNode animCurveTL -n "animCurveTL1735";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 -19.597047805786133 377 -19.597047805786133
		 382 -19.597047805786133 387 -19.597047805786133 392 -19.597047805786133 397 -19.597047805786133
		 402 -19.597047805786133 407 -19.597047805786133 411 -19.597047805786133;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1736";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 36.545459747314453 377 36.545459747314453
		 382 36.545459747314453 387 36.545459747314453 392 36.545459747314453 397 36.545459747314453
		 402 36.545459747314453 407 36.545459747314453 411 36.545459747314453;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1737";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 39.212558746337891 377 39.212558746337891
		 382 39.212558746337891 387 39.212558746337891 392 39.212558746337891 397 39.212558746337891
		 402 39.212558746337891 407 39.212558746337891 411 39.212558746337891;
	setAttr -s 9 ".ktl[0:8]" no yes yes yes yes yes yes yes yes;
	setAttr -s 9 ".kix[0:8]"  1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 9 ".kiy[0:8]"  0 0 0 0 0 0 0 0 0;
	setAttr -s 9 ".kox[0:8]"  1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 9 ".koy[0:8]"  0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1738";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 357 1 411 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 2.25;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1739";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 357 1 411 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 2.25;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1740";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 357 1 411 1;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 2.25;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1738";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  347 0 349 -1.147135853767395 357 -1.193290114402771
		 359 -0.55048567056655884 369 1.5239039659500122 379 3.3021323680877686 388 2.7588815689086914
		 411 0.80925571056700329;
	setAttr -s 8 ".kix[0:7]"  0.63780170679092407 0.99997377395629883 
		1 0.98920708894729614 0.99676042795181274 1 0.99811625480651855 0.95833396911621094;
	setAttr -s 8 ".kiy[0:7]"  0.77020061016082764 -0.0072497008368372917 
		0 0.14652428030967712 0.080427646636962891 0 -0.061350762844085693 -0.021209491416811943;
	setAttr -s 8 ".kox[0:7]"  0.97233128547668457 0.99997377395629883 
		1 0.98920708894729614 0.99676042795181274 1 0.99811625480651855 0.041667938232421875;
	setAttr -s 8 ".koy[0:7]"  -0.23360623419284821 -0.0072497008368372917 
		0 0.14652428030967712 0.080427646636962891 0 -0.061350762844085693 -0.00092213839525356889;
createNode animCurveTA -n "animCurveTA1739";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  347 0 349 -5.8298015594482422 357 0.56177258491516113
		 359 7.0088419914245605 369 -0.87373995780944824 379 -4.9002113342285156 388 -6.893303394317627
		 411 8.9030285004767968;
	setAttr -s 8 ".kix[0:7]"  0.43210116028785706 1 0.70569884777069092 
		1 0.92420428991317749 0.99156510829925537 1 0.95833396911621094;
	setAttr -s 8 ".kiy[0:7]"  -0.90182512998580933 0 0.70851194858551025 
		0 -0.38189849257469177 -0.12960904836654663 0 0.063621722161769867;
	setAttr -s 8 ".kox[0:7]"  0.63362306356430054 1 0.70569884777069092 
		1 0.92420428991317749 0.99156510829925537 1 0.041667938232421875;
	setAttr -s 8 ".koy[0:7]"  -0.77364194393157959 0 0.70851194858551025 
		0 -0.38189849257469177 -0.12960904836654663 0 0.0027661246713250875;
createNode animCurveTA -n "animCurveTA1740";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  347 0 349 -0.32535433769226074 357 0.34192198514938354
		 359 0.86192762851715088 369 -2.2279999256134033 379 -2.280015230178833 388 -0.94087725877761852
		 411 -0.4804172134156286;
	setAttr -s 8 ".kix[0:7]"  0.075506679713726044 1 0.99455171823501587 
		1 0.99997866153717041 1 0.99981528520584106 0.95833396911621094;
	setAttr -s 8 ".kiy[0:7]"  0.99714535474777222 0 0.10424444824457169 
		0 -0.0065362765453755856 0 0.019220178946852684 0.006110102403908968;
	setAttr -s 8 ".kox[0:7]"  0.99768638610839844 1 0.99455171823501587 
		1 0.99997866153717041 1 0.99981528520584106 0.041667938232421875;
	setAttr -s 8 ".koy[0:7]"  -0.067983873188495636 0 0.10424444824457169 
		0 -0.0065362765453755856 0 0.019220180809497833 0.00026565376901999116;
createNode animCurveTL -n "animCurveTL1738";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  347 51.6451416015625 349 51.900699615478516
		 357 51.730789184570313 359 52.207759857177734 411 51.782633880437189;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes yes;
	setAttr -s 5 ".kix[0:4]"  0.011319450102746487 1 1 1 2.1666641235351562;
	setAttr -s 5 ".kiy[0:4]"  -0.99993592500686646 0 0 0 -0.58393734693527222;
	setAttr -s 5 ".kox[0:4]"  0.31002110242843628 1 1 1 0.041667938232421875;
	setAttr -s 5 ".koy[0:4]"  0.9507296085357666 0 0 0 -0.011224090121686459;
createNode animCurveTL -n "animCurveTL1739";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  347 -11.264523506164551 349 -11.421937942504883
		 357 -11.673226356506348 359 -11.823294639587402 411 -11.39220039443927;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes yes;
	setAttr -s 5 ".kix[0:4]"  0.0054908771999180317 0.60344576835632324 
		0.61643916368484497 1 2.1666641235351562;
	setAttr -s 5 ".kiy[0:4]"  -0.99998492002487183 -0.79740399122238159 
		-0.78740262985229492 0 0.54606938362121582;
	setAttr -s 5 ".kox[0:4]"  0.46787458658218384 0.60344576835632324 
		0.61643916368484497 1 0.041667938232421875;
	setAttr -s 5 ".koy[0:4]"  -0.88379484415054321 -0.79740399122238159 
		-0.78740262985229492 0 0.0105025889351964;
createNode animCurveTL -n "animCurveTL1740";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  347 0 349 -1.6538584232330322 357 -0.81868577003479004
		 359 1.2962758541107178 411 1.2625163947598852;
	setAttr -s 5 ".ktl[0:4]" no yes yes yes yes;
	setAttr -s 5 ".kix[0:4]"  0.027898350730538368 1 0.13187772035598755 
		1 2.1666641235351562;
	setAttr -s 5 ".kiy[0:4]"  -0.99961072206497192 0 0.99126601219177246 
		0 -0.10127837955951691;
	setAttr -s 5 ".kox[0:4]"  0.050323769450187683 1 0.13187772035598755 
		1 0.041667938232421875;
	setAttr -s 5 ".koy[0:4]"  -0.99873292446136475 0 0.99126601219177246 
		0 -0.0019476875895634294;
createNode animCurveTU -n "animCurveTU1741";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 1 372 1 377 1 382 1 387 1 392 1 397 1
		 402 1 407 1 411 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1742";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 1 372 1 377 1 382 1 387 1 392 1 397 1
		 402 1 407 1 411 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1743";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 1 372 1 377 1 382 1 387 1 392 1 397 1
		 402 1 407 1 411 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1741";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  347 0 350 77.585823059082031 353 92.387771606445313
		 358 47.687965393066406 362 -26.429576873779297 367 90.097190856933594 372 -26.429576873779297
		 377 90.097190856933594 382 -26.429576873779297 387 90.097190856933594 392 -26.429576873779297
		 397 90.097190856933594 402 -26.429576873779297 407 90.097190856933594 411 -14.310997862541035;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 15 ".kix[0:14]"  0.15391828119754791 0.15922686457633972 
		1 0.088662594556808472 1 1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 15 ".kiy[0:14]"  0.9880836009979248 0.98724198341369629 
		0 -0.99606174230575562 0 0 0 0 0 0 0 0 0 0 -1.5619326829910278;
	setAttr -s 15 ".kox[0:14]"  0.091919533908367157 0.15922686457633972 
		1 0.088662594556808472 1 1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 15 ".koy[0:14]"  0.99576646089553833 0.98724198341369629 
		0 -0.99606174230575562 0 0 0 0 0 0 0 0 0 0 -0.3904787003993988;
createNode animCurveTA -n "animCurveTA1742";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  347 0 350 21.678476333618164 353 20.893610000610352
		 358 17.426675796508789 362 2.4861574172973633 367 24.85004997253418 372 2.4861574172973633
		 377 24.85004997253418 382 2.4861574172973633 387 24.85004997253418 392 2.4861574172973633
		 397 24.85004997253418 402 2.4861574172973633 407 24.85004997253418 411 4.8119629314071348;
	setAttr -s 15 ".kix[0:14]"  0.097633451223373413 1 0.98057752847671509 
		0.79549187421798706 1 1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 15 ".kiy[0:14]"  0.99522238969802856 0 -0.19613173604011536 
		-0.60596424341201782 0 0 0 0 0 0 0 0 0 0 -0.2997671365737915;
	setAttr -s 15 ".kox[0:14]"  0.3136964738368988 1 0.98057752847671509 
		0.79549187421798706 1 1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 15 ".koy[0:14]"  0.94952327013015747 0 -0.19613173604011536 
		-0.60596424341201782 0 0 0 0 0 0 0 0 0 0 -0.07494092732667923;
createNode animCurveTA -n "animCurveTA1743";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  347 0 350 56.694412231445313 353 54.232120513916016
		 358 50.7972412109375 362 18.746026992797852 367 55.607784271240234 372 18.746026992797852
		 377 55.607784271240234 382 18.746026992797852 387 55.607784271240234 392 18.746026992797852
		 397 55.607784271240234 402 18.746026992797852 407 55.607784271240234 411 22.579584953525114;
	setAttr -s 15 ".kix[0:14]"  1 1 0.95358526706695557 0.75695681571960449 
		1 1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 15 ".kiy[0:14]"  0 0 -0.30112329125404358 -0.65346485376358032 
		0 0 0 0 0 0 0 0 0 0 -0.49409756064414978;
	setAttr -s 15 ".kox[0:14]"  1 1 0.95358526706695557 0.75695681571960449 
		1 1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 15 ".koy[0:14]"  0 0 -0.30112329125404358 -0.65346485376358032 
		0 0 0 0 0 0 0 0 0 0 -0.12352291494607925;
createNode animCurveTL -n "animCurveTL1741";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 -19.609930038452148 372 -19.609930038452148
		 377 -19.609930038452148 382 -19.609930038452148 387 -19.609930038452148 392 -19.609930038452148
		 397 -19.609930038452148 402 -19.609930038452148 407 -19.609930038452148 411 -19.609930038452148;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1742";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 36.542636871337891 372 36.542636871337891
		 377 36.542636871337891 382 36.542636871337891 387 36.542636871337891 392 36.542636871337891
		 397 36.542636871337891 402 36.542636871337891 407 36.542636871337891 411 36.542636871337891;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1743";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  347 -39.208473205566406 372 -39.208473205566406
		 377 -39.208473205566406 382 -39.208473205566406 387 -39.208473205566406 392 -39.208473205566406
		 397 -39.208473205566406 402 -39.208473205566406 407 -39.208473205566406 411 -39.208473205566406;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 0.16666603088378906;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 0.041667938232421875;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1744";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1745";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1746";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1744";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  347 0 349 2.7669055461883545 357 2.2511858940124512
		 360 -3.0108215808868408 378 -0.83418542146682739 392 0.68260711431503296 411 -1.1277796952504684;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes yes;
	setAttr -s 7 ".kix[0:6]"  1 1 0.9967348575592041 1 0.99085885286331177 
		1 0.79166793823242188;
	setAttr -s 7 ".kiy[0:6]"  0 0 -0.080744542181491852 0 0.13490241765975952 
		0 -0.04249279573559761;
	setAttr -s 7 ".kox[0:6]"  0.86522078514099121 1 0.9967348575592041 
		1 0.99085885286331177 1 0.54166603088378906;
	setAttr -s 7 ".koy[0:6]"  0.50139099359512329 0 -0.080744542181491852 
		0 0.13490241765975952 0 -0.029074005782604218;
createNode animCurveTA -n "animCurveTA1745";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  347 0 349 -7.4300370216369629 357 -9.4605426788330078
		 360 23.034152984619141 378 20.748838424682617 392 -12.584556579589844 411 -0.95589596108078712;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes yes;
	setAttr -s 7 ".kix[0:6]"  1 0.95271384716033936 1 1 0.98751062154769897 
		1 0.79166793823242188;
	setAttr -s 7 ".kiy[0:6]"  0 -0.30386906862258911 0 0 -0.15755234658718109 
		0 0.27294403314590454;
	setAttr -s 7 ".kox[0:6]"  0.54061597585678101 0.95271384716033936 
		1 1 0.98751062154769897 1 0.54166603088378906;
	setAttr -s 7 ".koy[0:6]"  -0.84126949310302734 -0.30386906862258911 
		0 0 -0.15755234658718109 0 0.18675112724304199;
createNode animCurveTA -n "animCurveTA1746";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  347 0 349 -0.18468128144741058 357 -0.038327895104885101
		 360 2.4339675903320313 378 0.41737216711044312 392 2.7138121128082275 411 2.723464393512566;
	setAttr -s 7 ".ktl[0:6]" no yes yes yes yes yes yes;
	setAttr -s 7 ".kix[0:6]"  1 1 0.99973583221435547 1 1 0.99999988079071045 
		0.79166793823242188;
	setAttr -s 7 ".kiy[0:6]"  0 0 0.022983061149716377 0 0 0.0004062855732627213 
		5.3079442295711488e-005;
	setAttr -s 7 ".kox[0:6]"  0.99925273656845093 1 0.99973583221435547 
		1 1 0.99999994039535522 0.54166603088378906;
	setAttr -s 7 ".koy[0:6]"  -0.038650359958410263 0 0.022983061149716377 
		0 0 0.00040628711576573551 3.6316348996479064e-005;
createNode animCurveTL -n "animCurveTL1744";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -4.502251148223877 411 -4.502251148223877;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1745";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 37.76336669921875 411 37.76336669921875;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1746";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1747";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1748";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".kix[0:1]"  0.26086121797561646 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0.96537625789642334 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1749";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 1 411 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1747";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1748";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA1749";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1747";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 -1.4725730419158936 411 -1.4725730419158936;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1748";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 22.299345016479492 411 22.299345016479492;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL1749";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  347 0 411 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 2.6666679382324219;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1.4166641235351563;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU1750";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 1 351 1 363 1 411 1;
	setAttr -s 4 ".ktl[0:3]" no yes yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1751";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 1 351 1 363 1 411 1;
	setAttr -s 4 ".kix[0:3]"  0.45959380269050598 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  0.88812923431396484 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU1752";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 1 351 1 363 1 411 1;
	setAttr -s 4 ".ktl[0:3]" no yes yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1750";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 0 351 0 363 0 411 0;
	setAttr -s 4 ".kix[0:3]"  0.24691171944141388 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  0.96903800964355469 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA1751";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  347 0 351 -11.295269966125488 359 -14.119087219238281
		 363 7.2918925285339347 383 12.644637107849121 411 -9.8130502385837275;
	setAttr -s 6 ".kix[0:5]"  1 0.91410994529724121 1 0.94782978296279907 
		1 1.1666679382324219;
	setAttr -s 6 ".kiy[0:5]"  0 -0.40546643733978271 0 0.31877690553665161 
		0 -0.27346137166023254;
	setAttr -s 6 ".kox[0:5]"  1 0.91410994529724121 1 0.94782978296279907 
		1 0.20833396911621094;
	setAttr -s 6 ".koy[0:5]"  0 -0.40546643733978271 0 0.31877690553665161 
		0 -0.048832539469003677;
createNode animCurveTA -n "animCurveTA1752";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 0 351 0 363 0 411 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes yes;
	setAttr -s 4 ".kix[0:3]"  0.01349042821675539 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  0.99990904331207275 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL1750";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  347 0 351 -0.11524292081594467 359 -0.14405365288257599
		 363 1.8689652681350708 383 1.5097658634185791 411 -0.049265550687811022;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  0.00082396564539521933 0.96798813343048096 
		1 1 0.61174345016479492 1.1666679382324219;
	setAttr -s 6 ".kiy[0:5]"  0.99999964237213135 -0.25099611282348633 
		0 0 -0.79105621576309204 -0.71027237176895142;
	setAttr -s 6 ".kox[0:5]"  0.82251977920532227 0.96798813343048096 
		1 1 0.61174285411834717 0.20833396911621094;
	setAttr -s 6 ".koy[0:5]"  -0.56873643398284912 -0.25099611282348633 
		0 0 -0.79105669260025024 -0.12683470547199249;
createNode animCurveTL -n "animCurveTL1751";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  347 0 351 0 363 0 411 0;
	setAttr -s 4 ".ktl[0:3]" no yes yes yes;
	setAttr -s 4 ".kix[0:3]"  0.00039183077751658857 1 1 1.9999980926513672;
	setAttr -s 4 ".kiy[0:3]"  -0.99999994039535522 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 0.20833396911621094;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL1752";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  347 0 351 -7.6558980941772461 359 -9.5698728561401367
		 363 -1.3011295795440674 383 4.6103830337524414 411 -6.8964413504559712;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes yes;
	setAttr -s 6 ".kix[0:5]"  0.0012579974718391895 0.057954970747232437 
		1 0.046937495470046997 1 1.1666679382324219;
	setAttr -s 6 ".kiy[0:5]"  -0.99999922513961792 -0.99831914901733398 
		0 0.99889779090881348 0 -8.0280313491821289;
	setAttr -s 6 ".kox[0:5]"  0.021764591336250305 0.057954970747232437 
		1 0.046937495470046997 1 0.20833396911621094;
	setAttr -s 6 ".koy[0:5]"  -0.9997631311416626 -0.99831914901733398 
		0 0.99889779090881348 0 -1.4335817098617554;
createNode animCurveTU -n "animCurveTU1753";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 368 1 411 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1754";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 368 1 411 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU1755";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 1 368 1 411 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA1753";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  347 0 357 -2.3214211463928223 361 1.413865327835083
		 368 1.2732589244842529 382 -7.5648698806762704 394 -7.9772453308105469 411 -1.4157199000037646;
	setAttr -s 7 ".kix[0:6]"  0.95998662710189819 1 1 1 0.99906885623931885 
		1 0.70833206176757813;
	setAttr -s 7 ".kiy[0:6]"  -0.28004574775695801 0 0 0 -0.043143656104803085 
		0 0.094775103032588959;
	setAttr -s 7 ".kox[0:6]"  0.95998662710189819 1 1 1 0.99906885623931885 
		1 0.16666603088378906;
	setAttr -s 7 ".koy[0:6]"  -0.28004574775695801 0 0 0 -0.043143656104803085 
		0 0.022299913689494133;
createNode animCurveTA -n "animCurveTA1754";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  347 0 350 48.137622833251953 357 35.873260498046875
		 361 -27.149482727050781 368 -24.683996200561523 382 47.098731994628906 394 51.509532928466797
		 411 -0.59325588279150376;
	setAttr -s 8 ".kix[0:7]"  0.27231824398040771 1 0.41353914141654968 
		0.13645784556865692 0.25012287497520447 0.90783524513244629 1 0.70833206176757813;
	setAttr -s 8 ".kiy[0:7]"  0.96220725774765015 0 -0.91048640012741089 
		-0.9906458854675293 0.96821409463882446 0.41932699084281921 0 -0.75257605314254761;
	setAttr -s 8 ".kox[0:7]"  0.27231824398040771 1 0.41353914141654968 
		0.13645784556865692 0.25012287497520447 0.90783524513244629 1 0.16666603088378906;
	setAttr -s 8 ".koy[0:7]"  0.96220725774765015 0 -0.91048640012741089 
		-0.9906458854675293 0.96821409463882446 0.41932699084281921 0 -0.17707587778568268;
createNode animCurveTA -n "animCurveTA1755";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  347 0 357 -3.0042014122009277 361 -17.076021194458008
		 368 -12.243083000183105 375 -30.779043197631839 382 -23.568235397338867 394 -17.047666549682617
		 404 -29.297491073608398 411 -26.838663949771426;
	setAttr -s 9 ".kix[0:8]"  0.93555206060409546 0.93555206060409546 
		0.97041583061218262 1 1 0.82583737373352051 1 1 0.29166412353515625;
	setAttr -s 9 ".kiy[0:8]"  -0.35318875312805176 -0.35318875312805176 
		-0.24143961071968079 0 0 0.56390839815139771 0 0 0.11318747699260712;
	setAttr -s 9 ".kox[0:8]"  0.93555206060409546 0.93555206060409546 
		0.97041583061218262 1 1 0.82583737373352051 1 1 0.16666603088378906;
	setAttr -s 9 ".koy[0:8]"  -0.35318875312805176 -0.35318875312805176 
		-0.24143961071968079 0 0 0.56390839815139771 0 0 0.064678475260734558;
createNode animCurveTL -n "animCurveTL1753";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 -54.946929931640625 368 -54.946929931640625
		 411 -54.946929931640625;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1754";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 2.3482637405395508 368 2.3482637405395508
		 411 2.3482637405395508;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL1755";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  347 0 368 0 411 0;
	setAttr -s 3 ".ktl[0:2]" no yes yes;
	setAttr -s 3 ".kix[0:2]"  1 1 1.7916641235351562;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1.4583320617675781;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
select -ne :time1;
	setAttr ".o" 411;
	setAttr ".unw" 411;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "searchSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1717.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1718.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1719.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1717.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1718.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1719.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1717.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1718.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1719.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1720.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1721.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1722.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1720.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1721.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1722.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1720.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1721.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1722.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1723.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1724.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1725.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1723.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1724.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1725.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1723.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1724.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1725.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1726.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1727.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1728.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1726.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1727.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1728.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1726.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1727.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1728.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1729.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1730.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1731.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1729.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1730.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1731.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1729.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1730.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1731.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1732.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1733.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1734.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1732.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1733.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1734.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1732.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1733.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1734.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1735.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1736.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1737.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1735.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1736.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1737.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1735.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1736.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1737.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1738.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1739.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1740.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1738.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1739.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1740.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1738.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1739.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1740.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1741.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1742.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1743.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1741.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1742.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1743.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1741.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1742.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1743.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1744.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1745.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1746.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1744.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1745.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1746.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1744.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1745.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1746.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1747.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1748.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1749.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1747.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1748.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1749.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1747.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1748.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1749.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1750.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1751.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1752.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1750.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1751.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1752.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1750.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1751.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1752.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1753.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1754.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1755.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1753.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1754.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1755.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1753.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1754.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1755.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_search.ma
