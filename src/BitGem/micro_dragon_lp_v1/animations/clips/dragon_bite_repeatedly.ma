//Maya ASCII 2013 scene
//Name: dragon_bite_repeatedly.ma
//Last modified: Mon, Jul 14, 2014 01:37:29 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "bite_repeatedlySource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 512;
	setAttr ".se" 559;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU2029";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2030";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2031";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2029";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2030";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2031";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2029";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 3.2171449661254883 524 3.2171449661254883
		 526 3.2171449661254883 538 3.2171449661254883 540 3.2171449661254883 552 3.2171449661254883
		 554 3.2171449661254883 559 3.2171449661254883;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2030";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -26.658763885498047 524 -26.658763885498047
		 526 -26.658763885498047 538 -26.658763885498047 540 -26.658763885498047 552 -26.658763885498047
		 554 -26.658763885498047 559 -26.658763885498047;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2031";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -1.5793838500976563 524 -1.5793838500976563
		 526 -1.5793838500976563 538 -1.5793838500976563 540 -1.5793838500976563 552 -1.5793838500976563
		 554 -1.5793838500976563 559 -1.5793838500976563;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2032";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2033";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2034";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2032";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 -14.552690505981444 518 -9.3637275695800781
		 524 5.311272144317627 526 0 528 -14.552690505981444 532 -9.3637275695800781 538 5.311272144317627
		 540 0 542 -14.552690505981444 546 -9.3637275695800781 552 5.311272144317627 554 4.9914240837097168
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes yes 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 0.52289247512817383 0.69849544763565063 
		0.66853940486907959 1 0.52289247512817383 0.69849544763565063 0.66853940486907959 
		1 0.52289247512817383 1 0.98039793968200684 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0.85239869356155396 0.7156144380569458 
		-0.74367672204971313 0 0.85239869356155396 0.7156144380569458 -0.74367672204971313 
		0 0.85239869356155396 0 -0.19702731072902679 0;
	setAttr -s 14 ".kox[0:13]"  0.31174331903457642 1 0.52289247512817383 
		0.66853374242782593 0.31174331903457642 1 0.52289247512817383 0.66854220628738403 
		0.31174331903457642 1 0.52289247512817383 1 0.98039793968200684 1;
	setAttr -s 14 ".koy[0:13]"  -0.95016634464263916 0 0.85239869356155396 
		-0.74368178844451904 -0.95016634464263916 0 0.85239869356155396 -0.74367421865463257 
		-0.95016634464263916 0 0.85239869356155396 0 -0.19702731072902679 0;
createNode animCurveTA -n "animCurveTA2033";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 -15.798990249633791 518 -8.1717729568481445
		 524 2.7149062156677246 526 0 528 -15.798990249633791 532 -8.1717729568481445 538 2.7149062156677246
		 540 0 542 -15.798990249633791 546 -8.1717729568481445 552 2.7149062156677246 554 2.55141282081604
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes no 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 0.49888303875923157 0.79614937305450439 
		0.8692970871925354 1 0.49888303875923157 0.79614937305450439 0.8692970871925354 1 
		0.49888303875923157 0.79614937305450439 0.99941426515579224 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0.8666694164276123 0.60510021448135376 
		-0.4942898154258728 0 0.8666694164276123 0.60510021448135376 -0.4942898154258728 
		0 0.8666694164276123 0.60510021448135376 -0.034222450107336044 0;
	setAttr -s 14 ".kox[0:13]"  0.28928989171981812 1 0.49888303875923157 
		0.8692970871925354 0.28928989171981812 1 0.49888303875923157 0.8692970871925354 0.28928989171981812 
		1 0.49888303875923157 0.99941426515579224 0.97791004180908203 1;
	setAttr -s 14 ".koy[0:13]"  -0.95724153518676758 0 0.8666694164276123 
		-0.4942898154258728 -0.95724153518676758 0 0.8666694164276123 -0.4942898154258728 
		-0.95724153518676758 0 0.8666694164276123 -0.034221667796373367 -0.20902629196643829 
		0;
createNode animCurveTA -n "animCurveTA2034";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 44.818260192871094 518 -25.893665313720703
		 524 -19.001291275024414 526 0 528 44.818260192871094 532 -25.893665313720703 538 -19.001291275024414
		 540 0 542 44.818260192871094 546 -25.893665313720703 552 -19.001291275024414 554 -17.857021331787109
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes yes 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 1 0.90110832452774048 0.24370412528514862 
		1 1 0.90110832452774048 0.24370412528514862 1 1 0.90110838413238525 0.97246247529983521 
		1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0.43359395861625671 0.96984964609146118 
		0 0 0.43359395861625671 0.96984964609146118 0 0 0.43359392881393433 0.23305948078632355 
		0;
	setAttr -s 14 ".kox[0:13]"  0.10593409091234207 1 1 0.24370047450065613 
		0.10593409091234207 1 1 0.24370571970939636 0.10593409091234207 1 1 0.97246372699737549 
		0.55572694540023804 1;
	setAttr -s 14 ".koy[0:13]"  0.9943731427192688 0 0 0.96985059976577759 
		0.9943731427192688 0 0 0.96984928846359253 0.9943731427192688 0 0 0.23305441439151764 
		0.83136492967605591 0;
createNode animCurveTL -n "animCurveTL2032";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -4.7867727279663086 524 -4.7867727279663086
		 526 -4.7867727279663086 538 -4.7867727279663086 540 -4.7867727279663086 552 -4.7867727279663086
		 554 -4.7867727279663086 559 -4.7867727279663086;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2033";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 32.754745483398438 524 32.754745483398438
		 526 32.754745483398438 538 32.754745483398438 540 32.754745483398438 552 32.754745483398438
		 554 32.754745483398438 559 32.754745483398438;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2034";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -56.147525787353516 524 -56.147525787353516
		 526 -56.147525787353516 538 -56.147525787353516 540 -56.147525787353516 552 -56.147525787353516
		 554 -56.147525787353516 559 -56.147525787353516;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2035";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2036";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2037";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2035";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2036";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2037";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2035";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 3.2171449661254883 524 3.2171449661254883
		 526 3.2171449661254883 538 3.2171449661254883 540 3.2171449661254883 552 3.2171449661254883
		 554 3.2171449661254883 559 3.2171449661254883;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2036";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -26.658763885498047 524 -26.658763885498047
		 526 -26.658763885498047 538 -26.658763885498047 540 -26.658763885498047 552 -26.658763885498047
		 554 -26.658763885498047 559 -26.658763885498047;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2037";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1.5793838500976563 524 1.5793838500976563
		 526 1.5793838500976563 538 1.5793838500976563 540 1.5793838500976563 552 1.5793838500976563
		 554 1.5793838500976563 559 1.5793838500976563;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2038";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2039";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2040";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2038";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 1.1756051778793335 518 10.647705078125
		 524 -1.7353605031967163 526 0 528 1.1756051778793335 532 10.647705078125 538 -1.7353605031967163
		 540 0 542 1.1756051778793335 546 10.647705078125 552 -1.7353605031967163 554 -1.630855917930603
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes no 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 0.80435913801193237 1 0.75649929046630859 
		0.93984866142272949 0.80435913801193237 1 0.75649929046630859 0.93984866142272949 
		0.80435913801193237 1 0.75649929046630859 0.99976050853729248 1;
	setAttr -s 14 ".kiy[0:13]"  0 0.5941433310508728 0 -0.65399450063705444 
		0.34159094095230103 0.5941433310508728 0 -0.65399450063705444 0.34159094095230103 
		0.5941433310508728 0 -0.65399450063705444 0.021882478147745132 0;
	setAttr -s 14 ".kox[0:13]"  0.97100037336349487 0.80435913801193237 
		1 0.93984711170196533 0.97100037336349487 0.80435913801193237 1 0.93984955549240112 
		0.97100037336349487 0.80435913801193237 1 0.99976050853729248 0.99079519510269165 
		1;
	setAttr -s 14 ".koy[0:13]"  0.23907816410064697 0.5941433310508728 
		0 0.34159547090530396 0.23907816410064697 0.5941433310508728 0 0.3415885865688324 
		0.23907816410064697 0.5941433310508728 0 0.021881988272070885 0.13536946475505829 
		0;
createNode animCurveTA -n "animCurveTA2039";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 8.5887165069580078 518 -5.2819881439208984
		 524 -4.3195915222167969 526 0 528 8.5887165069580078 532 -5.2819881439208984 538 -4.3195915222167969
		 540 0 542 8.5887165069580078 546 -5.2819881439208984 552 -4.3195915222167969 554 -4.0594630241394043
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes yes 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 1 0.99775046110153198 0.7415611743927002 
		1 1 0.99775046110153198 0.7415611743927002 1 1 0.99775046110153198 0.99851912260055542 
		1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0.06703680008649826 0.67088526487350464 
		0 0 0.06703680008649826 0.67088526487350464 0 0 0.067036807537078857 0.054401349276304245 
		0;
	setAttr -s 14 ".kox[0:13]"  0.48588675260543823 1 1 0.74155610799789429 
		0.48588675260543823 1 1 0.74156361818313599 0.48588675260543823 1 1 0.99851924180984497 
		0.94674766063690186 1;
	setAttr -s 14 ".koy[0:13]"  0.87402182817459106 0 0 0.6708909273147583 
		0.87402182817459106 0 0 0.6708824634552002 0.87402182817459106 0 0 0.054400138556957245 
		0.3219764232635498 0;
createNode animCurveTA -n "animCurveTA2040";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 514 25.011196136474609 518 -29.146938323974613
		 524 -18.687202453613281 526 0 528 25.011196136474609 532 -29.146938323974613 538 -18.687202453613281
		 540 0 542 25.011196136474609 546 -29.146938323974613 552 -18.687202453613281 554 -17.561845779418945
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes no no yes yes no no yes yes yes 
		yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 1 0.80759948492050171 0.24755117297172546 
		1 1 0.80759948492050171 0.24755117297172546 1 1 0.80759948492050171 0.97332948446273804 
		1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0.58973127603530884 0.96887481212615967 
		0 0 0.58973127603530884 0.96887481212615967 0 0 0.58973127603530884 0.22941139340400696 
		0;
	setAttr -s 14 ".kox[0:13]"  0.18751412630081177 1 1 0.24755117297172546 
		0.18751412630081177 1 1 0.24755117297172546 0.18751412630081177 1 1 0.97333067655563354 
		0.56213265657424927 1;
	setAttr -s 14 ".koy[0:13]"  0.98226183652877808 0 0 0.96887481212615967 
		0.98226183652877808 0 0 0.96887481212615967 0.98226183652877808 0 0 0.22940646111965179 
		0.82704710960388184 0;
createNode animCurveTL -n "animCurveTL2038";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -4.7867727279663086 524 -4.7867727279663086
		 526 -4.7867727279663086 538 -4.7867727279663086 540 -4.7867727279663086 552 -4.7867727279663086
		 554 -4.7867727279663086 559 -4.7867727279663086;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2039";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 32.754745483398438 524 32.754745483398438
		 526 32.754745483398438 538 32.754745483398438 540 32.754745483398438 552 32.754745483398438
		 554 32.754745483398438 559 32.754745483398438;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2040";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 56.147525787353516 524 56.147525787353516
		 526 56.147525787353516 538 56.147525787353516 540 56.147525787353516 552 56.147525787353516
		 554 56.147525787353516 559 56.147525787353516;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes yes;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2041";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2042";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2043";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2041";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2042";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2043";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2041";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 40.544437408447266 524 40.544437408447266
		 526 40.544437408447266 538 40.544437408447266 540 40.544437408447266 552 40.544437408447266
		 554 40.544437408447266 559 40.544437408447266;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2042";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 43.055271148681641 524 43.055271148681641
		 526 43.055271148681641 538 43.055271148681641 540 43.055271148681641 552 43.055271148681641
		 554 43.055271148681641 559 43.055271148681641;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2043";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2044";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2045";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2046";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2044";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2045";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2046";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2044";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 8.0282459259033203 524 8.0282459259033203
		 526 8.0282459259033203 538 8.0282459259033203 540 8.0282459259033203 552 8.0282459259033203
		 554 8.0282459259033203 559 8.0282459259033203;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2045";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 9.9087905883789063 524 9.9087905883789063
		 526 9.9087905883789063 538 9.9087905883789063 540 9.9087905883789063 552 9.9087905883789063
		 554 9.9087905883789063 559 9.9087905883789063;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2046";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2047";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 1 519 1 521 1 522 1 524 1 526 1 533 1
		 535 1 536 1 538 1 540 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2048";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 1 519 1 521 1 522 1 524 1 526 1 533 1
		 535 1 536 1 538 1 540 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2049";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 1 519 1 521 1 522 1 524 1 526 1 533 1
		 535 1 536 1 538 1 540 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2047";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 -80.043121337890625 519 8.0542812347412109
		 521 -88.699951171875 522 -94.526527404785156 524 8.0542812347412109 526 0 528 -80.043121337890625
		 533 8.0542812347412109 535 -88.699951171875 536 -94.526527404785156 538 8.0542812347412109
		 540 0 542 -80.043121337890625 547 8.0542812347412109 549 -88.699951171875 550 -94.526527404785156
		 552 8.0542812347412109 554 43.330554962158203 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 1 1 0.13532054424285889 1 0.11920743435621262 
		1 1 1 0.13532054424285889 1 0.11920743435621262 1 1 1 0.13532054424285889 1 0.11920743435621262 
		0.1341249942779541 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 -0.99080193042755127 0 0.99286937713623047 
		0 0 0 -0.99080193042755127 0 0.99286937713623047 0 0 0 -0.99080193042755127 0 0.99286937713623047 
		0.99096441268920898 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 0.13532054424285889 1 0.11920743435621262 
		1 1 1 0.13532054424285889 1 0.11920743435621262 1 1 1 0.13532054424285889 1 0.11920743435621262 
		0.26558363437652588 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 -0.99080193042755127 0 0.99286937713623047 
		0 0 0 -0.99080193042755127 0 0.99286937713623047 0 0 0 -0.99080193042755127 0 0.99286937713623047 
		-0.9640878438949585 0;
createNode animCurveTA -n "animCurveTA2048";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 26.761899948120117 519 23.045242309570313
		 521 -18.751214981079102 522 -17.428119659423828 524 23.045242309570313 526 0 528 26.761899948120117
		 533 23.045242309570313 535 -18.751214981079102 536 -17.428119659423828 538 23.045242309570313
		 540 0 542 26.761899948120117 547 23.045242309570313 549 -18.751214981079102 550 -17.428119659423828
		 552 23.045242309570313 554 22.828098297119141 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 1 0.73077648878097534 1 0.51540881395339966 
		1 1 1 0.73077648878097534 1 0.51540881395339966 1 1 1 0.73077648878097534 1 0.51540881395339966 
		1 0.99896746873855591 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 -0.68261688947677612 0 0.8569445013999939 
		0 0 0 -0.68261688947677612 0 0.8569445013999939 0 0 0 -0.68261688947677612 0 0.8569445013999939 
		0 -0.045432105660438538 0;
	setAttr -s 20 ".kox[0:19]"  1 1 0.73077648878097534 1 0.51540881395339966 
		1 1 1 0.73077648878097534 1 0.51540881395339966 1 1 1 0.73077648878097534 1 0.51540881395339966 
		1 0.46336632966995239 1;
	setAttr -s 20 ".koy[0:19]"  0 0 -0.68261688947677612 0 0.8569445013999939 
		0 0 0 -0.68261688947677612 0 0.8569445013999939 0 0 0 -0.68261688947677612 0 0.8569445013999939 
		0 -0.88616681098937988 0;
createNode animCurveTA -n "animCurveTA2049";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 2.6492002010345459 519 79.238670349121094
		 521 49.735687255859375 522 49.643165588378906 524 79.238670349121094 526 0 528 2.6492002010345459
		 533 79.238670349121094 535 49.735687255859375 536 49.643165588378906 538 79.238670349121094
		 540 0 542 2.6492002010345459 547 79.238670349121094 549 49.735687255859375 550 49.643165588378906
		 552 79.238670349121094 554 78.492050170898438 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 0.51497858762741089 1 0.99330872297286987 
		1 1 1 0.51497858762741089 1 0.99330872297286987 1 1 1 0.51497858762741089 1 0.99330872297286987 
		1 1 0.98799318075180054 1;
	setAttr -s 20 ".kiy[0:19]"  0 0.85720312595367432 0 -0.11548865586519241 
		0 0 0 0.85720312595367432 0 -0.11548865586519241 0 0 0 0.85720312595367432 0 -0.11548865586519241 
		0 0 -0.15449747443199158 0;
	setAttr -s 20 ".kox[0:19]"  1 0.51497858762741089 1 0.99330872297286987 
		1 1 1 0.51497858762741089 1 0.99330872297286987 1 1 1 0.51497858762741089 1 0.99330872297286987 
		1 1 0.15034468472003937 1;
	setAttr -s 20 ".koy[0:19]"  0 0.85720312595367432 0 -0.11548865586519241 
		0 0 0 0.85720312595367432 0 -0.11548865586519241 0 0 0 0.85720312595367432 0 -0.11548865586519241 
		0 0 -0.98863357305526733 0;
createNode animCurveTL -n "animCurveTL2047";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 -19.597047805786133 519 -19.597047805786133
		 521 -19.597047805786133 522 -19.597047805786133 524 -19.597047805786133 526 -19.597047805786133
		 533 -19.597047805786133 535 -19.597047805786133 536 -19.597047805786133 538 -19.597047805786133
		 540 -19.597047805786133 547 -19.597047805786133 549 -19.597047805786133 550 -19.597047805786133
		 552 -19.597047805786133 554 -19.597047805786133 559 -19.597047805786133;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2048";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 36.545459747314453 519 36.545459747314453
		 521 36.545459747314453 522 36.545459747314453 524 36.545459747314453 526 36.545459747314453
		 533 36.545459747314453 535 36.545459747314453 536 36.545459747314453 538 36.545459747314453
		 540 36.545459747314453 547 36.545459747314453 549 36.545459747314453 550 36.545459747314453
		 552 36.545459747314453 554 36.545459747314453 559 36.545459747314453;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2049";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 39.212558746337891 519 39.212558746337891
		 521 39.212558746337891 522 39.212558746337891 524 39.212558746337891 526 39.212558746337891
		 533 39.212558746337891 535 39.212558746337891 536 39.212558746337891 538 39.212558746337891
		 540 39.212558746337891 547 39.212558746337891 549 39.212558746337891 550 39.212558746337891
		 552 39.212558746337891 554 39.212558746337891 559 39.212558746337891;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2050";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2051";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2052";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2050";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 0 513 -0.92624503374099743 515 -0.77915763854980469
		 519 -2.2666542530059814 524 -1.4140312671661377 526 0 527 -0.92624503374099743 529 -0.77915763854980469
		 533 -2.2666542530059814 538 -1.4140312671661377 540 0 541 -0.92624503374099743 543 -0.77915763854980469
		 547 -2.2666542530059814 552 -1.4140312671661377 554 -1.3288774490356445 559 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 0.99745869636535645 1 1 1 1 0.99745863676071167 
		1 1 1 1 0.99745869636535645 0.99984091520309448 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0.071247436106204987 0 0 0 0 0.071248084306716919 
		0 0 0 0 0.071247436106204987 0.01783202588558197 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 0.95883411169052124 1 1 1 1 0.95883584022521973 
		1 1 1 1 0.99984103441238403 0.99385994672775269 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0.28396692872047424 0 0 0 0 0.28396084904670715 
		0 0 0 0 0.017831621691584587 0.11064484715461731 0;
createNode animCurveTA -n "animCurveTA2051";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 0 513 -0.97197812795639038 515 -2.1859660148620605
		 519 5.6063799858093262 524 3.497488260269165 526 0 527 -0.97197812795639038 529 -2.1859660148620605
		 533 5.6063799858093262 538 3.497488260269165 540 0 541 -0.97197812795639038 543 -2.1859660148620605
		 547 5.6063799858093262 552 3.497488260269165 554 3.2868671417236328 559 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.81921190023422241 1 1 0.98474931716918945 
		1 0.81921190023422241 1 1 0.98474901914596558 1 0.81921190023422241 1 1 0.98474931716918945 
		0.99902838468551636 1;
	setAttr -s 17 ".kiy[0:16]"  0 -0.57349085807800293 0 0 -0.17397916316986084 
		0 -0.57349085807800293 0 0 -0.17398074269294739 0 -0.57349085807800293 0 0 -0.17397916316986084 
		-0.044070180505514145 0;
	setAttr -s 17 ".kox[0:16]"  1 0.81921190023422241 1 1 0.80671650171279907 
		1 0.81921190023422241 1 1 0.80672293901443481 1 0.81921190023422241 1 1 0.99902850389480591 
		0.96411621570587158 1;
	setAttr -s 17 ".koy[0:16]"  0 -0.57349085807800293 0 0 -0.59093856811523438 
		0 -0.57349085807800293 0 0 -0.59092986583709717 0 -0.57349085807800293 0 0 -0.044069185853004456 
		-0.26548057794570923 0;
createNode animCurveTA -n "animCurveTA2052";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 0 513 7.7344050407409668 515 -44.502704620361328
		 519 -5.0687093734741211 524 -1.0061088800430298 526 0 527 7.7344050407409668 529 -44.502704620361328
		 533 -5.0687093734741211 538 -1.0061088800430298 540 0 541 7.7344050407409668 543 -44.502704620361328
		 547 -5.0687093734741211 552 -1.0061088800430298 554 -0.94552028179168701 559 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 1 1 0.88328260183334351 0.94667267799377441 
		1 1 1 0.88328260183334351 0.94667184352874756 1 1 1 0.88328260183334351 0.94667267799377441 
		0.99991947412490845 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0.46884104609489441 0.3221968412399292 
		0 0 0 0.46884104609489441 0.32219946384429932 0 0 0 0.46884104609489441 0.3221968412399292 
		0.012688807211816311 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 0.88328260183334351 0.97851115465164185 
		1 1 1 0.88328260183334351 0.97851210832595825 1 1 1 0.88328260183334351 0.99991947412490845 
		0.99687737226486206 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0.46884104609489441 0.20619393885135651 
		0 0 0 0.46884104609489441 0.20618949830532074 0 0 0 0.46884104609489441 0.012688516639173031 
		0.078964859247207642 0;
createNode animCurveTL -n "animCurveTL2050";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 51.6451416015625 524 51.6451416015625
		 526 51.6451416015625 538 51.6451416015625 540 51.6451416015625 552 51.6451416015625
		 554 51.6451416015625 559 51.6451416015625;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2051";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -11.264523506164551 524 -11.264523506164551
		 526 -11.264523506164551 538 -11.264523506164551 540 -11.264523506164551 552 -11.264523506164551
		 554 -11.264523506164551 559 -11.264523506164551;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2052";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2053";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 1 514 1 519 1 521 1 522 1 524 1 526 1
		 528 1 533 1 535 1 536 1 538 1 540 1 542 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2054";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 1 514 1 519 1 521 1 522 1 524 1 526 1
		 528 1 533 1 535 1 536 1 538 1 540 1 542 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2055";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 1 514 1 519 1 521 1 522 1 524 1 526 1
		 528 1 533 1 535 1 536 1 538 1 540 1 542 1 547 1 549 1 550 1 552 1 554 1 559 1;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2053";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 85.5325927734375 519 -3.3403100967407227
		 521 76.521369934082031 522 76.521369934082031 524 -3.3403100967407227 526 0 528 85.5325927734375
		 533 -3.3403100967407227 535 76.521369934082031 536 76.521369934082031 538 -3.3403100967407227
		 540 0 542 85.5325927734375 547 -3.3403100967407227 549 76.521369934082031 550 76.521369934082031
		 552 -3.3403100967407227 554 -39.214645385742188 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes no no yes yes yes yes no no yes 
		yes yes yes no no yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 0.11739485710859299 1 1 1 1 1 
		0.11739485710859299 1 1 1 1 1 0.11739485710859299 0.13192839920520782 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 -0.99308532476425171 0 0 0 0 
		0 -0.99308532476425171 0 0 0 0 0 -0.99308532476425171 -0.99125927686691284 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 0.11739485710859299 1 1 1 1 1 
		0.11739485710859299 1 1 1 1 1 0.11739485710859299 0.29119867086410522 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 -0.99308532476425171 0 0 0 0 
		0 -0.99308532476425171 0 0 0 0 0 -0.99308532476425171 0.95666259527206421 0;
createNode animCurveTA -n "animCurveTA2054";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 -26.715377807617187 519 10.852347373962402
		 521 17.654531478881836 522 17.654531478881836 524 10.852347373962402 526 0 528 -26.715377807617187
		 533 10.852347373962402 535 17.654531478881836 536 17.654531478881836 538 10.852347373962402
		 540 0 542 -26.715377807617187 547 10.852347373962402 549 17.654531478881836 550 17.654531478881836
		 552 10.852347373962402 554 10.750091552734375 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes no no yes yes yes yes no no yes 
		yes yes yes no no yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 1 0.22782336175441742 1 1 1 1 1 0.22782336175441742 
		1 1 1 1 1 0.22782336175441742 1 1 1 0.99977076053619385 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0.97370243072509766 0 0 0 0 0 0.97370243072509766 
		0 0 0 0 0 0.97370243072509766 0 0 0 -0.021411856636404991 0;
	setAttr -s 20 ".kox[0:19]"  1 1 0.22782336175441742 1 1 1 1 1 0.22782336175441742 
		1 1 1 1 1 0.22782336175441742 1 1 1 0.74307131767272949 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0.97370243072509766 0 0 0 0 0 0.97370243072509766 
		0 0 0 0 0 0.97370243072509766 0 0 0 -0.6692122220993042 0;
createNode animCurveTA -n "animCurveTA2055";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 0.39031988382339478 519 65.929977416992188
		 521 67.973304748535156 522 67.973304748535156 524 65.929977416992188 526 0 528 0.39031988382339478
		 533 65.929977416992188 535 67.973304748535156 536 67.973304748535156 538 65.929977416992188
		 540 0 542 0.39031988382339478 547 65.929977416992188 549 67.973304748535156 550 67.973304748535156
		 552 65.929977416992188 554 59.859397888183594 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes no no yes yes yes yes no no yes 
		yes yes yes no no yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 0.97121930122375488 0.61449247598648071 
		1 1 0.61449247598648071 1 0.97121930122375488 0.61449247598648071 1 1 0.61449247598648071 
		1 0.97121930122375488 0.61449247598648071 1 1 0.61449247598648071 0.61820840835571289 
		1;
	setAttr -s 20 ".kiy[0:19]"  0 0.23818688094615936 0.78892272710800171 
		0 0 -0.78892272710800171 0 0.23818688094615936 0.78892272710800171 0 0 -0.78892272710800171 
		0 0.23818688094615936 0.78892272710800171 0 0 -0.78892272710800171 -0.78601425886154175 
		0;
	setAttr -s 20 ".kox[0:19]"  1 0.97121930122375488 0.61449247598648071 
		1 1 0.61449247598648071 1 0.97121930122375488 0.61449247598648071 1 1 0.61449247598648071 
		1 0.97121930122375488 0.61449247598648071 1 1 0.61449247598648071 0.19555953145027161 
		1;
	setAttr -s 20 ".koy[0:19]"  0 0.23818688094615936 0.78892272710800171 
		0 0 -0.78892272710800171 0 0.23818688094615936 0.78892272710800171 0 0 -0.78892272710800171 
		0 0.23818688094615936 0.78892272710800171 0 0 -0.78892272710800171 -0.98069185018539429 
		0;
createNode animCurveTL -n "animCurveTL2053";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 -19.609930038452148 514 -19.609930038452148
		 519 -19.609930038452148 521 -19.609930038452148 522 -19.609930038452148 524 -19.609930038452148
		 526 -19.609930038452148 528 -19.609930038452148 533 -19.609930038452148 535 -19.609930038452148
		 536 -19.609930038452148 538 -19.609930038452148 540 -19.609930038452148 542 -19.609930038452148
		 547 -19.609930038452148 549 -19.609930038452148 550 -19.609930038452148 552 -19.609930038452148
		 554 -19.609930038452148 559 -19.609930038452148;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2054";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 36.542636871337891 514 36.542636871337891
		 519 36.542636871337891 521 36.542636871337891 522 36.542636871337891 524 36.542636871337891
		 526 36.542636871337891 528 36.542636871337891 533 36.542636871337891 535 36.542636871337891
		 536 36.542636871337891 538 36.542636871337891 540 36.542636871337891 542 36.542636871337891
		 547 36.542636871337891 549 36.542636871337891 550 36.542636871337891 552 36.542636871337891
		 554 36.542636871337891 559 36.542636871337891;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2055";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 -39.208473205566406 514 -39.208473205566406
		 519 -39.208473205566406 521 -39.208473205566406 522 -39.208473205566406 524 -39.208473205566406
		 526 -39.208473205566406 528 -39.208473205566406 533 -39.208473205566406 535 -39.208473205566406
		 536 -39.208473205566406 538 -39.208473205566406 540 -39.208473205566406 542 -39.208473205566406
		 547 -39.208473205566406 549 -39.208473205566406 550 -39.208473205566406 552 -39.208473205566406
		 554 -39.208473205566406 559 -39.208473205566406;
	setAttr -s 20 ".kix[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2056";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 1 517 1 520 1 524 1 526 1 531 1 534 1
		 538 1 540 1 545 1 548 1 552 1 554 1 559 1;
	setAttr -s 14 ".ktl[3:13]" no no yes yes no no yes yes yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2057";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 1 517 1 520 1 524 1 526 1 531 1 534 1
		 538 1 540 1 545 1 548 1 552 1 554 1 559 1;
	setAttr -s 14 ".ktl[3:13]" no no yes yes no no yes yes yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2058";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 1 517 1 520 1 524 1 526 1 531 1 534 1
		 538 1 540 1 545 1 548 1 552 1 554 1 559 1;
	setAttr -s 14 ".ktl[3:13]" no no yes yes no no yes yes yes yes yes;
	setAttr -s 14 ".kix[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2056";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 -6.0033698081970215 515 -7.4241185188293466
		 517 -5.6832795143127441 520 -3.4084749221801758 524 -1.6783663034439087 526 0 528 -6.0033698081970215
		 529 -7.4241185188293466 531 -5.6832795143127441 534 -3.4084749221801758 538 -1.6783663034439087
		 540 0 542 -6.0033698081970215 543 -7.4241185188293466 545 -5.6832795143127441 548 -3.4084749221801758
		 552 -1.6783663034439087 554 -1.577293872833252 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes no no yes yes yes yes 
		no no yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 0.55972796678543091 1 0.91115331649780273 
		0.98375064134597778 0.98398107290267944 0.94341146945953369 0.55972796678543091 1 
		0.91115331649780273 0.98375064134597778 0.98398071527481079 0.94341146945953369 0.55972796678543091 
		1 0.91115331649780273 0.98375064134597778 0.98398071527481079 0.99977600574493408 
		1;
	setAttr -s 20 ".kiy[0:19]"  0 -0.82867640256881714 0 0.41206759214401245 
		0.17954008281230927 0.17827294766902924 0.33162444829940796 -0.82867640256881714 
		0 0.41206759214401245 0.17954008281230927 0.17827498912811279 0.33162444829940796 
		-0.82867640256881714 0 0.41206759214401245 0.17954008281230927 0.17827498912811279 
		0.021164121106266975 0;
	setAttr -s 20 ".kox[0:19]"  0.62246263027191162 0.55972796678543091 
		1 0.91115331649780273 0.98375064134597778 0.94341146945953369 0.62246263027191162 
		0.55972796678543091 1 0.91115331649780273 0.98375064134597778 0.94341146945953369 
		0.62246263027191162 0.55972796678543091 1 0.91115331649780273 0.98375064134597778 
		0.99977600574493408 0.99138224124908447 1;
	setAttr -s 20 ".koy[0:19]"  -0.78264951705932617 -0.82867640256881714 
		0 0.41206759214401245 0.17954008281230927 0.33162444829940796 -0.78264951705932617 
		-0.82867640256881714 0 0.41206759214401245 0.17954008281230927 0.33162444829940796 
		-0.78264951705932617 -0.82867640256881714 0 0.41206759214401245 0.17954008281230927 
		0.021163638681173325 0.13100111484527588 0;
createNode animCurveTA -n "animCurveTA2057";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 0.72100627422332764 515 0.064429111778736115
		 517 -2.1434977054595947 520 -2.9215860366821289 524 -2.4153056144714355 526 0 528 0.72100627422332764
		 529 0.064429111778736115 531 -2.1434977054595947 534 -2.9215860366821289 538 -2.4153056144714355
		 540 0 542 0.72100627422332764 543 0.064429111778736115 545 -2.1434977054595947 548 -2.9215860366821289
		 552 -2.4153056144714355 554 -2.3131284713745117 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes no no yes yes yes yes 
		no no yes yes yes yes yes yes yes;
	setAttr -s 20 ".kix[0:19]"  1 1 0.871135413646698 0.96156960725784302 
		1 0.99859756231307983 0.89232563972473145 1 0.871135413646698 0.96156960725784302 
		1 0.99859756231307983 0.89232563972473145 1 0.871135413646698 0.96156960725784302 
		1 0.99859756231307983 0.9997711181640625 0.98614346981048584;
	setAttr -s 20 ".kiy[0:19]"  0 0 -0.49104288220405579 -0.27456116676330566 
		0 0.05294279009103775 0.45139226317405701 0 -0.49104288220405579 -0.27456116676330566 
		0 0.0529433973133564 0.45139226317405701 0 -0.49104288220405579 -0.27456116676330566 
		0 0.0529433973133564 0.021395359188318253 0.16589458286762238;
	setAttr -s 20 ".kox[0:19]"  0.98878979682922363 1 0.871135413646698 
		0.96156960725784302 1 0.89232563972473145 0.98878979682922363 1 0.871135413646698 
		0.96156960725784302 1 0.89232563972473145 0.98878979682922363 1 0.871135413646698 
		0.96156960725784302 1 0.9997711181640625 0.98173636198043823 0.98614346981048584;
	setAttr -s 20 ".koy[0:19]"  0.1493145227432251 0 -0.49104288220405579 
		-0.27456116676330566 0 0.45139226317405701 0.1493145227432251 0 -0.49104288220405579 
		-0.27456116676330566 0 0.45139226317405701 0.1493145227432251 0 -0.49104288220405579 
		-0.27456116676330566 0 0.021394869312644005 0.19024614989757538 0.16589465737342834;
createNode animCurveTA -n "animCurveTA2058";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 514 -28.952939987182614 515 -26.441411972045898
		 517 4.3605389595031738 520 5.801365852355957 524 5.3569350242614746 526 0 528 -28.952939987182614
		 529 -26.441411972045898 531 4.3605389595031738 534 5.801365852355957 538 5.3569350242614746
		 540 0 542 -28.952939987182614 543 -26.441411972045898 545 4.3605389595031738 548 5.801365852355957
		 552 5.3569350242614746 554 5.1855583190917969 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes no no yes yes yes yes 
		no no yes yes yes yes yes yes yes;
	setAttr -s 20 ".kix[0:19]"  1 1 0.30204936861991882 0.85615581274032593 
		1 0.99891877174377441 0.66536957025527954 1 0.30204936861991882 0.85615581274032593 
		1 0.99891871213912964 0.66536957025527954 1 0.30204936861991882 0.85615581274032593 
		1 0.99891871213912964 0.99935644865036011 0.86207407712936401;
	setAttr -s 20 ".kiy[0:19]"  0 0 0.95329231023788452 0.51671767234802246 
		0 -0.046490009874105453 -0.74651414155960083 0 0.95329231023788452 0.51671767234802246 
		0 -0.046490542590618134 -0.74651414155960083 0 0.95329231023788452 0.51671767234802246 
		0 -0.046490542590618134 -0.0358705073595047 -0.5067821741104126;
	setAttr -s 20 ".kox[0:19]"  0.16271287202835083 1 0.30204936861991882 
		0.85615581274032593 1 0.66536957025527954 0.16271287202835083 1 0.30204936861991882 
		0.85615581274032593 1 0.66536957025527954 0.16271287202835083 1 0.30204936861991882 
		0.85615581274032593 1 0.99935644865036011 0.91718930006027222 0.86207425594329834;
	setAttr -s 20 ".koy[0:19]"  -0.98667347431182861 0 0.95329231023788452 
		0.51671767234802246 0 -0.74651414155960083 -0.98667347431182861 0 0.95329231023788452 
		0.51671767234802246 0 -0.74651414155960083 -0.98667347431182861 0 0.95329231023788452 
		0.51671767234802246 0 -0.035869687795639038 -0.39845177531242371 -0.50678199529647827;
createNode animCurveTL -n "animCurveTL2056";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  512 -4.502251148223877 514 5.6482486724853516
		 515 2.5126373767852783 516 9.2903833389282227 517 6.4029860496520996 520 -6.6977443695068359
		 524 -6.0379352569580078 526 -4.502251148223877 528 5.6482486724853516 529 2.5126373767852783
		 530 9.2903833389282227 531 6.4029860496520996 534 -6.6977443695068359 538 -6.0379352569580078
		 540 -4.502251148223877 542 5.6482486724853516 543 2.5126373767852783 544 9.2903833389282227
		 545 6.4029860496520996 548 -6.6977443695068359 552 -6.0379352569580078 554 -5.9454550743103027
		 559 -4.502251148223877;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes no no yes yes yes 
		yes yes no no yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 0.0057989703491330147 1 0.24490748345851898 
		0.054184909909963608 1 1 1 0.0057989703491330147 1 0.24490484595298767 0.054184909909963608 
		1 1 1 0.0057989703491330147 1 0.24490484595298767 0.66940909624099731 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 -0.99998319149017334 0 0.96954643726348877 
		0.99853086471557617 0 0 0 -0.99998319149017334 0 0.9695470929145813 0.99853086471557617 
		0 0 0 -0.99998319149017334 0 0.9695470929145813 0.74289393424987793 0;
	setAttr -s 23 ".kox[0:22]"  0.0082094939425587654 1 1 1 0.0057989703491330147 
		1 0.054184909909963608 0.0082094939425587654 1 1 1 0.0057989703491330147 1 0.054184909909963608 
		0.0082094939425587654 1 1 1 0.0057989703491330147 1 0.66941756010055542 0.14287295937538147 
		1;
	setAttr -s 23 ".koy[0:22]"  0.99996626377105713 0 0 0 -0.99998319149017334 
		0 0.99853086471557617 0.99996626377105713 0 0 0 -0.99998319149017334 0 0.99853086471557617 
		0.99996626377105713 0 0 0 -0.99998319149017334 0 0.74288630485534668 0.98974102735519409 
		0;
createNode animCurveTL -n "animCurveTL2057";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  512 37.76336669921875 514 41.89251708984375
		 515 41.02386474609375 516 44.723323822021484 517 42.937530517578125 520 36.770278930664063
		 524 37.068729400634766 526 37.76336669921875 528 41.89251708984375 529 41.02386474609375
		 530 44.723323822021484 531 42.937530517578125 534 36.770278930664063 538 37.068729400634766
		 540 37.76336669921875 542 41.89251708984375 543 41.02386474609375 544 44.723323822021484
		 545 42.937530517578125 548 36.770278930664063 552 37.068729400634766 554 37.110561370849609
		 559 37.76336669921875;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes no no yes yes yes 
		yes yes no no yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 0.010882384143769741 1 0.48756808042526245 
		0.11911261081695557 1 1 1 0.010882384143769741 1 0.4875638484954834 0.11911261081695557 
		1 1 1 0.010882384143769741 1 0.4875638484954834 0.89371669292449951 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 -0.99994081258773804 0 0.87308496236801147 
		0.99288070201873779 0 0 0 -0.99994081258773804 0 0.87308728694915771 0.99288070201873779 
		0 0 0 -0.99994081258773804 0 0.87308728694915771 0.44863173365592957 0;
	setAttr -s 23 ".kox[0:22]"  0.020177584141492844 1 1 1 0.010882384143769741 
		1 0.11911261081695557 0.020177584141492844 1 1 1 0.010882384143769741 1 0.11911261081695557 
		0.020177584141492844 1 1 1 0.010882384143769741 1 0.89372092485427856 0.30402767658233643 
		1;
	setAttr -s 23 ".koy[0:22]"  0.99979645013809204 0 0 0 -0.99994081258773804 
		0 0.99288070201873779 0.99979645013809204 0 0 0 -0.99994081258773804 0 0.99288070201873779 
		0.99979645013809204 0 0 0 -0.99994081258773804 0 0.44862338900566101 0.95266318321228027 
		0;
createNode animCurveTL -n "animCurveTL2058";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  512 0 514 1.8275718688964844 515 1.616051197052002
		 516 3.2285187244415283 517 2.6150286197662354 520 -0.5034019947052002 524 -0.35211506485939026
		 526 0 528 1.8275718688964844 529 1.616051197052002 530 3.2285187244415283 531 2.6150286197662354
		 534 -0.5034019947052002 538 -0.35211506485939026 540 0 542 1.8275718688964844 543 1.616051197052002
		 544 3.2285187244415283 545 2.6150286197662354 548 -0.5034019947052002 552 -0.35211506485939026
		 554 -0.33091047406196594 559 0;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes no no yes yes yes 
		yes yes no no yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 0.025179162621498108 1 0.74044704437255859 
		0.23030328750610352 1 1 1 0.025179162621498108 1 0.74044322967529297 0.23030328750610352 
		1 1 1 0.025179162621498108 1 0.74044322967529297 0.96911716461181641 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 -0.9996829628944397 0 0.67211461067199707 
		0.97311896085739136 0 0 0 -0.9996829628944397 0 0.67211884260177612 0.97311896085739136 
		0 0 0 -0.9996829628944397 0 0.67211884260177612 0.2466006875038147 0;
	setAttr -s 23 ".kox[0:22]"  0.04555046558380127 1 1 1 0.025179162621498108 
		1 0.23030328750610352 0.04555046558380127 1 1 1 0.025179162621498108 1 0.23030328750610352 
		0.04555046558380127 1 1 1 0.025179162621498108 1 0.96911853551864624 0.53277856111526489 
		1;
	setAttr -s 23 ".koy[0:22]"  0.99896204471588135 0 0 0 -0.9996829628944397 
		0 0.97311896085739136 0.99896204471588135 0 0 0 -0.9996829628944397 0 0.97311896085739136 
		0.99896204471588135 0 0 0 -0.9996829628944397 0 0.24659539759159088 0.84625470638275146 
		0;
createNode animCurveTU -n "animCurveTU2059";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2060";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2061";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2059";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2060";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2061";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2059";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -1.4725730419158936 524 -1.4725730419158936
		 526 -1.4725730419158936 538 -1.4725730419158936 540 -1.4725730419158936 552 -1.4725730419158936
		 554 -1.4725730419158936 559 -1.4725730419158936;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2060";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 22.299345016479492 524 22.299345016479492
		 526 22.299345016479492 538 22.299345016479492 540 22.299345016479492 552 22.299345016479492
		 554 22.299345016479492 559 22.299345016479492;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2061";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".ktl[1:7]" no no no no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2062";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  512 1 524 1 526 1 538 1 540 1 552 1 559 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2063";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  512 1 524 1 526 1 538 1 540 1 552 1 559 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2064";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  512 1 524 1 526 1 538 1 540 1 552 1 559 1;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2062";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  512 0 524 0 526 0 527 1.9249372482299807
		 528 5.6108942031860352 529 8.7262735366821289 532 8.1420097351074219 538 0 540 -1.2537802457809448
		 541 -3.6192505359649654 542 -5.490607738494873 543 -7.2084984779357901 546 -1.4302917718887329
		 550 -1.8589890003204346 552 -0.72171258926391602 559 0;
	setAttr -s 16 ".ktl[2:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 0.64807301759719849 0.45529621839523315 
		1 0.97133374214172363 0.92345726490020752 0.84734320640563965 0.67755013704299927 
		0.79933565855026245 1 1 1 0.99171113967895508 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0.7615782618522644 0.89033997058868408 
		0 -0.23771987855434418 -0.38370135426521301 -0.53104579448699951 -0.73547661304473877 
		-0.60088479518890381 0 0 0 0.12848763167858124 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 0.64807301759719849 0.45529621839523315 
		1 0.97133374214172363 0.92345726490020752 0.84734320640563965 0.67755013704299927 
		0.79933565855026245 1 1 1 0.99171113967895508 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0.7615782618522644 0.89033997058868408 
		0 -0.23771987855434418 -0.38370135426521301 -0.53104579448699951 -0.73547661304473877 
		-0.60088479518890381 0 0 0 0.12848763167858124 0;
createNode animCurveTA -n "animCurveTA2063";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  512 0 524 0 526 0 527 -6.0452733039855957
		 528 -12.691244125366211 529 -23.434333801269531 532 -23.946649551391602 538 0 540 1.7048685550689697
		 541 5.2452025413513184 542 7.901047706604003 543 13.320198059082031 546 24.684757232666016
		 550 5.2674875259399414 552 1.3208985328674316 559 0;
	setAttr -s 16 ".ktl[2:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 0.35212090611457825 0.20346702635288239 
		0.97773981094360352 1 0.70202380418777466 0.73592394590377808 0.61038172245025635 
		0.50897103548049927 0.30497100949287415 1 0.38452348113059998 0.97301805019378662 
		1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 -0.9359545111656189 -0.97908180952072144 
		-0.20982128381729126 0 0.71215349435806274 0.67706418037414551 0.79210740327835083 
		0.86078363656997681 0.95236164331436157 0 -0.92311525344848633 -0.23072898387908936 
		0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 0.35212090611457825 0.20346702635288239 
		0.97773981094360352 1 0.70202380418777466 0.73592394590377808 0.61038172245025635 
		0.50897103548049927 0.30497100949287415 1 0.38452348113059998 0.97301805019378662 
		1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 -0.9359545111656189 -0.97908180952072144 
		-0.20982128381729126 0 0.71215349435806274 0.67706418037414551 0.79210740327835083 
		0.86078363656997681 0.95236164331436157 0 -0.92311525344848633 -0.23072898387908936 
		0;
createNode animCurveTA -n "animCurveTA2064";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  512 0 513 -6.7621951103210449 514 -0.10713867843151093
		 515 24.928863525390625 518 29.138395309448242 524 4.7997183799743652 526 0 527 -8.0184431076049805
		 528 -0.78439539670944214 529 24.160072326660156 532 23.241233825683594 538 4.7997183799743652
		 540 -1.1457040309906006 541 -6.4681258201599121 542 -1.0330789089202881 543 34.199848175048828
		 546 34.2958984375 550 15.8931884765625 552 6.1810207366943359 559 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 1 0.11872877180576324 0.49331352114677429 
		1 0.5946001410484314 0.41662544012069702 1 0.10934454202651978 1 0.93326234817504883 
		0.61974471807479858 0.4988594651222229 1 0.14487083256244659 0.99919164180755615 
		1 0.3278273344039917 0.66946297883987427 1;
	setAttr -s 20 ".kiy[0:19]"  0 0 0.99292677640914917 0.86985158920288086 
		0 -0.80402153730392456 -0.90907824039459229 0 0.99400389194488525 0 -0.3591955304145813 
		-0.78480350971221924 -0.86668288707733154 0 0.98945063352584839 0.040200196206569672 
		0 -0.94473761320114136 -0.74284547567367554 0;
	setAttr -s 20 ".kox[0:19]"  1 1 0.11872877180576324 0.49331352114677429 
		1 0.5946001410484314 0.41662544012069702 1 0.10934454202651978 1 0.93326234817504883 
		0.61974471807479858 0.4988594651222229 1 0.14487083256244659 0.99919164180755615 
		1 0.3278273344039917 0.66946297883987427 1;
	setAttr -s 20 ".koy[0:19]"  0 0 0.99292677640914917 0.86985158920288086 
		0 -0.80402153730392456 -0.90907824039459229 0 0.99400389194488525 0 -0.3591955304145813 
		-0.78480350971221924 -0.86668288707733154 0 0.98945063352584839 0.040200196206569672 
		0 -0.94473761320114136 -0.74284547567367554 0;
createNode animCurveTL -n "animCurveTL2062";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  512 0 515 31.808847427368164 518 37.231517791748047
		 524 5.6158018112182617 526 0 529 31.808847427368164 532 37.231517791748047 538 5.6158018112182617
		 540 0 543 31.808847427368164 546 37.231517791748047 552 5.6158018112182617 559 0;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		yes no;
	setAttr -s 13 ".kix[0:12]"  1 0.0076835630461573601 1 0.0049462979659438133 
		1 0.0076835630461573601 1 0.0049462979659438133 1 0.0076835630461573601 1 0.01730966754257679 
		1;
	setAttr -s 13 ".kiy[0:12]"  0 0.99997049570083618 0 -0.99998778104782104 
		0 0.99997049570083618 0 -0.99998778104782104 0 0.99997049570083618 0 -0.99985015392303467 
		0;
	setAttr -s 13 ".kox[0:12]"  1 0.0076835630461573601 1 0.0049462979659438133 
		1 0.0076835630461573601 1 0.0049462979659438133 1 0.0076835630461573601 1 0.01730966754257679 
		1;
	setAttr -s 13 ".koy[0:12]"  0 0.99997049570083618 0 -0.99998778104782104 
		0 0.99997049570083618 0 -0.99998778104782104 0 0.99997049570083618 0 -0.99985015392303467 
		0;
createNode animCurveTL -n "animCurveTL2063";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 19 ".ktv[0:18]"  512 0 513 15.668669700622559 514 26.189048767089844
		 515 27.974813461303711 522 -4.2606906890869141 524 -3.3568856716156006 526 0 527 15.668669700622559
		 528 26.189048767089844 529 27.974813461303711 536 -4.2606906890869141 538 -3.3568856716156006
		 540 0 541 15.668669700622559 542 26.189048767089844 543 27.974813461303711 550 -4.2606906890869141
		 552 -3.3568856716156006 559 0;
	setAttr -s 19 ".ktl[0:18]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes no;
	setAttr -s 19 ".kix[0:18]"  1 0.0021706586703658104 0.0077773239463567734 
		1 1 0.039087396115064621 0.0082745831459760666 0.002219171728938818 0.0077773239463567734 
		1 1 0.039087396115064621 0.0082745831459760666 0.002219171728938818 0.0077773239463567734 
		1 1 0.030719751492142677 1;
	setAttr -s 19 ".kiy[0:18]"  0 0.99999761581420898 0.9999697208404541 
		0 0 0.99923574924468994 0.99996578693389893 0.99999755620956421 0.9999697208404541 
		0 0 0.99923574924468994 0.99996578693389893 0.99999755620956421 0.9999697208404541 
		0 0 0.99952805042266846 0;
	setAttr -s 19 ".kox[0:18]"  1 0.0021706586703658104 0.0077773239463567734 
		1 1 0.039087396115064621 0.0082745831459760666 0.002219171728938818 0.0077773239463567734 
		1 1 0.039087396115064621 0.0082745831459760666 0.002219171728938818 0.0077773239463567734 
		1 1 0.030719751492142677 1;
	setAttr -s 19 ".koy[0:18]"  0 0.99999761581420898 0.9999697208404541 
		0 0 0.99923574924468994 0.99996578693389893 0.99999755620956421 0.9999697208404541 
		0 0 0.99923574924468994 0.99996578693389893 0.99999755620956421 0.9999697208404541 
		0 0 0.99952805042266846 0;
createNode animCurveTL -n "animCurveTL2064";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  512 0 524 0 526 0 538 0 540 0 552 0 559 0;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2065";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2066";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU2067";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 1 524 1 526 1 538 1 540 1 552 1 554 1
		 559 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA2065";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 515 -20.642921447753906 522 0.05793369933962822
		 524 0.050622347742319107 526 0 529 -20.642921447753906 536 0.05793369933962822 538 0.050622347742319107
		 540 0 543 -20.642921447753906 550 0.05793369933962822 552 0.050622347742319107 554 0.047573838382959366
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 1 0.99999880790710449 1 1 1 0.99999880790710449 
		1 1 1 0.99999880790710449 0.99999982118606567 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 0 -0.0015312719624489546 0 0 0 -0.0015313071198761463 
		0 0 0 -0.0015312719624489546 -0.00063848838908597827 0;
	setAttr -s 14 ".kox[0:13]"  1 1 1 0.99994379281997681 1 1 1 0.99994385242462158 
		1 1 1 0.99999982118606567 0.99999207258224487 1;
	setAttr -s 14 ".koy[0:13]"  0 0 0 -0.010601884685456753 0 0 0 -0.010601644404232502 
		0 0 0 -0.00063847377896308899 -0.0039855297654867172 0;
createNode animCurveTA -n "animCurveTA2066";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  512 0 515 22.776775360107422 522 20.744972229003906
		 524 17.073383331298828 526 0 529 22.776775360107422 536 20.744972229003906 538 17.073383331298828
		 540 0 543 22.776775360107422 550 20.744972229003906 552 17.073383331298828 554 16.045211791992188
		 559 0;
	setAttr -s 14 ".ktl[0:13]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no;
	setAttr -s 14 ".kix[0:13]"  1 1 0.9394574761390686 0.79272395372390747 
		1 1 0.9394574761390686 0.7927170991897583 1 1 0.9394574761390686 0.79272395372390747 
		0.97759014368057251 1;
	setAttr -s 14 ".kiy[0:13]"  0 0 -0.34266564249992371 -0.6095808744430542 
		0 0 -0.34266564249992371 -0.60958963632583618 0 0 -0.34266564249992371 -0.6095808744430542 
		-0.21051700413227081 0;
	setAttr -s 14 ".kox[0:13]"  1 1 0.9394574761390686 0.26931759715080261 
		1 1 0.9394574761390686 0.26932325959205627 1 1 0.9394574761390686 0.97759115695953369 
		0.59687960147857666 1;
	setAttr -s 14 ".koy[0:13]"  0 0 -0.34266564249992371 -0.963051438331604 
		0 0 -0.34266564249992371 -0.96304988861083984 0 0 -0.34266564249992371 -0.21051241457462311 
		-0.80233079195022583 0;
createNode animCurveTA -n "animCurveTA2067";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  512 0 514 -43.262630462646484 515 -69.623542785644531
		 522 12.302217483520508 524 10.749651908874512 526 0 528 -43.262630462646484 529 -69.623542785644531
		 536 12.302217483520508 538 10.749651908874512 540 0 542 -43.262630462646484 543 -69.623542785644531
		 550 12.302217483520508 552 10.749651908874512 554 10.102300643920898 559 0;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes no;
	setAttr -s 17 ".kix[0:16]"  1 0.061224024742841721 1 1 0.95098769664764404 
		1 0.061224024742841721 1 1 0.95098555088043213 1 0.061224024742841721 1 1 0.95098769664764404 
		0.99093347787857056 1;
	setAttr -s 17 ".kiy[0:16]"  0 -0.99812400341033936 0 0 -0.30922877788543701 
		0 -0.99812400341033936 0 0 -0.30923515558242798 0 -0.99812400341033936 0 0 -0.30922877788543701 
		-0.13435372710227966 0;
	setAttr -s 17 ".kox[0:16]"  1 0.061224024742841721 1 1 0.40592190623283386 
		1 0.061224024742841721 1 1 0.4059298038482666 1 0.061224024742841721 1 1 0.99093383550643921 
		0.76331859827041626 1;
	setAttr -s 17 ".koy[0:16]"  0 -0.99812400341033936 0 0 -0.91390770673751831 
		0 -0.99812400341033936 0 0 -0.91390430927276611 0 -0.99812400341033936 0 0 -0.13435067236423492 
		-0.64602220058441162 0;
createNode animCurveTL -n "animCurveTL2065";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 -54.946929931640625 524 -54.946929931640625
		 526 -54.946929931640625 538 -54.946929931640625 540 -54.946929931640625 552 -54.946929931640625
		 554 -54.946929931640625 559 -54.946929931640625;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2066";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 2.3482637405395508 524 2.3482637405395508
		 526 2.3482637405395508 538 2.3482637405395508 540 2.3482637405395508 552 2.3482637405395508
		 554 2.3482637405395508 559 2.3482637405395508;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL2067";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  512 0 524 0 526 0 538 0 540 0 552 0 554 0
		 559 0;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
select -ne :time1;
	setAttr ".o" 559;
	setAttr ".unw" 559;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "bite_repeatedlySource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU2029.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU2030.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU2031.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA2029.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA2030.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA2031.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL2029.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL2030.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL2031.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU2032.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU2033.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU2034.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA2032.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA2033.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA2034.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL2032.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL2033.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL2034.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU2035.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU2036.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU2037.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA2035.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA2036.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA2037.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL2035.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL2036.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL2037.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU2038.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU2039.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU2040.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA2038.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA2039.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA2040.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL2038.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL2039.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL2040.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU2041.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU2042.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU2043.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA2041.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA2042.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA2043.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL2041.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL2042.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL2043.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU2044.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU2045.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU2046.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA2044.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA2045.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA2046.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL2044.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL2045.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL2046.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU2047.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU2048.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU2049.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA2047.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA2048.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA2049.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL2047.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL2048.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL2049.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU2050.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU2051.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU2052.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA2050.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA2051.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA2052.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL2050.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL2051.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL2052.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU2053.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU2054.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU2055.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA2053.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA2054.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA2055.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL2053.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL2054.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL2055.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU2056.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU2057.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU2058.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA2056.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA2057.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA2058.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL2056.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL2057.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL2058.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU2059.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU2060.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU2061.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA2059.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA2060.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA2061.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL2059.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL2060.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL2061.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU2062.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU2063.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU2064.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA2062.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA2063.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA2064.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL2062.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL2063.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL2064.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU2065.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU2066.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU2067.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA2065.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA2066.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA2067.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL2065.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL2066.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL2067.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_bite_repeatedly.ma
