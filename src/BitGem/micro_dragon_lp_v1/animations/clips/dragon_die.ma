//Maya ASCII 2013 scene
//Name: dragon_die.ma
//Last modified: Mon, Jul 14, 2014 01:06:21 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "dieSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 311;
	setAttr ".se" 346;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU1561";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1562";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1563";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1561";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1562";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1563";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1561";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 3.2171449661254883 323 3.2171449661254883
		 324 3.2171449661254883 325 3.2171449661254883 326 3.2171449661254883 346 3.2171449661254883;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1562";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -26.658763885498047 323 -26.658763885498047
		 324 -26.658763885498047 325 -26.658763885498047 326 -26.658763885498047 346 -26.658763885498047;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1563";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -1.5793838500976563 323 -1.5793838500976563
		 324 -1.5793838500976563 325 -1.5793838500976563 326 -1.5793838500976563 346 -1.5793838500976563;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1564";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1565";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1566";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1564";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 14 ".ktv[0:13]"  311 0 314 10.788095474243164 319 1.3140424489974976
		 323 6.9750180244445801 324 10.31563663482666 325 -53.820892333984375 326 -147.31710815429687
		 329 -40.571647644042969 332 -27.75865364074707 335 -160.9688720703125 336 -163.52816772460937
		 339 -144.86930847167969 342 -157.22457885742187 346 -157.22457885742187;
	setAttr -s 14 ".kit[0:13]"  2 1 1 1 1 1 1 1 
		1 1 1 1 1 1;
	setAttr -s 14 ".ktl[1:13]" no no no no yes yes yes yes yes yes yes 
		no no;
	setAttr -s 14 ".kix[1:13]"  0.55309033393859863 0.78327155113220215 
		0.86020725965499878 0.58142650127410889 0.018654260784387589 1 0.18316821753978729 
		1 0.29691305756568909 1 1 1 1;
	setAttr -s 14 ".kiy[1:13]"  0.83312129974365234 -0.62167978286743164 
		0.50994455814361572 0.81359893083572388 -0.99982595443725586 0 0.98308157920837402 
		0 -0.95490449666976929 0 0 0 0;
	setAttr -s 14 ".kox[0:13]"  0.55309039354324341 0.783272385597229 0.86020636558532715 
		0.58142954111099243 0.037197127938270569 0.018654260784387589 1 0.18316821753978729 
		1 0.29691305756568909 1 1 1 0.015182146802544594;
	setAttr -s 14 ".koy[0:13]"  0.83312129974365234 -0.62167865037918091 
		0.50994604825973511 0.81359672546386719 -0.99930799007415771 -0.99982595443725586 
		0 0.98308157920837402 0 -0.95490449666976929 0 0 0 0.99988478422164917;
createNode animCurveTA -n "animCurveTA1565";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 319 -1.120901346206665 323 3.2070965766906738
		 324 -7.2994294166564941 325 0 326 1.3251820802688599 329 -10.482328414916992 332 -9.8386411666870117
		 335 5.1783599853515625 336 3.7951459884643559 339 3.1817049980163574 342 4.1616153717041016
		 346 4.1616153717041016;
	setAttr -s 13 ".ktl[0:12]" no no no no yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.99828213453292847 0.91081827878952026 
		0.22157491743564606 0.51481235027313232 1 1 0.96551954746246338 1 0.96853619813919067 
		1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 -0.058589436113834381 0.4128074049949646 
		-0.97514337301254272 0.8573029637336731 0 0 0.26033058762550354 0 -0.24887271225452423 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.99828213453292847 0.91081780195236206 
		0.2215765118598938 0.3108552098274231 0.51481235027313232 1 1 0.96551954746246338 
		1 0.96853619813919067 1 1 0.49758720397949219;
	setAttr -s 13 ".koy[0:12]"  -0.058589324355125427 0.41280868649482727 
		-0.97514301538467407 0.95045721530914307 0.8573029637336731 0 0 0.26033058762550354 
		0 -0.24887271225452423 0 0 -0.86741393804550171;
createNode animCurveTA -n "animCurveTA1566";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 319 22.862342834472656 323 1.2020429372787476
		 324 8.175933837890625 325 0 326 10.547871589660645 329 -1.7747724056243896 332 -4.0442662239074707
		 335 16.096717834472656 336 13.30564022064209 339 7.9995126724243164 342 9.503291130065918
		 346 9.503291130065918;
	setAttr -s 13 ".ktl[0:12]" no no no no yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.64110797643661499 0.40340274572372437 
		0.32387232780456543 1 1 0.72476655244827271 1 1 0.41567423939704895 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.76745069026947021 -0.91502255201339722 
		0.94610083103179932 0 0 -0.68899458646774292 0 0 -0.90951353311538696 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.64110875129699707 0.40340131521224976 
		0.32387450337409973 0.28029149770736694 1 1 0.72476655244827271 1 1 0.41567423939704895 
		1 1 0.24363666772842407;
	setAttr -s 13 ".koy[0:12]"  0.76745009422302246 -0.9150230884552002 
		0.94610005617141724 -0.95991498231887817 0 0 -0.68899458646774292 0 0 -0.90951353311538696 
		0 0 -0.96986651420593262;
createNode animCurveTL -n "animCurveTL1564";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -4.7867727279663086 323 -4.7867727279663086
		 324 -4.7867727279663086 325 -4.7867727279663086 326 -4.7867727279663086 346 -4.7867727279663086;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1565";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 32.754745483398438 323 32.754745483398438
		 324 32.754745483398438 325 32.754745483398438 326 32.754745483398438 346 32.754745483398438;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1566";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -56.147525787353516 323 -56.147525787353516
		 324 -56.147525787353516 325 -56.147525787353516 326 -56.147525787353516 346 -56.147525787353516;
	setAttr -s 6 ".ktl[0:5]" no yes no yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1567";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1568";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1569";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1567";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1568";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1569";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1567";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 3.2171449661254883 323 3.2171449661254883
		 324 3.2171449661254883 325 3.2171449661254883 326 3.2171449661254883 346 3.2171449661254883;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1568";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -26.658763885498047 323 -26.658763885498047
		 324 -26.658763885498047 325 -26.658763885498047 326 -26.658763885498047 346 -26.658763885498047;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1569";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1.5793838500976563 323 1.5793838500976563
		 324 1.5793838500976563 325 1.5793838500976563 326 1.5793838500976563 346 1.5793838500976563;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1570";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 1 323 1 324 1 325 1 326 1 341 1 346 1;
	setAttr -s 7 ".ktl[0:6]" no yes no yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1571";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 1 323 1 324 1 325 1 326 1 341 1 346 1;
	setAttr -s 7 ".ktl[0:6]" no yes no yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1572";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 1 323 1 324 1 325 1 326 1 341 1 346 1;
	setAttr -s 7 ".ktl[0:6]" no yes no yes yes yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1570";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  311 0 315 -13.394008636474609 322 -1.6033740043640137
		 323 -7.400637149810791 324 9.1410350799560547 325 71.000450134277344 326 141.625732421875
		 331 28.415239334106445 335 149.89552307128906 338 134.69122314453125 341 149.89552307128906
		 346 149.89552307128906;
	setAttr -s 12 ".kit[0:11]"  2 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 12 ".ktl[1:11]" no no no no yes yes yes yes yes no no;
	setAttr -s 12 ".kix[1:11]"  0.58051925897598267 0.81709581613540649 
		0.38077917695045471 0.14284197986125946 0.023656386882066727 1 1 1 1 1 1;
	setAttr -s 12 ".kiy[1:11]"  -0.8142465353012085 0.57650190591812134 
		-0.92466598749160767 0.98974555730819702 0.99972015619277954 0 0 0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  0.58052009344100952 0.8170960545539856 
		0.38077431917190552 0.14284308254718781 0.03856426477432251 0.023656386882066727 
		1 1 1 1 1 0.015924407169222832;
	setAttr -s 12 ".koy[0:11]"  -0.81424599885940552 0.57650148868560791 
		-0.92466801404953003 0.9897453784942627 0.99925607442855835 0.99972015619277954 0 
		0 0 0 0 -0.99987322092056274;
createNode animCurveTA -n "animCurveTA1571";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  311 0 315 -2.5869998931884766 322 4.8460559844970703
		 323 2.8066353797912598 324 7.1137905120849609 325 -0.52748715877532959 326 2.9643154144287109
		 331 13.021206855773926 335 15.72467041015625 338 10.594924926757813 341 15.72467041015625
		 346 15.72467041015625;
	setAttr -s 12 ".kit[0:11]"  2 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 12 ".ktl[1:11]" no no no no yes yes yes yes yes no no;
	setAttr -s 12 ".kix[1:11]"  0.96520769596099854 0.91369330883026123 
		0.76033473014831543 0.48478314280509949 1 0.40697395801544189 0.87145817279815674 
		1 1 1 1;
	setAttr -s 12 ".kiy[1:11]"  -0.26148441433906555 0.40640443563461304 
		-0.64953142404556274 0.87463432550430298 0 0.91343975067138672 0.49046987295150757 
		0 0 0 0;
	setAttr -s 12 ".kox[0:11]"  0.96520781517028809 0.91369348764419556 
		0.76032978296279907 0.48478585481643677 0.29821166396141052 1 0.40697395801544189 
		0.87145817279815674 1 1 1 0.15009811520576477;
	setAttr -s 12 ".koy[0:11]"  -0.26148393750190735 0.406404048204422 
		-0.64953720569610596 0.87463295459747314 -0.95449978113174438 0 0.91343975067138672 
		0.49046987295150757 0 0 0 -0.98867112398147583;
createNode animCurveTA -n "animCurveTA1572";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 315 2.7722618579864502 318 -26.185152053833008
		 322 -26.818136215209961 323 -5.3825020790100098 324 -8.4549617767333984 325 4.3509631156921387
		 326 -0.057274173945188522 331 -12.404339790344238 335 -7.6826567649841309 338 -6.9205479621887207
		 341 -7.6826567649841309 346 -7.6826567649841309;
	setAttr -s 13 ".kit[0:12]"  2 2 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 13 ".kot[1:12]"  2 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 13 ".ktl[2:12]" no no no no yes yes yes yes yes no no;
	setAttr -s 13 ".kix[2:12]"  0.24009333550930023 0.9978102445602417 
		0.11068733781576157 0.61356151103973389 1 0.30671992897987366 1 0.9526364803314209 
		1 1 1;
	setAttr -s 13 ".kiy[2:12]"  -0.97074985504150391 -0.066140793263912201 
		0.99385523796081543 -0.78964692354202271 0 -0.95179986953735352 0 0.30411142110824585 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.960349440574646 0.24009338021278381 0.9978102445602417 
		0.11068566143512726 0.61356449127197266 0.18326751887798309 1 0.30671992897987366 
		1 0.9526364803314209 1 1 0.29674091935157776;
	setAttr -s 13 ".koy[0:12]"  0.27879908680915833 -0.97074979543685913 
		-0.066140666604042053 0.99385547637939453 -0.78964465856552124 0.98306310176849365 
		0 -0.95179986953735352 0 0.30411142110824585 0 0 0.95495802164077759;
createNode animCurveTL -n "animCurveTL1570";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 -4.7867727279663086 323 -4.7867727279663086
		 324 -4.7867727279663086 325 -4.7867727279663086 326 -5.1978421211242676 341 -5.1978421211242676
		 346 -5.1978421211242676;
	setAttr -s 7 ".ktl[0:6]" no yes no no no yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.1008434072136879;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0.99490231275558472;
createNode animCurveTL -n "animCurveTL1571";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 32.754745483398438 323 32.754745483398438
		 324 32.754745483398438 325 32.754745483398438 326 50.708225250244141 341 50.708225250244141
		 346 50.708225250244141;
	setAttr -s 7 ".ktl[0:6]" no yes no no no yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.0023207697086036205;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 -0.99999731779098511;
createNode animCurveTL -n "animCurveTL1572";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 56.147525787353516 323 56.147525787353516
		 324 56.147525787353516 325 56.147525787353516 326 53.501262664794922 341 53.501262664794922
		 346 53.501262664794922;
	setAttr -s 7 ".ktl[0:6]" no yes no no no yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 0.015743279829621315;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0.99987608194351196;
createNode animCurveTU -n "animCurveTU1573";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1574";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1575";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1573";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1574";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1575";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1573";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 40.544437408447266 323 40.544437408447266
		 324 40.544437408447266 325 40.544437408447266 326 40.544437408447266 346 40.544437408447266;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1574";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 43.055271148681641 323 43.055271148681641
		 324 43.055271148681641 325 43.055271148681641 326 43.055271148681641 346 43.055271148681641;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1575";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1576";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1577";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1578";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1576";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1577";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1578";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1576";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 8.0282459259033203 323 8.0282459259033203
		 324 8.0282459259033203 325 8.0282459259033203 326 8.0282459259033203 346 8.0282459259033203;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1577";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 9.9087905883789063 323 9.9087905883789063
		 324 9.9087905883789063 325 9.9087905883789063 326 9.9087905883789063 346 9.9087905883789063;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1578";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1579";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1580";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1581";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1579";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  311 0 313 -92.069427490234375 318 34.880775451660156
		 322 -88.593399047851562 323 -98.291969299316406 324 -85.960403442382812 325 -56.303268432617188
		 326 15.650372505187987 327 49.367424011230469 329 18.366724014282227 330 29.107028961181641
		 331 5.8019299507141113 332 -7.6413369178771982 333 -14.617094993591309 335 -19.937479019165039
		 339 -20.662185668945313 346 -20.662185668945313;
	setAttr -s 17 ".ktl[15:16]" no no;
	setAttr -s 17 ".kix[0:16]"  0.30361387133598328 1 1 0.08177599310874939 
		1 0.11298467218875885 0.046937901526689529 0.031515359878540039 1 1 1 0.083466432988643646 
		0.22769126296043396 0.44398760795593262 0.97504842281341553 1 1;
	setAttr -s 17 ".kiy[0:16]"  -0.95279514789581299 0 0 -0.99665075540542603 
		0 0.99359673261642456 0.99889779090881348 0.99950325489044189 0 0 0 -0.99651056528091431 
		-0.97373342514038086 -0.89603292942047119 -0.22199234366416931 0 0;
	setAttr -s 17 ".kox[0:16]"  0.051790118217468262 1 1 0.08177599310874939 
		1 0.11298467218875885 0.046937901526689529 0.031515359878540039 1 1 1 0.083466432988643646 
		0.22769126296043396 0.44398760795593262 0.97504842281341553 1 0.11477541923522949;
	setAttr -s 17 ".koy[0:16]"  -0.99865800142288208 0 0 -0.99665075540542603 
		0 0.99359673261642456 0.99889779090881348 0.99950325489044189 0 0 0 -0.99651056528091431 
		-0.97373342514038086 -0.89603292942047119 -0.22199234366416931 0 0.99339145421981812;
createNode animCurveTA -n "animCurveTA1580";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  311 0 313 -14.522864341735842 318 -66.600608825683594
		 322 -48.717964172363281 323 -53.985034942626953 324 -2.4396767616271973 325 13.611568450927734
		 326 17.682228088378906 327 4.4657740592956543 329 8.7989044189453125 330 8.1587228775024414
		 331 7.6302776336669922 332 6.3954343795776367 333 10.557422637939453 335 16.937234878540039
		 339 18.728769302368164 346 18.728769302368164;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no no;
	setAttr -s 17 ".kix[0:16]"  1 0.16382637619972229 1 1 1 0.054075982421636581 
		0.23087555170059204 1 1 1 0.9713289737701416 0.93807458877563477 1 0.38570007681846619 
		0.87145364284515381 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 -0.98648923635482788 0 0 0 0.99853682518005371 
		0.97298336029052734 0 0 0 -0.23773929476737976 -0.34643322229385376 0 0.92262423038482666 
		0.4904778003692627 0 0;
	setAttr -s 17 ".kox[0:16]"  0.31232377886772156 0.16382637619972229 
		1 1 1 0.054075982421636581 0.23087555170059204 1 1 1 0.9713289737701416 0.93807458877563477 
		1 0.38570007681846619 0.87145364284515381 1 0.12644322216510773;
	setAttr -s 17 ".koy[0:16]"  -0.94997572898864746 -0.98648923635482788 
		0 0 0 0.99853682518005371 0.97298336029052734 0 0 0 -0.23773929476737976 -0.34643322229385376 
		0 0.92262423038482666 0.4904778003692627 0 -0.99197381734848022;
createNode animCurveTA -n "animCurveTA1581";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  311 0 313 39.516876220703125 318 -8.3691234588623047
		 322 64.807533264160156 323 70.208450317382813 324 30.166221618652344 325 49.402050018310547
		 326 40.008029937744141 327 57.935512542724609 329 47.777175903320313 330 51.015285491943359
		 331 45.045696258544922 332 42.807579040527344 333 39.829147338867188 335 37.425624847412109
		 339 36.652378082275391 346 36.652378082275391;
	setAttr -s 17 ".ktl[0:16]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no no;
	setAttr -s 17 ".kix[0:16]"  0.072503961622714996 1 1 0.14576667547225952 
		1 1 1 1 1 1 1 0.50283515453338623 0.67517215013504028 0.75239098072052002 0.97173923254013062 
		1 1;
	setAttr -s 17 ".kiy[0:16]"  -0.99736815690994263 0 0 0.98931902647018433 
		0 0 0 0 0 0 0 -0.86438232660293579 -0.73766022920608521 -0.65871673822402954 -0.23605681955814362 
		0 0;
	setAttr -s 17 ".kox[0:16]"  0.11995406448841095 1 1 0.14576667547225952 
		1 1 1 1 1 1 1 0.50283515453338623 0.67517215013504028 0.75239098072052002 0.97173923254013062 
		1 0.064995355904102325;
	setAttr -s 17 ".koy[0:16]"  0.9927794337272644 0 0 0.98931902647018433 
		0 0 0 0 0 0 0 -0.86438232660293579 -0.73766022920608521 -0.65871673822402954 -0.23605681955814362 
		0 -0.99788552522659302;
createNode animCurveTL -n "animCurveTL1579";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -19.597047805786133 323 -19.597047805786133
		 324 -19.597047805786133 325 -19.597047805786133 326 -19.597047805786133 346 -19.597047805786133;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1580";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 36.545459747314453 323 36.545459747314453
		 324 36.545459747314453 325 36.545459747314453 326 36.545459747314453 346 36.545459747314453;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1581";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 39.212558746337891 323 39.212558746337891
		 324 39.212558746337891 325 39.212558746337891 326 39.212558746337891 346 39.212558746337891;
	setAttr -s 6 ".ktl[0:5]" no yes yes yes yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1582";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no no yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1583";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no no yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1584";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[0:5]" no yes no no yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1582";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  311 0 323 0 324 0 325 0 326 0 330 -4.4268579483032227
		 334 -6.5826826095581055 337 -0.84930199384689331 339 1.5670982599258423 342 -1.7359668016433716
		 346 -2.8828556537628174;
	setAttr -s 11 ".ktl[0:10]" no yes no no no yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 0.87923634052276611 1 0.65930187702178955 
		1 0.94079571962356567 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 -0.47638586163520813 0 0.75187832117080688 
		0 -0.33897411823272705 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 0.87923634052276611 1 0.65930187702178955 
		1 0.94079571962356567 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 -0.47638586163520813 0 0.75187832117080688 
		0 -0.33897411823272705 0;
createNode animCurveTA -n "animCurveTA1583";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 11 ".ktv[0:10]"  311 0 323 0 324 0 325 0 326 0 330 -3.4573965072631836
		 334 -10.759081840515137 337 3.5630810260772705 339 8.342707633972168 342 6.2516870498657227
		 346 4.9824342727661133;
	setAttr -s 11 ".ktl[0:10]" no yes no no no yes yes yes yes yes yes;
	setAttr -s 11 ".kix[0:10]"  1 1 1 1 1 0.67732083797454834 1 0.37442129850387573 
		1 0.92887735366821289 1;
	setAttr -s 11 ".kiy[0:10]"  0 0 0 0 0 -0.73568779230117798 0 0.92725867033004761 
		0 -0.37038746476173401 0;
	setAttr -s 11 ".kox[0:10]"  1 1 1 1 1 0.67732083797454834 1 0.37442129850387573 
		1 0.92887735366821289 1;
	setAttr -s 11 ".koy[0:10]"  0 0 0 0 0 -0.73568779230117798 0 0.92725867033004761 
		0 -0.37038746476173401 0;
createNode animCurveTA -n "animCurveTA1584";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  311 0 314 9.4496984481811523 323 15.490616798400879
		 324 17.995811462402344 325 -29.615400314331051 326 -23.549823760986328 330 -0.092008091509342194
		 334 -27.017528533935547 337 -32.716045379638672 339 -29.826162338256836 342 -31.064580917358398
		 346 -31.526632308959961;
	setAttr -s 12 ".kit[0:11]"  2 1 1 1 1 1 1 1 
		1 1 1 1;
	setAttr -s 12 ".ktl[2:11]" no no no yes yes yes yes yes yes yes;
	setAttr -s 12 ".kix[1:11]"  0.60402429103851318 0.96267420053482056 
		0.68987095355987549 0.050079077482223511 0.13446226716041565 1 0.38639965653419495 
		1 1 0.98962819576263428 1;
	setAttr -s 12 ".kiy[1:11]"  0.79696595668792725 0.27066266536712646 
		0.72393244504928589 -0.99874526262283325 0.99091869592666626 0 -0.92233145236968994 
		0 0 -0.14365248382091522 0;
	setAttr -s 12 ".kox[0:11]"  0.60402429103851318 0.96267431974411011 
		0.68987369537353516 0.050079643726348877 0.36623489856719971 0.13446226716041565 
		1 0.38639965653419495 1 1 0.98962819576263428 1;
	setAttr -s 12 ".koy[0:11]"  0.7969658374786377 0.27066269516944885 
		0.72392970323562622 -0.99874520301818848 0.93052244186401367 0.99091869592666626 
		0 -0.92233145236968994 0 0 -0.14365248382091522 0;
createNode animCurveTL -n "animCurveTL1582";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 51.6451416015625 323 54.380832672119141
		 324 53.260227203369141 325 51.389179229736328 326 51.372318267822266 330 56.520481109619141
		 334 55.325885772705078 346 55.325885772705078;
	setAttr -s 8 ".ktl[1:7]" no no no yes yes no no;
	setAttr -s 8 ".kix[0:7]"  1 0.1797909140586853 0.0371566042304039 
		0.02226363867521286 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0.98370480537414551 -0.9993094801902771 
		-0.99975210428237915 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  0.1797909140586853 0.037156891077756882 
		0.022263811901211739 0.92696577310562134 1 1 1 0.011319278739392757;
	setAttr -s 8 ".koy[0:7]"  0.98370480537414551 -0.9993094801902771 
		-0.99975216388702393 -0.37514582276344299 0 0 0 -0.99993592500686646;
createNode animCurveTL -n "animCurveTL1583";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 -11.264523506164551 323 -24.104467391967773
		 324 -62.26214599609375 325 2.0805473327636719 326 3.6952033042907715 330 -9.9185276031494141
		 334 -3.6762938499450684 346 -3.6762938499450684;
	setAttr -s 8 ".ktl[0:7]" no no no no yes yes no no;
	setAttr -s 8 ".kix[0:7]"  1 0.038911484181880951 0.0010919610504060984 
		0.00064757518703117967 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 -0.99924260377883911 -0.99999940395355225 
		0.99999982118606567 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  0.038911491632461548 0.0010919660562649369 
		0.00064758013468235731 0.025796309113502502 1 1 1 0.0054907915182411671;
	setAttr -s 8 ".koy[0:7]"  -0.99924266338348389 -0.99999940395355225 
		0.99999982118606567 0.99966722726821899 0 0 0 -0.99998492002487183;
createNode animCurveTL -n "animCurveTL1584";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 0 323 -1.15435791015625 324 -1.5445106029510498
		 325 0.34390118718147278 326 1.3505340814590454 330 -0.36122587323188782 334 1.4929357767105103
		 346 1.4929357767105103;
	setAttr -s 8 ".ktl[1:7]" no no no yes yes no no;
	setAttr -s 8 ".kix[0:7]"  1 0.39745897054672241 0.10619193315505981 
		0.02205902524292469 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 -0.91761994361877441 -0.99434560537338257 
		0.99975663423538208 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  0.39745897054672241 0.10619275271892548 
		0.022059196606278419 0.04135606437921524 1 1 1 0.027897924184799194;
	setAttr -s 8 ".koy[0:7]"  -0.91761994361877441 -0.99434554576873779 
		0.99975663423538208 0.99914443492889404 0 0 0 -0.99961072206497192;
createNode animCurveTU -n "animCurveTU1585";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1586";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1587";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1 326 1 346 1;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1585";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  311 0 313 80.998382568359375 315 -38.021381378173828
		 317 -57.122352600097656 320 65.858261108398438 323 99.528350830078125 324 89.554054260253906
		 325 55.826610565185547 326 -0.64893776178359985 329 43.180778503417969 331 53.261711120605469
		 333 -10.448629379272461 336 -16.460367202758789 340 -15.325519561767578 346 -15.325519561767578;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no no;
	setAttr -s 15 ".kix[0:14]"  1 1 0.083035282790660858 1 0.070725739002227783 
		1 0.10860893875360489 0.034672863781452179 1 0.1559457927942276 1 0.3690744936466217 
		1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 -0.99654662609100342 0 0.99749583005905151 
		0 -0.99408453702926636 -0.99939870834350586 0 0.9877656102180481 0 -0.92939984798431396 
		0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.05884571373462677 1 0.083035282790660858 
		1 0.070725739002227783 1 0.10860893875360489 0.034672863781452179 1 0.1559457927942276 
		1 0.3690744936466217 1 1 0.15391579270362854;
	setAttr -s 15 ".koy[0:14]"  0.99826711416244507 0 -0.99654662609100342 
		0 0.99749583005905151 0 -0.99408453702926636 -0.99939870834350586 0 0.9877656102180481 
		0 -0.92939984798431396 0 0 0.98808395862579346;
createNode animCurveTA -n "animCurveTA1586";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  311 0 313 18.959829330444336 315 -13.833868026733398
		 317 4.1623139381408691 320 71.039161682128906 323 34.178653717041016 324 1.5321577787399292
		 325 -10.962892532348633 326 4.2494359016418457 329 -3.8102059364318848 331 -5.9018120765686035
		 333 -26.261322021484375 336 -32.379497528076172 340 -24.335073471069336 346 -24.335073471069336;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no no;
	setAttr -s 15 ".kix[0:14]"  1 1 1 0.088094219565391541 1 0.10566577315330505 
		0.068858981132507324 1 1 0.78785181045532227 0.6055484414100647 0.36350858211517334 
		1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0 0.9961121678352356 0 -0.99440169334411621 
		-0.99762648344039917 0 0 -0.61586493253707886 -0.79580843448638916 -0.93159085512161255 
		0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.24420700967311859 1 1 0.088094219565391541 
		1 0.10566577315330505 0.068858981132507324 1 1 0.78785181045532227 0.6055484414100647 
		0.36350858211517334 1 1 0.097632050514221191;
	setAttr -s 15 ".koy[0:14]"  0.96972322463989258 0 0 0.9961121678352356 
		0 -0.99440169334411621 -0.99762648344039917 0 0 -0.61586493253707886 -0.79580843448638916 
		-0.93159085512161255 0 0 0.99522262811660767;
createNode animCurveTA -n "animCurveTA1587";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  311 0 313 29.294378280639652 315 30.937372207641602
		 317 39.010414123535156 320 78.436233520507813 323 74.543998718261719 324 15.55471897125244
		 325 43.769844055175781 326 37.479255676269531 329 41.229320526123047 331 41.119449615478516
		 333 46.857860565185547 336 48.194267272949219 340 46.762805938720703 346 46.762805938720703;
	setAttr -s 15 ".ktl[0:14]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes no no;
	setAttr -s 15 ".kix[0:14]"  1 0.69577234983444214 1 0.20691414177417755 
		1 0.52284252643585205 1 1 1 1 1 0.87258291244506836 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0.71826237440109253 0 0.97835910320281982 
		0 -0.85242933034896851 0 0 0 0 0 0.4884660542011261 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.16086716949939728 0.69577234983444214 
		1 0.20691414177417755 1 0.52284252643585205 1 1 1 1 1 0.87258291244506836 1 1 0.050984710454940796;
	setAttr -s 15 ".koy[0:14]"  0.9869760274887085 0.71826237440109253 
		0 0.97835910320281982 0 -0.85242933034896851 0 0 0 0 0 0.4884660542011261 0 0 -0.99869942665100098;
createNode animCurveTL -n "animCurveTL1585";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -19.609930038452148 323 -19.609930038452148
		 324 -19.609930038452148 325 -19.609930038452148 326 -19.609930038452148 346 -19.609930038452148;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1586";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 36.542636871337891 323 36.542636871337891
		 324 36.542636871337891 325 36.542636871337891 326 36.542636871337891 346 36.542636871337891;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1587";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -39.208473205566406 323 -39.208473205566406
		 324 -39.208473205566406 325 -39.208473205566406 326 -39.208473205566406 346 -39.208473205566406;
	setAttr -s 6 ".ktl[5]" no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1588";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 1 322 1 323 1 324 1 325 1.0884524583816528
		 326 1 346 1;
	setAttr -s 7 ".kit[3:6]"  2 1 1 1;
	setAttr -s 7 ".kot[3:6]"  2 1 1 1;
	setAttr -s 7 ".ktl[0:6]" no no no yes no yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.42614850401878357 0.42614850401878357 
		1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.90465319156646729 -0.90465319156646729 
		0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 0.42615121603012085 0.42614319920539856 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0.90465205907821655 -0.90465575456619263 
		0 0;
createNode animCurveTU -n "animCurveTU1589";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 322 1 323 1.2522977590560913 324 0.24455739557743073
		 325 0.24455739557743073 326 0.53665012121200562 329 1 346 1;
	setAttr -s 8 ".kit[0:7]"  1 2 2 2 2 1 1 1;
	setAttr -s 8 ".kot[0:7]"  1 2 2 2 2 1 1 1;
	setAttr -s 8 ".ktl[0:7]" no yes yes yes yes no yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.16293925046920776 0.041311644017696381 
		1 0.14121919870376587 0.26046296954154968 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0.98663604259490967 -0.99914628267288208 
		0 0.9899783730506897 0.9654839038848877 0;
	setAttr -s 8 ".kox[0:7]"  1 0.16293925046920776 0.041311644017696381 
		1 0.14121706783771515 0.26046299934387207 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0.98663604259490967 -0.99914628267288208 
		0 0.9899786114692688 0.96548384428024292 0 0;
createNode animCurveTU -n "animCurveTU1590";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 1 322 1 323 1 324 1 325 1.2062995433807373
		 326 1 346 1;
	setAttr -s 7 ".kit[3:6]"  2 1 1 1;
	setAttr -s 7 ".kot[3:6]"  2 1 1 1;
	setAttr -s 7 ".ktl[0:6]" no no no yes no yes no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 0.19797411561012268 0.19797411561012268 
		1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0.98020726442337036 -0.98020726442337036 
		0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 0.19797556102275848 0.19797119498252869 
		1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0.98020690679550171 -0.98020780086517334 
		0 0;
createNode animCurveTA -n "animCurveTA1588";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 0 322 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1589";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 0 322 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1590";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 0 322 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1588";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 -4.502251148223877 322 -4.502251148223877
		 323 -4.502251148223877 324 -4.502251148223877 325 -4.502251148223877 326 -4.502251148223877
		 346 -4.502251148223877;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1589";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 37.76336669921875 322 37.76336669921875
		 323 37.76336669921875 324 37.76336669921875 325 37.76336669921875 326 37.76336669921875
		 346 37.76336669921875;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1590";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  311 0 322 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 7 ".ktl[0:6]" no no no no no no no;
	setAttr -s 7 ".kix[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".kiy[0:6]"  0 0 0 0 0 0 0;
	setAttr -s 7 ".kox[0:6]"  1 1 1 1 1 1 1;
	setAttr -s 7 ".koy[0:6]"  0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1591";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1.2667344808578491
		 326 1 346 1;
	setAttr -s 6 ".kit[2:5]"  2 1 1 1;
	setAttr -s 6 ".kot[2:5]"  2 1 1 1;
	setAttr -s 6 ".ktl[0:5]" no no yes no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.15433855354785919 0.15433855354785919 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0.98801803588867188 -0.98801803588867188 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.15433971583843231 0.15433625876903534 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0.98801785707473755 -0.98801833391189575 
		0 0;
createNode animCurveTU -n "animCurveTU1592";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 0.84580302238464355 325 0.84580302238464355
		 326 0.84580302238464355 346 0.84580302238464355;
	setAttr -s 6 ".kit[1:5]"  2 2 1 1 1;
	setAttr -s 6 ".kot[1:5]"  2 2 1 1 1;
	setAttr -s 6 ".ktl[0:5]" no yes yes no no no;
	setAttr -s 6 ".kix[0:5]"  1 1 0.26086309552192688 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 -0.96537584066390991 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 0.26086309552192688 1 1 1 0.26085752248764038;
	setAttr -s 6 ".koy[0:5]"  0 -0.96537584066390991 0 0 0 0.9653773307800293;
createNode animCurveTU -n "animCurveTU1593";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 1 323 1 324 1 325 1.2928282022476196
		 326 1 346 1;
	setAttr -s 6 ".kit[2:5]"  2 1 1 1;
	setAttr -s 6 ".kot[2:5]"  2 1 1 1;
	setAttr -s 6 ".ktl[0:5]" no no yes no yes no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 0.1408715546131134 0.1408715546131134 
		1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0.99002790451049805 -0.99002790451049805 
		0;
	setAttr -s 6 ".kox[0:5]"  1 1 0.14087259769439697 0.14086945354938507 
		1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0.9900277853012085 -0.99002814292907715 
		0 0;
createNode animCurveTA -n "animCurveTA1591";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1592";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1593";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1591";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 -1.4725730419158936 323 -1.4725730419158936
		 324 -1.4725730419158936 325 -1.4725730419158936 326 -1.4725730419158936 346 -1.4725730419158936;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1592";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 22.299345016479492 323 22.299345016479492
		 324 22.299345016479492 325 22.299345016479492 326 22.299345016479492 346 22.299345016479492;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL1593";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  311 0 323 0 324 0 325 0 326 0 346 0;
	setAttr -s 6 ".ktl[0:5]" no no no yes no no;
	setAttr -s 6 ".kix[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".kiy[0:5]"  0 0 0 0 0 0;
	setAttr -s 6 ".kox[0:5]"  1 1 1 1 1 1;
	setAttr -s 6 ".koy[0:5]"  0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1594";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 1 325 1 326 1 332 1 339 1
		 346 1;
	setAttr -s 8 ".ktl[7]" no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1595";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 0.91948240995407104 325 0.57257473468780518
		 326 0.91948240995407104 332 0.91948240995407104 339 0.91948240995407104 346 0.91948240995407104;
	setAttr -s 8 ".ktl[1:7]" no yes yes no yes yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 0.16998469829559326 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 -0.98544669151306152 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 0.16998469829559326 1 1 1 1 0.45958822965621948;
	setAttr -s 8 ".koy[0:7]"  0 0 -0.98544669151306152 0 0 0 0 0.88813203573226929;
createNode animCurveTU -n "animCurveTU1596";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 1 325 1 326 1 332 1 339 1
		 346 1;
	setAttr -s 8 ".ktl[7]" no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1594";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  311 0 315 4.1167817115783691 323 -4.5950298309326172
		 324 -0.38386094570159912 325 -0.38386094570159912 326 -0.38386094570159912 332 -2.5559208393096924
		 335 -8.7616968154907227 339 -9.3693723678588867 346 -9.3693723678588867;
	setAttr -s 10 ".ktl[0:9]" no yes yes no yes no yes yes no no;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 0.91023963689804077 0.98226070404052734 
		1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 -0.41408175230026245 -0.18752041459083557 
		0 0;
	setAttr -s 10 ".kox[0:9]"  0.91829967498779297 1 1 1 1 1 0.91023963689804077 
		0.98226070404052734 1 0.24690802395343781;
	setAttr -s 10 ".koy[0:9]"  0.39588591456413269 0 0 0 0 0 -0.41408175230026245 
		-0.18752041459083557 0 0.96903890371322632;
createNode animCurveTA -n "animCurveTA1595";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  311 0 315 2.3205935955047607 323 11.359030723571777
		 324 7.3947577476501465 325 7.3947577476501465 326 7.3947577476501465 332 7.5339694023132324
		 335 7.931708812713623 339 7.964219093322753 346 7.964219093322753;
	setAttr -s 10 ".ktl[0:9]" no yes yes no yes no yes yes no no;
	setAttr -s 10 ".kix[0:9]"  1 0.80805784463882446 1 1 1 1 0.99957525730133057 
		0.99994784593582153 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0.58910322189331055 0 0 0 0 0.029143974184989929 
		0.010212758556008339 0 0;
	setAttr -s 10 ".kox[0:9]"  0.97171932458877563 0.80805784463882446 
		1 1 1 1 0.99957525730133057 0.99994784593582153 1 0.28712961077690125;
	setAttr -s 10 ".koy[0:9]"  0.23613902926445007 0.58910322189331055 
		0 0 0 0 0.029143974184989929 0.010212758556008339 0 -0.95789176225662231;
createNode animCurveTA -n "animCurveTA1596";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 12 ".ktv[0:11]"  311 0 315 -17.229801177978516 317 -43.015480041503906
		 319 -100.50183868408203 323 -158.94857788085937 324 -175.74687194824219 325 -175.74687194824219
		 326 -175.74687194824219 332 -176.03703308105469 335 -176.86604309082031 339 -176.95005798339844
		 346 -176.95005798339844;
	setAttr -s 12 ".ktl[0:11]" no yes yes yes yes no yes no yes yes no 
		no;
	setAttr -s 12 ".kix[0:11]"  1 0.26747694611549377 0.11392911523580551 
		0.109467513859272 0.15028423070907593 1 1 1 0.99815863370895386 0.99965184926986694 
		1 1;
	setAttr -s 12 ".kiy[0:11]"  0 -0.96356427669525146 -0.9934888482093811 
		-0.99399042129516602 -0.98864287137985229 0 0 0 -0.060658197849988937 -0.026388458907604218 
		0 0;
	setAttr -s 12 ".kox[0:11]"  0.48475828766822815 0.26747694611549377 
		0.11392911523580551 0.109467513859272 0.15028423070907593 1 1 1 0.99815863370895386 
		0.99965184926986694 1 0.013490141369402409;
	setAttr -s 12 ".koy[0:11]"  -0.87464809417724609 -0.96356427669525146 
		-0.9934888482093811 -0.99399042129516602 -0.98864287137985229 0 0 0 -0.060658197849988937 
		-0.026388458907604218 0 0.99990898370742798;
createNode animCurveTL -n "animCurveTL1594";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 315 2.3999931812286377 317 6.1229324340820313
		 319 3.3836722373962402 323 -27.507247924804688 324 -51.108295440673828 325 -51.641773223876953
		 326 -48.6094970703125 329 -45.775897979736328 332 -49.367130279541016 335 -50.5341796875
		 339 -50.568580627441406 346 -50.568580627441406;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.023142013698816299 1 0.010140093974769115 
		0.001911439117975533 0.026025904342532158 1 0.014702912420034409 1 0.035944368690252304 
		0.85021281242370605 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.99973219633102417 0 -0.99994862079620361 
		-0.99999815225601196 -0.99966120719909668 0 0.99989193677902222 0 -0.99935382604598999 
		-0.52643907070159912 0 0;
	setAttr -s 13 ".kox[0:12]"  0.069277919828891754 0.023142013698816299 
		1 0.010140093974769115 0.001911439117975533 0.026025904342532158 1 0.014702912420034409 
		1 0.035944368690252304 0.85021281242370605 1 0.00082395068602636456;
	setAttr -s 13 ".koy[0:12]"  0.997597336769104 0.99973219633102417 0 
		-0.99994862079620361 -0.99999815225601196 -0.99966120719909668 0 0.99989193677902222 
		0 -0.99935382604598999 -0.52643907070159912 0 0.99999964237213135;
createNode animCurveTL -n "animCurveTL1595";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 309.53298950195312 315 346.696533203125
		 317 354.493896484375 319 341.86544799804687 323 165.61935424804687 324 65.678993225097656
		 325 58.445964813232422 326 99.558502197265625 329 137.97732543945312 332 106.61407470703125
		 335 106.33885192871094 339 106.33885192871094 346 106.33885192871094;
	setAttr -s 13 ".ktl[10:12]" no yes no;
	setAttr -s 13 ".kix[0:12]"  0.00013461077469401062 0.0063179982826113701 
		1 0.002199606504291296 0.00039828851004131138 0.0019201997201889753 1 0.0010845352662727237 
		1 0.14968526363372803 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  1 0.99998003244400024 0 -0.99999761581420898 
		-0.99999994039535522 -0.99999815225601196 0 0.99999940395355225 0 -0.98873364925384521 
		0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.0044846488162875175 0.0063179982826113701 
		1 0.002199606504291296 0.00039828851004131138 0.0019201997201889753 1 0.0010845352662727237 
		1 0.14968526363372803 1 1 0.00039182085311040282;
	setAttr -s 13 ".koy[0:12]"  0.99998992681503296 0.99998003244400024 
		0 -0.99999761581420898 -0.99999994039535522 -0.99999815225601196 0 0.99999940395355225 
		0 -0.98873364925384521 0 0 -0.99999994039535522;
createNode animCurveTL -n "animCurveTL1596";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 315 5.5168495178222656 317 6.7534313201904297
		 319 7.4045219421386719 323 16.203227996826172 324 9.5698089599609375 325 9.4521112442016602
		 326 10.12110424041748 329 10.746265411376953 332 15.812214851379395 335 31.713827133178711
		 339 33.121353149414062 346 33.121353149414062;
	setAttr -s 13 ".ktl[0:12]" no yes yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.029397508129477501 1 0.042624685913324356 
		1 0.11719164252281189 1 0.094554156064987183 0.06650194525718689 0.0085773756727576256 
		0.039439685642719269 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.99956774711608887 0 0.99909120798110962 
		0 -0.99310928583145142 0 0.99551975727081299 0.99778622388839722 0.99996322393417358 
		0.99922192096710205 0 0;
	setAttr -s 13 ".kox[0:12]"  0.030196761712431908 0.029397508129477501 
		1 0.042624685913324356 1 0.11719164252281189 1 0.094554156064987183 0.06650194525718689 
		0.0085773756727576256 0.039439685642719269 1 0.001257977681234479;
	setAttr -s 13 ".koy[0:12]"  0.99954396486282349 0.99956774711608887 
		0 0.99909120798110962 0 -0.99310928583145142 0 0.99551975727081299 0.99778622388839722 
		0.99996322393417358 0.99922192096710205 0 -0.99999922513961792;
createNode animCurveTU -n "animCurveTU1597";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 1 325 1 326 1 331 1 340 1
		 346 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1598";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 1 325 1 326 1 331 1 340 1
		 346 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU1599";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 1 323 1 324 1 325 1 326 1 331 1 340 1
		 346 1;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA1597";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 320 -0.31776291131973267 323 -21.593015670776367
		 324 -16.731330871582031 325 0.43097010254859924 326 14.513401031494139 331 11.009120941162109
		 332 4.8894782066345215 334 11.281235694885254 337 1.5891982316970825 340 0.8174285888671875
		 342 1.2188594341278076 346 1.2188594341278076;
	setAttr -s 13 ".ktl[0:12]" no no yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.99989068508148193 1 0.2118714302778244 
		0.10600783675909042 1 0.75044488906860352 1 1 0.95151454210281372 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 -0.014787741005420685 0 0.97729760408401489 
		0.99436533451080322 0 -0.66093307733535767 0 0 -0.30760380625724792 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.99989068508148193 0.31904169917106628 
		1 0.2118714302778244 0.10600783675909042 1 0.75044488906860352 1 1 0.95151454210281372 
		1 1 0.89063262939453125;
	setAttr -s 13 ".koy[0:12]"  -0.01478774007409811 -0.94774067401885986 
		0 0.97729760408401489 0.99436533451080322 0 -0.66093307733535767 0 0 -0.30760380625724792 
		0 0 -0.4547235369682312;
createNode animCurveTA -n "animCurveTA1598";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  311 0 320 5.718846321105957 323 15.137806892395018
		 324 5.1072812080383301 325 -1.4418989419937134 326 1.844855785369873 331 1.7031530141830444
		 332 8.8011875152587891 334 20.408351898193359 337 -4.7511773109436035 340 -8.6759099960327148
		 342 -2.8117833137512207 346 -2.8117833137512207;
	setAttr -s 13 ".ktl[0:12]" no no yes yes yes yes yes yes yes yes yes 
		no no;
	setAttr -s 13 ".kix[0:12]"  1 0.96635496616363525 1 0.17324303090572357 
		1 1 1 0.1986226886510849 1 0.51968580484390259 1 1 1;
	setAttr -s 13 ".kiy[0:12]"  0 0.25721198320388794 0 -0.98487907648086548 
		0 0 0 0.98007601499557495 0 -0.85435748100280762 0 0 0;
	setAttr -s 13 ".kox[0:12]"  0.96635496616363525 0.60527408123016357 
		1 0.17324303090572357 1 1 1 0.1986226886510849 1 0.51968580484390259 1 1 0.64721888303756714;
	setAttr -s 13 ".koy[0:12]"  0.25721201300621033 0.79601716995239258 
		0 -0.98487907648086548 0 0 0 0.98007601499557495 0 -0.85435748100280762 0 0 0.76230418682098389;
createNode animCurveTA -n "animCurveTA1599";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 15 ".ktv[0:14]"  311 0 315 23.050628662109375 320 37.346958160400391
		 322 -98.62060546875 323 -106.30442810058594 324 -106.578857421875 325 -13.171567916870117
		 326 40.782901763916016 331 -72.1673583984375 332 -47.004276275634766 334 25.428878784179687
		 337 27.857112884521484 340 27.204971313476563 342 28.165576934814453 346 28.165576934814453;
	setAttr -s 15 ".ktl[0:14]" no yes no no yes yes yes yes yes yes yes 
		yes yes no no;
	setAttr -s 15 ".kix[0:14]"  1 1 0.64091271162033081 0.035094514489173889 
		0.057140108197927475 1 0.01976390928030014 1 1 0.046324107795953751 0.70107436180114746 
		1 1 1 1;
	setAttr -s 15 ".kiy[0:14]"  0 0 0.76761370897293091 -0.99938404560089111 
		-0.99836623668670654 0 0.99980467557907104 0 0 0.99892652034759521 0.71308815479278564 
		0 0 0 0;
	setAttr -s 15 ".kox[0:14]"  0.38273245096206665 1 0.035094514489173889 
		0.29670009016990662 0.057140108197927475 1 0.01976390928030014 1 1 0.046324107795953751 
		0.70107436180114746 1 1 1 0.084456309676170349;
	setAttr -s 15 ".koy[0:14]"  0.92385923862457275 0 -0.99938404560089111 
		-0.95497071743011475 -0.99836623668670654 0 0.99980467557907104 0 0 0.99892652034759521 
		0.71308815479278564 0 0 0 -0.99642717838287354;
createNode animCurveTL -n "animCurveTL1597";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 -54.946929931640625 323 -54.946929931640625
		 324 -54.946929931640625 325 -54.946929931640625 326 -64.218505859375 331 -54.518280029296875
		 340 -54.518280029296875 346 -54.518280029296875;
	setAttr -s 8 ".ktl[3:7]" no yes no yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 0.096747115254402161;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 -0.99530899524688721;
createNode animCurveTL -n "animCurveTL1598";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 2.3482637405395508 323 2.3482637405395508
		 324 2.3482637405395508 325 2.3482637405395508 326 7.1507077217102051 331 4.2278079986572266
		 340 4.2278079986572266 346 4.2278079986572266;
	setAttr -s 8 ".ktl[3:7]" no yes no yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 0.022162707522511482;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 -0.99975436925888062;
createNode animCurveTL -n "animCurveTL1599";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  311 0 323 0 324 0 325 0 326 -1.3410685062408447
		 331 -0.20625694096088409 340 -0.20625694096088409 346 -0.20625694096088409;
	setAttr -s 8 ".ktl[3:7]" no yes no yes no;
	setAttr -s 8 ".kix[0:7]"  1 1 1 1 1 1 1 1;
	setAttr -s 8 ".kiy[0:7]"  0 0 0 0 0 0 0 0;
	setAttr -s 8 ".kox[0:7]"  1 1 1 1 1 1 1 0.19801050424575806;
	setAttr -s 8 ".koy[0:7]"  0 0 0 0 0 0 0 0.98019993305206299;
select -ne :time1;
	setAttr ".o" 346;
	setAttr ".unw" 346;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "dieSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU1561.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU1562.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU1563.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA1561.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA1562.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA1563.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL1561.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL1562.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL1563.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU1564.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU1565.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU1566.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA1564.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA1565.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA1566.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL1564.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL1565.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL1566.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU1567.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU1568.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU1569.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA1567.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA1568.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA1569.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL1567.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL1568.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL1569.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU1570.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU1571.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU1572.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA1570.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA1571.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA1572.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL1570.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL1571.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL1572.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU1573.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU1574.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU1575.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA1573.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA1574.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA1575.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL1573.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL1574.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL1575.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU1576.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU1577.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU1578.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA1576.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA1577.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA1578.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL1576.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL1577.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL1578.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU1579.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU1580.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU1581.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA1579.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA1580.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA1581.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL1579.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL1580.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL1581.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU1582.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU1583.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU1584.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA1582.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA1583.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA1584.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL1582.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL1583.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL1584.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU1585.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU1586.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU1587.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA1585.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA1586.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA1587.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL1585.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL1586.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL1587.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU1588.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU1589.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU1590.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA1588.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA1589.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA1590.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL1588.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL1589.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL1590.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU1591.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU1592.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU1593.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA1591.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA1592.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA1593.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL1591.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL1592.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL1593.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU1594.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU1595.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU1596.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA1594.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA1595.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA1596.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL1594.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL1595.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL1596.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU1597.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU1598.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU1599.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA1597.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA1598.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA1599.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL1597.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL1598.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL1599.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_die.ma
