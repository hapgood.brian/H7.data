//Maya ASCII 2013 scene
//Name: dragon_panic.ma
//Last modified: Mon, Jul 14, 2014 01:03:10 PM
//Codeset: 1252
requires maya "2013";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2013";
fileInfo "version" "2013 x64";
fileInfo "cutIdentifier" "201202220241-825136";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode clipLibrary -n "clipLibrary1";
	setAttr -s 117 ".cel[0].cev";
	setAttr ".cd[0].cm" -type "characterMapping" 117 "right_arm_drag.scaleZ" 0 
		1 "right_arm_drag.scaleY" 0 2 "right_arm_drag.scaleX" 0 3 "right_arm_drag.rotateZ" 
		2 1 "right_arm_drag.rotateY" 2 2 "right_arm_drag.rotateX" 2 
		3 "right_arm_drag.translateZ" 1 1 "right_arm_drag.translateY" 1 
		2 "right_arm_drag.translateX" 1 3 "right_shoulder_drag.scaleZ" 0 
		4 "right_shoulder_drag.scaleY" 0 5 "right_shoulder_drag.scaleX" 0 
		6 "right_shoulder_drag.rotateZ" 2 4 "right_shoulder_drag.rotateY" 
		2 5 "right_shoulder_drag.rotateX" 2 6 "right_shoulder_drag.translateZ" 
		1 4 "right_shoulder_drag.translateY" 1 5 "right_shoulder_drag.translateX" 
		1 6 "left_arm_drag.scaleZ" 0 7 "left_arm_drag.scaleY" 0 
		8 "left_arm_drag.scaleX" 0 9 "left_arm_drag.rotateZ" 2 7 "left_arm_drag.rotateY" 
		2 8 "left_arm_drag.rotateX" 2 9 "left_arm_drag.translateZ" 1 
		7 "left_arm_drag.translateY" 1 8 "left_arm_drag.translateX" 1 
		9 "left_shoulder_drag.scaleZ" 0 10 "left_shoulder_drag.scaleY" 0 
		11 "left_shoulder_drag.scaleX" 0 12 "left_shoulder_drag.rotateZ" 2 
		10 "left_shoulder_drag.rotateY" 2 11 "left_shoulder_drag.rotateX" 2 
		12 "left_shoulder_drag.translateZ" 1 10 "left_shoulder_drag.translateY" 
		1 11 "left_shoulder_drag.translateX" 1 12 "brows_drag.scaleZ" 0 
		13 "brows_drag.scaleY" 0 14 "brows_drag.scaleX" 0 15 "brows_drag.rotateZ" 
		2 13 "brows_drag.rotateY" 2 14 "brows_drag.rotateX" 2 15 "brows_drag.translateZ" 
		1 13 "brows_drag.translateY" 1 14 "brows_drag.translateX" 1 
		15 "eyes_drag.scaleZ" 0 16 "eyes_drag.scaleY" 0 17 "eyes_drag.scaleX" 
		0 18 "eyes_drag.rotateZ" 2 16 "eyes_drag.rotateY" 2 17 "eyes_drag.rotateX" 
		2 18 "eyes_drag.translateZ" 1 16 "eyes_drag.translateY" 1 
		17 "eyes_drag.translateX" 1 18 "left_wing_drag.scaleZ" 0 19 "left_wing_drag.scaleY" 
		0 20 "left_wing_drag.scaleX" 0 21 "left_wing_drag.rotateZ" 2 
		19 "left_wing_drag.rotateY" 2 20 "left_wing_drag.rotateX" 2 21 "left_wing_drag.translateZ" 
		1 19 "left_wing_drag.translateY" 1 20 "left_wing_drag.translateX" 
		1 21 "snout_drag.scaleZ" 0 22 "snout_drag.scaleY" 0 23 "snout_drag.scaleX" 
		0 24 "snout_drag.rotateZ" 2 22 "snout_drag.rotateY" 2 23 "snout_drag.rotateX" 
		2 24 "snout_drag.translateZ" 1 22 "snout_drag.translateY" 1 
		23 "snout_drag.translateX" 1 24 "right_wing_drag.scaleZ" 0 25 "right_wing_drag.scaleY" 
		0 26 "right_wing_drag.scaleX" 0 27 "right_wing_drag.rotateZ" 2 
		25 "right_wing_drag.rotateY" 2 26 "right_wing_drag.rotateX" 2 27 "right_wing_drag.translateZ" 
		1 25 "right_wing_drag.translateY" 1 26 "right_wing_drag.translateX" 
		1 27 "head_drag.scaleZ" 0 28 "head_drag.scaleY" 0 29 "head_drag.scaleX" 
		0 30 "head_drag.rotateZ" 2 28 "head_drag.rotateY" 2 29 "head_drag.rotateX" 
		2 30 "head_drag.translateZ" 1 28 "head_drag.translateY" 1 
		29 "head_drag.translateX" 1 30 "body_drag.scaleZ" 0 31 "body_drag.scaleY" 
		0 32 "body_drag.scaleX" 0 33 "body_drag.rotateZ" 2 31 "body_drag.rotateY" 
		2 32 "body_drag.rotateX" 2 33 "body_drag.translateZ" 1 31 "body_drag.translateY" 
		1 32 "body_drag.translateX" 1 33 "root_drag.scaleZ" 0 34 "root_drag.scaleY" 
		0 35 "root_drag.scaleX" 0 36 "root_drag.rotateZ" 2 34 "root_drag.rotateY" 
		2 35 "root_drag.rotateX" 2 36 "root_drag.translateZ" 1 34 "root_drag.translateY" 
		1 35 "root_drag.translateX" 1 36 "tail_drag.scaleZ" 0 37 "tail_drag.scaleY" 
		0 38 "tail_drag.scaleX" 0 39 "tail_drag.rotateZ" 2 37 "tail_drag.rotateY" 
		2 38 "tail_drag.rotateX" 2 39 "tail_drag.translateZ" 1 37 "tail_drag.translateY" 
		1 38 "tail_drag.translateX" 1 39  ;
	setAttr ".cd[0].cim" -type "Int32Array" 117 0 1 2 3 4
		 5 6 7 8 9 10 11 12 13 14 15 16
		 17 18 19 20 21 22 23 24 25 26 27 28
		 29 30 31 32 33 34 35 36 37 38 39 40
		 41 42 43 44 45 46 47 48 49 50 51 52
		 53 54 55 56 57 58 59 60 61 62 63 64
		 65 66 67 68 69 70 71 72 73 74 75 76
		 77 78 79 80 81 82 83 84 85 86 87 88
		 89 90 91 92 93 94 95 96 97 98 99 100
		 101 102 103 104 105 106 107 108 109 110 111 112
		 113 114 115 116 ;
createNode animClip -n "panicSource";
	setAttr ".ihi" 0;
	setAttr -s 117 ".ac[0:116]" yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr ".ss" 141;
	setAttr ".se" 210;
	setAttr ".ci" no;
createNode animCurveTU -n "animCurveTU937";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU938";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU939";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA937";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA938";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA939";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL937";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 3.2171449661254883 210 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL938";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 -26.658763885498047 210 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL939";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 -1.5793838500976563 210 -1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU940";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 1 158 1 210 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU941";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 1 158 1 210 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU942";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 1 158 1 210 1;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTA -n "animCurveTA940";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  141 0 144 -19.877483367919922 145 -13.787949562072754
		 148 -35.788864135742188 151 -27.973016738891602 153 -15.125253677368166 155 -8.8949785232543945
		 158 -2.2662956714630127 161 -13.655542373657227 164 -7.3244314193725586 167 -20.784921646118164
		 171 -3.0675795078277588 173 -13.410502433776855 175 -13.135085105895996 178 -18.682941436767578
		 180 -27.688751220703125 182 -25.648448944091797 183 -22.56462287902832 185 -41.772346496582031
		 188 -12.756789207458496 191 -35.771598815917969 194 -11.923966407775879 199 -35.499729156494141
		 203 -12.57973575592041 210 0;
	setAttr -s 25 ".ktl[0:24]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 25 ".kix[0:24]"  1 0.33897420763969421 0.36499091982841492 
		1 0.46746727824211121 0.37234550714492798 0.66760545969009399 1 1 1 1 1 1 1 0.42713776230812073 
		1 0.6150590181350708 1 1 1 1 1 1 0.40488943457603455 1;
	setAttr -s 25 ".kiy[0:24]"  0 -0.9407956600189209 0.93101108074188232 
		0 0.88401037454605103 0.92809420824050903 0.74451518058776855 0 0 0 0 0 0 0 -0.90418660640716553 
		0 0.78848111629486084 0 0 0 0 0 0 0.91436570882797241 0;
	setAttr -s 25 ".kox[0:24]"  0.33897420763969421 0.3649897575378418 
		0.30954232811927795 1 0.46746727824211121 0.37234550714492798 0.66760545969009399 
		1 1 1 1 1 1 1 0.42713776230812073 1 0.6150590181350708 1 1 1 1 1 1 0.40488943457603455 
		1;
	setAttr -s 25 ".koy[0:24]"  -0.9407956600189209 0.93101149797439575 
		-0.95088565349578857 0 0.88401037454605103 0.92809420824050903 0.74451518058776855 
		0 0 0 0 0 0 0 -0.90418660640716553 0 0.78848111629486084 0 0 0 0 0 0 0.91436570882797241 
		0;
createNode animCurveTA -n "animCurveTA941";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  141 0 144 25.178989410400391 145 26.081197738647461
		 148 -20.01521110534668 151 -20.044706344604492 153 10.101008415222168 155 -34.687442779541016
		 158 31.748167037963864 161 -30.242408752441403 164 0.46298968791961664 167 -22.224239349365234
		 171 36.615806579589844 173 -8.5095367431640625 175 9.9843015670776367 178 8.2976608276367188
		 180 -28.960075378417972 182 18.817623138427734 183 17.236047744750977 185 -8.1271286010742187
		 188 16.306211471557617 191 -15.834910392761232 194 22.144069671630859 199 -3.3084437847137451
		 203 -10.281770706176758 210 0;
	setAttr -s 25 ".ktl[0:24]" no no no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 25 ".kix[0:24]"  1 0.27358996868133545 0.93542927503585815 
		0.9999237060546875 1 1 1 1 1 1 1 1 1 1 0.816730797290802 1 1 0.44946548342704773 
		1 1 1 1 0.41525176167488098 1 1;
	setAttr -s 25 ".kiy[0:24]"  0 0.96184641122817993 0.35351386666297913 
		-0.012353629805147648 0 0 0 0 0 0 0 0 0 0 -0.57701897621154785 0 0 -0.89329773187637329 
		0 0 0 0 -0.90970659255981445 0 0;
	setAttr -s 25 ".kox[0:24]"  0.27358996868133545 0.93542885780334473 
		0.15352730453014374 0.9999237060546875 1 1 1 1 1 1 1 1 1 1 0.816730797290802 1 1 
		0.44946548342704773 1 1 1 1 0.41525176167488098 1 1;
	setAttr -s 25 ".koy[0:24]"  0.96184641122817993 0.35351502895355225 
		-0.9881443977355957 -0.012353629805147648 0 0 0 0 0 0 0 0 0 0 -0.57701897621154785 
		0 0 -0.89329773187637329 0 0 0 0 -0.90970659255981445 0 0;
createNode animCurveTA -n "animCurveTA942";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 25 ".ktv[0:24]"  141 0 144 -40.843776702880859 145 -41.979770660400391
		 148 8.7105646133422852 151 73.70758056640625 153 -13.068252563476562 155 44.529083251953125
		 158 -34.939205169677734 161 21.855995178222656 164 -17.077287673950195 167 40.735824584960937
		 171 -14.216872215270996 173 30.606723785400391 175 -21.038337707519531 178 1.1344295740127563
		 180 43.127071380615234 182 -31.112714767456055 183 -22.178350448608398 185 37.224220275878906
		 188 -33.323284149169922 191 44.321502685546875 194 -32.952487945556641 199 13.550358772277832
		 203 38.826675415039063 210 0;
	setAttr -s 25 ".ktl[0:24]" no no no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 25 ".kix[0:24]"  1 0.17271514236927032 0.90298193693161011 
		0.059498939663171768 1 1 1 1 1 1 1 1 1 1 0.10705063492059708 1 1 0.088717788457870483 
		1 1 1 1 0.12494516372680664 1 1;
	setAttr -s 25 ".kiy[0:24]"  0 -0.98497182130813599 -0.4296785295009613 
		0.99822837114334106 0 0 0 0 0 0 0 0 0 0 0.99425357580184937 0 0 0.99605679512023926 
		0 0 0 0 0.99216371774673462 0 0;
	setAttr -s 25 ".kox[0:24]"  0.17271514236927032 0.9029812216758728 
		0.13989926874637604 0.059498939663171768 1 1 1 1 1 1 1 1 1 1 0.10705063492059708 
		1 1 0.088717788457870483 1 1 1 1 0.12494516372680664 1 1;
	setAttr -s 25 ".koy[0:24]"  -0.98497182130813599 -0.42967987060546875 
		0.99016577005386353 0.99822837114334106 0 0 0 0 0 0 0 0 0 0 0.99425357580184937 0 
		0 0.99605679512023926 0 0 0 0 0.99216371774673462 0 0;
createNode animCurveTL -n "animCurveTL940";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 -4.7867727279663086 158 -4.7867727279663086
		 210 -4.7867727279663086;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL941";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 32.754745483398438 158 32.754745483398438
		 210 32.754745483398438;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTL -n "animCurveTL942";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  141 -56.147525787353516 158 -56.147525787353516
		 210 -56.147525787353516;
	setAttr -s 3 ".kix[0:2]"  1 1 1;
	setAttr -s 3 ".kiy[0:2]"  0 0 0;
	setAttr -s 3 ".kox[0:2]"  1 1 1;
	setAttr -s 3 ".koy[0:2]"  0 0 0;
createNode animCurveTU -n "animCurveTU943";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU944";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU945";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA943";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA944";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA945";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL943";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 3.2171449661254883 210 3.2171449661254883;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL944";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 -26.658763885498047 210 -26.658763885498047;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL945";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1.5793838500976563 210 1.5793838500976563;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU946";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 1 144 1 146 1 210 1;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU947";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 1 144 1 146 1 210 1;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU948";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 1 144 1 146 1 210 1;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTA -n "animCurveTA946";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 14.600318908691406 146 22.936433792114258
		 150 24.914630889892578 152 20.635513305664062 155 10.395877838134766 158 9.7414340972900391
		 161 18.307945251464844 164 11.152754783630371 167 11.933575630187988 170 12.58457088470459
		 173 15.602058410644529 176 6.582343578338623 179 16.663330078125 182 8.4440793991088867
		 185 12.922273635864258 188 9.4770612716674805 192 16.743400573730469 195 9.1328773498535156
		 199 2.7357797622680664 203 2.502626895904541 210 0;
	setAttr -s 22 ".ktl[0:21]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 22 ".kix[0:21]"  1 0.44040271639823914 0.84934335947036743 
		1 0.49117997288703918 0.96441882848739624 1 1 1 0.9950411319732666 0.96873414516448975 
		1 1 1 1 1 1 1 0.55894249677658081 0.99732810258865356 0.99732810258865356 1;
	setAttr -s 22 ".kiy[0:21]"  0 0.89780032634735107 0.52784079313278198 
		0 -0.87105810642242432 -0.2643791139125824 0 0 0 0.09946393221616745 0.24810093641281128 
		0 0 0 0 0 0 0 -0.82920640707015991 -0.073051400482654572 -0.073051400482654572 0;
	setAttr -s 22 ".kox[0:21]"  0.44040271639823914 0.4970146119594574 
		0.84934335947036743 1 0.49117997288703918 0.96441882848739624 1 1 1 0.9950411319732666 
		0.96873414516448975 1 1 1 1 1 1 1 0.55894249677658081 0.99732810258865356 0.99732810258865356 
		1;
	setAttr -s 22 ".koy[0:21]"  0.89780032634735107 0.86774218082427979 
		0.52784079313278198 0 -0.87105810642242432 -0.2643791139125824 0 0 0 0.09946393221616745 
		0.24810093641281128 0 0 0 0 0 0 0 -0.82920640707015991 -0.073051400482654572 -0.073051400482654572 
		0;
createNode animCurveTA -n "animCurveTA947";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -37.765335083007813 146 18.302919387817383
		 150 -36.705852508544922 152 11.446368217468262 155 -22.749265670776367 158 7.2209291458129883
		 161 -16.960193634033203 164 9.4410324096679687 167 -7.7038803100585929 170 10.544995307922363
		 173 -25.987054824829102 176 3.3319308757781982 179 -24.304834365844727 182 -0.12093729525804521
		 185 -18.742792129516602 188 4.0646939277648926 192 -33.195449829101562 195 -3.9809632301330571
		 199 -8.1564922332763672 203 -19.195522308349609 210 0;
	setAttr -s 22 ".ktl[0:21]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 22 ".kix[0:21]"  1 0.18632325530052185 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 0.60625338554382324 1 1;
	setAttr -s 22 ".kiy[0:21]"  0 -0.98248851299285889 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 -0.79527157545089722 0 0;
	setAttr -s 22 ".kox[0:21]"  0.18632325530052185 0.084850966930389404 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.60625338554382324 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.98248851299285889 0.99639362096786499 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.79527157545089722 0 0;
createNode animCurveTA -n "animCurveTA948";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -36.310764312744141 146 21.304538726806641
		 150 -53.132514953613281 152 23.45582389831543 155 -45.383335113525391 158 17.848726272583008
		 161 -53.391963958740234 164 0.72656393051147461 167 -51.338008880615234 170 28.0177001953125
		 173 -48.376445770263672 176 22.376224517822266 179 -42.484565734863281 182 19.248279571533203
		 185 -33.908306121826172 188 11.754589080810547 192 -41.674652099609375 195 23.030899047851563
		 199 7.8716487884521484 203 -8.584284782409668 210 0;
	setAttr -s 22 ".ktl[0:21]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 22 ".kix[0:21]"  1 0.19351282715797424 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 0.20549589395523071 1 1;
	setAttr -s 22 ".kiy[0:21]"  0 -0.9810977578163147 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 -0.97865796089172363 0 0;
	setAttr -s 22 ".kox[0:21]"  0.19351282715797424 0.08258829265832901 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.20549589395523071 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.9810977578163147 0.99658375978469849 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.97865796089172363 0 0;
createNode animCurveTL -n "animCurveTL946";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 -4.7867727279663086 144 -4.7867727279663086
		 146 -4.7867727279663086 210 -4.7867727279663086;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL947";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 32.754745483398438 144 32.754745483398438
		 146 32.754745483398438 210 32.754745483398438;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTL -n "animCurveTL948";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  141 56.147525787353516 144 56.147525787353516
		 146 56.147525787353516 210 56.147525787353516;
	setAttr -s 4 ".ktl[0:3]" no no yes yes;
	setAttr -s 4 ".kix[0:3]"  1 1 1 1;
	setAttr -s 4 ".kiy[0:3]"  0 0 0 0;
	setAttr -s 4 ".kox[0:3]"  1 1 1 1;
	setAttr -s 4 ".koy[0:3]"  0 0 0 0;
createNode animCurveTU -n "animCurveTU949";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU950";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU951";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA949";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA950";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA951";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL949";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 40.544437408447266 210 40.544437408447266;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL950";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 43.055271148681641 210 43.055271148681641;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL951";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU952";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU953";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU954";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA952";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA953";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA954";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL952";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 8.0282459259033203 210 8.0282459259033203;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL953";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 9.9087905883789063 210 9.9087905883789063;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL954";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU955";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU956";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU957";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA955";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -13.628609657287598 146 40.362548828125
		 149 40.362548828125 151 -25.860088348388672 154 65.657096862792969 157 -82.747314453125
		 160 65.657096862792969 163 -82.747314453125 166 65.657096862792969 169 -82.747314453125
		 172 65.657096862792969 175 -82.747314453125 178 65.657096862792969 181 -82.747314453125
		 184 65.657096862792969 187 -82.747314453125 190 65.657096862792969 194 -82.747314453125
		 198 65.657096862792969 204 -12.319516181945801 210 0;
	setAttr -s 22 ".ktl[0:21]" no yes no no yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 22 ".kox[0:21]"  0.46518772840499878 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.8852120041847229 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA956";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -22.638528823852539 146 50.466983795166016
		 149 50.466983795166016 151 -57.445209503173828 154 68.488235473632812 157 -66.843460083007812
		 160 68.488235473632812 163 -66.843460083007812 166 68.488235473632812 169 -66.843460083007812
		 172 68.488235473632812 175 -66.843460083007812 178 68.488235473632812 181 -66.843460083007812
		 184 68.488235473632812 187 -66.843460083007812 190 68.488235473632812 194 -66.843460083007812
		 198 68.488235473632812 204 -35.637241363525391 210 0;
	setAttr -s 22 ".ktl[0:21]" no yes no no yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 22 ".kox[0:21]"  0.30162778496742249 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.95342576503753662 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA957";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -2.4545130729675293 146 12.896030426025391
		 149 12.896030426025391 151 22.955572128295898 154 68.391815185546875 157 82.624984741210938
		 160 68.391815185546875 163 82.624984741210938 166 68.391815185546875 169 82.624984741210938
		 172 68.391815185546875 175 82.624984741210938 178 68.391815185546875 181 82.624984741210938
		 184 68.391815185546875 187 82.624984741210938 190 68.391815185546875 194 82.624984741210938
		 198 68.391815185546875 204 16.289134979248047 210 0;
	setAttr -s 22 ".ktl[2:21]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 0.16873729228973389 0.23342360556125641 
		1 1 1 1 1 1 1 1 1 1 1 1 1 0.24634271860122681 0.38635337352752686 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0.98566102981567383 0.97237521409988403 
		0 0 0 0 0 0 0 0 0 0 0 0 0 -0.96918278932571411 -0.92235082387924194 0;
	setAttr -s 22 ".kox[0:21]"  0.9459872841835022 1 1 1 0.16873729228973389 
		0.23342360556125641 1 1 1 1 1 1 1 1 1 1 1 1 1 0.24634271860122681 0.38635337352752686 
		1;
	setAttr -s 22 ".koy[0:21]"  -0.3242037296295166 0 0 0 0.98566102981567383 
		0.97237521409988403 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.96918278932571411 -0.92235082387924194 
		0;
createNode animCurveTL -n "animCurveTL955";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 -19.597047805786133 149 -19.597047805786133
		 160 -19.597047805786133 163 -19.597047805786133 166 -19.597047805786133 169 -19.597047805786133
		 172 -19.597047805786133 175 -19.597047805786133 178 -19.597047805786133 181 -19.597047805786133
		 184 -19.597047805786133 187 -19.597047805786133 190 -19.597047805786133 194 -19.597047805786133
		 198 -19.597047805786133 210 -19.597047805786133;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL956";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 36.545459747314453 149 36.545459747314453
		 160 36.545459747314453 163 36.545459747314453 166 36.545459747314453 169 36.545459747314453
		 172 36.545459747314453 175 36.545459747314453 178 36.545459747314453 181 36.545459747314453
		 184 36.545459747314453 187 36.545459747314453 190 36.545459747314453 194 36.545459747314453
		 198 36.545459747314453 210 36.545459747314453;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL957";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 39.212558746337891 149 39.212558746337891
		 160 39.212558746337891 163 39.212558746337891 166 39.212558746337891 169 39.212558746337891
		 172 39.212558746337891 175 39.212558746337891 178 39.212558746337891 181 39.212558746337891
		 184 39.212558746337891 187 39.212558746337891 190 39.212558746337891 194 39.212558746337891
		 198 39.212558746337891 210 39.212558746337891;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU958";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU959";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU960";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA958";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  141 0 144 0.15314114093780518 145 2.4268949031829834
		 148 -5.7124981880187988 151 -1.6871942281723022 154 -8.1011257171630859 157 -5.882166862487793
		 160 -16.872138977050781 163 -9.0811138153076172 166 -15.352037429809569 169 -3.1893124580383301
		 172 -10.46162223815918 175 -3.1919405460357666 178 -9.9326705932617188 181 0.17486105859279633
		 184 -7.4188008308410645 187 1.1447429656982422 190 -7.2908916473388672 193 2.3228559494018555
		 196 -4.5260272026062012 201 -6.5128464698791504 205 -2.1943528652191162 210 0;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.89466249942779541 
		1 0.88770300149917603 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.44674268364906311 
		0 0.46041661500930786 0;
	setAttr -s 23 ".kox[0:22]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.89466249942779541 
		1 0.88770300149917603 1;
	setAttr -s 23 ".koy[0:22]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.44674268364906311 
		0 0.46041661500930786 0;
createNode animCurveTA -n "animCurveTA959";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  141 0 144 0.41687840223312378 145 27.798250198364258
		 148 -13.731769561767578 151 15.298051834106444 154 -7.4048972129821777 157 7.593167781829834
		 160 -19.15080451965332 163 14.581023216247559 166 -5.5295014381408691 169 16.896673202514648
		 172 -3.1978645324707031 175 15.823470115661619 178 -0.25628194212913513 181 15.76479434967041
		 184 -3.7766363620758057 187 11.084150314331055 190 -1.8230822086334231 193 14.460785865783691
		 196 -4.4225821495056152 201 -7.5643668174743652 205 -1.1153246164321899 210 0;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.78482824563980103 
		1 0.96288609504699707 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.61971336603164673 
		0 0.26990824937820435 0;
	setAttr -s 23 ".kox[0:22]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.78482824563980103 
		1 0.96288609504699707 1;
	setAttr -s 23 ".koy[0:22]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.61971336603164673 
		0 0.26990824937820435 0;
createNode animCurveTA -n "animCurveTA960";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 23 ".ktv[0:22]"  141 0 144 -3.5012648105621338 145 10.006522178649902
		 148 2.7076160907745361 151 5.022162914276123 154 2.3888578414916992 157 0.47268775105476379
		 160 1.3349894285202026 163 -1.3035768270492554 166 1.0205843448638916 169 4.6396269798278809
		 172 6.1693019866943359 175 4.2148380279541016 178 3.1671922206878662 181 1.1342153549194336
		 184 2.6301209926605225 187 -1.1155076026916504 190 -3.0366220474243164 193 -2.9898233413696289
		 196 -5.9159402847290039 201 3.2189860343933105 205 -8.4285354614257813 210 0;
	setAttr -s 23 ".ktl[0:22]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 23 ".kix[0:22]"  1 1 1 1 1 0.86228156089782715 1 1 1 0.83419567346572876 
		0.94105273485183716 1 0.97873479127883911 0.97764551639556885 1 1 0.8394891619682312 
		1 1 1 1 1 1;
	setAttr -s 23 ".kiy[0:22]"  0 0 0 0 0 -0.50642907619476318 0 0 0 0.5514686107635498 
		0.33825978636741638 0 -0.20512989163398743 -0.21026022732257843 0 0 -0.54337650537490845 
		0 0 0 0 0 0;
	setAttr -s 23 ".kox[0:22]"  1 1 1 1 1 0.86228156089782715 1 1 1 0.83419567346572876 
		0.94105273485183716 1 0.97873479127883911 0.97764551639556885 1 1 0.8394891619682312 
		1 1 1 1 1 1;
	setAttr -s 23 ".koy[0:22]"  0 0 0 0 0 -0.50642907619476318 0 0 0 0.5514686107635498 
		0.33825978636741638 0 -0.20512989163398743 -0.21026022732257843 0 0 -0.54337650537490845 
		0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL958";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 51.6451416015625 210 51.6451416015625;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL959";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 -11.264523506164551 210 -11.264523506164551;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL960";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU961";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU962";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU963";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 149 1 160 1 163 1 166 1 169 1 172 1
		 175 1 178 1 181 1 184 1 187 1 190 1 194 1 198 1 210 1;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA961";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -13.628609657287598 146 40.362548828125
		 149 40.362548828125 151 -25.860088348388672 154 65.657096862792969 157 -82.747314453125
		 160 65.657096862792969 163 -82.747314453125 166 65.657096862792969 169 -82.747314453125
		 172 65.657096862792969 175 -82.747314453125 178 65.657096862792969 181 -82.747314453125
		 184 65.657096862792969 187 -82.747314453125 190 65.657096862792969 194 -82.747314453125
		 198 65.657096862792969 204 -12.319516181945801 210 0;
	setAttr -s 22 ".ktl[0:21]" no yes no no yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 22 ".kox[0:21]"  0.46518772840499878 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.8852120041847229 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA962";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -22.638528823852539 146 50.466983795166016
		 149 50.466983795166016 151 -57.445209503173828 154 68.488235473632812 157 -66.843460083007812
		 160 68.488235473632812 163 -66.843460083007812 166 68.488235473632812 169 -66.843460083007812
		 172 68.488235473632812 175 -66.843460083007812 178 68.488235473632812 181 -66.843460083007812
		 184 68.488235473632812 187 -66.843460083007812 190 68.488235473632812 194 -66.843460083007812
		 198 68.488235473632812 204 -35.637241363525391 210 0;
	setAttr -s 22 ".ktl[0:21]" no yes no no yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 22 ".kox[0:21]"  0.30162778496742249 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 22 ".koy[0:21]"  -0.95342576503753662 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA963";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 22 ".ktv[0:21]"  141 0 144 -2.4545130729675293 146 12.896030426025391
		 149 12.896030426025391 151 22.955572128295898 154 68.391815185546875 157 82.624984741210938
		 160 68.391815185546875 163 82.624984741210938 166 68.391815185546875 169 82.624984741210938
		 172 68.391815185546875 175 82.624984741210938 178 68.391815185546875 181 82.624984741210938
		 184 68.391815185546875 187 82.624984741210938 190 68.391815185546875 194 82.624984741210938
		 198 68.391815185546875 204 16.289134979248047 210 0;
	setAttr -s 22 ".ktl[2:21]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes;
	setAttr -s 22 ".kix[0:21]"  1 1 1 1 0.16873729228973389 0.23342360556125641 
		1 1 1 1 1 1 1 1 1 1 1 1 1 0.24634271860122681 0.38635337352752686 1;
	setAttr -s 22 ".kiy[0:21]"  0 0 0 0 0.98566102981567383 0.97237521409988403 
		0 0 0 0 0 0 0 0 0 0 0 0 0 -0.96918278932571411 -0.92235082387924194 0;
	setAttr -s 22 ".kox[0:21]"  0.9459872841835022 1 1 1 0.16873729228973389 
		0.23342360556125641 1 1 1 1 1 1 1 1 1 1 1 1 1 0.24634271860122681 0.38635337352752686 
		1;
	setAttr -s 22 ".koy[0:21]"  -0.3242037296295166 0 0 0 0.98566102981567383 
		0.97237521409988403 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.96918278932571411 -0.92235082387924194 
		0;
createNode animCurveTL -n "animCurveTL961";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 -19.609930038452148 149 -19.609930038452148
		 160 -19.609930038452148 163 -19.609930038452148 166 -19.609930038452148 169 -19.609930038452148
		 172 -19.609930038452148 175 -19.609930038452148 178 -19.609930038452148 181 -19.609930038452148
		 184 -19.609930038452148 187 -19.609930038452148 190 -19.609930038452148 194 -19.609930038452148
		 198 -19.609930038452148 210 -19.609930038452148;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL962";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 36.542636871337891 149 36.542636871337891
		 160 36.542636871337891 163 36.542636871337891 166 36.542636871337891 169 36.542636871337891
		 172 36.542636871337891 175 36.542636871337891 178 36.542636871337891 181 36.542636871337891
		 184 36.542636871337891 187 36.542636871337891 190 36.542636871337891 194 36.542636871337891
		 198 36.542636871337891 210 36.542636871337891;
	setAttr -s 16 ".ktl[15]" no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL963";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 -39.208473205566406 149 -39.208473205566406
		 160 -39.208473205566406 163 -39.208473205566406 166 -39.208473205566406 169 -39.208473205566406
		 172 -39.208473205566406 175 -39.208473205566406 178 -39.208473205566406 181 -39.208473205566406
		 184 -39.208473205566406 187 -39.208473205566406 190 -39.208473205566406 194 -39.208473205566406
		 198 -39.208473205566406 210 -39.208473205566406;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU964";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU965";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU966";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 1 210 1;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA964";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA965";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTA -n "animCurveTA966";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL964";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 -4.502251148223877 210 -4.502251148223877;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTL -n "animCurveTL965";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 37.76336669921875 210 37.76336669921875;
	setAttr -s 2 ".ktl[0:1]" no no;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 0.0055149625986814499;
	setAttr -s 2 ".koy[0:1]"  0 0.99998480081558228;
createNode animCurveTL -n "animCurveTL966";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  141 0 210 0;
	setAttr -s 2 ".ktl[0:1]" no yes;
	setAttr -s 2 ".kix[0:1]"  1 1;
	setAttr -s 2 ".kiy[0:1]"  0 0;
	setAttr -s 2 ".kox[0:1]"  1 1;
	setAttr -s 2 ".koy[0:1]"  0 0;
createNode animCurveTU -n "animCurveTU967";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 1 169 1 172 1 175 1 178 1 181 1 184 1
		 187 1 190 1 210 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU968";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 1 169 1 172 1 175 1 178 1 181 1 184 1
		 187 1 190 1 210 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU969";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 1 169 1 172 1 175 1 178 1 181 1 184 1
		 187 1 190 1 210 1;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA967";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  141 0 142 0.66019779443740845 144 0.15790966153144836
		 146 -2.034477710723877 149 4.0423603057861328 151 -3.0986001491546631 154 15.440417289733887
		 157 4.2742800712585449 160 12.816834449768066 163 -4.2236857414245605 166 6.3229117393493652
		 169 -4.2236857414245605 172 6.3229117393493652 175 -4.2236857414245605 178 6.3229117393493652
		 181 -4.2236857414245605 184 6.3229117393493652 187 -4.2236857414245605 190 6.3229117393493652
		 193 -7.5031347274780273 198 0.10878373682498932 202 0.56546264886856079 206 -1.6496788263320923
		 210 0;
	setAttr -s 24 ".ktl[0:23]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 24 ".kix[0:23]"  1 0.96382421255111694 0.95363545417785645 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.98986434936523438 1 1 1;
	setAttr -s 24 ".kiy[0:23]"  0 0.2665388286113739 -0.30096438527107239 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.142015740275383 0 0 0;
	setAttr -s 24 ".kox[0:23]"  0.96382385492324829 0.99451214075088501 
		0.95363545417785645 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.98986434936523438 1 1 1;
	setAttr -s 24 ".koy[0:23]"  0.26653978228569031 -0.10462145507335663 
		-0.30096438527107239 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.142015740275383 0 0 0;
createNode animCurveTA -n "animCurveTA968";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  141 0 142 4.0741119384765625 144 6.9946722984313965
		 146 -22.703670501708984 149 -20.319826126098633 151 14.505525588989256 154 -13.715165138244629
		 157 11.145787239074707 160 -13.6524658203125 163 10.50881290435791 166 -15.45158004760742
		 169 10.50881290435791 172 -15.45158004760742 175 10.50881290435791 178 -15.45158004760742
		 181 10.50881290435791 184 -15.45158004760742 187 10.50881290435791 190 -15.45158004760742
		 193 13.291768074035645 198 -16.775362014770508 202 -12.75534725189209 206 -0.74857568740844727
		 210 0;
	setAttr -s 24 ".ktl[0:23]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 24 ".kix[0:23]"  1 0.50556999444961548 1 1 0.70762211084365845 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.62077337503433228 0.97344362735748291 1;
	setAttr -s 24 ".kiy[0:23]"  0 0.86278563737869263 0 0 0.70659106969833374 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78399008512496948 0.22892665863037109 0;
	setAttr -s 24 ".kox[0:23]"  0.50556868314743042 0.85306602716445923 
		1 1 0.70762211084365845 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.62077337503433228 0.97344362735748291 
		1;
	setAttr -s 24 ".koy[0:23]"  0.86278641223907471 0.52180308103561401 
		0 0 0.70659106969833374 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78399008512496948 0.22892665863037109 
		0;
createNode animCurveTA -n "animCurveTA969";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 24 ".ktv[0:23]"  141 0 142 6.640416145324707 144 13.600775718688965
		 146 3.3011953830718994 149 -12.527929306030273 151 -5.3320503234863281 154 -4.2189521789550781
		 157 -1.7309420108795166 160 -9.1509609222412109 163 -1.1141934394836426 166 -7.0143947601318359
		 169 -1.1141934394836426 172 -7.0143947601318359 175 -1.1141934394836426 178 -7.0143947601318359
		 181 -1.1141934394836426 184 -7.0143947601318359 187 -1.1141934394836426 190 -7.0143947601318359
		 193 0.29298275709152222 198 5.7582249641418457 202 -7.3015131950378409 206 -5.1211161613464355
		 210 0;
	setAttr -s 24 ".ktl[0:23]" no no yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes no;
	setAttr -s 24 ".kix[0:23]"  1 0.33831474184989929 1 0.24158640205860138 
		1 0.90632665157318115 0.96982109546661377 1 1 1 1 1 1 1 1 1 1 1 1 0.58857351541519165 
		1 1 0.82500678300857544 1;
	setAttr -s 24 ".kiy[0:23]"  0 0.9410330057144165 0 -0.97037935256958008 
		0 0.42257773876190186 0.24381767213344574 0 0 0 0 0 0 0 0 0 0 0 0 0.80844366550445557 
		0 0 0.56512290239334106 0;
	setAttr -s 24 ".kox[0:23]"  0.33831354975700378 0.56567662954330444 
		1 0.24158640205860138 1 0.90632665157318115 0.96982109546661377 1 1 1 1 1 1 1 1 1 
		1 1 1 0.58857351541519165 1 1 0.82500678300857544 1;
	setAttr -s 24 ".koy[0:23]"  0.94103342294692993 0.82462716102600098 
		0 -0.97037935256958008 0 0.42257773876190186 0.24381767213344574 0 0 0 0 0 0 0 0 
		0 0 0 0 0.80844366550445557 0 0 0.56512290239334106 0;
createNode animCurveTL -n "animCurveTL967";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 -1.4725730419158936 169 -1.4725730419158936
		 172 -1.4725730419158936 175 -1.4725730419158936 178 -1.4725730419158936 181 -1.4725730419158936
		 184 -1.4725730419158936 187 -1.4725730419158936 190 -1.4725730419158936 210 -1.4725730419158936;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL968";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 22.299345016479492 169 22.299345016479492
		 172 22.299345016479492 175 22.299345016479492 178 22.299345016479492 181 22.299345016479492
		 184 22.299345016479492 187 22.299345016479492 190 22.299345016479492 210 22.299345016479492;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes no;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL969";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 10 ".ktv[0:9]"  141 0 169 0 172 0 175 0 178 0 181 0 184 0
		 187 0 190 0 210 0;
	setAttr -s 10 ".ktl[0:9]" no yes yes yes yes yes yes yes yes yes;
	setAttr -s 10 ".kix[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".kiy[0:9]"  0 0 0 0 0 0 0 0 0 0;
	setAttr -s 10 ".kox[0:9]"  1 1 1 1 1 1 1 1 1 1;
	setAttr -s 10 ".koy[0:9]"  0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU970";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  141 1 149 1 155 1 158 1 161 1 164 1 167 1
		 170 1 173 1 176 1 179 1 182 1 185 1 188 1 191 1 194 1 210 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU971";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  141 1 149 1 155 1 158 1 161 1 164 1 167 1
		 170 1 173 1 176 1 179 1 182 1 185 1 188 1 191 1 194 1 210 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU972";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 17 ".ktv[0:16]"  141 1 149 1 155 1 158 1 161 1 164 1 167 1
		 170 1 173 1 176 1 179 1 182 1 185 1 188 1 191 1 194 1 210 1;
	setAttr -s 17 ".kix[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".kiy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 17 ".kox[0:16]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 17 ".koy[0:16]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA970";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  141 0 145 -0.48884466290473932 149 -16.877599716186523
		 152 14.130607604980469 155 -16.877599716186523 158 14.130607604980469 161 -16.877599716186523
		 164 14.130607604980469 167 -16.877599716186523 170 14.130607604980469 173 -16.877599716186523
		 176 14.130607604980469 179 -16.877599716186523 182 14.130607604980469 185 -16.877599716186523
		 188 14.130607604980469 191 -16.877599716186523 194 14.130607604980469 203 -5.7718510627746582
		 210 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 0.98841196298599243 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 -0.15179543197154999 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  0.99869227409362793 0.98841196298599243 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  -0.051124796271324158 -0.15179543197154999 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA971";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 20 ".ktv[0:19]"  141 0 145 -0.56020033359527588 149 -5.9869489669799805
		 152 2.1751303672790527 155 -5.9869489669799805 158 2.1751303672790527 161 -5.9869489669799805
		 164 2.1751303672790527 167 -5.9869489669799805 170 2.1751303672790527 173 -5.9869489669799805
		 176 2.1751303672790527 179 -5.9869489669799805 182 2.1751303672790527 185 -5.9869489669799805
		 188 2.1751303672790527 191 -5.9869489669799805 194 2.1751303672790527 203 -1.300859808921814
		 210 0;
	setAttr -s 20 ".ktl[0:19]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes no;
	setAttr -s 20 ".kix[0:19]"  1 0.98486405611038208 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".kiy[0:19]"  0 -0.17332834005355835 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 20 ".kox[0:19]"  0.99828368425369263 0.98486405611038208 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 20 ".koy[0:19]"  -0.058563411235809326 -0.17332834005355835 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA972";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  141 0 145 -6.1640443801879883 149 -13.821798324584961
		 152 -14.938698768615723 155 -13.821798324584961 158 -14.938698768615723 161 -13.821798324584961
		 164 -14.938698768615723 167 -13.821798324584961 170 -14.938698768615723 173 -13.821798324584961
		 176 -14.938698768615723 179 -13.821798324584961 182 -14.938698768615723 185 -13.821798324584961
		 188 -14.938698768615723 191 -13.821798324584961 194 -14.938698768615723 197 -19.808525085449219
		 203 -10.987922668457031 210 0;
	setAttr -s 21 ".ktl[0:20]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes no;
	setAttr -s 21 ".kix[0:20]"  1 0.69083613157272339 0.9057733416557312 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.9057733416557312 1 0.66437321901321411 1;
	setAttr -s 21 ".kiy[0:20]"  0 -0.723011314868927 -0.42376255989074707 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.42376255989074707 0 0.74740087985992432 0;
	setAttr -s 21 ".kox[0:20]"  0.8401678204536438 0.69083613157272339 
		0.9057733416557312 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0.9057733416557312 1 0.66437321901321411 
		1;
	setAttr -s 21 ".koy[0:20]"  -0.54232656955718994 -0.723011314868927 
		-0.42376255989074707 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.42376255989074707 0 0.74740087985992432 
		0;
createNode animCurveTL -n "animCurveTL970";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  141 0 145 0.52488952875137329 149 0 155 0
		 158 0 161 0 164 0 167 0 170 0 173 0 176 0 179 0 182 0 185 0 188 0 191 0 194 0 210 0;
	setAttr -s 18 ".ktl[0:17]" no yes no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes;
	setAttr -s 18 ".kix[0:17]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 18 ".kiy[0:17]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 18 ".kox[0:17]"  0.30263674259185791 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 18 ".koy[0:17]"  0.95310598611831665 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL971";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  141 0 145 10.329450607299805 149 0 155 0
		 158 0 161 0 164 0 167 0 170 0 173 0 176 0 179 0 182 0 185 0 188 0 191 0 194 0 210 0;
	setAttr -s 18 ".ktl[0:17]" no yes no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes no;
	setAttr -s 18 ".kix[0:17]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 18 ".kiy[0:17]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 18 ".kox[0:17]"  0.016132978722453117 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1;
	setAttr -s 18 ".koy[0:17]"  0.99986988306045532 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL972";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 18 ".ktv[0:17]"  141 0 145 0.059024430811405182 149 0 155 0
		 158 0 161 0 164 0 167 0 170 0 173 0 176 0 179 0 182 0 185 0 188 0 191 0 194 0 210 0;
	setAttr -s 18 ".ktl[0:17]" no yes no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes;
	setAttr -s 18 ".kix[0:17]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 18 ".kiy[0:17]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 18 ".kox[0:17]"  0.94263315200805664 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 18 ".koy[0:17]"  0.33383053541183472 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU973";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 144 1 159 1 162 1 165 1 168 1 171 1
		 174 1 177 1 180 1 183 1 186 1 189 1 192 1 206 1 210 1;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU974";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 144 1 159 1 162 1 165 1 168 1 171 1
		 174 1 177 1 180 1 183 1 186 1 189 1 192 1 206 1 210 1;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "animCurveTU975";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 1 144 1 159 1 162 1 165 1 168 1 171 1
		 174 1 177 1 180 1 183 1 186 1 189 1 192 1 206 1 210 1;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA973";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  141 0 144 2.4720375537872314 146 15.700746536254883
		 150 -10.778719902038574 153 14.239334106445313 156 -4.5023674964904785 159 20.94810676574707
		 162 -4.5023674964904785 165 20.474536895751953 168 -4.5023674964904785 171 22.185457229614258
		 174 -4.5023674964904785 177 11.834030151367188 180 -4.5023674964904785 183 21.89454460144043
		 186 -4.5023674964904785 189 20.828466415405273 192 -4.5023674964904785 197 4.5115094184875488
		 206 -3.8971114158630371 210 0;
	setAttr -s 21 ".ktl[20]" no;
	setAttr -s 21 ".kix[0:20]"  1 0.69467425346374512 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 21 ".kiy[0:20]"  0 0.71932446956634521 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 21 ".kox[0:20]"  0.94527566432952881 0.69467425346374512 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 21 ".koy[0:20]"  0.32627281546592712 0.71932446956634521 
		0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA974";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  141 0 144 -14.48128604888916 146 54.711021423339844
		 150 -3.8350827693939209 153 46.686782836914063 156 0.55123639106750488 159 44.195415496826172
		 162 0.55123639106750488 165 55.757747650146484 168 0.55123639106750488 171 50.662616729736328
		 174 0.55123639106750488 177 53.571029663085938 180 0.55123639106750488 183 49.980602264404297
		 186 0.55123639106750488 189 45.521087646484375 192 0.55123639106750488 197 53.117656707763672
		 206 -10.227420806884766 210 0;
	setAttr -s 21 ".ktl[0:20]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes no;
	setAttr -s 21 ".kix[0:20]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1;
	setAttr -s 21 ".kiy[0:20]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 21 ".kox[0:20]"  0.44331371784210205 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1 1 1 1;
	setAttr -s 21 ".koy[0:20]"  -0.89636659622192383 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "animCurveTA975";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 21 ".ktv[0:20]"  141 0 144 -0.16766785085201263 146 8.9907608032226563
		 150 15.571274757385256 153 26.952968597412109 156 29.010919570922852 159 26.176851272583008
		 162 29.010919570922852 165 19.247501373291016 168 29.010919570922852 171 23.290960311889648
		 174 29.010919570922852 177 13.240889549255371 180 29.010919570922852 183 25.535364151000977
		 186 29.010919570922852 189 28.174226760864258 192 29.010919570922852 197 9.8060998916625977
		 206 -14.167373657226563 210 0;
	setAttr -s 21 ".ktl[0:20]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 21 ".kix[0:20]"  1 1 0.60864436626434326 0.65971672534942627 
		0.75742363929748535 1 1 1 1 1 1 1 1 1 1 1 1 1 0.37874323129653931 1 1;
	setAttr -s 21 ".kiy[0:20]"  0 0 0.79344314336776733 0.75151443481445313 
		0.65292370319366455 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.92550176382064819 0 0;
	setAttr -s 21 ".kox[0:20]"  0.9997260570526123 1 0.60864436626434326 
		0.65971672534942627 0.75742363929748535 1 1 1 1 1 1 1 1 1 1 1 1 1 0.37874323129653931 
		1 1;
	setAttr -s 21 ".koy[0:20]"  -0.023404428735375404 0 0.79344314336776733 
		0.75151443481445313 0.65292370319366455 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.92550176382064819 
		0 0;
createNode animCurveTL -n "animCurveTL973";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 -54.946929931640625 144 -54.946929931640625
		 159 -54.946929931640625 162 -54.946929931640625 165 -54.946929931640625 168 -54.946929931640625
		 171 -54.946929931640625 174 -54.946929931640625 177 -54.946929931640625 180 -54.946929931640625
		 183 -54.946929931640625 186 -54.946929931640625 189 -54.946929931640625 192 -54.946929931640625
		 206 -54.946929931640625 210 -54.946929931640625;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL974";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 2.3482637405395508 144 2.3482637405395508
		 159 2.3482637405395508 162 2.3482637405395508 165 2.3482637405395508 168 2.3482637405395508
		 171 2.3482637405395508 174 2.3482637405395508 177 2.3482637405395508 180 2.3482637405395508
		 183 2.3482637405395508 186 2.3482637405395508 189 2.3482637405395508 192 2.3482637405395508
		 206 2.3482637405395508 210 2.3482637405395508;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "animCurveTL975";
	setAttr ".tan" 1;
	setAttr ".wgt" no;
	setAttr -s 16 ".ktv[0:15]"  141 0 144 0 159 0 162 0 165 0 168 0 171 0
		 174 0 177 0 180 0 183 0 186 0 189 0 192 0 206 0 210 0;
	setAttr -s 16 ".ktl[0:15]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes no;
	setAttr -s 16 ".kix[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".kiy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 16 ".kox[0:15]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 16 ".koy[0:15]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
select -ne :time1;
	setAttr ".o" 210;
	setAttr ".unw" 210;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :characterPartition;
connectAttr "panicSource.cl" "clipLibrary1.sc[0]";
connectAttr "animCurveTU937.a" "clipLibrary1.cel[0].cev[0].cevr";
connectAttr "animCurveTU938.a" "clipLibrary1.cel[0].cev[1].cevr";
connectAttr "animCurveTU939.a" "clipLibrary1.cel[0].cev[2].cevr";
connectAttr "animCurveTA937.a" "clipLibrary1.cel[0].cev[3].cevr";
connectAttr "animCurveTA938.a" "clipLibrary1.cel[0].cev[4].cevr";
connectAttr "animCurveTA939.a" "clipLibrary1.cel[0].cev[5].cevr";
connectAttr "animCurveTL937.a" "clipLibrary1.cel[0].cev[6].cevr";
connectAttr "animCurveTL938.a" "clipLibrary1.cel[0].cev[7].cevr";
connectAttr "animCurveTL939.a" "clipLibrary1.cel[0].cev[8].cevr";
connectAttr "animCurveTU940.a" "clipLibrary1.cel[0].cev[9].cevr";
connectAttr "animCurveTU941.a" "clipLibrary1.cel[0].cev[10].cevr";
connectAttr "animCurveTU942.a" "clipLibrary1.cel[0].cev[11].cevr";
connectAttr "animCurveTA940.a" "clipLibrary1.cel[0].cev[12].cevr";
connectAttr "animCurveTA941.a" "clipLibrary1.cel[0].cev[13].cevr";
connectAttr "animCurveTA942.a" "clipLibrary1.cel[0].cev[14].cevr";
connectAttr "animCurveTL940.a" "clipLibrary1.cel[0].cev[15].cevr";
connectAttr "animCurveTL941.a" "clipLibrary1.cel[0].cev[16].cevr";
connectAttr "animCurveTL942.a" "clipLibrary1.cel[0].cev[17].cevr";
connectAttr "animCurveTU943.a" "clipLibrary1.cel[0].cev[18].cevr";
connectAttr "animCurveTU944.a" "clipLibrary1.cel[0].cev[19].cevr";
connectAttr "animCurveTU945.a" "clipLibrary1.cel[0].cev[20].cevr";
connectAttr "animCurveTA943.a" "clipLibrary1.cel[0].cev[21].cevr";
connectAttr "animCurveTA944.a" "clipLibrary1.cel[0].cev[22].cevr";
connectAttr "animCurveTA945.a" "clipLibrary1.cel[0].cev[23].cevr";
connectAttr "animCurveTL943.a" "clipLibrary1.cel[0].cev[24].cevr";
connectAttr "animCurveTL944.a" "clipLibrary1.cel[0].cev[25].cevr";
connectAttr "animCurveTL945.a" "clipLibrary1.cel[0].cev[26].cevr";
connectAttr "animCurveTU946.a" "clipLibrary1.cel[0].cev[27].cevr";
connectAttr "animCurveTU947.a" "clipLibrary1.cel[0].cev[28].cevr";
connectAttr "animCurveTU948.a" "clipLibrary1.cel[0].cev[29].cevr";
connectAttr "animCurveTA946.a" "clipLibrary1.cel[0].cev[30].cevr";
connectAttr "animCurveTA947.a" "clipLibrary1.cel[0].cev[31].cevr";
connectAttr "animCurveTA948.a" "clipLibrary1.cel[0].cev[32].cevr";
connectAttr "animCurveTL946.a" "clipLibrary1.cel[0].cev[33].cevr";
connectAttr "animCurveTL947.a" "clipLibrary1.cel[0].cev[34].cevr";
connectAttr "animCurveTL948.a" "clipLibrary1.cel[0].cev[35].cevr";
connectAttr "animCurveTU949.a" "clipLibrary1.cel[0].cev[36].cevr";
connectAttr "animCurveTU950.a" "clipLibrary1.cel[0].cev[37].cevr";
connectAttr "animCurveTU951.a" "clipLibrary1.cel[0].cev[38].cevr";
connectAttr "animCurveTA949.a" "clipLibrary1.cel[0].cev[39].cevr";
connectAttr "animCurveTA950.a" "clipLibrary1.cel[0].cev[40].cevr";
connectAttr "animCurveTA951.a" "clipLibrary1.cel[0].cev[41].cevr";
connectAttr "animCurveTL949.a" "clipLibrary1.cel[0].cev[42].cevr";
connectAttr "animCurveTL950.a" "clipLibrary1.cel[0].cev[43].cevr";
connectAttr "animCurveTL951.a" "clipLibrary1.cel[0].cev[44].cevr";
connectAttr "animCurveTU952.a" "clipLibrary1.cel[0].cev[45].cevr";
connectAttr "animCurveTU953.a" "clipLibrary1.cel[0].cev[46].cevr";
connectAttr "animCurveTU954.a" "clipLibrary1.cel[0].cev[47].cevr";
connectAttr "animCurveTA952.a" "clipLibrary1.cel[0].cev[48].cevr";
connectAttr "animCurveTA953.a" "clipLibrary1.cel[0].cev[49].cevr";
connectAttr "animCurveTA954.a" "clipLibrary1.cel[0].cev[50].cevr";
connectAttr "animCurveTL952.a" "clipLibrary1.cel[0].cev[51].cevr";
connectAttr "animCurveTL953.a" "clipLibrary1.cel[0].cev[52].cevr";
connectAttr "animCurveTL954.a" "clipLibrary1.cel[0].cev[53].cevr";
connectAttr "animCurveTU955.a" "clipLibrary1.cel[0].cev[54].cevr";
connectAttr "animCurveTU956.a" "clipLibrary1.cel[0].cev[55].cevr";
connectAttr "animCurveTU957.a" "clipLibrary1.cel[0].cev[56].cevr";
connectAttr "animCurveTA955.a" "clipLibrary1.cel[0].cev[57].cevr";
connectAttr "animCurveTA956.a" "clipLibrary1.cel[0].cev[58].cevr";
connectAttr "animCurveTA957.a" "clipLibrary1.cel[0].cev[59].cevr";
connectAttr "animCurveTL955.a" "clipLibrary1.cel[0].cev[60].cevr";
connectAttr "animCurveTL956.a" "clipLibrary1.cel[0].cev[61].cevr";
connectAttr "animCurveTL957.a" "clipLibrary1.cel[0].cev[62].cevr";
connectAttr "animCurveTU958.a" "clipLibrary1.cel[0].cev[63].cevr";
connectAttr "animCurveTU959.a" "clipLibrary1.cel[0].cev[64].cevr";
connectAttr "animCurveTU960.a" "clipLibrary1.cel[0].cev[65].cevr";
connectAttr "animCurveTA958.a" "clipLibrary1.cel[0].cev[66].cevr";
connectAttr "animCurveTA959.a" "clipLibrary1.cel[0].cev[67].cevr";
connectAttr "animCurveTA960.a" "clipLibrary1.cel[0].cev[68].cevr";
connectAttr "animCurveTL958.a" "clipLibrary1.cel[0].cev[69].cevr";
connectAttr "animCurveTL959.a" "clipLibrary1.cel[0].cev[70].cevr";
connectAttr "animCurveTL960.a" "clipLibrary1.cel[0].cev[71].cevr";
connectAttr "animCurveTU961.a" "clipLibrary1.cel[0].cev[72].cevr";
connectAttr "animCurveTU962.a" "clipLibrary1.cel[0].cev[73].cevr";
connectAttr "animCurveTU963.a" "clipLibrary1.cel[0].cev[74].cevr";
connectAttr "animCurveTA961.a" "clipLibrary1.cel[0].cev[75].cevr";
connectAttr "animCurveTA962.a" "clipLibrary1.cel[0].cev[76].cevr";
connectAttr "animCurveTA963.a" "clipLibrary1.cel[0].cev[77].cevr";
connectAttr "animCurveTL961.a" "clipLibrary1.cel[0].cev[78].cevr";
connectAttr "animCurveTL962.a" "clipLibrary1.cel[0].cev[79].cevr";
connectAttr "animCurveTL963.a" "clipLibrary1.cel[0].cev[80].cevr";
connectAttr "animCurveTU964.a" "clipLibrary1.cel[0].cev[81].cevr";
connectAttr "animCurveTU965.a" "clipLibrary1.cel[0].cev[82].cevr";
connectAttr "animCurveTU966.a" "clipLibrary1.cel[0].cev[83].cevr";
connectAttr "animCurveTA964.a" "clipLibrary1.cel[0].cev[84].cevr";
connectAttr "animCurveTA965.a" "clipLibrary1.cel[0].cev[85].cevr";
connectAttr "animCurveTA966.a" "clipLibrary1.cel[0].cev[86].cevr";
connectAttr "animCurveTL964.a" "clipLibrary1.cel[0].cev[87].cevr";
connectAttr "animCurveTL965.a" "clipLibrary1.cel[0].cev[88].cevr";
connectAttr "animCurveTL966.a" "clipLibrary1.cel[0].cev[89].cevr";
connectAttr "animCurveTU967.a" "clipLibrary1.cel[0].cev[90].cevr";
connectAttr "animCurveTU968.a" "clipLibrary1.cel[0].cev[91].cevr";
connectAttr "animCurveTU969.a" "clipLibrary1.cel[0].cev[92].cevr";
connectAttr "animCurveTA967.a" "clipLibrary1.cel[0].cev[93].cevr";
connectAttr "animCurveTA968.a" "clipLibrary1.cel[0].cev[94].cevr";
connectAttr "animCurveTA969.a" "clipLibrary1.cel[0].cev[95].cevr";
connectAttr "animCurveTL967.a" "clipLibrary1.cel[0].cev[96].cevr";
connectAttr "animCurveTL968.a" "clipLibrary1.cel[0].cev[97].cevr";
connectAttr "animCurveTL969.a" "clipLibrary1.cel[0].cev[98].cevr";
connectAttr "animCurveTU970.a" "clipLibrary1.cel[0].cev[99].cevr";
connectAttr "animCurveTU971.a" "clipLibrary1.cel[0].cev[100].cevr";
connectAttr "animCurveTU972.a" "clipLibrary1.cel[0].cev[101].cevr";
connectAttr "animCurveTA970.a" "clipLibrary1.cel[0].cev[102].cevr";
connectAttr "animCurveTA971.a" "clipLibrary1.cel[0].cev[103].cevr";
connectAttr "animCurveTA972.a" "clipLibrary1.cel[0].cev[104].cevr";
connectAttr "animCurveTL970.a" "clipLibrary1.cel[0].cev[105].cevr";
connectAttr "animCurveTL971.a" "clipLibrary1.cel[0].cev[106].cevr";
connectAttr "animCurveTL972.a" "clipLibrary1.cel[0].cev[107].cevr";
connectAttr "animCurveTU973.a" "clipLibrary1.cel[0].cev[108].cevr";
connectAttr "animCurveTU974.a" "clipLibrary1.cel[0].cev[109].cevr";
connectAttr "animCurveTU975.a" "clipLibrary1.cel[0].cev[110].cevr";
connectAttr "animCurveTA973.a" "clipLibrary1.cel[0].cev[111].cevr";
connectAttr "animCurveTA974.a" "clipLibrary1.cel[0].cev[112].cevr";
connectAttr "animCurveTA975.a" "clipLibrary1.cel[0].cev[113].cevr";
connectAttr "animCurveTL973.a" "clipLibrary1.cel[0].cev[114].cevr";
connectAttr "animCurveTL974.a" "clipLibrary1.cel[0].cev[115].cevr";
connectAttr "animCurveTL975.a" "clipLibrary1.cel[0].cev[116].cevr";
// End of dragon_panic.ma
